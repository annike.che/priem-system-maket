import { Pipe, PipeTransform } from '@angular/core';
import * as roleStrategy from '@/common/functions/roles-strategy';

@Pipe({
  name: 'cabinetsNameButton'
})
export class CabinetsNameButtonPipe implements PipeTransform {

  transform(cabinet: string): any {
    return roleStrategy.getCabinetName(cabinet);
  }

}
