import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CabinetsNameButtonPipe } from './cabinets-name-button.pipe';



@NgModule({
  declarations: [
      CabinetsNameButtonPipe
  ],
  imports: [
    CommonModule
  ], exports: [
    CabinetsNameButtonPipe
  ]
})
export class CommonPipesModule { }
