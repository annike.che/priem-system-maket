import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {UserProfileService} from '@/common/services/user-profile.service';
import * as userStrategy from '@/common/functions/roles-strategy';

@Injectable({
  providedIn: 'root'
})
export class RedirectGuard implements CanActivate {


  constructor( private userProfileService: UserProfileService,
               private router: Router) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | boolean  {

    const user = this.userProfileService.getProfile();
    let roleInfo;
    if ( user.hasOwnProperty('role') ) {
     roleInfo = this.userProfileService.getRolesDataByCode(user.role.code);
    }

    //console.log('redirect guard user', user);

    if ( user.hasOwnProperty('id') && user.hasOwnProperty('role') && roleInfo) {
      let path = '';

      console.log(user.role.code, roleInfo);

      if( roleInfo.pathResolve.indexOf('university') >= 0 &&  user.hasOwnProperty('organization') ) {
        path = 'university';
      } else {
        path = roleInfo.pathDefault;
      }
      this.router.navigate(['/cabinets/' + path]);
      return true;
    } else {
      this.router.navigate(['/auth']);
      return false;
    }
  }
}
