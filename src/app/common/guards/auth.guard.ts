import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {AuthService} from '@/open/pages/auth/auth.service';
import {UserProfileService} from '@/common/services/user-profile.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
      private authService: AuthService,
      private userProfileService: UserProfileService,
      private router: Router
  ) {}


  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | boolean {

    console.log('is Auth Guard', state);

    const user = this.userProfileService.getProfile();

    if ( !user.hasOwnProperty('id') || user.hasOwnProperty('role') ) {
        this.router.navigate(['auth']);
        return true;
      } else {
        this.router.navigate(['cabinets']);
        return false;
    }
  }

}
