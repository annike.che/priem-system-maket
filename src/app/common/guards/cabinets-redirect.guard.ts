import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {UserProfileService} from '@/common/services/user-profile.service';

@Injectable({
  providedIn: 'root'
})
export class CabinetsRedirectGuard implements CanActivate {

  constructor(
      private userProfile: UserProfileService,
      private router: Router
  ) {

  }


  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | boolean  {

    const user = this.userProfile.getProfile();
    const roles = this.userProfile.getRolesDataByCode(user.role.code);

    if ( user.hasOwnProperty('id') && user.hasOwnProperty('role') && user.hasOwnProperty('organization') ) {



      this.router.navigate(['/cabinets/' + roles[user.role.code]]);

      return true;
    } else {
      this.router.navigate(['/auth']);
      return false;
    }

  }

}
