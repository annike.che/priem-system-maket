import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {UserProfileService} from '@/common/services/user-profile.service';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {

  constructor(
      private userProfile: UserProfileService,
      private router: Router
  ) {}

  canActivate(
      route: ActivatedRouteSnapshot,
      state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    const user = this.userProfile.getProfile();

    if ( user.role ) {
      // проверяем, допустимо ли с такой ролью ходить на этот роут

      const resolveRole = this.userProfile.getRolesDataByCode(user.role.code);

      if ( resolveRole && resolveRole.pathResolve.indexOf('admin') >= 0 && !user.organization )  {
        return true;
      }
    }

    this.router.navigate(['/auth']);
    return false;
  }
}
