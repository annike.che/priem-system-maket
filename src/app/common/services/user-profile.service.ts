import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {isObservable, Observable, Subject} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserProfileService {

  urlPrefix: string;
  urlGroup = '/user';

  private user: User ;
  private organizations: Array<any>;

  private roles = {
    provider: {
      pathResolve: ['university'],
      pathDefault: 'university',
      /*pathParams: {
        university: {
          needOrgId: true
        }
      }*/
    },
    administrator: {
      pathResolve: ['admin', 'university'],
      pathDefault: 'admin',
      /*pathParams: {
        admin: {
          needOrgId: false
        },
        university: {
          needOrgId: true
        }
      }*/
    }
  };

 /* private roles = {
    provider: 'university',
    administrator: 'admin'
  };*/

  constructor(private httpClient: HttpClient) {
    this.urlPrefix = environment.apiUrl;
    this.user = {};
    this.organizations = [];
  }


  getUinfo(): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + '/info';
    return this.httpClient.get( url );
  }

  loadUserData() {
    return new Promise( (resolve, reject) => {
      this.getUinfo()
          .subscribe( res => {
            if (res.done) {
              this.user = res.data.user_info;
              this.organizations = res.data.organizations_list;
            }
            //console.log('load user data', this.user);
            resolve(true);
          });
    });
  }

  setProfile(userData): User {
    if (userData) {
      this.user = userData;
      console.log('set user', this.user);
    }
    return this.user;
  }

  setOrganizationsList(organizations ): Array<any> {
    if ( organizations ) {
      console.log( 'set organization' );
      this.organizations = organizations;
    }
    return this.organizations;

  }

  getProfile(): User {
    //console.log('getProfile', this.user);
    return this.user;
  }

  getOrganizationsList(): any {
    console.log('getorganizations', this.organizations);

    return this.organizations;

  }

  setNewOrganization(item): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + '/current-org';
    return this.httpClient.post( url, {id: item.id} );
       /* .pipe(
            map( res => {
              console.log('map', res);
              return res;
            })
        );*/


  }

  removeOrganization(): Observable<any> {
    const url =  this.urlPrefix + this.urlGroup + '/current-org/remove';
    return  this.httpClient.get(url);
  }

  getRolesDataByCode(code: string): any {
    if (code && this.roles.hasOwnProperty(code)) {
      return this.roles[code];
    }
    return null;
  }

  setIsOovo(data: boolean) {
    if( this.user ) {
      if( this.user.hasOwnProperty( 'organization' ) ) {
        this.user.organization.is_oovo = data;
      }
    }
  }




}
