import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OlympicsService {

  urlPrefix: string;
  urlGroup = '/olympics';

  constructor(private httpClient: HttpClient) {
    this.urlPrefix = environment.apiUrl;

  }

  getOlympicList(search?) {
    const url = this.urlPrefix + this.urlGroup + '/list';

    const sentParams: any = search && search.trim() ? {search} : {};

    return this.httpClient.get(url, {params: sentParams})
        .pipe(
            map((resp: any) => {
              return (resp.done) ? resp.data : [];
            } )
        );
  }
}
