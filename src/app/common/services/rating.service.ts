import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RatingService {
  urlPrefix: string;
  urlGroup = '/rating';

  constructor(private httpClient: HttpClient) {
    this.urlPrefix = environment.apiUrl;

  }

  getCompetitiveRatingList(id, params): Observable<any> {
    const url = this.urlPrefix + this.urlGroup +  `/competitive-groups/${id}/applications/list`;

    return this.httpClient.get(url, {params});
  }

  saveCompetitiveRating(id, data): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/competitive-groups/applications/${id}/edit`;

    return this.httpClient.post(url, data);
  }

  updateApplication(idCompetitive): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/competitive-groups/${idCompetitive}/applications/refresh`;

    return this.httpClient.get(url);
  }

  sendToEpgu( idCompetitive ): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/competitive-groups/${idCompetitive}/applications/sync`;

    return this.httpClient.get(url);
  }

  getEpguStatus( idCompetitive ): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/competitive-groups/${idCompetitive}/applications/sync-status`;

    return this.httpClient.get(url);
  }
}
