import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() { }

  setData(key, data) {
    localStorage.setItem(key, data);
  }
  getData(key) {
    return localStorage.getItem(key);
  }

  setelementInObject(key: string, data: any) {
    let item = this.getData(key);
  }
}
