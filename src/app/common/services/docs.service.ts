import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {toFormData} from '@/cabinets/university/entrants-menu/entrants.service';

@Injectable({
  providedIn: 'root'
})
export class DocsService {

  urlPrefix: string;
  urlGroup = '/docs';

  constructor(private httpClient: HttpClient) {
    this.urlPrefix = environment.apiUrl;
  }

  getDocFile(id, type = 'general'): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${type}/${id}/file`;

    /*const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    });*/

    return this.httpClient.get<any>(url, {responseType: 'blob' as 'json'});
  }

  deleteDocFile(id: number, type = 'general'): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${type}/${id}/file/remove`;

    return this.httpClient.post(url, {});
  }

  addDocFile(idDoc: number, data, type = 'general'): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${type}/${idDoc}/file/add`;

    const headers = new HttpHeaders({
      'key': 'Content-Type',
      'value': 'multipart/form-data'
    });

    return this.httpClient.post(url, toFormData(data), { headers});
  }
}
