import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OrgsCommonService {

  urlPrefix: string;
  urlGroup = '/organizations';

  constructor(private httpClient: HttpClient) {
    this.urlPrefix = environment.apiUrl;
  }

  getOrgsSelectList(search?) {
    const url = `${this.urlPrefix}${this.urlGroup}/short`;

    const sentParams: any = search && search.trim() ? {search} : {};

    return this.httpClient.get(url, {params: sentParams})
        .pipe(
            map((resp: any) => {
              return (resp.done) ? resp.data : [];
            } )
        );
  }


}
