import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ClsService {

  urlPrefix: string;
  urlGroup = '/cls';

  tableNames = [
      'achievement_categories',
      'appeal_statuses',
      'benefits',                   // льготы
      'campaign_statuses',          // статусы ПК
      'campaign_types',             // тип ПК
      'composition_themes',
      'compatriot_categories',
      'v_direction_specialty',                 // направления
      'disability_types',
      'document_categories',
      'document_sys_category',
      'v_document_types',
      'education_forms',            // формы обучения
      'education_levels',           // уровни образования
      'education_sources',
      'entrance_test_types',
      'gender',                     // пол
      'level_budget',
      'military_categories',
      'olympic_diploma_types',
      'olympic_levels',
      'v_okcm',                       // гражданство
      'orphan_categories',
      'parents_lost_categories',
      'radiation_work_categories',
      'regions',
      'subjects',
      'veteran_categories',
      'v_edu_levels_campaign_types',
      'v_okso_specialty',
      'v_okso_enlarged_group',
      'direction',
      'sys_category',
      'document_education_levels',
      'return_types'
  ];

  constructor(private httpClient: HttpClient) {
    this.urlPrefix = environment.apiUrl;

  }

  getClsData(tableName, search?: string, params = {}): Observable<any> | any {
    if ( this.tableNames.indexOf(tableName) < 0) { return null; }

    const url = this.urlPrefix + this.urlGroup + '/list/' + tableName;

    const sentParams: any = search && search.trim() ? {search} : {};

    for ( const key in params ) {
        sentParams.filter = key;
        sentParams.value = params[key];

    }
   /// console.log('sentParams', sentParams);

    return this.httpClient.get(url, {params: sentParams})
        .pipe(
            map((resp: any) => {
              return (resp.done) ? resp.data : [];
            } )
        );

  }

  getSysCategories(search?) {
      const url = this.urlPrefix + this.urlGroup + '/sys_category';

      const sentParams: any = search && search.trim() ? {search} : {};

      return this.httpClient.get(url, {params: sentParams})
          .pipe(
              map((resp: any) => {
                  return (resp.done) ? resp.data : [];
              } )
          );
  }

  getCampaignList(search?) {
    const url = this.urlPrefix + '/campaign/short';

    const sentParams: any = search && search.trim() ? {search} : {};

    return this.httpClient.get(url, {params: sentParams})
        .pipe(
            map((resp: any) => {
                return (resp.done) ? resp.data : [];
            } )
        );
  }

  getCompetitiveSelectList(search?) {
    const url = this.urlPrefix + '/competitive/short';

    const sentParams: any = search && search.trim() ? {search} : {};

    return this.httpClient.get(url, {params: sentParams})
        .pipe(
            map((resp: any) => {
              return (resp.done) ? resp.data : [];
            } )
        );
  }


  getCampaignStatuses() {
    const url = this.urlPrefix + this.urlGroup + '/cmpstatuses/short';

    return this.httpClient.get(url)
        .pipe(
            map((resp: any) => {
              return (resp.done) ? resp.data : [];
            } )
        );
  }

  getDirectionsList(params?) {
      const url = this.urlPrefix + '/directions/list';

      return  this.httpClient.get(url, {params});
  }

  /*getDirectionsList(search?, idEducLevels?) {
    const url = this.urlPrefix + '/organizations/directions/list';

    let sentParams: any = search && search.trim() ? {search} : {};

    if (idEducLevels) {
        sentParams.id_education_level = idEducLevels;
    }

    return this.httpClient.get(url, {params: sentParams})
        .pipe(
            map((resp: any) => {
                return (resp.done) ? resp.data : [];
            } )
        );
  }*/

  getDirectionByIdEntrant(idEntrant?: any): Observable<any> {
      let url = this.urlPrefix + '/directions/short';

      return this.httpClient.get(url, { params: { id_entrant: idEntrant }})
          .pipe( map((resp: any) => {
              return (resp.done) ? resp.data : [];
          } ));

  }

  getCompetitiveSelectListByEntrantId( params? ): Observable<any> {
      let url = this.urlPrefix + '/competitive/list';

      return this.httpClient.get(url,  {params})
          .pipe( map((resp: any) => {
              return (resp.done) ? resp.data : [];
          } ));
  }

  getAppStatusList(params = {}): Observable<any> {
      const url = this.urlPrefix + this.urlGroup + '/appstatuses/short';

      return this.httpClient.get(url, {params})
          .pipe( map((resp: any) => {
              return (resp.done) ? resp.data : [];
          } ));
  }
}
