interface User {
    id?: number;
    login?: string;
    name?: string;
    surname?: string;
    patronymic?: string;
    role?: Role;
    organization?: any;
}

interface Role {
    id: number;
    name: string;
    code: string;
    description: string;
}


