interface SortEvent {
  active: string; // column name
  direction: string; // 'asc' or 'desc'
}
