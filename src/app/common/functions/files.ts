

export function handlingResponse(res: any, filename: string) {
  console.log('res', res);
  const blob = new Blob([res], { type: res.type });
  downloadFile(blob, filename);
}

export function handlingError(err): string {
  console.log('err', err);
  let returnMessage =  'Произошла ошибка' + (err.message ? ': ' + err.message : '');
  return returnMessage;
}

export function downloadFile(file: Blob, fileName: string = '') {

  const downloadLink = document.createElement("a");
  const objectUrl = URL.createObjectURL(file);


  downloadLink.href = objectUrl;
  downloadLink.download = fileName;
  downloadLink.target = '_self';
  document.body.appendChild(downloadLink);

  downloadLink.click();
  URL.revokeObjectURL(objectUrl);
}
