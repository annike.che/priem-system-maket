/*export const rolesStrategy = {
  administrator: [ 'university', 'admin' ],
  provider: [ 'university' ]

};*/

const rolesStrategy = {
  administrator: [ 'admin',  'university'],
  provider: [ 'university' ]
};

const cabinetsName = {
  university: 'Перейти в личный кабинет ВУЗа',
  admin: 'Перейти в панель Администратора'
};

export function allRoles(): Array<string> {
  return Object.keys(rolesStrategy);
}

export function getCabinetsByRole(role): Array<string> | null {
  if (!rolesStrategy.hasOwnProperty(role)) { return null; }
  return rolesStrategy[role];
}

export function getCabinetName(cabinetKey: string): string {
  console.log('getCabinetName');
  return cabinetsName[cabinetKey] || '';
}



