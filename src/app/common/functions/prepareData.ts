import * as moment from 'moment';

export function prepareDate(data, keys?) {
    keys = keys || [ 'issue_date', 'birthday', 'result_date' ];

    keys.forEach( (key) => {
        console.log('key', key, data.hasOwnProperty(key), data[key] );
        if (data.hasOwnProperty(key) && data[key]) {
            data[key] = moment(data[key]).format();
        }
    } );

    console.log('data', data);

    return data;

}
