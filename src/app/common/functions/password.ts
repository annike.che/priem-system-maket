import * as generator from 'generate-password-browser';
import {sha256} from 'js-sha256';

(window as any).global = window;

export function generatePass(): string {
  let password = generator.generate({
    length: 10,
    numbers: true,
    symbols: true,
  });

  return password;
}

export function hashPass(login: string, pass: string) {
  return  sha256(`${login.toUpperCase()}${pass}`);
}
