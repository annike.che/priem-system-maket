import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DynamicFormsDirective } from '@/common/modules/dynamic-forms/dynamic-forms.directive';



@NgModule({
  declarations: [
      DynamicFormsDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [
    DynamicFormsDirective
  ],
  entryComponents: []
})
export class DynamicFormsModule { }
