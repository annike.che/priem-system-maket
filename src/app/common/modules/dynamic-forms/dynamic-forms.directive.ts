import {
  ComponentFactoryResolver,
  Directive,
  EventEmitter,
  Input, OnChanges,
  OnInit,
  Output, SimpleChanges,
  Type,
  ViewContainerRef
} from '@angular/core';
import {FormGroup} from '@angular/forms';
@Directive({
  selector: '[appDynamicForms]'
})
export class DynamicFormsDirective implements OnChanges {

  @Input() componentName: Type<any>;

  /*@Input() set componentName( data: Type<any> ) {
    console.log('component');
    this._componentName = data;
  }*/

  @Input() data?: any;
  @Input() form?: FormGroup;

  @Input() canSave = false;

  @Output() saveData = new EventEmitter();

  componentRef: any;

  constructor(   private resolver: ComponentFactoryResolver,
                 private container: ViewContainerRef ) {

  }

  ngOnChanges(changes: SimpleChanges): void {

    console.log('SimpleChanges', changes);



    const factory = this.resolver.resolveComponentFactory(this.componentName);

    if (this.container.length > 0 ) {
      this.container.clear();
    }
    //console.log('this.container', this.container, this.componentRef);
    this.componentRef = this.container.createComponent(factory);

    console.log('saveData', this.saveData);

    this.componentRef.instance.data = this.data;
    if( this.canSave ) {
      this.componentRef.instance.saveData.subscribe(this.saveData);
    }


  }

}
