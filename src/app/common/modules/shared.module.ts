import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MaterialModule} from '@/common/modules/material/material.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {NgxMatSelectSearchModule} from 'ngx-mat-select-search';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {QuillModule} from 'ngx-quill';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MaterialModule,
    FlexLayoutModule,
    NgxMatSelectSearchModule,
    FormsModule,
    ReactiveFormsModule,
    QuillModule.forRoot()

  ],

  exports: [
    CommonModule,
    MaterialModule,
    FlexLayoutModule,
    NgxMatSelectSearchModule,
    FormsModule,
    ReactiveFormsModule,
    QuillModule
  ]
})
export class SharedModule { }
