import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {PageEvent} from '@angular/material/paginator';
import {ActivatedRoute, Router} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog';
import {debounceTime, distinctUntilChanged, switchMap, tap} from 'rxjs/operators';
import {untilDestroyed} from 'ngx-take-until-destroy';
import {SortDirection} from '@angular/material/sort';
import * as cloneDeep from 'lodash/cloneDeep';
import {artAnimations} from '@art-lib/animations';
import {OrgsAdminService} from '@/cabinets/admin/orgs-menu/orgs-admin.service';

@Component({
  selector: 'app-orgs-list',
  templateUrl: './orgs-list.component.html',
  styleUrls: ['./orgs-list.component.scss'],
  animations: artAnimations
})
export class OrgsListComponent implements OnInit, OnDestroy{

  orgs: Array<OrgItem>;

  displayedColumns: Array<string>;
  searchColumns: string[];
  pageSizeOptions: number[];

  isLoad: boolean;

  search = new Subject<string>();
  sortEvent = new Subject<SortEvent>();

  pagination = new Subject<PageEvent>();

  defaultParams = {
    page: 1,
    limit: 20
  };
  total: number;

  params: Params = {
    ...this.defaultParams
  };

  constructor(
      private orgsService: OrgsAdminService,
      private route: ActivatedRoute,
      private router: Router,
      private titleService: Title,
      private snackBar: MatSnackBar,
      private dialog: MatDialog
  ) {
    this.isLoad = false;
    this.titleService.setTitle('Список организаций');

    this.orgs = [];

    this.displayedColumns = ['title', 'kpp', 'ogrn', 'created', 'actual'];
    this.searchColumns = ['searchTitle', 'searchKpp', 'searchOgrn', 'searchСreated', 'searchActions'];
    this.pageSizeOptions = [5, 10, 15, 20];

  }

  ngOnInit() {
    this.getQueryParamsFromUrl();
    this.subscribeOnChanges();
    this.getOrgs();
  }

  subscribeOnChanges() {
    this.search.pipe(
        distinctUntilChanged(),
        debounceTime(500),
        tap(() => {
          this.params.page = 1;
        }),
        switchMap((value, index) => {
          // console.log('value', value);
          return this.getDataFromService();
        }),
        untilDestroyed(this)
    ).subscribe(res => this.setData(res));

    this.pagination.pipe(
        debounceTime(200),
        tap((event: PageEvent) => {
          if (this.params.limit !== event.pageSize) {
            this.params.limit = event.pageSize;
            this.params.page = 1;
          }
          if (this.params.page !== event.pageIndex + 1) {
            this.params.page = event.pageIndex + 1;
          }
        }),
        switchMap(() => this.getDataFromService()),
        untilDestroyed(this)
    )
        .subscribe(response => this.setData(response));

    this.sortEvent.pipe(
        debounceTime(200),
        tap((event: SortEvent) => {
          this.params.order = event.direction as SortDirection;
          this.params.sortby = event.active;
        }),
        switchMap(() => this.getDataFromService()),
        untilDestroyed(this)
    )
        .subscribe(response => this.setData(response));


  }

  getDataFromService(): Observable<any> {
    this.isLoad = true;
    const queryParams = this.getQueryParams(this.params);
    this.passQueryParamsToUrl(queryParams);
    return this.orgsService.getOrgsList(this.params);
  }

  getQueryParams(params) {
    const queryParams: any = {};
    Object.keys(params).forEach((key) => {
      if (this.defaultParams.hasOwnProperty(key) && (this.defaultParams[key] !== params[key] && params[key] !== '') ||
          (!this.defaultParams.hasOwnProperty(key) && params[key] !== '')) {
        if ((key === 'frDt' || key === 'toDt') && params.dateF !== 'FromTo') {
          return;
        }
        queryParams[key] = params[key];
      }
    });

    return queryParams;
  }

  passQueryParamsToUrl(queryParams) {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams
    });
  }

  setData(res) {
    if (res.done) {

      this.orgs = res.data;
      this.params.page = res.paginator.page;
      this.params.limit = res.paginator.limit;
      this.total = res.paginator.total;

    } else {
      this.snackBar.open('Произошла ошибка' + ( res.message ? ': ' + res.message : '' ) );
    }

    this.isLoad = false;
  }

  getQueryParamsFromUrl() {
    const queryParams = cloneDeep(this.route.snapshot.queryParams);
    this.setParams(queryParams);
  }


  setParams(params) {
    Object.keys(params).forEach(key => {
      switch (key) {
        case 'page':
          this.params[key] = +params[key];
          break;
        default:
          this.params[key] = params[key];
      }
    });
  }


  getOrgs() {
    this.getDataFromService()
        .subscribe(response => this.setData(response));
  }

  clearFilter($event) {
    this.params.search_short_title = '';
    this.params.search_kpp = '';
    this.params.search_ogrn = '';

    this.search.next($event);
  }

  ngOnDestroy(): void {

  }

}

interface Params {
  page: number;
  limit: number;
  search_short_title?: string;
  search_kpp?: string;
  search_ogrn?: string;
  order?: SortDirection; // 'ASC' || 'DESC'
  sortby?: string;
}
