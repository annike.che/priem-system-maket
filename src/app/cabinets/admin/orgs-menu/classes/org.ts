interface OrgItem {
  id: number;
  short_title: string;
  kpp: string;
  ogrn: string;
  actual: boolean;
  is_oovo: boolean;
  created: string;
}
