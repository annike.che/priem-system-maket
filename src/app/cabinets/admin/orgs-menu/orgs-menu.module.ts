import { NgModule } from '@angular/core';
import { OrgsListComponent } from './pages/orgs-list/orgs-list.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '@/common/modules/shared.module';
import {ArtLibModule} from '@art-lib/art-lib.module';
import {IMaskModule} from 'angular-imask';

const routes: Routes = [
  {path: '', component: OrgsListComponent},
];

@NgModule({
  declarations: [OrgsListComponent],
  imports: [
    RouterModule.forChild(routes),
    SharedModule,
    ArtLibModule,
    IMaskModule,
  ]
})
export class OrgsMenuModule { }
