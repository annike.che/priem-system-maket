import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';


const routes: Routes = [
  { path: '', redirectTo: 'users', pathMatch: 'full' },
  { path: 'users',
    loadChildren: () => import('./users-menu/users-menu.module').then( m => m.UsersMenuModule )
  },
  { path: 'orgs',
    loadChildren: () => import('./orgs-menu/orgs-menu.module').then( m => m.OrgsMenuModule )
  },
  { path: 'news',
    loadChildren: () => import('./news-menu/news-menu-admin.module').then(m=>m.NewsMenuAdminModule)
  },
];


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class AdminModule { }
