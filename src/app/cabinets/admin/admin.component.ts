import { Component, OnInit } from '@angular/core';
import {Subject} from 'rxjs';
import {Router} from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  menuItems: Array<IMenuItems>;

  toggleMenu: Subject<void> = new Subject<void>();

  currUrl = '';

  constructor(private router: Router) {
    this.currUrl = router.url;

    this.menuItems  = [
      {
        title: 'Пользователи',
        router: 'users',
        icon: 'users'
      },
      {
        title: 'Организации',
        router: 'orgs',
        icon: 'student-hat'
      },
      {
        title: 'Новости',
        router: 'news',
        icon: 'news'
      },
    ];

  }

  ngOnInit() {
  }

  emitEventToggleMenu($event) {
    this.toggleMenu.next($event);
  }

}
