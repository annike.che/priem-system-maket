import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs';
import {toFormData} from '@/cabinets/university/entrants-menu/entrants.service';

@Injectable({
  providedIn: 'root'
})
export class NewsService {
  urlPrefix: string;
  urlGroup = '/new';

  constructor(private httpClient: HttpClient) {
    this.urlPrefix = environment.apiUrl + '/admin';
  }

  getNewsList(params?) {
    const url = `${this.urlPrefix}${this.urlGroup}/list`;
    return this.httpClient.get(url, { params });
  }

  getDocFile(id): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/file/${id}`;
    return this.httpClient.get(url);
  }

  // получение детальной информации о новости

  getById(id): Observable<any> {
    const url = `${this.urlPrefix}${this.urlGroup}/${id}/main`;
    return this.httpClient.get(url);
  }

  editNews(id, content) {
    const url = `${this.urlPrefix}${this.urlGroup}/${id}/edit`;
    return this.httpClient.post(url, content);
  }

  removeAndRecoveryNews(id, action) {
    const url = `${this.urlPrefix}${this.urlGroup}/${id}/deleted`;
    return this.httpClient.post(url, action);
  }

  addFile(id, content) {
    const headers = new HttpHeaders({
      'key': 'Content-Type',
      'value': 'multipart/form-data'
    });
    const url = `${this.urlPrefix}${this.urlGroup}/${id}/file/add`;
    return this.httpClient.post(url, toFormData(content), {headers});
  }

  removeDocFile(id) {
    const url = this.urlPrefix + this.urlGroup + `/file/${id}/remove`;
    return this.httpClient.post(url, id);
  }

  createNews(content) {
    const headers = new HttpHeaders({
      'key': 'Content-Type',
      'value': 'multipart/form-data'
    });
    const url = `${this.urlPrefix}${this.urlGroup}/add`;
    return this.httpClient.post(url, toFormData(content), {headers});
  }

}
