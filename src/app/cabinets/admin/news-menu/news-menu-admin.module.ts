import {NgModule} from '@angular/core';
import {SharedModule} from '@/common/modules/shared.module';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgxMaskModule} from 'ngx-mask';
import {IMaskModule} from 'angular-imask';
import {DynamicFormsModule} from '@/common/modules/dynamic-forms/dynamic-forms.module';
import {ArtLibModule} from '@art-lib/art-lib.module';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {ArtPaginationModule} from '@art-lib/components/pagination/pagination.module';

import {NewsAddComponent} from '@/cabinets/admin/news-menu/pages/news-add/news-add.component';
import {NewsEditComponent} from '@/cabinets/admin/news-menu/pages/news-edit/news-edit.component';
import {NewsListComponent} from '@/cabinets/admin/news-menu/pages/news-list/news-list.component';
import {NewsDetailComponent} from '@/cabinets/admin/news-menu/pages/news-detail/news-detail.component';





const MY_DATE_FORMATS = {
  parse: {
    dateInput: ['DD.MM.YYYY', 'LL', ],
  },
  display: {
    dateInput: 'DD.MM.YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY'
  },
};

const routes: Routes = [
  {path: '', component: NewsListComponent},
  {path: 'add', component: NewsAddComponent},
  {path: ':id', component: NewsDetailComponent},
  {path: ':id/edit', component: NewsEditComponent},
  {path: '**', redirectTo: '/news'}
];

@NgModule({
  declarations: [
    NewsListComponent,
    NewsAddComponent,
    NewsEditComponent,
    NewsDetailComponent
  ],
  imports: [
    SharedModule,
    RouterModule.forChild(routes),
    NgxMaskModule.forRoot(),
    IMaskModule,
    ArtLibModule,
    ArtPaginationModule
  ],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS }
  ]
})

export class NewsMenuAdminModule { }
