import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NewsService} from '@/cabinets/admin/news-menu/news.service';
import {ActivatedRoute, Router} from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';
import * as moment from 'moment';

@Component({
  selector: 'app-news-add',
  templateUrl: './news-add.component.html',
  styleUrls: ['./news-add.component.scss']
})
export class NewsAddComponent implements OnInit {

  createNewsForm: FormGroup;

  isLoad: boolean = false;
  modules = {
    toolbar: [
      ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
      ['blockquote', 'code-block'],

      [{'header': 1}, {'header': 2}],               // custom button values
      [{'list': 'ordered'}, {'list': 'bullet'}],
      [{'script': 'sub'}, {'script': 'super'}],      // superscript/subscript
      [{'indent': '-1'}, {'indent': '+1'}],          // outdent/indent
      [{'direction': 'rtl'}],                         // text direction

      [{'size': ['small', false, 'large', 'huge']}],  // custom dropdown
      [{'header': [1, 2, 3, 4, 5, 6, false]}],

      [{'color': []}, {'background': []}],          // dropdown with defaults from theme
      [{'font': []}],
      [{'align': []}],

      ['clean'],                                         // remove formatting button

      ['link']                         // link and image, video
    ]
  };


  constructor(private newsService: NewsService,
              private fb: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private _snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.initForm();
  }


  initForm() {
    this.createNewsForm = this.fb.group({
      title: [null, Validators.required],
      date_news: [null, Validators.required],
      content: [null, Validators.required],
      published: [false],
      files: [null]
    });
  }

  create() {
    const formData = {
      ...this.createNewsForm.value,
      date_news: moment(this.createNewsForm.controls.date_news.value).format(),
    };

    this.isLoad = true;

    this.newsService.createNews(formData)
      .subscribe(res => {
        if (res) {
          this._snackBar.open('Новость успешно создана');
          this.isLoad = false;
          this.createNewsForm.reset();
          this.goToNewsList();
        } else {
          this._snackBar.open('Ошибка заполнения данных');
        }
      });
  }

  goToNewsList() {
    this.router.navigate(['../'], { relativeTo: this.route });

  }


}
