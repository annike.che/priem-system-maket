import {Component, OnInit} from '@angular/core';
import {switchMap} from 'rxjs/operators';
import {ActivatedRoute, Params} from '@angular/router';
import {artAnimations} from '@art-lib/animations';
import {MatSnackBar} from '@angular/material/snack-bar';
import {NewsService} from '@/cabinets/admin/news-menu/news.service';

@Component({
  selector: 'app-news-detail',
  templateUrl: './news-detail.component.html',
  styleUrls: ['./news-detail.component.scss'],
  animations: artAnimations
})
export class NewsDetailComponent implements OnInit {
  id: number;
  data: Inews;
  fileData: any;
  isLoad: boolean;

  constructor(private newsService: NewsService,
              private _snackBar: MatSnackBar,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.isLoad = true;
    this.route.params
      .pipe(switchMap((params: Params) => {
        this.id = +params.id;
        return this.newsService.getById(+params.id)
      }))
      .subscribe((response) => {
        if (response) {
          this.data = response.data;
          this.isLoad = false;
        }
        console.log(this.data, 'this.data');
      });
  }
  initFileData(id) {
    this.newsService.getDocFile(id)
      .subscribe(res => {
        console.log('res', res);
        if (res.done) {
          this.fileData = res.data;
          console.log('file', this.fileData);
          this.openFile(this.fileData);
        } else {
          this._snackBar.open('Ошибка! Возможно, файл не существует или был перемещен в другую директорию');
        }
      });
  }

  openFile(file) {
    if (file.type === '.pdf') {
      const pdfWindow = window.open('', '_self');
      pdfWindow.document.write('<html><head><title>' + file.title + '</title></head><body style="margin:0px">' +
        '<iframe style="border-width: 0px;margin: 0px;" width="100%" height="100%" src="data:application/pdf;base64,' +
        encodeURI(file.content) + '"></iframe>');
    } else {

      const linkSource = `data:application/${file.type};base64,${file.content}`;
      const downloadLink = document.createElement('a');
      const fileName = file.title;

      downloadLink.href = linkSource;
      downloadLink.download = fileName;
      downloadLink.click();
    }
  }
}
