import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

import {ActivatedRoute, Params, Router} from '@angular/router';
import {switchMap} from 'rxjs/operators';
import * as moment from 'moment';
import {artAnimations} from '@art-lib/animations';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Subscription} from 'rxjs';
import {NewsService} from '@/cabinets/admin/news-menu/news.service';
import {ConfirmModalComponent} from '@art-lib/components/confirm-modal/confirm-modal.component';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-news-edit',
  templateUrl: './news-edit.component.html',
  styleUrls: ['./news-edit.component.scss'],
  animations: artAnimations
})
export class NewsEditComponent implements OnInit, OnDestroy {
  modules = {
    toolbar: [
      ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
      ['blockquote', 'code-block'],

      [{'header': 1}, {'header': 2}],               // custom button values
      [{'list': 'ordered'}, {'list': 'bullet'}],
      [{'script': 'sub'}, {'script': 'super'}],      // superscript/subscript
      [{'indent': '-1'}, {'indent': '+1'}],          // outdent/indent
      [{'direction': 'rtl'}],                         // text direction

      [{'size': ['small', false, 'large', 'huge']}],  // custom dropdown
      [{'header': [1, 2, 3, 4, 5, 6, false]}],

      [{'color': []}, {'background': []}],          // dropdown with defaults from theme
      [{'font': []}],
      [{'align': []}],

      ['clean'],                                         // remove formatting button

      ['link']                         // link and image, video
    ]
  };

  editForm: FormGroup;
  data: any;
  id: number;
  unsubscribe: Subscription;
  isLoad: boolean = false;

  news: any;

  constructor(private newsService: NewsService,
              private fb: FormBuilder,
              private route: ActivatedRoute,
              private _snackBar: MatSnackBar,
              private dialog: MatDialog,
              private router: Router
  ) {
  }

  ngOnInit() {
    this.route.params
      .pipe(switchMap((params: Params) => {
        this.id = +params.id;
        return this.newsService.getById(+params.id);
      }))
      .subscribe((response) => {
        if (response.done) {
          this.data = response.data;
          this.initForm();
          this.fillForm(this.data);
        } else {
          this._snackBar.open('Произошла ошибка' + (response.message ? ': ' + response.message : '' ));
        }
      });
  }
  initForm() {
    this.editForm = this.fb.group({
      title: [ null, [
            Validators.required,
            Validators.minLength(4)
          ]],
      content: [null, Validators.required],
      date_news: [null, Validators.required],
      published: [ null ],
      file: [ null ]
    });
  }

  fillForm(data) {
    for ( let key in this.editForm.controls ) {
      if (data.hasOwnProperty(key)) {
        this.editForm.get(key).setValue( this.data[key] );
      }
    }
    this.editForm.markAsPristine();
    this.editForm.markAsUntouched();
  }


  getData() {
    this.newsService.getById(this.id)
      .subscribe(res => {
        if( res.done ) {
          this.data = res.data;
        } else {
          this._snackBar.open('Произошла ошибка' + (res.message ? ': ' + res.message : '' ));
        }
    })
  }

  saveChanges() {
    const formData = {
      title: this.editForm.controls.title.value,
      content: this.editForm.controls.content.value,
      date_news: moment(this.editForm.controls.date_news.value).format(),
      published: this.editForm.controls.published.value
    };

    const filesData = {
      files: this.editForm.controls.file.value
    };

    this.isLoad = true;

    this.unsubscribe = this.newsService.editNews(this.id, formData)
        .subscribe((resp: any) => {
          if (!resp.done) {
            this._snackBar.open('Произошла ошибка' + (resp.message ? ': ' + resp.message : '' ));
            this.isLoad = false;
          } else {

            if (filesData.files) {
              this.newsService.addFile(this.id, filesData)
                  .subscribe((res: any) => {
                    if (res.done) {
                      this.editForm.get('file').reset();
                      this.getData();
                      this._snackBar.open('Новость успешно отредактирована');
                    } else {
                      this._snackBar.open('Произошла ошибка' + (res.message ? ': ' + res.message : '' ));
                    }
                    this.isLoad = false;
                  });
            } else {
              this._snackBar.open('Новость успешно отредактирована');
              this.editForm.markAsPristine();
              this.editForm.markAsUntouched();
              this.isLoad = false;
            }



          }
        });
  }

  ngOnDestroy() {
    if (this.unsubscribe) {
      this.unsubscribe.unsubscribe();
    }
  }

  removeFile(id, index) {
    this.isLoad = true;
    this.newsService.removeDocFile(id)
        .subscribe((resp: any) => {
            if (resp.done) {
              this._snackBar.open('Файл удалён');
              this.data.files.splice(index, 1);
            } else {
              this._snackBar.open('Произошла ошибка' + (resp.message ? ': ' + resp.message : ''));
            }
            this.isLoad = false;
    });
  }

  deleteNew(id) {
    this.dialog.open(ConfirmModalComponent, {
      data: {
        title: 'Удаление новости',
        content: 'Вы уверены, что хотите удалить новость?'
      }
    })
      .afterClosed().subscribe(res => {
        const removeNew = {
          deleted: true
        };
      if (res) {
        this.newsService.removeAndRecoveryNews(id, removeNew).subscribe((res: any) => {
          if(res.done) {
            this._snackBar.open('Новость удалена');
            this.data.deleted = true;
          } else {
            this._snackBar.open('Произошла ошибка' + (res.message ? ': ' + res.message : '' ));
          }
        });
      }
    });

  }

  recoveryNew(id) {
    this.dialog.open(ConfirmModalComponent, {
      data: {
        title: 'Восстановление новости',
        content: 'Вы уверены, что хотите восстановить новость?'
      }
    })
      .afterClosed().subscribe(res => {
      const recoveryNew = {
        deleted: false
      };
      if (res) {
        this.newsService.removeAndRecoveryNews(id, recoveryNew).subscribe((res: any) => {
          if(res.done) {
            this._snackBar.open('Новость восстановлена');
            this.data.deleted = false;
          } else {
            this._snackBar.open('Произошла ошибка' + (res.message ? ': ' + res.message : '' ));
          }
        })
      }
    });
  }
}

