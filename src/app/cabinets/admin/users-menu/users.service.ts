import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs';
import {toFormData} from '@/cabinets/university/entrants-menu/entrants.service';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  urlPrefix: string;
  urlGroup = '/users';

  constructor(private httpClient: HttpClient) {
    this.urlPrefix = environment.apiUrl + '/admin';
  }

  getUsersList(params?): Observable<any> {
    const url = `${this.urlPrefix}${this.urlGroup}/list`;

    console.log('paramsData', params);

    return this.httpClient.get(url, { params });
  }

  toggleUser(id: number, type: boolean): Observable<any> {
    const code = type ? 'unblock' : 'block';
    const url = this.urlPrefix + this.urlGroup + `/${id}/` + code;

    return this.httpClient.post(url, {});
  }

  createUser(data): Observable<any> {
    const url = `${this.urlPrefix}${this.urlGroup}/create`;

    return this.httpClient.post(url,  toFormData(data));
  }

  getUserInfo(id: number): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${id}/main`;

    return this.httpClient.get(url);
  }

  getUserOrgs(id: number): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${id}/links`;

    return this.httpClient.get(url);
  }

  usersAddLink(idUser: number, data): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${idUser}/links/add`;

    const headers = new HttpHeaders({
      'key': 'Content-Type',
      'value': 'multipart/form-data'
    });

    return this.httpClient.post(url, toFormData(data), { headers});
  }

  offUserOrgLink(idUser: number, idLink: number, comment: string): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${idUser}/links/${idLink}/break-off`;
    return this.httpClient.post(url, {comment});
  }

  resetPass(idUser: number, passHash: string): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${idUser}/password/edit`;
    return this.httpClient.post(url, {password: passHash});

  }
}
