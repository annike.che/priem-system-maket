import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import * as generator from '@/common/functions/password';
import {ActivatedRoute, Router} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {MatSnackBar} from '@angular/material/snack-bar';
import {UsersService} from '@/cabinets/admin/users-menu/users.service';
import {password} from '@rxweb/reactive-form-validators';
import {OrgsAdminService} from '@/cabinets/admin/orgs-menu/orgs-admin.service';
import {OrgsCommonService} from '@/common/services/orgs-common.service';

@Component({
  selector: 'app-users-add',
  templateUrl: './users-add.component.html',
  styleUrls: ['./users-add.component.scss']
})
export class UsersAddComponent implements OnInit {

  form: FormGroup;

  organizationList: Array<any> = [];
  searchSelect = '';

  constructor(
      private fb: FormBuilder,
      private orgsSevice: OrgsCommonService,
      private usersService: UsersService,
      private route: ActivatedRoute,
      private router: Router,
      private titleService: Title,
      private snackBar: MatSnackBar,
  ) { }

  ngOnInit() {
    this.createForm();
    this.initOrgList();

  }

  createForm() {
    this.form = this.fb.group({
      name:               [ null, Validators.required ],
      surname:            [ null, Validators.required ],
      patronymic:         [ null ],
      login:              [ null, [ Validators.required,
                                    Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')
                          ]],
      password:           [ null, Validators.required ],
      phone:              [ null ],
      email:              [ null, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')],
      id_organization:    [ null, Validators.required ],
      file:               [ null, Validators.required ]
    });
  }

  generatePass() {
    let pass = generator.generatePass();
    console.log('pass', pass);
    this.form.get('password').setValue(pass);
    console.log();
  }

  initOrgList() {
    this.orgsSevice.getOrgsSelectList()
        .subscribe( res => {
          this.organizationList = res;
        } );
  }

  create() {
    let data = {
      ...this.form.value,
      password: generator.hashPass(this.form.get('login').value, this.form.get('password').value)
    };
    this.usersService.createUser(data)
        .subscribe( res => {
          if ( res.data ) {
            this.snackBar.open('Пользователь успешно создан');
            this.router.navigate(['../'], { relativeTo: this.route });
          } else {
            this.snackBar.open('Произошла ошибка' + ( res.message ? ': ' + res.message : '' ) );
          }
        } );
  }
}
