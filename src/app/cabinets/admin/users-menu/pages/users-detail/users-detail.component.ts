import { Component, OnInit } from '@angular/core';
import {UsersService} from '@/cabinets/admin/users-menu/users.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog';
import {artAnimations} from '@art-lib/animations';
import {ConfirmModalComponent} from '@art-lib/components/confirm-modal/confirm-modal.component';
import {CommentModalComponent} from '@art-lib/components/comment-modal/comment-modal.component';
import * as generator from '@/common/functions/password';
import {ShowPassDialogComponent} from '@/cabinets/admin/users-menu/components/show-pass-dialog/show-pass-dialog.component';
import {OrgsAdminService} from '@/cabinets/admin/orgs-menu/orgs-admin.service';
import {OrgsCommonService} from '@/common/services/orgs-common.service';

@Component({
  selector: 'app-users-detail',
  templateUrl: './users-detail.component.html',
  styleUrls: ['./users-detail.component.scss'],
  animations: artAnimations
})
export class UsersDetailComponent implements OnInit {

  user: UserItem;
  userId: number;

  orgs: Array<any>;

  displayedColumns = ['title', 'ogrn', 'kpp', 'file', 'actions'];
  addingColumns = ['selectOrg', 'selectFile', 'actionsSave'];

  addingData: any;

  organizationList: Array<any> = [];
  searchSelect = '';

  isLoad: boolean;

  isAddOrgLink = (index, item) => { return  item ? !item.id : true; };

  constructor(private usersService: UsersService,
              private orgsSevice: OrgsCommonService,
              private route: ActivatedRoute,
              private router: Router,
              private titleService: Title,
              private snackBar: MatSnackBar,
              private dialog: MatDialog) { }

  ngOnInit() {
    this.route.params.subscribe( res => {
      this.userId = +res.id;
      if( this.userId ) {
        this.getUserInfo();
        this.getUserOrgs();
        this.initOrgList();
      } else {
        this.snackBar.open('Указан неверный id');
        this.goToList();
      }
    });
  }

  getUserInfo() {
    this.isLoad = true;
    this.usersService.getUserInfo(this.userId)
        .subscribe( res => {
            if(res.done) {
              this.user = res.data;
            } else {
              this.snackBar.open('Произошла ошибка' + ( res.message ? ': ' + res.message : '' ) );
              this.goToList();
            }
            this.isLoad = false;
        });
  }

  getUserOrgs() {
    this.isLoad = true;
    this.usersService.getUserOrgs(this.userId)
        .subscribe( res => {
            if( res.done ) {
              this.orgs = res.data;
            } else {
              this.snackBar.open('Произошла ошибка' + ( res.message ? ': ' + res.message : '' ) );
            }
            this.isLoad = false;
        });

  }

  initOrgList() {
    this.orgsSevice.getOrgsSelectList()
        .subscribe( res => {
            this.organizationList = res;
        } );
  }

  goToList() {
    this.router.navigate(['../'], {relativeTo: this.route});
  }

  openFile($event: MouseEvent, row: any) {

  }

  addOrg() {
    this.addingData = {
      id_organization: null,
      file: null
    };

    this.orgs.unshift( this.addingData);
    this.orgs = [ ...this.orgs];
  }

  save() {
    this.isLoad = true;
    this.usersService.usersAddLink( this.userId, this.addingData)
        .subscribe( res => {
          if (res.done) {
            this.deleteForm();
            this.getUserOrgs();
            this.snackBar.open('Связь успешно добвлена');
          } else {
            this.snackBar.open('Произошла ошибка' + ( (res.message) ? ': ' + res.message : '' ));
          }

          this.isLoad = false;
        } );
  }

  deleteForm() {
    this.orgs.splice(0, 1);
    this.orgs = [ ...this.orgs];
    this.addingData = undefined;
  }

  offLink($event: MouseEvent, id: any, index: number) {
    this.dialog.open(ConfirmModalComponent, {
      data: {
        title: 'Отвязать пользователя',
        content: 'Вы уверены, что хотите отвязать пользователя от данной организации?'
      }
    })
        .afterClosed().subscribe(data => {
      if (data) {
        this.dialog.open(CommentModalComponent, {
          data: {
            title: 'Отвязать организацию',
            label: 'Введите комментарий'
          },
          minWidth: '400px'
      }).afterClosed().subscribe( resp => {
        if (resp && resp.hasOwnProperty('comment'))  {
          this.usersService.offUserOrgLink(this.userId, id, resp.comment)
              .subscribe( res => {
                if (res.done) {
                  this.orgs.splice(index, 1);
                  this.orgs = [ ... this.orgs];
                  this.snackBar.open('Организация успешно отвязана');
                } else {
                  this.snackBar.open('Произошла ошибка' + (res.message ? ': ' + res.message : '' ) );
                }
              });
        }
      } );
        /*
        this.usersService.offUserOrgLink(id)
            .subscribe( (res: any) => {
              if(res.done) {
                this.achievements.splice(index, 1);
                this.achievements = [ ... this.achievements];
                this.snackBar.open('Индивидуальное достижение успешно удалено');
              } else {
                this.snackBar.open('При удалении индивидуального достижения произошла ошибка' + (res.message? ': '+res.message : '' ) );
              }
            });*/

      }
    });
  }

  resetPass() {
    this.dialog.open(ConfirmModalComponent, {
      data: {
        title: 'Сброс пароля',
        content: 'Вы уверены, что хотите сбросить пользователю пароль и создать новый?'
      }
    }).afterClosed().subscribe( res => {
      if( res ) {
          let pass = generator.generatePass();
          let hash = generator.hashPass(this.user.login, pass);
          this.usersService.resetPass(this.userId, hash)
              .subscribe( res => {
                    if( res.done) {

                      this.dialog.open(ShowPassDialogComponent, { data: pass, disableClose: true });

                    } else {
                      this.snackBar.open('Произошла ошибка' + (res.message ? ': ' + res.message : '' ) );
                    }
                  }
              );
      }
    } );
  }
}
