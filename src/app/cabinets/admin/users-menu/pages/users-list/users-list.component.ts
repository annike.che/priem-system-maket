import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {PageEvent} from '@angular/material/paginator';
import {ActivatedRoute, Router} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {UsersService} from '@/cabinets/admin/users-menu/users.service';
import {debounceTime, distinctUntilChanged, switchMap, tap} from 'rxjs/operators';
import {untilDestroyed} from 'ngx-take-until-destroy';
import {SortDirection} from '@angular/material/sort';
import {MatSnackBar} from '@angular/material/snack-bar';
import {artAnimations} from '@art-lib/animations';
import * as cloneDeep from 'lodash/cloneDeep';
import {MatDialog} from '@angular/material/dialog';
import {ConfirmModalComponent} from '@art-lib/components/confirm-modal/confirm-modal.component';
import {MatSlideToggleChange} from '@angular/material/slide-toggle';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss'],
  animations: artAnimations
})
export class UsersListComponent implements OnInit, OnDestroy {

  persons: Array<UserItem>;

  displayedColumns: Array<string>;
  searchColumns: string[];
  pageSizeOptions: number[];

  isLoad: boolean;

  search = new Subject<string[]>();
  sortEvent = new Subject<SortEvent>();

  pagination = new Subject<PageEvent>();

  defaultParams = {
    page: 1,
    limit: 20
  };
  total: number;

  params: Params = {
    ...this.defaultParams
  };


  constructor( private usersService: UsersService,
               private route: ActivatedRoute,
               private router: Router,
               private titleService: Title,
               private snackBar: MatSnackBar,
               private dialog: MatDialog) {

    this.isLoad = false;
    this.titleService.setTitle('Список пользователей');

    this.persons = [];

    this.displayedColumns = ['surname', 'name', 'patronymic', 'role', 'login', 'registrationDate', 'actual'];
    this.searchColumns = ['searchSurname', 'searchName', 'searchPatronymic', 'searchRole', 'searchLogin', 'searchRegistrationDate' , 'searchActions'];
    this.pageSizeOptions = [5, 10, 15, 20];
  }

  ngOnInit() {
    this.getQueryParamsFromUrl();
    this.subscribeOnChanges();
    this.getPersons();
  }

  subscribeOnChanges() {
    this.search.pipe(
        distinctUntilChanged(),
        debounceTime(500),
        tap(() => {
          this.params.page = 1;
        }),
        switchMap((value, index) => {
          // console.log('value', value);
          return this.getDataFromService();
        }),
        untilDestroyed(this)
    ).subscribe(res => this.setData(res));

    this.pagination.pipe(
        debounceTime(200),
        tap((event: PageEvent) => {
          if (this.params.limit !== event.pageSize) {
            this.params.limit = event.pageSize;
            this.params.page = 1;
          }
          if (this.params.page !== event.pageIndex + 1) {
            this.params.page = event.pageIndex + 1;
          }
        }),
        switchMap(() => this.getDataFromService()),
        untilDestroyed(this)
    )
        .subscribe(response => this.setData(response));

    this.sortEvent.pipe(
        debounceTime(200),
        tap((event: SortEvent) => {
          this.params.order = event.direction as SortDirection;
          this.params.sortby = event.active;
        }),
        switchMap(() => this.getDataFromService()),
        untilDestroyed(this)
    )
        .subscribe(response => this.setData(response));


  }

  getDataFromService(): Observable<any> {
    this.isLoad = true;
    const queryParams = this.getQueryParams(this.params);
    this.passQueryParamsToUrl(queryParams);
    return this.usersService.getUsersList(this.params);
  }

  getQueryParams(params) {
    const queryParams: any = {};
    Object.keys(params).forEach((key) => {
      if (this.defaultParams.hasOwnProperty(key) && (this.defaultParams[key] !== params[key] && params[key] !== '') ||
          (!this.defaultParams.hasOwnProperty(key) && params[key] !== '')) {
        if ((key === 'frDt' || key === 'toDt') && params.dateF !== 'FromTo') {
          return;
        }
        queryParams[key] = params[key];
      }
    });

    return queryParams;
  }

  passQueryParamsToUrl(queryParams) {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams
    });
  }

  setData(res) {
    if (res.done) {

      this.persons = res.data;
      this.params.page = res.paginator.page;
      this.params.limit = res.paginator.limit;
      this.total = res.paginator.total;

    } else {
      this.snackBar.open('Произошла ошибка' + ( res.message ? ': ' + res.message : '' ) );
    }

    this.isLoad = false;
  }

  getQueryParamsFromUrl() {
    const queryParams = cloneDeep(this.route.snapshot.queryParams);
    this.setParams(queryParams);
  }


  setParams(params) {
    Object.keys(params).forEach(key => {
      switch (key) {
        case 'page':
          this.params[key] = +params[key];
          break;
        default:
          this.params[key] = params[key];
      }
    });
  }


  getPersons() {
    this.getDataFromService()
        .subscribe(response => this.setData(response));
  }

  ngOnDestroy(): void {
  }

  toggleUser($event: MatSlideToggleChange, row: UserItem) {
    console.log($event, row.actual);

    const title = ($event.checked) ? 'Разблокировка пользователя' : 'Блокировка пользователя';
    const content = ($event.checked) ? 'разблокировать пользователя' : 'заблокировать пользователя';

    this.dialog.open(ConfirmModalComponent, {
      data: {
        title,
        content: `Вы уверены, что хотите ${content}?`
      }
    }).afterClosed().subscribe(data => {
      if ( !data ) {
        row.actual = !$event.checked;
      } else {
        this.isLoad = true;
        this.usersService.toggleUser(row.id, $event.checked)
            .subscribe( res => {
              if (res.done) {
                this.snackBar.open('Пользователь успешно ' + ($event.checked ? 'разблокирован' : 'заблокирован'));
              } else {
                this.snackBar.open('Произошла ошибка' + ( res.message ? ': ' + res.message : '' ) );
                row.actual = !$event.checked;
              }
              this.isLoad = false;
            } );

      }

    });
  }

  clearFilter($event) {
    this.params.search_name = '';
    this.params.search_surname = '';
    this.params.search_patronymic = '';
    this.params.search_login = '';

    this.search.next($event);
  }
}

interface Params {
  page: number;
  limit: number;
  search_name?: string;
  search_surname?: string;
  search_patronymic?: string;
  search_login?: string;
  order?: SortDirection; // 'ASC' || 'DESC'
  sortby?: string;
}
