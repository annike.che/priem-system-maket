import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { UsersListComponent } from './pages/users-list/users-list.component';
import {SharedModule} from '@/common/modules/shared.module';
import {ArtLibModule} from '@art-lib/art-lib.module';
import { UsersDetailComponent } from './pages/users-detail/users-detail.component';
import { ShowPassDialogComponent } from './components/show-pass-dialog/show-pass-dialog.component';
import { UsersAddComponent } from './pages/users-add/users-add.component';
import {IMaskModule} from 'angular-imask';

const routes: Routes = [
  {path: '', component: UsersListComponent},
  {path: 'add', component: UsersAddComponent},
  {path: ':id', component: UsersDetailComponent},
 /* {path: 'new', loadChildren: () => import('./pages/application-new/application-new.module').then( m => m.ApplicationNewModule )},
  {path: ':id',
    loadChildren: () => import('./pages/application-detail/application-detail.module').then( m => m.ApplicationDetailModule ),
    data: {preload: true}
  }*/
];


@NgModule({
  declarations: [
    UsersListComponent,
    UsersDetailComponent,
    UsersAddComponent,
    ShowPassDialogComponent,
  ],
  imports: [
    RouterModule.forChild(routes),
    SharedModule,
    ArtLibModule,
    IMaskModule,
  ],
  entryComponents: [ ShowPassDialogComponent ]
})
export class UsersMenuModule { }
