import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-show-pass-dialog',
  templateUrl: './show-pass-dialog.component.html',
  styleUrls: ['./show-pass-dialog.component.scss']
})
export class ShowPassDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: string) { }

  ngOnInit() {
  }

}
