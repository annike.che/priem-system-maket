interface UserItem {
  id: number;
  login: string;
  name: string;
  surname: string;
  patronymic: string;

  code_role: string;
  id_role: number;
  name_role: number;
  actual: boolean;

  id_region: number;
  name_region: string;

  email: string;
  phone: number;


  registration_date: string;

}

/*interface User {
  /!*id: number;
  login: string;
  name: string;
  surname: string;
  patronymic: string;

  actual: true,
  adress: null,
  changed: null,
  code_role: provider,
  email: chernousova@sfedu.ru,

  id_author: null,
  id_region: 0,
  id_role: 1,
  name_region: "",
  name_role: Поставщик данных,

  post: null,
  registration_date: 2020-07-10T10:14:37.061186+03:00,
  snils: null,

  work: null*!/
}*/
