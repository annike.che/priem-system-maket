import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {MaterialModule} from '@/common/modules/material/material.module';
import {UniversityComponent} from '@/cabinets/university/university.component';
import {AdminComponent} from '@/cabinets/admin/admin.component';
import {MainToolbarModule} from '@art-lib/components/main-toolbar/main-toolbar.module';
import {LeftSidenavModule} from '@art-lib/components/left-sidebar/left-sidenav.module';
import {RedirectGuard} from '@/common/guards/redirect.guard';
import {UniversityGuard} from '@/common/guards/university.guard';
import {AdminGuard} from '@/common/guards/admin.guard';


const routes: Routes = [
  {
    path: '',
    canActivate: [RedirectGuard]
  },
  {
    path: 'university',
    component: UniversityComponent,
    canActivate: [UniversityGuard],
    loadChildren: () => import('./university/university.module').then( m => m.UniversityModule)
  },
  {
    path: 'admin',
    component: AdminComponent,
    canActivate: [AdminGuard],
    loadChildren: () => import('./admin/admin.module').then( m => m.AdminModule)
  }
];


@NgModule({
  declarations: [UniversityComponent, AdminComponent],
  imports: [
    CommonModule,
    MainToolbarModule,
    LeftSidenavModule,
    RouterModule.forChild(routes),
    MaterialModule
  ]
})
export class CabinetsModule { }
