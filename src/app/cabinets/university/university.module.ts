import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';


const routes: Routes = [
  { path: '', redirectTo: 'campaign', pathMatch: 'full' },
  { path: 'campaign',
    loadChildren: () => import('./campaign-menu/campaign-menu.module').then( m => m.CampaignMenuModule )
  },
  { path: 'application',
    loadChildren: () => import('./application-menu/application-menu.module').then( m => m.ApplicationMenuModule )
  },
  {
    path: 'entrants',
    loadChildren: () => import('./entrants-menu/entrants-menu.module').then( m => m.EntrantsMenuModule )
  },
  {
    path: 'orders',
    loadChildren: () => import('./orders-menu/orders-menu.module').then( m => m.OrdersMenuModule )
  },
  {
    path: 'orgs',
    loadChildren: () => import('./orgs-menu/orgs-menu.module').then( m => m.OrgsMenuModule )
  },
  {
    path: 'packages',
    loadChildren: () => import('./packages-menu/packages-menu.module').then( m => m.PackagesMenuModule )
  },
  {
    path: 'news',
    loadChildren: () => import('./news-menu/news-menu.module').then( m => m.NewsMenuModule )
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ]
})
export class UniversityModule { }
