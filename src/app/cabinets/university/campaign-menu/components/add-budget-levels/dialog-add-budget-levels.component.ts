import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {CampaignService} from '@/cabinets/university/campaign-menu/campaign.service';
import {ClsService} from '@/common/services/cls.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-dialog-add-budget-levels',
  templateUrl: './dialog-add-budget-levels.component.html',
  styleUrls: ['./dialog-add-budget-levels.component.scss']
})
export class DialogAddBudgetLevelsComponent implements OnInit {

  idAdmission: number;
  nameSpeciality: string;

  clsBudgetLevels: Array<any>;
  defineBudgetLevels = [];

  data = [];

  constructor(public dialogRef: MatDialogRef<DialogAddBudgetLevelsComponent>,
              @Inject(MAT_DIALOG_DATA) public resp,
              private campaignService: CampaignService,
              private clsService: ClsService,
              private snackBar: MatSnackBar) {
    console.log('data', resp);
    this.idAdmission = resp.idAdmission;
    this.nameSpeciality = resp.nameSpeciality;

  }

  ngOnInit() {
  this.clsService.getClsData('level_budget')
      .subscribe( res => {
        this.clsBudgetLevels = res;
        console.log('this.clsBudgetLevels', this.clsBudgetLevels);
      } );

  this.campaignService.getBudgetLevelsSelectList(this.idAdmission)
      .subscribe( res => {
        if (res.done) {
          this.defineBudgetLevels = res.data || [];
        }
      });
  }

  sendData() {
    this.campaignService.addBudgetLevel(this.idAdmission,
        {
          id_admission: this.idAdmission,
          levels_budget: this.data
        })
        .subscribe( res => {
             if (res.done) {
               this.dialogRef.close({done: true});
               this.snackBar.open('Уровни бюджета успешно добавлены');
             } else {
               this.snackBar.open('При попытке добавить уровни бюджета произошла ошибка');
             }
         });
  }


}
