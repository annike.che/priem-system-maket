import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';


@Component({
  selector: 'app-admission-table-list-settings',
  templateUrl: './admission-table-list-settings.component.html',
  styleUrls: ['./admission-table-list-settings.component.scss']
})
export class AdmissionTableListSettingsComponent implements OnInit {


  constructor(
      public dialogRef: MatDialogRef<AdmissionTableListSettingsComponent>,
      @Inject(MAT_DIALOG_DATA) public settings,
     ) {
  }

  ngOnInit() {
    console.log(this.settings);
  }

  save() {
    console.log('settings', this.settings);

    this.dialogRef.close( { settings: this.settings  } );
  }

}


