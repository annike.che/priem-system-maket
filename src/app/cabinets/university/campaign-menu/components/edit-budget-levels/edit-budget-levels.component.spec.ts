import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditBudgetLevelsComponent } from './edit-budget-levels.component';

describe('EditBudgetLevelsComponent', () => {
  let component: EditBudgetLevelsComponent;
  let fixture: ComponentFixture<EditBudgetLevelsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditBudgetLevelsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditBudgetLevelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
