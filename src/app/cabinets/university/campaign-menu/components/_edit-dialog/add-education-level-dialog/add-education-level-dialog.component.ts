import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ClsService} from '@/common/services/cls.service';
import {CampaignService} from '@/cabinets/university/campaign-menu/campaign.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-add-education-level-dialog',
  templateUrl: './add-education-level-dialog.component.html',
  styleUrls: ['./add-education-level-dialog.component.scss']
})
export class AddEducationLevelDialogComponent implements OnInit {

  allEducationLevels = [];
  campaign: ICampaignMainInfo;

  newEducationLevels = [];

  constructor(private clsService: ClsService,
              private campaignService: CampaignService,
              private snackBar: MatSnackBar,
              public dialogRef: MatDialogRef<AddEducationLevelDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: ICampaignMainInfo
  ) {
    this.campaign = data;
  }

  ngOnInit() {
    this.initCls();
  }

  initCls() {
    let params = {id_campaign_types: this.data.id_campaign_type};
    let tableName = 'v_edu_levels_campaign_types';

    this.clsService.getClsData(tableName, null, params)
        .subscribe( res => {
             this.allEducationLevels = res;
        });
  }

  save() {
    this.campaignService.addEducationLevels(this.data.id, this.newEducationLevels)
        .subscribe( res => {
            if (res.done) {
              this.snackBar.open( 'Уровни Образования успешно добавлены');
              this.dialogRef.close({ done: true });
            } else {
              this.snackBar.open( 'Произошла ошибка' + (res.message? ': ' + res.message : '')  );
            }
        });
  }
}
