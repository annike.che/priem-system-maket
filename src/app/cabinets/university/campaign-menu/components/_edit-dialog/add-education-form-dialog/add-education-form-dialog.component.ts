import {Component, Inject, OnInit} from '@angular/core';
import {ClsService} from '@/common/services/cls.service';
import {CampaignService} from '@/cabinets/university/campaign-menu/campaign.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-add-education-form-dialog',
  templateUrl: './add-education-form-dialog.component.html',
  styleUrls: ['./add-education-form-dialog.component.scss']
})
export class AddEducationFormDialogComponent implements OnInit {

  allEducationForms = [];
  campaign: ICampaignMainInfo;

  newEducationForms = [];

  constructor(private clsService: ClsService,
              private campaignService: CampaignService,
              private snackBar: MatSnackBar,
              public dialogRef: MatDialogRef<AddEducationFormDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: ICampaignMainInfo
  ) {
    this.campaign = data;
  }

  ngOnInit() {
    this.initCls();
  }

  initCls() {
    let tableName = 'education_forms';

    this.clsService.getClsData(tableName)
        .subscribe( res => {
          this.allEducationForms = res;
        });
  }

  save() {
    this.campaignService.addEducationForms(this.data.id, this.newEducationForms)
        .subscribe( res => {
          if (res.done) {
            this.snackBar.open( 'Формы обучения успешно добавлены');
            this.dialogRef.close({ done: true });
          } else {
            this.snackBar.open( 'Произошла ошибка' + (res.message ? ': ' + res.message : '')  );
          }
        });
  }
}
