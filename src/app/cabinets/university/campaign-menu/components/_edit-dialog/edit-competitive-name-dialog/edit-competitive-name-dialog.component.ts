import {Component, Inject, OnInit} from '@angular/core';
import {CampaignService} from '@/cabinets/university/campaign-menu/campaign.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-edit-competitive-name-dialog',
  templateUrl: './edit-competitive-name-dialog.component.html',
  styleUrls: ['./edit-competitive-name-dialog.component.scss']
})
export class EditCompetitiveNameDialogComponent implements OnInit {

  nameOld: string;
  name: string;

  constructor(private campaignService: CampaignService,
              private snackBar: MatSnackBar,
              public dialogRef: MatDialogRef<EditCompetitiveNameDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: CompetitiveMain ) {
    this.nameOld = data.name;
    this.name = data.name;

  }

  ngOnInit() {
  }

  save() {
    this.campaignService.editCompetitiveComment(this.data.id, this.name)
        .subscribe( res => {
          if( res.done ) {
            this.snackBar.open('Данные успешно обновлены');
            this.dialogRef.close( {name: this.name} );
          } else {
            this.snackBar.open('Произошла ошибка' + ( res.message ? ': ' + res.message : '' ) );
          }
        } );
  }

}
