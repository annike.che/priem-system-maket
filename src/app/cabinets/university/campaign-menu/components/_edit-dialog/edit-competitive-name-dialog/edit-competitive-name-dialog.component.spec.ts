import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditCompetitiveNameDialogComponent } from './edit-competitive-name-dialog.component';

describe('EditCompetitiveNameDialogComponent', () => {
  let component: EditCompetitiveNameDialogComponent;
  let fixture: ComponentFixture<EditCompetitiveNameDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditCompetitiveNameDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditCompetitiveNameDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
