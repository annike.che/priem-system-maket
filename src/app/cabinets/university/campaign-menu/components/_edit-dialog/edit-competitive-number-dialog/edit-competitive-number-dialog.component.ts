import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {CampaignService} from '@/cabinets/university/campaign-menu/campaign.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-edit-competitive-number-dialog',
  templateUrl: './edit-competitive-number-dialog.component.html',
  styleUrls: ['./edit-competitive-number-dialog.component.scss']
})
export class EditCompetitiveNumberDialogComponent implements OnInit {

  numberOld: number;
  number: number;

  constructor(
      private campaignService: CampaignService,
      private snackBar: MatSnackBar,
      public dialogRef: MatDialogRef<EditCompetitiveNumberDialogComponent>,
      @Inject(MAT_DIALOG_DATA) public data: CompetitiveMain  )
  {
    this.numberOld = data.number;
    this.number = data.number;

  }

  ngOnInit() {
  }

  save() {
    this.campaignService.editCompetitiveNumber(this.data.id, this.number)
        .subscribe( res => {
            if( res.done ) {
              this.snackBar.open('Данные успешно обновлены');
              this.dialogRef.close( {number: this.number} );
            } else {
              this.snackBar.open('Произошла ошибка' + ( res.message ? ': ' + res.message : '' ) );
            }
        } );
  }

}
