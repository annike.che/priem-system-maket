import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditCompetitiveCommentDialogComponent } from './edit-competitive-comment-dialog.component';

describe('EditCompetitiveCommentDialogComponent', () => {
  let component: EditCompetitiveCommentDialogComponent;
  let fixture: ComponentFixture<EditCompetitiveCommentDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditCompetitiveCommentDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditCompetitiveCommentDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
