import {Component, Inject, OnInit} from '@angular/core';
import {CampaignService} from '@/cabinets/university/campaign-menu/campaign.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-edit-competitive-comment-dialog',
  templateUrl: './edit-competitive-comment-dialog.component.html',
  styleUrls: ['./edit-competitive-comment-dialog.component.scss']
})
export class EditCompetitiveCommentDialogComponent implements OnInit {

  commentOld: string;
  comment: string;

  constructor(private campaignService: CampaignService,
              private snackBar: MatSnackBar,
              public dialogRef: MatDialogRef<EditCompetitiveCommentDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: CompetitiveMain ) {
    this.commentOld = data.comment;
    this.comment = data.comment;

  }

  ngOnInit() {
  }

  save() {
    this.campaignService.editCompetitiveComment(this.data.id, this.comment)
        .subscribe( res => {
          if( res.done ) {
            this.snackBar.open('Данные успешно обновлены');
            this.dialogRef.close( {comment: this.comment} );
          } else {
            this.snackBar.open('Произошла ошибка' + ( res.message ? ': ' + res.message : '' ) );
          }
        } );
  }

}
