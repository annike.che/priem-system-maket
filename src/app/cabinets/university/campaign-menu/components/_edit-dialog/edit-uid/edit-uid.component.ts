import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {CampaignService} from '@/cabinets/university/campaign-menu/campaign.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-edit-uid',
  templateUrl: './edit-uid.component.html',
  styleUrls: ['./edit-uid.component.scss']
})
export class EditUidComponent implements OnInit {

  uidOld: string;

  isLoad = true;

  constructor(
      private campaignService: CampaignService,
      private snackBar: MatSnackBar,
      public dialogRef: MatDialogRef<EditUidComponent>,
      @Inject(MAT_DIALOG_DATA) public data: EditUidData ) {

    this.uidOld = data.uid;

  }
  ngOnInit() {
      this.isLoad = false;
  }

  save() {

    this.isLoad = true;

    this.campaignService.editUid(this.data.tableKey, this.data.id, this.data.uid)
        .subscribe( res => {
            if ( res.done ) {
              this.snackBar.open('UID успешно обновлен');
              this.dialogRef.close({uid: this.data.uid});
            } else {
              this.snackBar.open( 'Произошла ошибка' + (res.message ? ': ' + res.message : '' ) );
            }

            this.isLoad = false;
        });
  }
}

interface EditUidData {
  uid: string;
  tableKey: string;
  id: number;
}
