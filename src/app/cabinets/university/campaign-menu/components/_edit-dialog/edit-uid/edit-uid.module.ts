import { NgModule } from '@angular/core';
import { EditUidComponent } from './edit-uid.component';
import {SharedModule} from '@/common/modules/shared.module';
import {CommonDirectivesModule} from '@art-lib/directives/common-directives.module';
import {ArtLibModule} from '@art-lib/art-lib.module';



@NgModule({
  declarations: [EditUidComponent],
  imports: [
    SharedModule,
    CommonDirectivesModule,
      ArtLibModule
  ],
  entryComponents: [EditUidComponent],
  exports: [EditUidComponent]
})
export class EditUidModule { }
