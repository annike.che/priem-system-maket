import {Component, Inject, OnInit} from '@angular/core';
import {ClsService} from '@/common/services/cls.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-change-campaign-status-dialog',
  templateUrl: './change-campaign-status-dialog.component.html',
  styleUrls: ['./change-campaign-status-dialog.component.scss']
})
export class ChangeCampaignStatusDialogComponent implements OnInit {

  statuses = [];
  currStatus: string;
  currStatus_c: string;

  constructor(private clsService: ClsService,
              public dialogRef: MatDialogRef<ChangeCampaignStatusDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.currStatus = data.code;
    this.currStatus_c = data.code;
  }

  ngOnInit() {
    this.clsService.getCampaignStatuses()
        .subscribe( res => {
          this.statuses = res;
        } );
  }

  close() {
    this.dialogRef.close();
  }

  save() {
    if (this.currStatus !== this.currStatus_c) {
      this.dialogRef.close({status: this.currStatus});
    }
  }
}
