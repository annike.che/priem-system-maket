import { NgModule } from '@angular/core';
import { CampaignItemComponent } from './campaign-item.component';
import {SharedModule} from '@/common/modules/shared.module';
import {RouterModule} from '@angular/router';
import {CommonDirectivesModule} from '@art-lib/directives/common-directives.module';



@NgModule({
  declarations: [CampaignItemComponent],
  imports: [
    SharedModule,
    RouterModule,
    CommonDirectivesModule
  ],
  exports: [ CampaignItemComponent ]
})
export class CampaignItemModule { }
