import {Component, Input, OnInit} from '@angular/core';

import {Router} from '@angular/router';

@Component({
  selector: 'app-campaign-item',
  templateUrl: './campaign-item.component.html',
  styleUrls: ['./campaign-item.component.scss']
})
export class CampaignItemComponent implements OnInit {

  @Input() item: ICampaignItem;

  constructor() { }

  ngOnInit() {
  }

}
