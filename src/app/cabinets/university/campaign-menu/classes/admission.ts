interface Admission {
    id?: number;
    id_campaign: number;

    code_specialty: string;
    name_specialty: string;
    id_specialty: number;

    count?: number;
    id_items?: Array<number>;

    name_groups?: string;
    code_groups?: number;
    id_groups?: number;

    name_education_level: string;
    id_education_level: number;

    has_distributed?: boolean;  // есть ли уровни бюджетов
    distributed?: Array<any>;

    id_admission_volume?: null;


    budget_o: number;
    budget_oz: number;
    budget_z: number;
    paid_o?: number;
    paid_oz?: number;
    paid_z?: number;
    quota_o: number;
    quota_oz: number;
    quota_z: number;
    target_o: number;
    target_oz: number;
    target_z: number;

    sum_distributed: number;
    sum_competitive: number;

    uid: string;
}

interface BudgetLevels {
    code_specialty: string;
    name_specialty: string;

    id: number;

    id_admission_volume: number;
    id_level_budget: number;
    name_level_budget: string;

    name_education_level: string;
    id_education_level: number;

    id_specialty: number;
  //  name_specialty: string;

    budget_o: number;
    budget_oz: number;
    budget_z: number;
    target_o: number;
    target_oz: number;
    target_z: number;
    quota_o: number;
    quota_oz: number;
    quota_z: number;

    paid_o?: number;
    paid_oz?: number;
    paid_z?: number;

    sum_competitive: number;
    sum_distributed?: number;

    uid: string;
}
