interface Achievement {
    id: number;
    uid: string;
    name: string;
    id_campaign: number;
    name_campaign: string;
    id_category: number;
    name_category: string;
    max_value: number;
    created: string;
}
