

interface CompetitiveItem {
    id: number;
    name: string;
    name_education_level: string;
    id_education_level: number;

    name_direction: string;
    id_direction_name: number;
    code_specialty: string;



    name_education_source: string;
    id_education_source: number;

    name_education_form: string;
    id_education_form: number;

    number: number;
    uid: string;
}

interface Competitive {
    competitive: CompetitiveMain;
    education_programs: Array<EducationPrograms>;
    entrance_tests: Array<EntrantsTests>;
}

interface CompetitiveMain {
    code_direction: string;
    id: number;
    id_campaign: number;
    id_direction: number;
    id_education_form: number;
    id_education_level: number;
    id_education_source: number;
    id_level_budget: number;
    name: string;
    name_campaign: string;
    name_direction: string;
    name_education_form: string;
    name_education_level: string;
    name_education_source: string;
    name_level_budget: string;
    number: number;
    uid: number;
    comment: string;
}

interface EducationPrograms {
    id: number;
    id_subdivision_org: number;
    name: string;
    uid: string;
}

interface EntrantsTests {
    id: number;
    id_entrance_test_type: number;
    id_subject: number;
    is_ege: boolean;
    min_score: number;
    name_entrance_test_type: string;
    name_subject: string;
    priority: number;
    test_name: string;
    uid: string;

    isOpen: boolean;

    entrance_test_calendar: Array<EntrantsTestCalendar>;
    new_entrance_test_calendar: any;
}

interface EntrantsTestCalendar {
    id: number;
    exam_location: string;
    entrance_test_date: string;
    count_c: number;
    uid?: string;
}

interface CompetitiveRating {
    id: number;
    admission_volume: number;
    agreed_rating: number;
    app_number: string;
    change_rating: number;
    changed: string;
    common_rating: number;
    count_agreed: number;
    count_application: number;
    count_first_step: number;
    count_second_step: number;
    first_rating: number;
    id_application: number;
    id_competitive_group: number;
    id_organization: number;
    name: string;
    patronymic: string;
    surname: string;
    uid_epgu: number;
}
