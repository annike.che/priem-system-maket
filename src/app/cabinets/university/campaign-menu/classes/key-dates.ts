interface KeyDatesItem {
  id_end_application?: number;
  id_education_level: number;
  name_education_level: string;
  id_education_form: number;
  name_education_form: string;
  end_date?: string;
  order_end_app?: string;
  uid: string;
}
