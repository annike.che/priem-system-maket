interface ICampaignItem {
    id: number;
    name: string;
    id_campaign_type: number;
    name_campaign_type: string;
    id_education_level: number;
    education_level_name: string;
    year_start: number;
    year_end: number;
    name_campaign_status: string;
    code_campaign_status: string;
}

interface ICampaignMainInfo {
    id: number;
    uid: string;
    name: string;
    id_campaign_type: number;
    name_campaign_type: string;

    id_campaign_status: number;
    name_campaign_status: string;
    education_forms: Array<number>;
    education_forms_names: Array<string>;
    education_levels: Array<number>;
    education_levels_names: Array<string>;
    year_start: number | string;
    year_end: number | string;
    created: string;
    code_campaign_status: string;
}

interface ICampaignAdding {
  name: string;
  year_start: number;
  year_end: number;
  education_forms: Array<number>;
  education_levels: Array<number>;
  id_campaign_type: number;
  uid?: string;
}
