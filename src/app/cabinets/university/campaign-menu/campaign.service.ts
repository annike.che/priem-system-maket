import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CampaignService {

  urlPrefix: string;
  urlGroup = '/campaign';

  constructor(private httpClient: HttpClient) {
    this.urlPrefix = environment.apiUrl;


  }

  getCampaignList(params?): Observable<any> {
    const url = `${this.urlPrefix}${this.urlGroup}/list`;

    console.log('params', params);


    return this.httpClient.get(url, { params });
  }

  createNewCampaign(formData) {
    const url = this.urlPrefix + '/campaign/add';
    return this.httpClient.post(url , formData);
  }
  editCampaign(id, formData) {
    const url = this.urlPrefix + `/campaign/${id}/edit`;
    return this.httpClient.post(url , formData);
  }

  getCampaignMainInfo(id): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${id}/main`;

    return  this.httpClient.get(url);
  }

  removeCampaign(id): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${id}/remove`;

    return  this.httpClient.post(url, {});
  }

  addEducationLevels(id, data): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${id}/education_levels/add`;

    return this.httpClient.post(url, {education_levels: data});
  }

  addEducationForms(id, data): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${id}/education_forms/add`;

    return this.httpClient.post(url, {education_forms: data});
  }

  getCampaignAchievementsList(id, params?): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${id}/achievements`;

    return  this.httpClient.get(url, {params});
  }

  getCampaignAdmission(id, params?): Observable<any> {
    //const url = this.urlPrefix + this.urlGroup + `/${id}/admission`;
    const url = this.urlPrefix + this.urlGroup + `/${id}/admission2`;

    return  this.httpClient.get(url, {params});
  }

  getCampaignCompetitiveList(id, params?): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${id}/competitive`;

    return this.httpClient.get(url, {params});
  }

  getCompetitiveListInExcel(): Observable<any> {
    console.log('get competitive');
    const url = this.urlPrefix + `/generate/competitive_groups`;
    return this.httpClient.get(url, {responseType: 'blob' as 'json'});
  }

  getEducationLevelsByCampaignId( id ): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${id}/education_levels`;

    return this.httpClient.get(url).pipe(
        map((resp: any) => {
          return (resp.done) ? resp.data : [];
        } )
    );
  }
  getEducationFormsByCampaignId( id ): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${id}/education_forms`;

    return this.httpClient.get(url).pipe(
        map((resp: any) => {
          return (resp.done) ? resp.data : [];
        } )
    );
  }

  getEndDates(id): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${id}/enddate`;
    return this.httpClient.get(url);
  }

  saveEndDate(idCampaign, data): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${idCampaign}/enddate/edit`;
    return this.httpClient.post(url, data);
  }

  removeEndDate(idCampaign, id): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${idCampaign}/enddate/${id}/remove`;

    return this.httpClient.post(url, {});
  }

  changeCampaignStatus(idCampaign, code: string): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${idCampaign}/status/set`;
    return this.httpClient.post(url, { code });
  }

  addAdmissionVolume(params, ): Observable<any> {
    const url = this.urlPrefix + `/admission/add`;

    return this.httpClient.post(url, params);
  }

  editAdmissionVolume(id, data): Observable<any> {
    const url = this.urlPrefix + `/admission/${id}/edit`;

    return this.httpClient.post(url, data);
  }

  getBudgetLevelsSelectList( idAdmission ): Observable<any> {
    const url = this.urlPrefix + `/admission/${idAdmission}/budget`;

    return this.httpClient.get(url);
  }

  getAdmissionInfo( idAdmission ): Observable<any> {
    const url = this.urlPrefix + `/admission/${idAdmission}/main`;

    return this.httpClient.get(url);
  }

  addBudgetLevel( idAdmission, data ): Observable<any> {
    const url = this.urlPrefix + `/admission/${idAdmission}/budget/add`;

    return this.httpClient.post(url, data);
  }

  deleteAdmission(idAdmission): Observable<any> {
    const url = this.urlPrefix + `/admission/${idAdmission}/remove`;

    return this.httpClient.get(url);
  }

  editBudgetLevel( idAdmission, idBudget, data ): Observable<any> {
    const url = this.urlPrefix + `/admission/${idAdmission}/budget/${idBudget}/edit`;

    return this.httpClient.post(url, data);
  }

  deleteAdmissionBudgetLevels(idAdmission, idBudget): Observable<any> {
    const url = this.urlPrefix + `/admission/${idAdmission}/budget/${idBudget}/remove`;

    return this.httpClient.get(url);
  }

  addCompetitive(data): Observable<any> {
    const url = this.urlPrefix + `/competitive/add`;

    return this.httpClient.post(url, data);
  }

  getAchievementsByCompetitiveIdSelectList(id): Observable<any> {
    const url = this.urlPrefix + `/competitive/${id}/achievements/select`;

    return this.httpClient.get(url)
        .pipe(
            map((resp: any) => {
              return (resp.done) ? resp.data : [];
            } )
        );

  }

  getTestListByCompetitiveIdSelectList(id): Observable<any> {
    const url = this.urlPrefix + `/competitive/${id}/tests/select`;
    return this.httpClient.get(url)
        .pipe(
            map((resp: any) => {
              return (resp.done) ? resp.data : [];
            } )
        );
  }

  getSubjectByCompanyId(id): Observable<any> {
    const url = this.urlPrefix +  this.urlGroup  + `/${id}/subjects`;

    return this.httpClient.get(url)
        .pipe(
            map((resp: any) => {
              return (resp.done) ? resp.data : [];
            })
        );
  }

  deleteCompetitive(id): Observable<any> {
    const url = this.urlPrefix +   `/competitive/${id}/remove`;

    return this.httpClient.get(url);
  }

  getCompetitiveInfoMain(id): Observable<any> {
    const url = this.urlPrefix +   `/competitive/${id}/main`;

    return this.httpClient.get(url);
  }
  getCompetitiveInfoPrograms(id): Observable<any> {
    const url = this.urlPrefix +   `/competitive/${id}/programs`;

    return this.httpClient.get(url);
  }

  getCompetitiveInfoTests(id): Observable<any> {
    const url = this.urlPrefix +   `/competitive/${id}/tests`;

    return this.httpClient.get(url);
  }

  editCompetitiveInfoMain(id, data): Observable<any> {
    const url = this.urlPrefix +   `/competitive/${id}/edit`;

    return this.httpClient.post(url, data);
  }

  editCompetitiveNumber( id, num: number ): Observable<any> {
    const url = this.urlPrefix +   `/competitive/${id}/number/edit`;

    return this.httpClient.post(url, {number: num});
  }

  editCompetitiveComment( id, comment: string ): Observable<any> {
    const url = this.urlPrefix +   `/competitive/${id}/comment/edit`;

    return this.httpClient.post(url, {comment});
  }

  editCompetitiveName( id, name: string ): Observable<any> {
    const url = this.urlPrefix +   `/competitive/${id}/name/edit`;

    return this.httpClient.post(url, {name});
  }

  deleteEducationProgram(idCompetitive: number, idProgram: number ): Observable<any> {
    const url = this.urlPrefix +   `/competitive/${idCompetitive}/program/${idProgram}/remove`;

    return this.httpClient.get(url);

  }

  deleteEntranceTests(idCompetitive: number, idTest: number) {
    const url = this.urlPrefix +   `/competitive/${idCompetitive}/entrance/${idTest}/remove`;

    return this.httpClient.get(url);
  }

  syncEntranceTests(idCompetitive: number) {
    const url = this.urlPrefix +   `/competitive/${idCompetitive}/sync-test-calendar`;

    return this.httpClient.post(url, {});
  }

  addEntranceTestDate( idEntrantTest: number, data: any ): Observable<any> {
    const url = this.urlPrefix +   `/competitive/entrance/${idEntrantTest}/date/add`;

    return this.httpClient.post(url, {entrance_test_calendar: data});
  }

  deleteEntranceTestDate(id: number): Observable<any> {
    const url = this.urlPrefix + `/competitive/entrance/date/${id}/remove`;
    return this.httpClient.post(url, {});
  }

  loadEntranceTestCalendar( idTest: number ): Observable<any> {
    const url = this.urlPrefix + `/competitive/entrance/${idTest}/date`;

    return this.httpClient.get(url);
  }

  addEducationProgram(idCompetitive: number, data: any) {
    const url = this.urlPrefix +  `/competitive/${idCompetitive}/program/add`;

    return this.httpClient.post(url, data);
  }

  addEntranceTests(idCompetitive: number, data: any) {
    const url = this.urlPrefix +  `/competitive/${idCompetitive}/entrance/add`;

    return this.httpClient.post(url, data);
  }

  editUid( key, id, uid ): Observable<any> {
    let keys = [
      'achievements',
      'admission_volume',
      'competitive',
      'campaign',
      'distributed_admission_volume',
      'entrance_test',
      'entrance_test_calendar',
      'enddate',
      'programs'
    ];

    if(keys.indexOf(key) < 0) {
      return  new Observable(subscriber => {
        subscriber.next({ done: false, message: 'Данного ключа не существует' });
      });
    }
    const url = this.urlPrefix + `/${key}/${id}/uid/edit`;

    return this.httpClient.post(url, {uid});
  }


  /*
  * checked
  * */

  checkCampaignEditRemove(id): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${id}/check/edit-remove`;

    return this.httpClient.get(url);
  }

  checkKeyDatesEditRemove(idCampaign): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${idCampaign}/enddate/check/edit-remove`;

    return this.httpClient.get(url);

  }

  checkAdmissionAdd(idCampaign): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${idCampaign}/admission/check/add`;

    return this.httpClient.get(url);
  }

  checkAdmissionEditRemove(idCampaign): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${idCampaign}/admission/check/edit-remove`;
    return this.httpClient.get(url);
  }

  checkCompetitiveAddRemove(idCampaign): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${idCampaign}/competitive/check/add-remove`;
    return this.httpClient.get(url);
  }

  checkCompetitiveEdit(id): Observable<any> {
    const url = this.urlPrefix + `/competitive/${id}/check/edit`;
    return this.httpClient.get(url);
  }

  checkCompetitiveProgramsAddRemove(id): Observable<any> {
    const url = this.urlPrefix + `/competitive/${id}/programs/check/add-remove`;
    return this.httpClient.get(url);
  }

  checkCompetitiveTestsAddRemove(id): Observable<any> {
    const url = this.urlPrefix + `/competitive/${id}/tests/check/add-remove`;
    return this.httpClient.get(url);
  }

  checkCompetitiveTestCalendarAddRemove(idCompetitive): Observable<any> {
    const url = this.urlPrefix + `/competitive/${idCompetitive}/dates/check/add-remove`;
    return this.httpClient.get(url);
  }

  checkAchievementsAdd(idCampaign): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${idCampaign}/achievements/check/add`;
    return this.httpClient.get(url);
  }

  checkAchievementsEditRemove(idCampaign): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${idCampaign}/achievements/check/edit-remove`;
    return this.httpClient.get(url);
  }

}


