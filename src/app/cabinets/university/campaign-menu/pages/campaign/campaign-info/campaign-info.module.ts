import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AchievementListComponent } from './achievement-list/achievement-list.component';
import { AdmissionVolumeListComponent } from './admission-volume-list/admission-volume-list.component';
import { CompetitiveListComponent } from './competitive-list/competitive-list.component';
import { CampaignInfoComponent } from './campaign-info.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '@/common/modules/shared.module';
import {CampaignDetailComponent} from './campaign-detail/campaign-detail.component';
import {FormsModule} from '@angular/forms';
import {ArtLibModule} from '@art-lib/art-lib.module';
import {NgxMaskModule} from 'ngx-mask';
// tslint:disable-next-line:max-line-length
import {DialogAddBudgetLevelsComponent} from '@/cabinets/university/campaign-menu/components/add-budget-levels/dialog-add-budget-levels.component';
import {AdmissionTableListSettingsComponent} from '@/cabinets/university/campaign-menu/components/admission-table-list-settings/admission-table-list-settings.component';
import { KeyDatesListComponent } from './key-dates-list/key-dates-list.component';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {ChangeCampaignStatusDialogComponent} from '@/cabinets/university/campaign-menu/components/change-campaign-status-dialog/change-campaign-status-dialog.component';
import {AddEducationLevelDialogComponent} from '@/cabinets/university/campaign-menu/components/_edit-dialog/add-education-level-dialog/add-education-level-dialog.component';
import {AddEducationFormDialogComponent} from '@/cabinets/university/campaign-menu/components/_edit-dialog/add-education-form-dialog/add-education-form-dialog.component';
import {EditUidModule} from '@/cabinets/university/campaign-menu/components/_edit-dialog/edit-uid/edit-uid.module';

const MY_DATE_FORMATS = {
    parse: {
        dateInput: ['DD.MM.YYYY', 'LL', ],
    },
    display: {
        dateInput: 'DD.MM.YYYY',
        monthYearLabel: 'YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY'
    },
};

const routes: Routes = [
    { path: '', component: CampaignInfoComponent,
        children: [
            { path: '', redirectTo: 'main', pathMatch: 'full' },
            { path: 'main', component: CampaignDetailComponent},
            { path: 'achievement', component: AchievementListComponent},
            { path: 'admission', component: AdmissionVolumeListComponent},
            { path: 'competitive', component: CompetitiveListComponent},
            { path: 'key-dates', component: KeyDatesListComponent }
        ]
    }
];

@NgModule({
  declarations: [
      CampaignDetailComponent,
      AchievementListComponent,
      AdmissionVolumeListComponent,
      CompetitiveListComponent,
      CampaignInfoComponent,
      DialogAddBudgetLevelsComponent,
      AdmissionTableListSettingsComponent,
      KeyDatesListComponent,
      ChangeCampaignStatusDialogComponent,
      AddEducationLevelDialogComponent,
      AddEducationFormDialogComponent
  ],
    imports: [
        RouterModule.forChild(routes),
        SharedModule,
        ArtLibModule,
        NgxMaskModule,
        EditUidModule
    ],
    entryComponents: [
        DialogAddBudgetLevelsComponent,
        AdmissionTableListSettingsComponent,
        ChangeCampaignStatusDialogComponent,
        AddEducationLevelDialogComponent,
        AddEducationFormDialogComponent
    ],
    providers: [
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
        { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS }
    ]
})
export class CampaignInfoModule { }
