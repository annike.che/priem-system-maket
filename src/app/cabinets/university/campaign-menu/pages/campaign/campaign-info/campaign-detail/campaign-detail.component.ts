import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CampaignService} from '@/cabinets/university/campaign-menu/campaign.service';
import {ClsService} from '@/common/services/cls.service';
import {MatDialog} from '@angular/material/dialog';
import {ConfirmModalComponent} from '@art-lib/components/confirm-modal/confirm-modal.component';
import {MatSnackBar} from '@angular/material/snack-bar';
import {AddEducationLevelDialogComponent} from '@/cabinets/university/campaign-menu/components/_edit-dialog/add-education-level-dialog/add-education-level-dialog.component';
import {AddEducationFormDialogComponent} from '@/cabinets/university/campaign-menu/components/_edit-dialog/add-education-form-dialog/add-education-form-dialog.component';
import {EditUidComponent} from '@/cabinets/university/campaign-menu/components/_edit-dialog/edit-uid/edit-uid.component';

@Component({
  selector: 'app-campaign-detail',
  templateUrl: './campaign-detail.component.html',
  styleUrls: ['./campaign-detail.component.scss']
})
export class CampaignDetailComponent implements OnInit {

  id: number;
  data: ICampaignMainInfo;

  enums = {
    campaignTypes: {
        data$: null ,
        tableName: 'campaign_types'
    },
    educationLevels: {
        data$: null,
        tableName: 'education_levels'
    },
    educationForms: {
        data$: null,
        tableName: 'education_forms'
    },
  };

  hasError: boolean;
  isLoad: boolean;

  checkEditRemove: boolean = false;

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private campaignService: CampaignService,
      private clsService: ClsService,
      private dialog: MatDialog,
      private snackBar: MatSnackBar

  ) { }

  ngOnInit() {
    this.route.parent.params.subscribe(params => {
      this.id = params.id;
      //this.getEnums();
      this.getData();
      this.checkedEvent();
    });
  }

  checkedEvent() {
    this.campaignService.checkCampaignEditRemove(this.id)
        .subscribe( res => {
          if( res.done ) {
            this.checkEditRemove = res.data.can;
          }
        });

  }

  getEnums() {
      for ( const key in this.enums) {
          this.enums[key].data$ = this.clsService.getClsData(this.enums[key].tableName);
      }
  }

  getData() {
    this.isLoad = true;
    this.campaignService.getCampaignMainInfo(this.id)
        .subscribe( res => {
            if (res.done) {
              this.data = res.data;
            } else {
              this.hasError = true;
            }
            this.isLoad = false;
        });
  }

  goToEdit() {
    this.router.navigate(['../../', this.id, 'edit'], {relativeTo: this.route});
  }

  removeCampaign() {
    this.dialog.open(ConfirmModalComponent, {
      data: {
        title: 'Удаление Приемной Кампании',
        content: `Вы уверены, что хотите удалить Приемную кампанию: ${this.data.name} ?`
      }
    }).afterClosed().subscribe( res => {
        if( res ) {
          this.campaignService.removeCampaign(this.id)
              .subscribe( res => {
                  if( res.done ) {
                    this.router.navigate(['../../', {relativeTo: this.route}]);
                    this.snackBar.open('Приемная кампания удалена');
                  } else {
                    this.snackBar.open( 'Произошла ошибка' + (res.message ? ': ' + res.message : '' ) );
                  }
              });
        }
    });
  }

  addEducationLevel() {
    this.dialog.open(AddEducationLevelDialogComponent, {
      data: this.data,
      minWidth: '400px'
    }).afterClosed()
        .subscribe( res => {
            if(res) {
              this.getData();
            }
        });
  }

  addEducationForms() {
    this.dialog.open(AddEducationFormDialogComponent, {
      data: this.data,
      minWidth: '400px'
    }).afterClosed()
        .subscribe( res => {
          if(res) {
            this.getData();
          }
        });
  }

  editUid() {
    this.dialog.open(EditUidComponent, {
      data: {
        uid: this.data.uid,
        tableKey: 'campaign',
        id:  this.id
      },
      minWidth: '400px',
    }).afterClosed()
        .subscribe( data => {
          if (data) {
            if (data.hasOwnProperty('uid')) {
              this.data.uid = data.uid;
            }
          }
        });
  }
}
