import {
  AfterViewChecked,
  AfterViewInit,
  ChangeDetectorRef,
  Component, HostListener,
  OnDestroy,
  OnInit,
  ViewChild
} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CampaignService} from '@/cabinets/university/campaign-menu/campaign.service';
import {from, Observable, of, Subject} from 'rxjs';
import * as cloneDeep from 'lodash/cloneDeep';
import {debounceTime, distinctUntilChanged, filter, map, reduce, switchMap, tap} from 'rxjs/operators';
import {untilDestroyed} from 'ngx-take-until-destroy';
import {PageEvent} from '@angular/material/paginator';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog';
import {DialogAddBudgetLevelsComponent} from '@/cabinets/university/campaign-menu/components/add-budget-levels/dialog-add-budget-levels.component';
import {AdmissionTableListSettingsComponent} from '@/cabinets/university/campaign-menu/components/admission-table-list-settings/admission-table-list-settings.component';
import {ConfirmModalComponent} from '@art-lib/components/confirm-modal/confirm-modal.component';
import {MatTable} from '@angular/material/table';
import {EditUidComponent} from '@/cabinets/university/campaign-menu/components/_edit-dialog/edit-uid/edit-uid.component';

@Component({
  selector: 'app-admission-volume-list',
  templateUrl: './admission-volume-list.component.html',
  styleUrls: ['./admission-volume-list.component.scss']
})
export class AdmissionVolumeListComponent implements OnInit, OnDestroy, AfterViewInit  {

  campingId: number;

  checkAdd: boolean;
  checkEditRemove: boolean;


  admission: Array<Admission | BudgetLevels>;

  dataSource: Array<Admission | BudgetLevels> = [];

  specialty: Array<Admission>;
  groups: Array<Admission>;

  pagination = new Subject<PageEvent>();
  searchText = new Subject<string>();

  defaultParams = {
    page: 1,
    limit: 15,
    search_code_name: ''
  };

  params: Params = {
    ...this.defaultParams
  };

  total: number;


  editingId: number;
  editingData: BudgetLevels | Admission;

  editingBudgetLevels: Array<Admission | BudgetLevels>;

  isEditBudgetLevels = false;

  isLoad = true;

  finishFirstInit = false;


  sumKeys = ['budget_o', 'budget_oz', 'budget_z', 'quota_o', 'quota_oz', 'quota_z', 'target_o', 'target_oz', 'target_z'];

  settingsDefault = {
    stiky: {
      header: false,
      education_level: false,
      speciality: false,
      code_specialty: false,
      sum: false,
      actions: false
    },
    display: {
      education_level: {
        display: true,
        subheaders: []
      },
      speciality: {
        display: true,
        subheaders: []
      },
      code_specialty: {
        display: true,
        subheaders: []
      },
      budget: {
        display: true,
        subheaders: [
          {name: 'budget_o', display: true},
          {name: 'budget_oz', display: true},
          {name: 'budget_z', display: true},
        ]
      },

      quota: {
        display: true,
        subheaders: [
          {name: 'quota_o', display: true},
          {name: 'quota_oz', display: true},
          {name: 'quota_z', display: true},
        ]
      },
      target: {
        display: true,
        subheaders: [
          {name: 'target_o', display: true},
          {name: 'target_oz', display: true},
          {name: 'target_z', display: true},
        ]
      },
      sum: {
        display: true,
        subheaders: [
          {name: 'sum_available', display: true},
          {name: 'sum_distributed', display: true}
        ]
      },
      paid: {
        display: true,
        subheaders: [
          {name: 'paid_o', display: true},
          {name: 'paid_oz', display: true},
          {name: 'paid_z', display: true}
        ]
      },
      uid: {
        display: true,
        subheaders: []
      }

    }

  };

  settings: any = {};

  spans = [];


  settingFromStorage: any;
  displayedHeaderColumns = [];
  displayedHeaderTwoColumns = [];
  displayedColumns = [];

  @ViewChild(MatTable, {static: false}) table: MatTable<any>;


  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private campaignService: CampaignService,
    private snackBar: MatSnackBar,
    private changeDetectorRefs: ChangeDetectorRef,
    public dialog: MatDialog
  ) {

  }

  ngOnInit() {

    this.route.parent.params.subscribe(params => {
      this.campingId = params.id;
      this.getQueryParamsFromUrl();

      if ( this.params.budgetLevels ) {
        this.editBudgetLevels();
      } else {
        this.getData();
      }
      this.subscribeOnChanges();

      this.initTableSettings();
      this.getDisplayedHeaderColumns();

      this.checkAddEvent();
      this.checkEditRemoveEvent();
    });
  }


  getDisplayedHeaderColumns() {
    let startDisplayedHeaderColumns = [];
    let startDisplayedHeaderTwoColumns = [];
    let startDisplayedColumns = [];



    this.settingFromStorage = this.settings.display;

    //  получаем имена ключей LocalStorage
    const displayedColumns = this.settingFromStorage;

    let arSubheader = [];
    let arHeaderWithoutSubheaders = [];

    for ( let key in  displayedColumns) {
        if ( displayedColumns[key].display ) {
          startDisplayedHeaderColumns.push(key);

          if (displayedColumns[key].subheaders.length > 0) {
              let subKeys =  displayedColumns[key].subheaders.map( el => { if (el.display) return el.name; } );

              startDisplayedHeaderTwoColumns.push(...subKeys);
              startDisplayedColumns.push(...subKeys);
          } else {
            startDisplayedColumns.push(key);
          }
        }
    }


    startDisplayedHeaderColumns.push('action-two');
    startDisplayedHeaderTwoColumns.push('action');
    startDisplayedColumns.push('action');

    this.displayedHeaderColumns = [... startDisplayedHeaderColumns];
    this.displayedHeaderTwoColumns = [ ... startDisplayedHeaderTwoColumns];
    this.displayedColumns = [ ...startDisplayedColumns];

    this.changeDetectorRefs.detectChanges();

    console.log(this.displayedHeaderColumns, this.displayedHeaderTwoColumns);

  }


  getQueryParamsFromUrl() {
    const queryParams = cloneDeep(this.route.snapshot.queryParams);
    this.setParams(queryParams);
  }

  setParams(params) {
    Object.keys(params).forEach(key => {
      switch (key) {
        case 'page':
          this.params[key] = +params[key];
          break;
        default:
          this.params[key] = params[key];
      }
    });
  }

  subscribeOnChanges() {
    this.pagination.pipe(
      debounceTime(200),
      tap((event: PageEvent) => {
        if (this.params.limit !== event.pageSize) {
          this.params.limit = event.pageSize;
          this.params.page = 1;
        }
        if (this.params.page !== event.pageIndex + 1) {
          this.params.page = event.pageIndex + 1;
        }
      }),
      switchMap(() => this.getDataFromService()),
      untilDestroyed(this)
    )
      .subscribe(response => this.setData(response));


    this.searchText.pipe(
        distinctUntilChanged(),
        debounceTime(600),
        tap(() => {
          this.params.page = 1;
        }),
        switchMap(() => this.getDataFromService()),
        untilDestroyed(this)
    ).subscribe(response => this.setData(response));
  }

  getDataFromService(): Observable<any> {
    this.isLoad = true;
    const queryParams = this.getQueryParams(this.params);
    this.passQueryParamsToUrl(queryParams);
    return this.campaignService.getCampaignAdmission(this.campingId, this.params);
  }

  setData(res) {
    if (res.done
      && res.data.hasOwnProperty('items')
      && res.data.hasOwnProperty('groups')) {

      this.specialty = res.data.items;
      this.groups = res.data.groups;

      this.params.page = res.paginator.page;
      this.params.limit = res.paginator.limit;
      this.total = res.paginator.total;

      this.mergeSpeciallity();

      this.initCacheSpan();
      this.dataSource = [...this.admission];



    } else {
      this.dataSource = [];

    }

    this.updateTableView();
    this.isLoad = false;
  }

  passQueryParamsToUrl(queryParams) {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams
    });
  }

  getQueryParams(params) {
    const queryParams: any = {};
    Object.keys(params).forEach((key) => {
      if (this.defaultParams.hasOwnProperty(key) && (this.defaultParams[key] !== params[key] && params[key] !== '') ||
        (!this.defaultParams.hasOwnProperty(key) && params[key] !== '')) {
        if ((key === 'frDt' || key === 'toDt') && params.dateF !== 'FromTo') {
          return;
        }
        queryParams[key] = params[key];
      }
    });

    //console.log('queryparams', queryParams);
    return queryParams;
  }


  getData() {
    this.getDataFromService()
      .subscribe(response => this.setData(response));
  }

  mergeSpeciallity() {
    let data = this.specialty;

    // console.log(this.groups[0]);

    this.groups.sort((a: Admission, b: Admission) => {
      return a.id_items[0] - b.id_items[0];
    });

    //console.log(this.groups[0]);

    for (let i = 0; i < this.groups.length; i++) {
      const group = this.groups[i];
      //console.log('group', group);
      data.splice(group.id_items[0] + i, 0, group);
    }
    this.admission = data;
  }

  initCacheSpan(data = this.admission) {
    this.spans = [];
    this.cacheSpan('education_level', d => d.name_education_level, data);
    //console.log('spans', this.spans);
    /*
      this.cacheSpan('speciality', d => d.name);
      this.cacheSpan('Weight', d => d.weight);
    */
  }

  cacheSpan(key, accessor, data) {
    for (let i = 0; i < data.length;) {
      const currentValue = accessor(data[i]);
      let count = 1;

      // Iterate through the remaining rows to see how many match
      // the current value as retrieved through the accessor.
      for (let j = i + 1; j < data.length; j++) {
        if (currentValue !== accessor(data[j])) {
          break;
        }

        count++;
      }

      if (!this.spans[i]) {
        this.spans[i] = {};
      }

      // Store the number of similar values that were found (the span)
      // and skip i to the next unique row.
      this.spans[i][key] = count;
      i += count;
    }
  }

  getRowSpan(col, index) {
    return this.spans[index] && this.spans[index][col];
  }

  add() {
    this.router.navigate([`./../admission-volume/new`], {
      relativeTo: this.route.parent,
      queryParams: {id_campaign: this.campingId}
    });
  }

  idEditing(id: any) {
    if (!id) return false;
    return id === this.editingId;
  }

  edit(id: number, index: number) {
    this.editingData = Object.assign({}, this.dataSource[index]);
    this.editingId = id;

  }

  save(index) {
    console.log('saveData', this.editingId, this.editingData);

    this.campaignService.editAdmissionVolume(this.editingId, this.editingData)
      .subscribe(res => {
        if (res.done) {
          const parentData = res.data.group;

          if( !this.isEditBudgetLevels ) {
            this.updateGroupInfo(parentData, index);
            console.log('index', index);
            //this.admission[index] = {};//this.editingData;
            this.admission[index] =  this.editingData;
            console.log( 'this.admission', this.admission[index] );
            this.dataSource = [ ... this.admission];

          } else {

            this.editingBudgetLevels[index] = this.editingData;
            this.dataSource = [ ...this.editingBudgetLevels];
          }

          this.editingId = this.editingData = undefined;
          this.changeDetectorRefs.detectChanges();
          this.snackBar.open('Изменение КЦП прошло успешно');
        } else {
          this.snackBar.open('Во время изменения КЦП произошла ошибка' + ((res.message) ? `: ${res.message}` : ''));
        }

      });
  }

  updateGroupInfo(data, childIndex = this.admission.length) {
    if (!data) return;
    for (let i = childIndex; i >= 0; i--) {
      if (this.admission[i].id_specialty === data.id_specialty) {

        Object.assign(this.admission[i], data);
        return;
      }
    }
  }

  deleteAdmission(item: Admission, index: number) {
    if (!item.id) return;

    this.dialog.open(ConfirmModalComponent, {
      data: {
        title: 'Удаление КЦП',
        content: 'Вы уверены, что хотите удалить КЦП по направлению подготовки ' +  item.code_specialty + ' ' + item.name_specialty +'?'
      }
    })
     .afterClosed().subscribe(res => {
      if (res) {
        this.campaignService.deleteAdmission(item.id)
            .subscribe( res => {
                if ( res.done ) {
                  this.getData();
                  this.snackBar.open( 'КЦП успешно удалено' );
                } else {
                  this.snackBar.open( 'При попытке удалить КЦП произошла ошибка' + ( res.message ? ': '+ res.message : '')   );
                }
            });

      }
    });
  }


  cancelEdit() {
    this.editingData = this.editingId = undefined;
  }

  addBudgetLevels(item: Admission) {
    const data = {
      idAdmission: item.id,
      nameSpeciality: item.code_specialty + ' ' + item.name_specialty
    };
    this.dialog.open(DialogAddBudgetLevelsComponent, {
      minWidth: '450px',
      data
    }).afterClosed().subscribe(res => {
        if (res) {
          if (res.done) {
            item.has_distributed = true;
          }
        }
    });
  }

  editBudgetLevels(idAdmission?: number) {

    this.isLoad = true;

    if (idAdmission) {
      this.params.budgetLevels = idAdmission;
      const queryParams = this.getQueryParams(this.params);
      this.passQueryParamsToUrl(queryParams);
    } else {
      idAdmission = this.params.budgetLevels;
    }

    this.campaignService.getAdmissionInfo(idAdmission)
      .subscribe(res => {
        if (res.done) {
          const data = res.data;
          this.editingBudgetLevels = (new Array(data)).concat(data.distributed);

          /*const arr = [ ... this.admission ];
          arr.push(data[0]);
          for ( let i = 0; i < data.length; i++  ) {
            this.admission.splice(index + i + 1, 0, data[i]);
          }
          this.initCacheSpan();*/
          this.initCacheSpan(this.editingBudgetLevels);
          this.dataSource = [...this.editingBudgetLevels];
          this.isEditBudgetLevels = true;



        } else {
          this.snackBar.open('При загрузке Уровней бюджета произошла ошибка');
        }

        this.isLoad = false;

      });

  }


  isBudgetLevel(item: any) {
    return !item.hasOwnProperty('paid_o');
  }


  saveBudget(index: number) {
    console.log('saveData', index, this.editingId, this.editingData);

    this.campaignService.editBudgetLevel(this.editingData.id_admission_volume, this.editingId, this.editingData)
        .subscribe(res => {
          if (res.done) {
            /*const parentData = res.data.group;
            this.updateGroupInfo(parentData, index);

            console.log('index', index);

            Object.assign(this.admission[index], this.editingData);

            //this.admission[index] = {};//this.editingData;*/

            this.editingBudgetLevels[index] = this.editingData;
            this.editingBudgetLevels[0].sum_distributed = res.data.sum_distributed;

            this.editingId = this.editingData = undefined;
            this.dataSource = [ ...this.editingBudgetLevels];

            this.snackBar.open('Изменение уровня бюджета прошло успешно');
          } else {
            this.snackBar.open('Во время изменения уровня бюджета произошла ошибка' + ((res.message) ? `: ${res.message}` : ''));
          }

        });
  }

  tableSetting() {
    this.dialog.open(AdmissionTableListSettingsComponent, {
      minWidth: '500px',
      data: cloneDeep(this.settings)
    }).afterClosed().subscribe(data => {
      if (data) {
        this.settings = data.settings;
        localStorage.setItem('adm-vol-list-table-setting', JSON.stringify(this.settings));
        console.log('settings', this.settings);
        this.getDisplayedHeaderColumns();
        this.changeDetectorRefs.detectChanges();
      }
    });
  }

  private initTableSettings() {
    let storageSetting = {};
    try {
      storageSetting = JSON.parse(localStorage.getItem('adm-vol-list-table-setting'));
    } catch (e) {
      localStorage.removeItem('adm-vol-list-table-setting');
    }

    let check = this.checkStorageSettings(storageSetting);

    if ( !check) {
      localStorage.removeItem('adm-vol-list-table-setting');
      this.settings = this.settingsDefault;
    } else {
      this.settings = storageSetting;
    }

    this.changeDetectorRefs.detectChanges();
  }

  checkStorageSettings( settings ) {

    console.log('settings', settings);
    if ( !settings ) return false;
    if (settings.hasOwnProperty('stiky')) {
      for ( let key in this.settingsDefault.stiky) {
        if ( !settings.stiky.hasOwnProperty(key) )
          return false;
      }
    } else { return false; }

    if ( settings.hasOwnProperty('display') ) {
      for ( let key in this.settingsDefault.display) {
        if ( !settings.display.hasOwnProperty(key) )
          return false;
      }
    } else { return false; }

    return true;
  }


    ngOnDestroy(): void {
    }

  deleteBudgetLevel(item: BudgetLevels) {
    console.log('item', item);

    if (!item.id) return;

    this.dialog.open(ConfirmModalComponent, {
      data: {
        title: 'Удаление Уровня бюджета',
        content: 'Вы уверены, что хотите удалить уровень бюджета "' +
            item.name_level_budget + '" из КЦП по направлению подготовки ' +  item.code_specialty + ' ' + item.name_specialty + '?'
      }
    })
        .afterClosed().subscribe(res => {
      if (res) {
        this.campaignService.deleteAdmissionBudgetLevels(item.id_admission_volume, item.id)
            .subscribe( res => {
              if ( res.done ) {
                this.editBudgetLevels(item.id_admission_volume);
                this.snackBar.open( 'Уровень бюджета успешно удалено' );
              } else {
                this.snackBar.open( 'При попытке удалить уровень бюджета произошла ошибка' + ( res.message ? ': ' + res.message : '')   );
              }
            });

      }
    });
  }

  goToAdmission() {
    this.params.budgetLevels = undefined;
    this.getData();
    this.isEditBudgetLevels = false;
    this.editingBudgetLevels = undefined;
    this.editingId = undefined;
    this.editingData = undefined;
  }

  checkAddEvent() {
    this.campaignService.checkAdmissionAdd(this.campingId)
        .subscribe(res => {
          if (res.done) {
            this.checkAdd = res.data.can;
          }
        });
  }

  checkEditRemoveEvent() {
    this.campaignService.checkAdmissionEditRemove(this.campingId)
        .subscribe( res => {
          if (res.done) {
            this.checkEditRemove = res.data.can;
          }
        });
  }

  updateTableView() {

    setTimeout( () => {
      this.table.renderRows();
      this.table.updateStickyColumnStyles();
      this.table.updateStickyHeaderRowStyles();
      this.table.updateStickyFooterRowStyles();
      this.changeDetectorRefs.detectChanges();
    }, 500);


  }


  editUidBudget(item: BudgetLevels, index: number) {
    this.dialog.open(EditUidComponent, {
      data: {
        uid: item.uid,
        tableKey: 'distributed_admission_volume',
        id:  item.id
      },
      minWidth: '400px'
    }).afterClosed()
        .subscribe( data => {
          if(data) {
            if (data.hasOwnProperty('uid')) {
              item.uid = data.uid;
            }
          }

        });
  }

  editUidAdmissionVolume(item: any, index: any) {
    this.dialog.open(EditUidComponent, {
      data: {
        uid: item.uid,
        tableKey: 'admission_volume',
        id:  item.id
      },
      minWidth: '400px'
    }).afterClosed()
        .subscribe( data => {
          if(data) {
            if (data.hasOwnProperty('uid')) {
              item.uid = data.uid;
            }
          }

        });
  }

  ngAfterViewInit() {
    //console.log('viewchecked', this.table, this.dataSource);
    this.updateTableView();
    this.finishFirstInit = true;
    this.changeDetectorRefs.detectChanges();
  }
}

interface Params {
page: number;
limit: number;
budgetLevels?: number;
// dateF: string;
search_code_name?: string;
}


