import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CampaignService} from '@/cabinets/university/campaign-menu/campaign.service';
import {ConfirmModalComponent} from '@art-lib/components/confirm-modal/confirm-modal.component';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Observable, Subject} from 'rxjs';
import {PageEvent} from '@angular/material/paginator';
import {debounceTime, distinctUntilChanged, switchMap, tap} from 'rxjs/operators';
import {untilDestroyed} from 'ngx-take-until-destroy';
import * as cloneDeep from 'lodash/cloneDeep';
import {SortDirection} from '@angular/material/sort';
import * as fileHandling from '@/common/functions/files';
import * as moment from 'moment';

@Component({
  selector: 'app-competitive-list',
  templateUrl: './competitive-list.component.html',
  styleUrls: ['./competitive-list.component.scss']
})
export class CompetitiveListComponent implements OnInit, OnDestroy {

 campaignId: number;

 competitiveList: Array<CompetitiveItem>;

  displayedColumns = [ 'id', 'name', 'education_level', 'direction', 'education_source', 'budget_level', 'education_form', 'count' ,'uid' ,'actions' ];

    pagination = new Subject<PageEvent>();
    sortEvent = new Subject<SortEvent>();
    searchText = new Subject<string>();

    defaultParams = {
        page: 1,
        limit: 20,
        search_name: ''
    };

    params: Params = {
        ...this.defaultParams
    };

    total: number;
    isLoad = true;

    checkEdit: boolean;
    checkAddRemove: boolean;


  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private campaignService: CampaignService,
      public dialog: MatDialog,
      private snackBar: MatSnackBar
  ) {
      this.competitiveList = [];

  }

  ngOnInit() {
    this.route.parent.params.subscribe(params => {
      this.campaignId = params.id;
      this.getQueryParamsFromUrl();
      this.subscribeOnChanges();
      this.getData();
      this.checkAddRemoveEvent();
    });

  }

    getQueryParamsFromUrl() {
        const queryParams = cloneDeep(this.route.snapshot.queryParams);
        this.setParams(queryParams);
    }

    setParams(params) {
        Object.keys(params).forEach(key => {
            switch (key) {
                case 'page':
                    this.params[key] = +params[key];
                    break;
                default:
                    this.params[key] = params[key];
            }
        });
    }

    subscribeOnChanges() {
        this.pagination.pipe(
            debounceTime(200),
            tap((event: PageEvent) => {
              if (this.params.limit !== event.pageSize) {
                this.params.limit = event.pageSize;
                this.params.page = 1;
              }
              if (this.params.page !== event.pageIndex + 1) {
                this.params.page = event.pageIndex + 1;
              }
            }),
            switchMap(() => this.getDataFromService()),
            untilDestroyed(this)
        )
            .subscribe(response => this.setData(response));

        this.searchText.pipe(
            distinctUntilChanged(),
            debounceTime(600),
            tap(() => {
              this.params.page = 1;
            }),
            switchMap(() => this.getDataFromService()),
            untilDestroyed(this)
        ).subscribe(response => this.setData(response));

      this.sortEvent.pipe(
          debounceTime(200),
          tap((event: SortEvent) => {
            this.params.order = event.direction as SortDirection;
            this.params.sortby = event.active;
          }),
          switchMap(() => this.getDataFromService()),
          untilDestroyed(this)
      )
          .subscribe(response => this.setData(response));
    }

  getDataFromService(): Observable<any>  {
      this.isLoad = true;
      const queryParams = this.getQueryParams(this.params);
      this.passQueryParamsToUrl(queryParams);

      return this.campaignService.getCampaignCompetitiveList(this.campaignId, this.params);
  }

    setData(res) {
        if (res.done) {
            this.competitiveList = res.data;

            this.params.page = res.paginator.page;
            this.params.limit = res.paginator.limit;
            this.total = res.paginator.total;

        }

        this.isLoad = false;
    }

    passQueryParamsToUrl(queryParams) {
        this.router.navigate([], {
            relativeTo: this.route,
            queryParams
        });
    }

    getQueryParams(params) {
        const queryParams: any = {};
        Object.keys(params).forEach((key) => {
            if (this.defaultParams.hasOwnProperty(key) && (this.defaultParams[key] !== params[key] && params[key] !== '') ||
                (!this.defaultParams.hasOwnProperty(key) && params[key] !== '')) {
                if ((key === 'frDt' || key === 'toDt') && params.dateF !== 'FromTo') {
                    return;
                }
                queryParams[key] = params[key];
            }
        });

        //console.log('queryparams', queryParams);
        return queryParams;
    }

    getData() {
        this.getDataFromService()
            .subscribe(response => this.setData(response));
    }

    delete(id: any, index: number) {
        this.dialog.open(ConfirmModalComponent, {data: {
                title: 'Удаление Конкурсной группы',
                content: 'Вы уверены, что хотите удалить конкурсную группу?'
            }})
            .afterClosed().subscribe( res => {
            if (res) {
                this.campaignService.deleteCompetitive(id)
                    .subscribe( resp => {
                        if ( resp.done ) {
                            this.snackBar.open('Конкурсная группа успешно удалена');
                            this.competitiveList.splice(index, 1);
                        } else {
                            this.snackBar.open('При попытке удалить конкурсную группу произошла ошибка' + ((res.message) ? `: ${res.message}` : '') )
                        }
                    });
            }
        });
    }

    checkAddRemoveEvent() {
      this.campaignService.checkCompetitiveAddRemove(this.campaignId)
          .subscribe( res => {
            if ( res.done ) {
              this.checkAddRemove = res.data.can;
            }
          });
    }

    ngOnDestroy(): void {}

  getCompetitiveToExcel() {
    this.campaignService.getCompetitiveListInExcel()
        .subscribe( res => {
          fileHandling.handlingResponse(res, 'Список_конкурсных_групп_' + moment().format( 'DD-MM-YYYY_HH:mm:ss') + '.xlsx');
        }, err => {
          this.snackBar.open( fileHandling.handlingError(err));
        });
  }
}

interface Params {
    page: number;
    limit: number;
    search_name?: string;
    order?: SortDirection; // 'ASC' || 'DESC'
    sortby?: string;
}


