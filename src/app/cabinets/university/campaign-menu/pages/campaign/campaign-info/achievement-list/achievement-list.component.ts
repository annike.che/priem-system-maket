import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CampaignService} from '@/cabinets/university/campaign-menu/campaign.service';
import * as cloneDeep from 'lodash/cloneDeep';
import {debounceTime, distinctUntilChanged, switchMap, tap} from 'rxjs/operators';
import {untilDestroyed} from 'ngx-take-until-destroy';
import {Observable, Subject} from 'rxjs';
import {PageEvent} from '@angular/material/paginator';
import {SortDirection} from '@angular/material/sort';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog';
import {ConfirmModalComponent} from '@art-lib/components/confirm-modal/confirm-modal.component';
import {AchievementService} from '@/cabinets/university/campaign-menu/pages/achievement/achievement.service';

@Component({
  selector: 'app-achievement-list',
  templateUrl: './achievement-list.component.html',
  styleUrls: ['./achievement-list.component.scss']
})

// custom snackbar

export class AchievementListComponent implements OnInit, OnDestroy {
  campaignId: number;


  achievements: Array<Achievement>;
  columnsToDisplay = ['id', 'name', 'category_name', 'max_value', 'uid', 'action'];

  checkAdd: boolean;
  checkEditRemove: boolean;


  isLoad = true;

  defaultParams = {
    page: 1,
    limit: 5
  };


  params: Params = {
    ...this.defaultParams
  };
  total: number;
  pageToShow = 3;

  /* search */
  searchText = new Subject<string>();

  pageEvent = new Subject<PageEvent>();
  sortEvent = new Subject<SortEvent>();
  message: any = {
    delete: 'Успешно удалено'
  };

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private campaignService: CampaignService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private achievementService: AchievementService,
  ) {

  }

  ngOnInit() {
    this.route.parent.params.subscribe(params => {
      console.log('params', params);
      this.campaignId = params.id;
      this.getQueryParamsFromUrl();
      this.subscribeOnChanges();
      this.getData();
      this.checkAddEvent();
      this.checkEditRemoveEvent();
    });
  }

  getQueryParamsFromUrl() {
    let queryParams: any;
    queryParams = cloneDeep(this.route.snapshot.queryParams);
    this.setParams(queryParams);
  }

  setParams(params) {
    Object.keys(params).forEach(key => {
      switch (key) {
        case 'page':
          this.params[key] = +params[key];
          break;
        default:
          this.params[key] = params[key];
      }
    });
  }

  subscribeOnChanges() {
    this.pageEvent.pipe(
      debounceTime(200),
      tap((event: PageEvent) => {
        if (this.params.limit !== event.pageSize) {
          this.params.limit = event.pageSize;
          this.params.page = 1;
        }
        if (this.params.page !== event.pageIndex + 1) {
          this.params.page = event.pageIndex + 1;
        }
      }),
      switchMap(() => this.getDataFromService()),
      untilDestroyed(this)
    )
      .subscribe(response => this.setData(response));

    this.sortEvent.pipe(
      debounceTime(200),
      tap((event: SortEvent) => {
        this.params.order = event.direction as SortDirection;
        this.params.sortby = event.active;
      }),
      switchMap(() => this.getDataFromService()),
      untilDestroyed(this)
    )
      .subscribe(response => this.setData(response));

    this.searchText.pipe(
      distinctUntilChanged(),
      debounceTime(500),
      tap(() => {
        this.params.page = 1;
      }),
      switchMap(() => this.getDataFromService()),
      untilDestroyed(this)
    ).subscribe(response => this.setData(response));
  }

  getData() {
    this.getDataFromService()
      .subscribe(response => this.setData(response));
  }

  setData(response) {
    console.log('response', response);
    if (response.done) {

      this.achievements = response.data;
      this.params.page = response.paginator.page;
      this.params.limit = response.paginator.limit;
      this.total = response.paginator.total;

    }

    this.isLoad = false;
  }


  getDataFromService(): Observable<any> {
    this.isLoad = true;
    const queryParams = this.getQueryParams(this.params);
    this.passQueryParamsToUrl(queryParams);
    return this.campaignService.getCampaignAchievementsList(this.campaignId, this.params);
  }

  passQueryParamsToUrl(queryParams) {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams
    });
  }

  getQueryParams(params) {
    const queryParams: any = {};
    Object.keys(params).forEach((key) => {
      if (this.defaultParams.hasOwnProperty(key) && (this.defaultParams[key] !== params[key] && params[key] !== '') ||
        (!this.defaultParams.hasOwnProperty(key) && params[key] !== '')) {
        if ((key === 'frDt' || key === 'toDt') && params.dateF !== 'FromTo') {
          return;
        }
        queryParams[key] = params[key];
      }
    });

    //console.log('queryparams', queryParams);
    return queryParams;
  }

  goToEdit(item: Achievement) {
    this.router.navigate([`./../achievement/${item.id}/edit`], {relativeTo: this.route.parent});
  }

  delete(item: Achievement) {
    this.dialog.open(ConfirmModalComponent, {
      data: {
        title: 'Удаление Индивидуального Достижения',
        content: 'Вы уверены, что хотите удалить индивидуальное достижение?'
      }
    })
      .afterClosed().subscribe(res => {
      if (res) {
        this.achievementService.removeAchievement(item.id)
          .subscribe(resp => {
            this.openSnackBar(this.message.delete);
            this.getData();
          })
      }
    });

  }



  add() {
    this.router.navigate([`./../achievement/new`], {
      relativeTo: this.route.parent,
      queryParams: {id_campaign: this.campaignId}
    });
  }

  openSnackBar(message?: string) {
    this.snackBar.open(message, '', {
      duration: 2000
    });
  }

  checkAddEvent() {
    this.campaignService.checkAchievementsAdd(this.campaignId)
        .subscribe( res => {
          if( res.done ) {
            this.checkAdd = res.data.can;
          }
        });
  }
  checkEditRemoveEvent() {
    this.campaignService.checkAchievementsEditRemove(this.campaignId)
        .subscribe( res => {
          if( res.done ) {
            this.checkEditRemove = res.data.can;
          }
        });
  }




  ngOnDestroy(): void {
  }

}

interface Params {
  page: number;
  limit: number;
  search?: string;
  order?: SortDirection; // 'ASC' || 'DESC'
  sortby?: string;
}

