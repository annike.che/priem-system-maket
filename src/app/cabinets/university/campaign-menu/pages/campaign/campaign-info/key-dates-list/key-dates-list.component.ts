import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CampaignService} from '@/cabinets/university/campaign-menu/campaign.service';
import * as cloneDeep from 'lodash/cloneDeep';
import * as moment from 'moment';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog';
import {ConfirmModalComponent} from '@art-lib/components/confirm-modal/confirm-modal.component';
import {EditUidComponent} from '@/cabinets/university/campaign-menu/components/_edit-dialog/edit-uid/edit-uid.component';

@Component({
  selector: 'app-key-dates-list',
  templateUrl: './key-dates-list.component.html',
  styleUrls: ['./key-dates-list.component.scss']
})
export class KeyDatesListComponent implements OnInit {

  isLoad: boolean;

  campaignId: number;

  checkEditRemove = false;

  data: Array<KeyDatesItem>;

  editData: KeyDatesItem;
  editIndex: number;
  columnsHeader = ['id', 'educationLevel', 'educationForm', 'endDate', 'uid' ,'action'];



  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private campaignService: CampaignService,
      private snackBar: MatSnackBar,
      private dialog: MatDialog
  ) { }

  ngOnInit() {
    this.route.parent.params.subscribe(params => {
      this.campaignId = params.id;
      this.getDates();
      this.checkEditRemoveEvent();
    });
  }

  getDates() {
    this.isLoad = true;
    this.campaignService.getEndDates(this.campaignId)
        .subscribe( res => {
          if(res.done) {
            this.data = res.data;
          } else {
            this.data = [];
          }

          this.isLoad = false;
        } );
  }

  edit(item: any, index: number) {
    console.log(index);
    this.editIndex = index;
    this.editData = cloneDeep(item);
  }

  save(item: any, index: any) {
    this.isLoad = true;
    const data = {
      id_education_level: this.editData.id_education_level,
      id_education_form: this.editData.id_education_form,
      end_date:  moment(this.editData.end_date).format(),
      uid: this.editData.uid
    };

    this.campaignService.saveEndDate( this.campaignId, data )
        .subscribe( res => {
          if(res.done) {
            this.editData.id_end_application = res.data.id_end_application;
            this.data[index] = this.editData;
            this.data = [... this.data];
            console.log('item', item);
            this.editData = this.editIndex = undefined;
            this.snackBar.open('Данные успешно обновлены');
          } else {
            this.snackBar.open('При попытке обновить данные произошла ошибка' + (res.message ? ': ' + res.message : ''));
          }
          this.isLoad = false;
        } );
  }

  cancel(item: any, index: any) {
    //item = this.editData;
    this.editData = this.editIndex = undefined;
  }

  checkEditRemoveEvent() {
    this.campaignService.checkKeyDatesEditRemove(this.campaignId)
        .subscribe( res => {
            if( res.done ) {
              this.checkEditRemove = res.data.can;
            }
        } );
  }

  remove(item: any, index: any) {
    this.dialog.open(ConfirmModalComponent, {
      data: {
        title: 'Удаление Контрольной даты',
        content: 'Вы уверены, что хотите удалить контрольную дату?'
      }
    })
     .afterClosed().subscribe(res => {
      if (res) {
        this.isLoad = true;
        this.campaignService.removeEndDate(this.campaignId, item.id_end_application)
            .subscribe(res => {
              if (res.done) {
                item.end_date = null;
                this.data = [...this.data];
                this.snackBar.open('Дата успешно удалена');
              } else {
                this.snackBar.open('Произошла ошибка' + (res.message ? ': ' + res.message : ''));
              }

              this.isLoad = false;
            });
      }
    });
  }

  editUid(item: KeyDatesItem) {
    this.dialog.open(EditUidComponent, {
      data:  {
        uid: item.uid,
        tableKey: 'enddate',
        id: item.id_end_application
      },
      minWidth: '400px'
    }).afterClosed()
        .subscribe( data => {
          if (data) {
            if (data.hasOwnProperty('uid')) {
              item.uid = data.uid;
            }
          }
        });
  }
}
