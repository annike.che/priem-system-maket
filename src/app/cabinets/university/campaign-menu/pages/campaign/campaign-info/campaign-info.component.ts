import {AfterViewChecked, Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CampaignService} from '@/cabinets/university/campaign-menu/campaign.service';
import {Title} from '@angular/platform-browser';
import {MatTabNav} from '@angular/material/tabs';
import {MatDialog} from '@angular/material/dialog';
import {ChangeCampaignStatusDialogComponent} from '@/cabinets/university/campaign-menu/components/change-campaign-status-dialog/change-campaign-status-dialog.component';
import {ConfirmModalComponent} from '@art-lib/components/confirm-modal/confirm-modal.component';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-campaign-info',
  templateUrl: './campaign-info.component.html',
  styleUrls: ['./campaign-info.component.scss']
})
export class CampaignInfoComponent implements OnInit, AfterViewChecked {

  navLinks: Array<any>;
  data: ICampaignMainInfo;
  id: number;

  @ViewChild('tabs', {static: false}) tabs: MatTabNav;

  constructor(
      private route: ActivatedRoute,
      private router: Router,
      private campaignService: CampaignService,
      private titleService: Title,
      private dialog: MatDialog,
      private snackBar: MatSnackBar
  ) {
    this.titleService.setTitle('Информация о приёмной кампании');
    this.navLinks = [
      {
        path: './main',
        label: 'Общая информация'
      },
      {
        path: './key-dates',
        label: 'Контрольные даты'
      },
      {
        path: './admission',
        label: 'КЦП'
      },
      {
        path: './achievement',
        label: 'Индивидуальные достижения'
      },
      {
        path: './competitive',
        label: 'Конкурсные группы'
      },
      ];

  }


  ngOnInit() {

    this.route.params.subscribe(params => {
      this.id = params.id;
      this.getCampaignInfo();
    });

  }

  getCampaignInfo() {
    this.campaignService.getCampaignMainInfo(this.id)
        .subscribe(resp => {
          if (resp.done) {
            this.data = resp.data;
          }

          //console.log('tabs', this.tabs);
        });
  }

  ngAfterViewChecked(): void {
    // ререндер нижнего подчеркивания в табах, чтобы не съезжал
    setTimeout(() => this.tabs._alignInkBarToSelectedTab(), 0);
  }

  changeState() {
    this.dialog.open(ChangeCampaignStatusDialogComponent, {
      data: {code: this.data.code_campaign_status}
    }).afterClosed().subscribe(data => {
        if(data) {
          this.campaignService.changeCampaignStatus(this.id, data.status)
              .subscribe( res => {
                 if( res.done) {
                   this.getCampaignInfo();
                   this.snackBar.open( 'Статус успешно изменен' );
                 } else {
                    this.snackBar.open('Произошла ошибка' + ( res.message ? ': ' + res.message : '' ) );
                 }
              });
        }
    });
  }
}
