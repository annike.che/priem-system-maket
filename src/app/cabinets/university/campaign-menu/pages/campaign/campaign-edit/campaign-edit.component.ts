import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BehaviorSubject} from 'rxjs';
import {debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {ClsService} from '@/common/services/cls.service';
import {DatePipe} from '@angular/common';
import {CampaignService} from '@/cabinets/university/campaign-menu/campaign.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ConfirmModalComponent} from '@art-lib/components/confirm-modal/confirm-modal.component';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-campaign-edit',
  templateUrl: './campaign-edit.component.html',
  styleUrls: ['./campaign-edit.component.scss'],
  providers: [DatePipe]
})
export class CampaignEditComponent implements OnInit {

  form: FormGroup;
  currentDate: Date = new Date();
  minDateCampaign: number;
  maxDateCampaign: number;
  data: ICampaignAdding;
  id: number;

  isLoad: boolean;

  searchCls = {
    educationDoc: new BehaviorSubject<string>(''),
    educationLevel: new BehaviorSubject<number>(null),
    educationForms: new BehaviorSubject<string>(''),
  };

  cls = {
    education_types$: {
      init: false,
      data: this.searchCls.educationDoc.pipe(
          distinctUntilChanged(),
          debounceTime(800),
          switchMap((term: any) => this.clsService.getClsData('campaign_types', term))
      ),
    },
    education_levels$: {
      init: false,
      data: this.searchCls.educationLevel.pipe(
          distinctUntilChanged(),
          debounceTime(800),
          switchMap((campaign: number) => {
            //данные для отправки по умолчанию
            let params = {};
            let tableName = 'education_levels';

            // если id типа ПК, то берем данные из таблицы-связки и отправляем парамертр на сервер
            if (campaign) {
              params = {id_campaign_types: campaign};
              tableName = 'v_edu_levels_campaign_types';
            }
            return this.clsService.getClsData(tableName, null, params);
          })
      ),
    },
    education_forms$: {
      init: false,
      data: this.searchCls.educationForms.pipe(
          distinctUntilChanged(),
          debounceTime(800),
          switchMap((term: any) => this.clsService.getClsData('education_forms', term))
      ),
    }
  };

  constructor(private http: HttpClient,
              private router: Router,
              private clsService: ClsService,
              private fb: FormBuilder,
              private datePipe: DatePipe,
              private campaignService: CampaignService,
              private route: ActivatedRoute,
              private snackBar: MatSnackBar,
              private dialog: MatDialog
  ) {
  }

  ngOnInit() {
    this.minDateCampaign = +this.datePipe.transform(this.currentDate, 'yyyy') - 1;
    this.maxDateCampaign = +this.datePipe.transform(this.currentDate, 'yyyy') + 1;
    this.route.params.subscribe(params => {
      this.id = params.id;
    });
    this.getCampaignInfo();
    this.initForm();
  }

  initForm() {
    this.form = this.fb.group({
      name: [null, Validators.required],
      year_start:
          [null,
            [
              Validators.required,
              Validators.min(this.minDateCampaign),
              Validators.max(this.maxDateCampaign),
              Validators.minLength(4),
              Validators.maxLength(4)
            ]
          ],
      year_end: [null,
        [
          Validators.required,
          Validators.min(this.minDateCampaign),
          Validators.max(this.maxDateCampaign),
          Validators.minLength(4),
          Validators.maxLength(4)
        ]],
      education_forms: [null, Validators.required],
      education_levels: [null, Validators.required],
      id_campaign_type: [null, Validators.required],
      uid: [null],
    });


  }

  fillForm() {
    for ( let key in this.form.controls ) {
      if (this.data.hasOwnProperty(key)) {
        this.form.get(key).setValue( this.data[key] );
      }
    }
    this.form.markAsPristine();
    this.form.markAsUntouched();
  }

  subscribeFormChanges() {
    // подписываемся на события изменения контрола
    this.form.controls['id_campaign_type'].valueChanges.subscribe(res => {

      // обнуляем значение контрола
      this.form.controls['education_levels'].setValue(null);

      // эмитим новое событие в behaviorSubject и передаем новое значение
      this.searchCls.educationLevel.next(res);
    });
  }

  getCampaignInfo() {
    this.campaignService.getCampaignMainInfo(this.id)
        .subscribe(resp => {
          if (resp.done) {
            this.data = resp.data;
            this.fillForm();
            this.subscribeFormChanges();

          } else {
            this.snackBar.open('Произошла ошибка' + (resp.message ? ': ' + resp.message : '') );
          }
          this.isLoad = false;
        });
  }

  saveChanges() {
    const formData = {
      year_start: +this.form.value.year_start,
      year_end: +this.form.value.year_end,
      ...this.form.value};

    this.campaignService.editCampaign(this.id, formData)
        .subscribe((res: any) => {
          if (res.done) {
            this.snackBar.open('Данные успешно сохранены');
          } else {
            this.snackBar.open('Ошибка: ' + res.message, '', {duration: 5000});
          }
        });

  }

  declineChanges() {
    const dialog = this.dialog.open(ConfirmModalComponent, {
      data: {
        title: 'Сброс значений',
        content: 'Вы уверены, что хотите отменить изменения и перейти на страницу ПК?'
      },

    });
    dialog.afterClosed().subscribe(res => {
      if (res) {
        this.router.navigate(['../main'], {relativeTo: this.route});
      }
    });
  }
}
