import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {ClsService} from '@/common/services/cls.service';
import {debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';
import {BehaviorSubject} from 'rxjs';
import {DatePipe} from '@angular/common';
import {MatSnackBar} from '@angular/material/snack-bar';
import {CampaignService} from '@/cabinets/university/campaign-menu/campaign.service';

@Component({
  selector: 'app-campaign-new',
  templateUrl: './campaign-new.component.html',
  styleUrls: ['./campaign-new.component.scss'],
  providers: [DatePipe]
})
export class CampaignNewComponent implements OnInit {
  form: FormGroup;
  currentDate: Date = new Date();
  minDateCampaign: number;
  maxDateCampaign: number;

  searchCls = {
    educationDoc: new BehaviorSubject<string>(''),
    educationLevel: new BehaviorSubject<number>(null),
    educationForms: new BehaviorSubject<string>(''),
  };

  cls = {
    education_types$: {
      init: false,
      data: this.searchCls.educationDoc.pipe(
        distinctUntilChanged(),
        debounceTime(800),
        switchMap((term: any) => this.clsService.getClsData('campaign_types', term))
      ),
    },
    education_levels$: {
      init: false,
      data: this.searchCls.educationLevel.pipe(
        distinctUntilChanged(),
        debounceTime(800),
        switchMap((campaign: number) => {
          //данные для отправки по умолчанию
          let params = {};
          let tableName = 'education_levels';

          // если id типа ПК, то берем данные из таблицы-связки и отправляем парамертр на сервер
          if (campaign) {
            params = {id_campaign_types: campaign};
            tableName = 'v_edu_levels_campaign_types';
          }
          return this.clsService.getClsData(tableName, null, params);
        })
      ),
    },
    education_forms$: {
      init: false,
      data: this.searchCls.educationForms.pipe(
        distinctUntilChanged(),
        debounceTime(800),
        switchMap((term: any) => this.clsService.getClsData('education_forms', term))
      ),
    }
  };

  constructor(private http: HttpClient,
              private router: Router,
              private clsService: ClsService,
              private campaignService: CampaignService,
              private fb: FormBuilder,
              private datePipe: DatePipe,
              private snackBar: MatSnackBar
  ) {
  }

  ngOnInit() {
    this.minDateCampaign = +this.datePipe.transform(this.currentDate, 'yyyy') - 1;
    this.maxDateCampaign = +this.datePipe.transform(this.currentDate, 'yyyy') + 1;


    //console.log('date: ', this.maxDateCampaign, this.minDateCampaign)
    this.form = this.fb.group({
      name: [null, Validators.required],
      year_start:
        [null,
          [
            Validators.required,
            Validators.min(this.minDateCampaign),
            Validators.max(this.maxDateCampaign),
            Validators.minLength(4),
            Validators.maxLength(4)
          ]
        ],
      year_end: [null,
        [
          Validators.required,
          Validators.min(this.minDateCampaign),
          Validators.max(this.maxDateCampaign),
          Validators.minLength(4),
          Validators.maxLength(4)
        ]],
      education_forms: [null, Validators.required],
      education_levels: [null, Validators.required],
      id_campaign_type: [null, Validators.required],
      uid: [null, Validators.maxLength(36)],
    });

    // подписываемся на события изменения контрола
    this.form.controls['id_campaign_type'].valueChanges.subscribe(res => {

      // обнуляем значение контрола
      this.form.controls['education_levels'].setValue(null);

      // эмитим новое событие в behaviorSubject и передаем новое значение
      this.searchCls.educationLevel.next(res);
    });
  }

  submit() {
    const formData =
    {
      ...this.form.value,
      year_start: +this.form.value.year_start,
      year_end: +this.form.value.year_end
    };

    this.campaignService.createNewCampaign(formData)
      .subscribe((res: any) => {
        if (res.done) {
          this.snackBar.open( 'Приемная кампания успешно создана' );
          this.router.navigate(['/cabinets/university/campaign', res.data ]);
        } else {
          this.snackBar.open( 'Произошла ошибка' + ( res.message ? ': ' + res.message : '')   );
        }
      });

  }

  /*onChangeTypeCampaign(selectedTypeCampaign) {
    this.numTypeCampaign = +selectedTypeCampaign;
    if (this.numTypeCampaign === 0) {
      return
    }
    console.log(this.numTypeCampaign) ;
    return this.numTypeCampaign;
  }*/

}
