import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AchievementDetailComponent } from './achievement-detail/achievement-detail.component';
import { AchievementEditComponent } from './achievement-edit/achievement-edit.component';
import { AchievementNewComponent } from './achievement-new/achievement-new.component';
import {RouterModule, Routes} from '@angular/router';
import {AchievementService} from '@/cabinets/university/campaign-menu/pages/achievement/achievement.service';
import {MaterialModule} from '@/common/modules/material/material.module';
import {SharedModule} from '@/common/modules/shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ArtLibModule} from '@art-lib/art-lib.module';
import {EditUidModule} from '@/cabinets/university/campaign-menu/components/_edit-dialog/edit-uid/edit-uid.module';

const routes: Routes = [
    { path: '', redirectTo: 'new', pathMatch: 'full' },
    {path: 'new', component: AchievementNewComponent },
    {path: ':id', component: AchievementDetailComponent },
    {path: ':id/edit', component: AchievementEditComponent },
];

@NgModule({
  declarations: [
      AchievementDetailComponent,
      AchievementEditComponent,
      AchievementNewComponent],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        ArtLibModule,
        EditUidModule
    ]
})
export class AchievementModule { }
