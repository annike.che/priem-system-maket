import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {ClsService} from '@/common/services/cls.service';
import {HttpClient} from '@angular/common/http';
import {debounceTime, delay, distinctUntilChanged} from 'rxjs/operators';
import {CampaignService} from '@/cabinets/university/campaign-menu/campaign.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {duration} from 'moment';
import {pipe} from 'rxjs';

@Component({
  selector: 'app-achievement-new',
  templateUrl: './achievement-new.component.html',
  styleUrls: ['./achievement-new.component.scss']
})
export class AchievementNewComponent implements OnInit {
  achievementForm: FormGroup;
  searchOrg = new FormControl('');
  searchCat = new FormControl('');
  idCampaign: number;
  message: any = {
    done: 'Успешно',
    error: 'Произошла ошибка'
  }

  cls = {
    education_level: {
      data: []
    },
    campaign: {
      data: [],
      category: []
    },

  };

  constructor(private fb: FormBuilder,
              private route: ActivatedRoute,
              private campaignService: CampaignService,
              private router: Router,
              private clsService: ClsService,
              private http: HttpClient,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {

    this.searchOrg.valueChanges
      .pipe(
        distinctUntilChanged(),
        debounceTime(600))
      .subscribe((val) => {
        this.loadCompanyList(val);
      });

    this.searchCat.valueChanges
      .pipe(
        distinctUntilChanged(),
        debounceTime(600))
      .subscribe((val) => {
        this.loadCategory(val);
      });


    this.route.queryParams.subscribe(res => {
      this.idCampaign = +res.id_campaign;

      this.loadCompanyList();
      this.loadCategory()
      if (this.idCampaign) this.initEducationLevel(this.idCampaign);

      this.initForm();
    });
  }

  loadCompanyList(search = '') {
    this.clsService.getCampaignList(search)
      .subscribe(res => {
        this.cls.campaign.data = res;
        console.log(this.cls.campaign);
      });


  }
  loadCategory(search = '') {
    this.clsService.getClsData('achievement_categories', search)
      .subscribe(res => {
        this.cls.campaign.category = res;
      });
  }

  initEducationLevel(idCampaign) {
    this.campaignService.getEducationLevelsByCampaignId(idCampaign)
      .subscribe(res => {
        if (res) {
          this.cls.education_level.data = res.data;
        }
      });
  }

  initForm() {
    this.achievementForm = this.fb.group({
      id_campaign: [this.idCampaign],
      name: [null],
      uid: [null],
      max_value: [ null,
        [
          Validators.required,
          Validators.min(1),
          Validators.max(999)
        ]
      ],
      id_category: [null]
    })

    this.achievementForm.controls['id_campaign'].valueChanges.subscribe(res => {
      this.router.navigate(['.'], {relativeTo: this.route, queryParams: {id_campaign: res}});
    });

  }


  create() {
    const formData = {
      id_campaign: this.idCampaign,
      ...this.achievementForm.value
    }

    this.http.post('/api/achievements/add', formData)
      .pipe(
        debounceTime(600)
      )
      .subscribe((res:any) => {
        if (res.done) {
          this.router.navigate([`/cabinets/university/campaign/${this.idCampaign}/achievement`]);
        } else {
          this.openSnackBar(this.message.error)
        }

      })
  }
  openSnackBar(message) {
    this.snackBar.open(message, '', {
      duration: 1000
    })
  }
}
