import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AchievementNewComponent } from './achievement-new.component';

describe('AchievementNewComponent', () => {
  let component: AchievementNewComponent;
  let fixture: ComponentFixture<AchievementNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AchievementNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AchievementNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
