import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AchievementService} from '@/cabinets/university/campaign-menu/pages/achievement/achievement.service';
import {EditUidComponent} from '@/cabinets/university/campaign-menu/components/_edit-dialog/edit-uid/edit-uid.component';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-achievement-detail',
  templateUrl: './achievement-detail.component.html',
  styleUrls: ['./achievement-detail.component.scss']
})
export class AchievementDetailComponent implements OnInit {

  id: number;
  data: Achievement;

  hasError: boolean;

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private achievementService: AchievementService,
      private dialog: MatDialog
  ) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params.id;

      this.getData();
    });
  }

  getData() {
    this.achievementService.getAchievementInfo(this.id)
        .subscribe(res => {
          if (res.done) {
            this.data = res.data;
          }
        });
  }

  editUid() {
    this.dialog.open(EditUidComponent, {
      data: {
        uid: this.data.uid,
        tableKey: 'achievements',
        id: this.id
      },
      minWidth: '400px'
    }).afterClosed()
        .subscribe(data => {
          if (data) {
            if (data.hasOwnProperty('uid')) {
              this.data.uid = data.uid;
            }
          }
        });
  }
}
