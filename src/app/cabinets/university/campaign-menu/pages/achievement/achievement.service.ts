import { Injectable } from '@angular/core';
import {AchievementModule} from '@/cabinets/university/campaign-menu/pages/achievement/achievement.module';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class AchievementService {

  urlPrefix: string;
  urlGroup = '/achievements';

  constructor(private httpClient: HttpClient) {
    this.urlPrefix = environment.apiUrl;

  }

  getAchievementInfo(id): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${id}/main`;
    return this.httpClient.get( url );
  }

  removeAchievement(id): Observable<any> {
    const urlAchieve = this.urlPrefix + this.urlGroup + `/${id}/remove`;
    return  this.httpClient.get( urlAchieve );
  }
  editAchievement(id, data): Observable<any> {
    const urlAchieve = this.urlPrefix + this.urlGroup + `/${id}/edit`;
    return  this.httpClient.post( urlAchieve, data );
  }
}
