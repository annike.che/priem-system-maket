import {AfterViewInit, Component, Inject, OnChanges, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AchievementService} from '@/cabinets/university/campaign-menu/pages/achievement/achievement.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ClsService} from '@/common/services/cls.service';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {ConfirmModalComponent} from '@art-lib/components/confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-achievement-edit',
  templateUrl: './achievement-edit.component.html',
  styleUrls: ['./achievement-edit.component.scss']
})
export class AchievementEditComponent implements OnInit {

  id: number;
  data: Achievement;

  hasError: boolean;
  formData: FormGroup;
  searchCat = new FormControl('');
  cls = {
    category: {
      data: [],
      search: ''
    },
  };
  idCampaign: number;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private achievementService: AchievementService,
    private clsService: ClsService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private fb: FormBuilder

  ) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params.id;
      this.loadCategory();
      this.getData();

    });

    this.initForm();


  }


  initForm() {
    this.formData = this.fb.group({
      id_category:  [ null, Validators.required ],
      name:         [ null, Validators.required ],
      max_value:    [ null, [   Validators.min(1),
                                Validators.max(999),
                                Validators.required ]],
      uid:          [ null ]
    });
  }

  loadCategory(search = '') {
    this.clsService.getClsData('achievement_categories', search)
      .subscribe(res => {
        this.cls.category.data = res;
      });
  }

  getData() {
    this.achievementService.getAchievementInfo(this.id)
      .subscribe(res => {
        if (res.done) {
          this.data = res.data;
          this.idCampaign = +this.data.id_campaign;
          this.fillForm();
        } else {
          this.snackBar.open('Произошла ошибка: ' + res.message);
        }
      });
  }

  fillForm() {
    for ( let key in this.formData.controls ) {
      if (this.data.hasOwnProperty(key)) {
        this.formData.get(key).setValue( this.data[key] );
      }
    }
    this.formData.markAsPristine();
    this.formData.markAsUntouched();
  }

  saveChanges() {
    this.achievementService.editAchievement(this.id, this.formData.value)
      .subscribe(res => {
        if (res.done) {
          this.snackBar.open('Данные успешно сохранены');
          this.router.navigate(['../../../', this.idCampaign, 'achievement'], {relativeTo: this.route});
        } else {
          this.snackBar.open('Произошла ошибка: ' + res.message);
        }
      })
  }


  declineChanges() {
    const dialog = this.dialog.open(ConfirmModalComponent, {
      data: {
        title: 'Сброс значений',
        content: 'Вы уверены, что хотите отменить изменения?'
      },

    });
    dialog.afterClosed().subscribe(res =>{
      if (res) {
        this.router.navigate(['../../../', this.idCampaign, 'achievement'], {relativeTo: this.route});
      }
    });
  }
}
