import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AchievementEditComponent } from './achievement-edit.component';

describe('AchievementEditComponent', () => {
  let component: AchievementEditComponent;
  let fixture: ComponentFixture<AchievementEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AchievementEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AchievementEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
