import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {CompetitiveDetailComponent} from './competitive-detail/competitive-detail.component';
import {CompetitiveEditComponent} from './competitive-edit/competitive-edit.component';
import {CompetitiveNewComponent} from './competitive-new/competitive-new.component';
import {SharedModule} from '@/common/modules/shared.module';
import {ArtLibModule} from '@art-lib/art-lib.module';
import {NgxMaskModule} from 'ngx-mask';
import {RxReactiveFormsModule} from '@rxweb/reactive-form-validators';
import { CompetitiveDetailMainComponent } from './competitive-detail/_tabs/competitive-detail-main/competitive-detail-main.component';
import { CompetitiveDetailTestsComponent } from './competitive-detail/_tabs/competitive-detail-tests/competitive-detail-tests.component';
import { CompetitiveDetailProgramsComponent } from './competitive-detail/_tabs/competitive-detail-programs/competitive-detail-programs.component';
import {EditUidModule} from '@/cabinets/university/campaign-menu/components/_edit-dialog/edit-uid/edit-uid.module';
import {EditCompetitiveNumberDialogComponent} from '@/cabinets/university/campaign-menu/components/_edit-dialog/edit-competitive-number-dialog/edit-competitive-number-dialog.component';
import {EditCompetitiveCommentDialogComponent} from '@/cabinets/university/campaign-menu/components/_edit-dialog/edit-competitive-comment-dialog/edit-competitive-comment-dialog.component';
import {EditCompetitiveNameDialogComponent} from '@/cabinets/university/campaign-menu/components/_edit-dialog/edit-competitive-name-dialog/edit-competitive-name-dialog.component';
import { CompetitiveDetailRatingCompetitiveComponent } from './competitive-detail/_tabs/competitive-detail-rating-competitive/competitive-detail-rating-competitive.component';
import {IMaskModule} from 'angular-imask';

const routes: Routes = [
  { path: '', redirectTo: 'new', pathMatch: 'full' },
  {path: 'new', component: CompetitiveNewComponent },
  {path: ':id', component: CompetitiveDetailComponent,
    children: [
      { path: '', redirectTo: 'main', pathMatch: 'full' },
      { path: 'main', component: CompetitiveDetailMainComponent },
      { path: 'program', component: CompetitiveDetailProgramsComponent },
      { path: 'tests', component: CompetitiveDetailTestsComponent },
      { path: 'rating-competitive', component: CompetitiveDetailRatingCompetitiveComponent },
    ]
  },
  {path: ':id/edit', component: CompetitiveEditComponent },
];


@NgModule({
  declarations: [
      CompetitiveDetailComponent,
      CompetitiveEditComponent,
      CompetitiveNewComponent,
      CompetitiveDetailMainComponent,
      CompetitiveDetailTestsComponent,
      CompetitiveDetailProgramsComponent,
      EditCompetitiveNumberDialogComponent,
      EditCompetitiveCommentDialogComponent,
      EditCompetitiveNameDialogComponent,
      CompetitiveDetailRatingCompetitiveComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    ArtLibModule,
    NgxMaskModule,
    RxReactiveFormsModule,
    EditUidModule,
    IMaskModule
  ],
  entryComponents: [
    EditCompetitiveNumberDialogComponent,
    EditCompetitiveCommentDialogComponent,
    EditCompetitiveNameDialogComponent,
  ]
})
export class CompetitiveModule { }
