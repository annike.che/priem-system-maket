import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CampaignService} from '@/cabinets/university/campaign-menu/campaign.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog';
import {FormBuilder} from '@angular/forms';
import {ClsService} from '@/common/services/cls.service';
import {RatingService} from '@/common/services/rating.service';
import {Observable, Subject} from 'rxjs';
import {PageEvent} from '@angular/material/paginator';
import {Title} from '@angular/platform-browser';
import {debounceTime, distinctUntilChanged, switchMap, tap} from 'rxjs/operators';
import {untilDestroyed} from 'ngx-take-until-destroy';
import {SortDirection} from '@angular/material/sort';
import {artAnimations} from '@art-lib/animations';
import * as cloneDeep from 'lodash/cloneDeep';

@Component({
  selector: 'app-competitive-detail-rating-competitive',
  templateUrl: './competitive-detail-rating-competitive.component.html',
  styleUrls: ['./competitive-detail-rating-competitive.component.scss'],
  animations: artAnimations
})
export class CompetitiveDetailRatingCompetitiveComponent implements OnInit, OnDestroy {

  idCompetitive: number;

  upguInfo: any;

  data: Array<CompetitiveRating> = [];
  displayedColumns: Array<string>;
  editColumns: Array<string>;
  pageSizeOptions: number[];

  isLoad: boolean;
  
  editData: CompetitiveRating;
  isEditData = false;


  defaultParams = {
    page: 1,
    limit: 20
  };

  search = new Subject<string>();
  sortEvent = new Subject<SortEvent>();
  pagination = new Subject<PageEvent>();

  total: number;

  params: Params = {
    ...this.defaultParams
  };

  isEditRow = (index, item) => { console.log('isEditRow', item.id); return  (this.isEditData) ? this.editData.id === item.id : false; };


  constructor(  private route: ActivatedRoute,
                private router: Router,
                private ratingService: RatingService,
                private snackBar: MatSnackBar,
                private dialog: MatDialog,
                private fb: FormBuilder,
                private clsService: ClsService,
                private changeDetectorRefs: ChangeDetectorRef,
                private titleService: Title) {

    this.titleService.setTitle('Рейтинговый список по конкурсу');

    this.displayedColumns = ['id', 'fio',
      'uidEpgu', 'admissionVolume', 'сommonRating', 'agreedRating',
      'firstRating', 'сhangeRating', 'countApplication', 'countAgreed',
      'countFirstStep', 'countSecondStep',  'changed', 'action'];

    this.editColumns = [ 'id', 'fio',
      'uidEpgu', 'admissionVolume', 'сommonRatingEdit', 'agreedRatingEdit',
      'firstRatingEdit', 'сhangeRatingEdit', 'countApplicationEdit', 'countAgreedEdit',
      'countFirstStepEdit', 'countSecondStepEdit',  'changed', 'actionEdit' ];

    this.pageSizeOptions = [5, 10, 15, 20, 30, 50, 100];

  }

  ngOnInit() {
    this.route.parent.params.subscribe( res => {
      this.idCompetitive = +res['id'];
      if( this.idCompetitive ) {

        this.getQueryParamsFromUrl();
        this.subscribeOnChanges();
        this.loadData();
        this.updateUpguStatus();
      }
    } );
  }

  subscribeOnChanges() {

    this.search.pipe(
        distinctUntilChanged(),
        debounceTime(700),
        tap(() => {
          this.params.page = 1;
        }),
        switchMap((value, index) => {
          console.log('value', value);
          return this.getDataFromService();
        }),
        untilDestroyed(this)
    ).subscribe(response => this.setData(response));

    this.pagination.pipe(
        debounceTime(200),
        tap((event: PageEvent) => {
          if (this.params.limit !== event.pageSize) {
            this.params.limit = event.pageSize;
            this.params.page = 1;
          }
          if (this.params.page !== event.pageIndex + 1) {
            this.params.page = event.pageIndex + 1;
          }
        }),
        switchMap(() => this.getDataFromService()),
        untilDestroyed(this)
    )
        .subscribe(response => this.setData(response));


    this.sortEvent.pipe(
        debounceTime(200),
        tap((event: SortEvent) => {
          this.params.order = event.direction as SortDirection;
          this.params.sortby = event.active;
        }),
        switchMap(() => this.getDataFromService()),
        untilDestroyed(this)
    )
        .subscribe(response => this.setData(response));

  }

  loadData() {
    this.getDataFromService()
        .subscribe(response => this.setData(response));
  }

  setData(response) {
    if (response.done) {

      this.data = response.data;
      this.params.page = response.paginator.page;
      this.params.limit = response.paginator.limit;
      this.total = response.paginator.total;

    }
    this.isLoad = false;
  }


  getQueryParamsFromUrl() {
    const queryParams = cloneDeep(this.route.snapshot.queryParams);
    this.setParams(queryParams);
  }

  setParams(params) {
    Object.keys(params).forEach(key => {
      switch (key) {
        case 'page':
          this.params[key] = +params[key];
          break;
        default:
          this.params[key] = params[key];
      }
    });
  }

  passQueryParamsToUrl(queryParams) {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams
    });
  }

  getDataFromService(): Observable<any> {
    this.isLoad = true;
    const queryParams = this.getQueryParams(this.params);
    this.passQueryParamsToUrl(queryParams);
    return this.ratingService.getCompetitiveRatingList(this.idCompetitive, this.params);
  }

  getQueryParams(params) {
    const queryParams: any = {};
    Object.keys(params).forEach((key) => {
      if (this.defaultParams.hasOwnProperty(key) && (this.defaultParams[key] !== params[key] && params[key] !== '') ||
          (!this.defaultParams.hasOwnProperty(key) && params[key] !== '')) {
        queryParams[key] = params[key];
      }
    });

    return queryParams;
  }

  ngOnDestroy(): void {

  }

  edit(row: CompetitiveRating, index: any) {
      this.isEditData = true;
      this.editData = {... row};
      this.data = [... this.data];
      console.log(this.editData.id);
      this.changeDetectorRefs.detectChanges();
  }

  save(row: CompetitiveRating, index: number) {
    console.log(index);
    this.ratingService.saveCompetitiveRating(this.editData.id, this.editData)
        .subscribe( res => {
          if( res.done ) {
            this.data[index] = this.editData;
            this.isEditData = false;
            this.editData = undefined;
            this.data = [ ...this.data ];
            this.snackBar.open('Данные успешно обновлены');
          } else {
            this.snackBar.open( 'Произошла ошибка' + ( res.message ? ': ' + res.message : '' ) );
          }
        } );
  }

  cancel() {
    this.isEditData = false;
    this.editData = undefined;
    this.data = [ ...this.data ];
  }

  updateApplication() {
    this.isLoad = true;
    this.ratingService.updateApplication(this.idCompetitive)
        .subscribe( res => {
            if( res.done ) {
              this.snackBar.open( 'Данные успешно загружены' );
              this.loadData();
            } else {
              this.snackBar.open( 'Произошла ошибка' + ( res.message ? ': ' + res.message : '' ) );
              this.isLoad = false;
            }
        });
  }

  sendToEpgu() {
    this.isLoad = true;
    this.ratingService.sendToEpgu(this.idCompetitive)
        .subscribe( res => {
          if( res.done ) {
            this.snackBar.open( 'Данные успешно отправлены' );
            this.updateUpguStatus();
          } else {
            this.snackBar.open( 'Произошла ошибка' + ( res.message ? ': ' + res.message : '' ) );
          }
          this.isLoad = false;
        });
  }

  updateUpguStatus() {
    this.ratingService.getEpguStatus(this.idCompetitive)
        .subscribe( res => {
          if( res.done ) {
            this.upguInfo = res.data;
          } else {
            this.snackBar.open( 'Произошла ошибка' + ( res.message ? ': ' + res.message : '' ) );
          }
        })
  }
}

interface Params {
  page: number;
  limit: number;
  order?: SortDirection; // 'ASC' || 'DESC'
  sortby?: string;
}
