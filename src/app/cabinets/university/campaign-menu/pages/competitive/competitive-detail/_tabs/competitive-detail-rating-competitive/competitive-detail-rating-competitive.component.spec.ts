import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompetitiveDetailRatingCompetitiveComponent } from './competitive-detail-rating-competitive.component';

describe('CompetitiveDetailRatingCompetitiveComponent', () => {
  let component: CompetitiveDetailRatingCompetitiveComponent;
  let fixture: ComponentFixture<CompetitiveDetailRatingCompetitiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompetitiveDetailRatingCompetitiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompetitiveDetailRatingCompetitiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
