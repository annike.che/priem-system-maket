import {Component, Input, OnInit} from '@angular/core';
import {CampaignService} from '@/cabinets/university/campaign-menu/campaign.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {takeUntil} from 'rxjs/operators';
import {ConfirmModalComponent} from '@art-lib/components/confirm-modal/confirm-modal.component';
import {MatDialog} from '@angular/material/dialog';
import {EditUidComponent} from '@/cabinets/university/campaign-menu/components/_edit-dialog/edit-uid/edit-uid.component';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-competitive-detail-programs',
  templateUrl: './competitive-detail-programs.component.html',
  styleUrls: ['./competitive-detail-programs.component.scss']
})
export class CompetitiveDetailProgramsComponent implements OnInit {

  idCompetitive: number;

  data: Array<EducationPrograms | null>;

  isLoad: boolean;

  educationProgramForm: FormGroup;

  checkAddRemove: boolean;

  isAddProgram = (index, item) => { return  item ? !item.id : true; };

  constructor(
      private route: ActivatedRoute,
      private campaignService: CampaignService,
      private snackBar: MatSnackBar,
      private dialog: MatDialog,
      private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.route.parent.params.subscribe( res => {
      this.idCompetitive = +res['id'];
      if( this.idCompetitive ) {
        this.loadInfo();
        this.checkAddRemoveEvent();
      }
    } );
  }

  loadInfo() {
    this.isLoad = true;
    this.campaignService.getCompetitiveInfoPrograms(this.idCompetitive)
        .subscribe(res => {
          if( res.done ) {
            this.data = res.data || [];
          } else {
            this.snackBar.open('Произошла ошибка' + ( res.message ? ': ' + res.message : '' ) );
          }

          this.isLoad = false;
        });
  }

  initEducationProgramForm() {
    this.educationProgramForm = this.fb.group({
      name: [null, Validators.required],
      uid:  [null]
    });
  }

  addProgram() {
    this.initEducationProgramForm();
    this.data.unshift(null);
    this.data = [... this.data];
  }

  deleteEducationPrograms(id: number, index: number) {
    this.dialog.open(ConfirmModalComponent, {
      data: {
        title: 'Удаление Образовательной программы',
        content: 'Вы уверены, что хотите удалить образовательную программу?'
      }
    })
    .afterClosed().subscribe(res => {
      if( res ) {
        this.campaignService.deleteEducationProgram(this.idCompetitive, id)
            .subscribe( res => {
              if( res.done ) {
                this.snackBar.open('Образовательная программа успешно удалена');
                this.data.splice(index,1);
                this.data = [... this.data];
              } else {
                this.snackBar.open('При попытке удалить образовательную программу произошла ошибка' + ((res.message) ? `: ${res.message}` : '') );
              }
            });
      }
    });
  }

  saveEducationPrograms() {
    let program = this.educationProgramForm.value;
    const data = {
      education_programs: [program]
    };
    this.campaignService.addEducationProgram(this.idCompetitive, data)
        .subscribe( (res: any) => {
          if (res.done) {
            program.id = res.data.id_education_programs[0];
            this.data[0] = program;
            this.data = [... this.data];
            this.educationProgramForm = undefined;
            this.snackBar.open('Образовательная программа успешно добавлена' );
          } else {
            this.snackBar.open('При попытке добавить образовательную программу произошла ошибка' + ((res.message) ? `: ${res.message}` : '') );
          }
        });
  }

  cancel() {
   this.educationProgramForm = undefined;
    if (!this.data[0] && this.data.length >= 0) {
      this.data.splice(0, 1);
      this.data = [ ... this.data];
    }

  }

  checkAddRemoveEvent() {
      this.campaignService.checkCompetitiveProgramsAddRemove(this.idCompetitive)
          .subscribe( res => {
            if (res.done) {
              this.checkAddRemove = res.data.can;
            }
            //console.log('checkAddRemove', this.checkAddRemove);
          });
  }

  editUid(item: EducationPrograms) {
    this.dialog.open(EditUidComponent, {
      data: {
        uid: item.uid,
        tableKey: 'programs',
        id:  item.id
      },
      minWidth: '400px'
    }).afterClosed()
        .subscribe( data => {
          if(data) {
            if (data.hasOwnProperty('uid')) {
              item.uid = data.uid;
            }
          }

        });
  }
}
