import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CampaignService} from '@/cabinets/university/campaign-menu/campaign.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {EditUidComponent} from '@/cabinets/university/campaign-menu/components/_edit-dialog/edit-uid/edit-uid.component';
import {MatDialog} from '@angular/material/dialog';
import {EditCompetitiveNumberDialogComponent} from '@/cabinets/university/campaign-menu/components/_edit-dialog/edit-competitive-number-dialog/edit-competitive-number-dialog.component';
import {EditCompetitiveCommentDialogComponent} from '@/cabinets/university/campaign-menu/components/_edit-dialog/edit-competitive-comment-dialog/edit-competitive-comment-dialog.component';
import {EditCompetitiveNameDialogComponent} from '@/cabinets/university/campaign-menu/components/_edit-dialog/edit-competitive-name-dialog/edit-competitive-name-dialog.component';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-competitive-detail-main',
  templateUrl: './competitive-detail-main.component.html',
  styleUrls: ['./competitive-detail-main.component.scss']
})
export class CompetitiveDetailMainComponent implements OnInit {

  idCompetitive: number;
  checkEdit: boolean;

  data: CompetitiveMain;

  isLoad: boolean;

  constructor(
      private route: ActivatedRoute,
      private campaignService: CampaignService,
      private snackBar: MatSnackBar,
      private dialog: MatDialog
  ) { }

  ngOnInit() {

    this.route.parent.params.subscribe( res => {

      this.idCompetitive = +res['id'];
      if( this.idCompetitive ) {
        this.loadInfo();
        this.checkEditEvent();
      }
    } );
  }

  loadInfo() {
    this.isLoad = true;
    this.campaignService.getCompetitiveInfoMain(this.idCompetitive)
        .subscribe(res => {
            if( res.done ) {
                this.data = res.data;
                //this.campagnId.emit(this.data.id_campaign);
            } else {
              this.snackBar.open('Произошла ошибка' + ( res.message ? ': ' + res.message : '' ) );
            }

            this.isLoad = false;
        });
  }

  editUid() {
    this.dialog.open(EditUidComponent, {
      data: {
        uid: this.data.uid,
        tableKey: 'competitive',
        id:  this.idCompetitive
      },
      minWidth: '400px'
    }).afterClosed()
        .subscribe( data => {
          if(data) {
            if (data.hasOwnProperty('uid')) {
              this.data.uid = data.uid;
            }
          }

        });
  }

  editNumber() {
    this.dialog.open(EditCompetitiveNumberDialogComponent, {
      data: this.data,
      minWidth: '400px'
    }).afterClosed()
        .subscribe( res => {
          if (res) {
            this.data.number = res.number;
          }
        });

  }


  editComment() {
    this.dialog.open(EditCompetitiveCommentDialogComponent, {
      data: this.data,
      minWidth: '600px'
    }).afterClosed()
        .subscribe( res => {
          if (res) {
            this.data.comment = res.comment;
          }
        });
  }

  editName() {
    this.dialog.open(EditCompetitiveNameDialogComponent, {
      data: this.data,
      minWidth: '600px'
    }).afterClosed()
        .subscribe( res => {
          if (res) {
            this.data.name = res.name;
          }
        });
  }

  checkEditEvent() {
    this.campaignService.checkCompetitiveEdit(this.idCompetitive)
        .subscribe( res => {
          if( res.done ) {
            this.checkEdit = res.data.can;
          }
        });

  }
}
