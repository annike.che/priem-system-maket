import {ChangeDetectorRef, Component, Input, OnDestroy, OnInit} from '@angular/core';
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  NgForm,
  ValidatorFn,
  Validators
} from '@angular/forms';
import {CampaignService} from '@/cabinets/university/campaign-menu/campaign.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog';
import {ClsService} from '@/common/services/cls.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {RxwebValidators} from '@rxweb/reactive-form-validators';
import {ConfirmModalComponent} from '@art-lib/components/confirm-modal/confirm-modal.component';
import * as prepare from '@/common/functions/prepareData';
import * as moment from 'moment';
import {EditUidComponent} from '@/cabinets/university/campaign-menu/components/_edit-dialog/edit-uid/edit-uid.component';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-competitive-detail-tests',
  templateUrl: './competitive-detail-tests.component.html',
  styleUrls: ['./competitive-detail-tests.component.scss']
})
export class CompetitiveDetailTestsComponent implements OnInit, OnDestroy {

  idCompetitive: number;

  idCampaign: number;

  data: Array<EntrantsTests | null>;

  isLoad: boolean;

  checkAddRemove: boolean;
  checkAddRemoveCalendar: boolean;

  entranceTestsForm: FormGroup;

  subjectsList: Array<number> = [];
  testNameList: Array<string> = [];
  priorityList: Array<number> = [];

  private ngUnsubscribe: Subject<void> = new Subject<void>();

  columnsHeader = ['id', 'testType', 'subject', 'ege', 'mark', 'priority', 'uid', 'actions' ];

  cls = {
    entrance_test_types: {
      data: [],
      initFunc: () => this.clsService.getClsData('entrance_test_types'),
      isLoad: true

    },
    subjects_ege: {
      data: [],
      initFunc: () => this.campaignService.getSubjectByCompanyId(this.idCampaign),
      isLoad: true,
      search: ''
    },
    subjects_no_ege: {
      data: [],
      initFunc: () => this.clsService.getClsData('subjects', '', {is_ege: false}),
      isLoad: true
    },

  };
  isOpen = (index, item) => item.isOpen;

  form: FormGroup;

  constructor(
      private route: ActivatedRoute,
      private campaignService: CampaignService,
      private snackBar: MatSnackBar,
      private dialog: MatDialog,
      private fb: FormBuilder,
      private clsService: ClsService,
      private changeDetectorRefs: ChangeDetectorRef,
  ) { }

  ngOnInit() {
    this.route.parent.params.subscribe( res => {
      this.idCompetitive = +res['id'];
      if( this.idCompetitive ) {

        this.loadInfo();
        this.checkAddRemoveEvent();
        this.checkAddRemoveCalendarEvent();

      }
    } );

  }

  loadInfo() {
    this.isLoad = true;
    this.campaignService.getCompetitiveInfoTests(this.idCompetitive)
        .subscribe(res => {
          if ( res.done ) {
            this.data = res.data.tests || [];
            this.idCampaign = res.data.id_campaign;
          } else {
            this.snackBar.open('Произошла ошибка' + ( res.message ? ': ' + res.message : '' ) );
          }

          this.isLoad = false;
        });
  }

  initCls() {
    for (let key in this.cls) {
      if ( this.cls[key].isLoad && !this.cls[key].data.length ) {
        console.log('initFunc', this.cls[key].initFunc);
        this.cls[key].initFunc()
            .pipe(  takeUntil(this.ngUnsubscribe) )
            .subscribe( res => {
              this.cls[key].data = res;
            });
      }
    }
  }

  initEntranceTestsForm() {

    this.initCls();

    this.entranceTestsForm = this.fb.group( {
      id_entrance_test_type:  [null, Validators.required],
      id_subject:             [null, [Validators.required, this.validatorDuplicate(this.subjectsList)]],
      priority:               [null, [Validators.required,
                                      Validators.min(1),
                                      Validators.max(10),
                                      this.validatorDuplicate(this.priorityList)]],
      uid:                    [null],
      test_name:              [null, [  ]],
      min_score:              [null, Validators.required],
      is_ege:                 [true, Validators.required],
    });


    console.log( this.subjectsList, this.testNameList, this.priorityList );
  }

  initDuplicateArray() {
    this.subjectsList = [];
    this.testNameList = [];
    this.priorityList = [];

    this.data.forEach( val => {
      if ( val.id_subject ) {
        this.subjectsList.push(val.id_subject);
      } else {
        this.testNameList.push( val.test_name );
      }
      this.priorityList.push( val.priority );
    });
  }

  setValidatorsDuplicate() {
    let item = this.entranceTestsForm as FormGroup;

    item.get('priority').setValidators([Validators.required, this.validatorDuplicate(this.subjectsList)]);
    item.get('priority').updateValueAndValidity();
    if ( item.get('is_ege').value === true) {
      item.get('id_subject').setValidators([ this.validatorDuplicate(this.subjectsList), Validators.required ]);
      item.get('id_subject').updateValueAndValidity();
    } else {
      item.get('test_name').setValidators([this.validateTestNameDuplicate(this.testNameList), Validators.required ]);
      item.get('test_name').updateValueAndValidity();
    }
  }

  addEntranceTests() {
    this.initDuplicateArray();
    this.initEntranceTestsForm();
  }

  changeEge($event) {
   //console.log('event', $event);
    let item = this.entranceTestsForm as FormGroup;
    if ($event === true) {
      // тип ЕГЭ
      item.get('test_name').setValidators([]);
      item.get('test_name').setValue(null);
      item.get('id_subject').setValidators([ this.validatorDuplicate(this.subjectsList), Validators.required ]);

    } else {
      //тип не егэ
      item.get('id_subject').setValidators([]);
      item.get('id_subject').setValue(null);
      item.get('min_score').setValidators([ Validators.min(0), item.get('min_score').validator]);

      item.get('test_name').setValidators([this.validateTestNameDuplicate(this.testNameList), Validators.required ]);
    }
    item.get('test_name').updateValueAndValidity();
    item.get('id_subject').updateValueAndValidity();
    item.get('min_score').updateValueAndValidity();
  }

  changeSubject(id: any) {
    let item = this.entranceTestsForm as FormGroup;
    const subject = this.cls.subjects_no_ege.data.find( (element) => element.id === id);
    if ( subject ) {
      if ( subject.min_scope >= item.get('min_score').value) item.get('min_score').setValue(subject.min_scope);
      item.get('min_score').setValidators([
        Validators.min(subject.min_scope),
        item.get('min_score').validator
      ]);
    } else {
      item.get('min_score').setValidators([
        Validators.min(0),
        item.get('min_score').validator
      ]);
    }

  }

  validateTestNameDuplicate(tests: Array<string>): ValidatorFn {
    return (control: AbstractControl) => {
      if (!control.value) return null;

      //console.log('tests', tests);

      const duplicate = tests.filter(el => {
        //console.log('el', el);
        return el.toLowerCase().replace(/\s+/g, ' ').trim() ===
            control.value.toLowerCase().replace(/\s+/g, ' ').trim();
      });
      return duplicate.length >= 1 ? { duplicateTestName: true } : null;
    };
  }


  validatorDuplicate(items: Array<number>): ValidatorFn {
    return(control: AbstractControl) => {
      if (!control.value) return null;

      return items.indexOf(control.value) >= 0 ? { duplicate: true } : null;
    };
  }

  cancel() {
    this.entranceTestsForm = undefined;
    this.subjectsList = this.testNameList = this.priorityList = undefined;
  }


  saveEntranceTests() {
    this.isLoad = true;
    let tests = this.entranceTestsForm.value;
    const data = {
      entrance_tests: [tests]
    };
    this.campaignService.addEntranceTests(this.idCompetitive, data)
        .subscribe( (res: any) => {
          if (res.done) {
            tests.id = res.data.id_entrance_tests[0];
            this.loadInfo();
            this.cancel();
            this.snackBar.open('Вступительное испытание успешно добавлено' );
          } else {
            this.snackBar.open('При попытке добавить вступительное испытание произошла ошибка' + ((res.message) ? `: ${res.message}` : '') );
            this.isLoad = false;
          }
        });
  }


  getForm() {
    console.log(this.entranceTestsForm);
  }

  deleteTest(id: any, index: any) {
    this.dialog.open(ConfirmModalComponent, {
      data: {
        title: 'Удаление Вступительного испытания',
        content: 'Вы уверены, что хотите удалить Вступительное испытание?'
      }
    })
        .afterClosed().subscribe(res => {
      if (res) {
        this.campaignService.deleteEntranceTests(this.idCompetitive, id)
            .subscribe( (resp: any) => {
              if( resp.done ) {
                this.data.splice(index, 1);
                this.data = [ ... this.data ];
                this.initDuplicateArray();
                this.setValidatorsDuplicate();
                this.snackBar.open('Вступительное испытание успешно удалено' );
              } else {
                this.snackBar.open('Произошла ошибка' + ( resp.message ? `: ${resp.message}` : '') );
              }
            });
      }
    });
  }

  checkAddRemoveEvent() {
    this.campaignService.checkCompetitiveTestsAddRemove(this.idCompetitive)
        .subscribe( res => {
          if (res.done) {
            this.checkAddRemove = res.data.can;
          }
          //console.log('checkAddRemove', this.checkAddRemove);
        });
  }

  addTestCalendar(row: EntrantsTests) {

    ( row.new_entrance_test_calendar.get('data') as FormArray).insert(0,
        this.fb.group({
          exam_location: [null, Validators.required],
          entrance_test_date: [null, Validators.required],
          entrance_test_time: [null, Validators.required],
          count_c: [null],
          uid: [null, Validators.maxLength(36)]
        })
    );

    this.changeDetectorRefs.detectChanges();

    console.log('row', row.new_entrance_test_calendar);

  }


  ngOnDestroy() {
    console.log('unsubscribe');
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  toggleTestCalendar(index: number) {
      this.data[index].isOpen = !this.data[index].isOpen;

      this.data[index].new_entrance_test_calendar = this.fb.group( {
        data: this.fb.array([])
      });

      this.data = [...this.data];

  }

  deleteTestDate(item: EntrantsTestCalendar, index: number, indexRow: number) {
    this.dialog.open(ConfirmModalComponent, {
      data: {
        title: 'Удалить дату проведения ВИ',
        content: 'Вы уверены, что хотите удалить дату проведения вступительного испытания?'
      }
    }).afterClosed().subscribe( resp => {
        if(resp) {
          this.campaignService.deleteEntranceTestDate(item.id)
              .subscribe( res => {
                  if ( res.done ) {
                     this.data[indexRow].entrance_test_calendar.splice(index, 1);
                     this.data = [ ...this.data ];
                     this.snackBar.open('Дата проведения ВИ удалена');
                  } else {
                    this.snackBar.open( 'Произошла ошибка' + (res.message ? ': ' + res.message : '') );
                  }
              });
        }
    } );
  }

  saveEntranceTestsCalendar(row: EntrantsTests) {
      let sendData = row.new_entrance_test_calendar.get('data').value;
      sendData.forEach( el => {
          const time = moment(el.entrance_test_time, 'HH:mm');
          el.entrance_test_date =  moment(el.entrance_test_date).set({
            hour:   time.get('hour'),
            minute: time.get('minute'),
            second: time.get('second')
          }).format();
      });

      this.campaignService.addEntranceTestDate( row.id, sendData )
          .subscribe( res =>  {
              if ( res.done ) {
                this.loadEntrantTestCalendar(row);
                ( row.new_entrance_test_calendar.get('data') as FormArray).clear();
                this.snackBar.open('Данные успешно сохранены');
              } else {
                this.snackBar.open( 'Произошла ошибка' + (res.message ? ': ' + res.message : '') );
              }
          });


  }

  deleteNewTestDate(array: FormArray, index: number) {
    array.removeAt(index);
  }

  loadEntrantTestCalendar(row: EntrantsTests) {
      this.campaignService.loadEntranceTestCalendar(row.id)
          .subscribe( res => {
              if( res.done ) {
                row.entrance_test_calendar = res.data;
              } else {
                this.snackBar.open( 'Не удалось обновить данные календаря' + (res.message ? ': ' + res.message : '') );
              }
          });

  }

  editUidTest(item: EntrantsTests) {
    this.dialog.open(EditUidComponent, {
      data: {
        uid: item.uid,
        tableKey: 'entrance_test',
        id:  item.id
      },
      minWidth: '400px'
    }).afterClosed()
        .subscribe( data => {
          if(data) {
            if (data.hasOwnProperty('uid')) {
              item.uid = data.uid;
            }
          }

        });
  }

  editUidCalendar(item: EntrantsTestCalendar) {
    this.dialog.open(EditUidComponent, {
      data: {
        uid: item.uid,
        tableKey: 'entrance_test_calendar',
        id:  item.id
      },
      minWidth: '400px'
    }).afterClosed()
        .subscribe( data => {
          if(data) {
            if (data.hasOwnProperty('uid')) {
              item.uid = data.uid;
            }
          }

        });
  }

  private checkAddRemoveCalendarEvent() {
    this.campaignService.checkCompetitiveTestCalendarAddRemove(this.idCompetitive)
        .subscribe( res => {
          if (res.done) {
            this.checkAddRemoveCalendar = res.data.can;
          }
        });
  }

  sendTestsToEpgu() {
    this.isLoad = true;
    this.campaignService.syncEntranceTests(this.idCompetitive)
        .subscribe( (res: any) => {
            if ( res.done ) {
              this.snackBar.open('Данные успешно синхронизированы');
            } else {
              this.snackBar.open('Произошла ошибка' + (res.message ? ': ' + res.message  : ''));
            }
            this.isLoad = false;
        } );
  }
}
