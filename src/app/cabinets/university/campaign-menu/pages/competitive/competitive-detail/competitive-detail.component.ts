import {AfterViewChecked, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {CampaignService} from '@/cabinets/university/campaign-menu/campaign.service';
import {EditUidComponent} from '@/cabinets/university/campaign-menu/components/_edit-dialog/edit-uid/edit-uid.component';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatTabNav} from '@angular/material/tabs';

@Component({
  selector: 'app-competitive-detail',
  templateUrl: './competitive-detail.component.html',
  styleUrls: ['./competitive-detail.component.scss']
})
export class CompetitiveDetailComponent implements OnInit, OnDestroy, AfterViewChecked {

  idCompetitive: number;

  data: CompetitiveMain;

  tabs: Array<any>;

  isLoad: boolean;


  @ViewChild('navs', {static: false}) navs: MatTabNav;


  constructor(
      public route: ActivatedRoute,
      public router: Router,
      public dialog: MatDialog,
      private campaignService: CampaignService,
      private snackBar: MatSnackBar,
  ) {
   this.tabs = [
    {
      path: './main',
      label: 'Основные данные'
    },
    {
      path: './program',
      label: 'Образовательные программы'
    },
    {
      path: './tests',
      label: 'Вступительные испытания'
    },
    {
      path: './rating-competitive',
      label: 'Рейтинги по конкурсу'
    }];
  }

  ngOnInit() {
    this.route.params.subscribe( res => {

      this.idCompetitive = +res['id'];
      if( this.idCompetitive ) {
        this.loadData();
      }
    } );
  }

  loadData() {
    this.isLoad = true;
    this.campaignService.getCompetitiveInfoMain(this.idCompetitive)
        .subscribe(res => {
          if( res.done ) {
            this.data = res.data;
            //this.campagnId.emit(this.data.id_campaign);
          } else {
            this.snackBar.open('Произошла ошибка' + ( res.message ? ': ' + res.message : '' ) );
          }

          this.isLoad = false;
        });
  }


  ngOnDestroy() {
    console.log('unsubscribe');
  }

  ngAfterViewChecked(): void {
    // ререндер нижнего подчеркивания в табах, чтобы не съезжал
    setTimeout(() => this.navs._alignInkBarToSelectedTab(), 0);
  }

}
