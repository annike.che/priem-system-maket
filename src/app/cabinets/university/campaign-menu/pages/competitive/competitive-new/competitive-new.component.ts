import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CampaignService} from '@/cabinets/university/campaign-menu/campaign.service';
import {ClsService} from '@/common/services/cls.service';
import {AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {BehaviorSubject, ReplaySubject, Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, switchMap, takeUntil} from 'rxjs/operators';
import {MatSnackBar} from '@angular/material/snack-bar';
import {OrgsService} from '@/cabinets/university/orgs-menu/orgs.service';
import {RxwebValidators} from '@rxweb/reactive-form-validators';

@Component({
  selector: 'app-competitive-new',
  templateUrl: './competitive-new.component.html',
  styleUrls: ['./competitive-new.component.scss']
})
export class CompetitiveNewComponent implements OnInit, OnDestroy {

  readonly EDUC_SOURC_PAID = 3;

  idCampaign: number;

  private ngUnsubscribe: Subject<void> = new Subject<void>();
  educationLevelChange = new Subject<number>();

  competitiveForm: FormGroup;
  educationGroupForm: FormGroup;
  entranceTestsGroupForm: FormGroup;

  educationPrograms: FormArray;
  entranceTests: FormArray;




  search = {
    campaign: '',
    direction: '',
    subject_ege: '',
    subject_no_ege: ''
  };


  cls = {
    campaign: {
      data: []
    },
    education_level: {
      data: [],
      initFunc:  () => this.campaignService.getEducationLevelsByCampaignId( this.idCampaign ),
      isLoad: true
    },
    education_forms: {
      data: [],
      initFunc: () => this.campaignService.getEducationFormsByCampaignId( this.idCampaign ),
      isLoad: true
    },
    education_source: {
      data: [],
      initFunc: () => this.clsService.getClsData('education_sources'),
      isLoad: true
    },
    level_budget: {
      data: [],
      initFunc: () => this.clsService.getClsData('level_budget'),
      isLoad: true
    },
    direction: {
      data: [],
      initFunc: (idEducLevel?) => this.orgsService.getDirectionsSelectList('', {id_education_level: idEducLevel}),
      isLoad: true
    },

    entrance_test_types: {
      data: [],
      initFunc: () => this.clsService.getClsData('entrance_test_types'),
      isLoad: true

    },
    subjects_ege: {
      data: [],
      initFunc: () => this.campaignService.getSubjectByCompanyId(this.idCampaign),
      isLoad: true
    },
    subjects_no_ege: {
      data: [],
      initFunc: () => this.clsService.getClsData('subjects', '', {is_ege: false}),
      isLoad: true
    },

  };
  stateCtrl: any;


  constructor(private router: Router,
              private route: ActivatedRoute,
              private campaignService: CampaignService,
              private clsService: ClsService,
              private orgsService: OrgsService,
              private fb: FormBuilder,
              private snackBar: MatSnackBar,) { }

  ngOnInit() {

    this.route.queryParams.subscribe( res => {
      this.idCampaign = +res.id_campaign;

      this.loadCompanyList();

      if ( this.idCampaign ) {
        //console.log('clsInitSubject');
        this.initCls();
      }


      this.initForms();

      this.subscribeValueChanged();
    } );
  }

  loadCompanyList(search = '') {
    this.clsService.getCampaignList(search)
        .pipe( takeUntil(this.ngUnsubscribe))
        .subscribe( res => {
          this.cls.campaign.data = res;
          console.log(this.cls.campaign);
        } );
  }

  initCls() {
    for (let key in this.cls) {
      if ( this.cls[key].isLoad ) {
       // console.log('initFunc', this.cls[key].initFunc);
        this.cls[key].initFunc()
            .pipe(  takeUntil(this.ngUnsubscribe) )
            .subscribe( res => {
          this.cls[key].data = res;
        });
      }
    }

  }


  initForms() {
    console.log('init form', this.idCampaign >= 0);
    this.competitiveForm = this.fb.group({
      id_campaign:          [this.idCampaign || null, Validators.required ],
      name:                 [null, Validators.required ],
      uid:                  [null],
      id_education_level:   [null, Validators.required],
      id_education_source:  [null, Validators.required],
      id_level_budget:      [null, Validators.required],
      id_education_form:    [null, Validators.required],
      id_direction:         [null],
      number:               [null, [Validators.required,  Validators.pattern("^[0-9]*$")]],
      comment:              [null]
    });

    this.educationGroupForm = this.fb.group(
        {
          education_programs: this.fb.array([])
        }
    );

    this.educationPrograms = this.educationGroupForm.get('education_programs') as FormArray;

    this.entranceTestsGroupForm = this.fb.group(
        {
          entrance_tests: this.fb.array([])
        }
    );

    this.entranceTests = this.entranceTestsGroupForm.get('entrance_tests') as FormArray;


  }

  subscribeValueChanged() {
    this.competitiveForm.controls['id_campaign'].valueChanges.subscribe( res => {
      this.router.navigate(['.'], {relativeTo: this.route, queryParams: { id_campaign: res }});
    });

    this.competitiveForm.controls['id_education_source'].valueChanges.subscribe( res => {
      if (res === this.EDUC_SOURC_PAID ) {
        this.competitiveForm.controls['id_level_budget'].setValue(null);
        this.competitiveForm.controls['id_level_budget'].setValidators([]);
      }
    });

    this.educationLevelChange.pipe(
        takeUntil(this.ngUnsubscribe),
        distinctUntilChanged(),
        switchMap((id: any) => {
          //console.log('term', id);
          return this.cls.direction.initFunc(id);
        })
    ).subscribe( res => {
        this.competitiveForm.controls['id_direction'].setValue(null);
        this.cls.direction.data = res;
    } );

  }


  testsValidator(arr: FormArray ) {
    const controls = arr.controls;

    for ( let i = 0; i < controls.length; i++ ) {
       const test_name = controls[i].get('test_name').value;
      console.log('test_name', test_name);
         if(test_name) {

            const map = controls.filter((el, ind) => {
              let result = false;
              console.log('value', el.value);
              if (el.get('test_name').value) {
                result = el.get('test_name').value.toLowerCase().replace(/\s+/g, ' ').trim()  ===
                    test_name.toLowerCase().replace(/\s+/g, ' ').trim()
                    && ind !== i;
              }
              return result;
            });

           // console.log('map', map, controls[i]);
            if ( map.length > 0 ) {
              controls[i].get('test_name').setErrors( { duplicateTest: true } );
            } else {
              if( controls[i].get('test_name').hasError('duplicateTest') ) {
                delete controls[i].get('test_name').errors['duplicateTest'];
                controls[i].get('test_name').updateValueAndValidity();
              }
            }
        }
    }

    return null;
  }

  validateSize(arr: FormArray ) {
    return arr.length <= 0 ? {
      invalidSize: true
    } : null;
  }

  validatePriorityDuplicate( control: FormControl ) {
    if (!control.value) return null;

    const tests = this.entranceTests.value as Array<any>;
    const priority = tests.map( item => {
      return item.priority;
    });

    return priority.indexOf(control.value) >= 0 ? { duplicatePriority: true } : null;

  }

  validateTestNameDuplicate(control: FormControl) {
    const tests = this.entranceTests.value as Array<any>;

    if (!control.value) return null;

    const duplicate = tests.filter(el => {
      if (el.test_name) {
        return el.test_name.toLowerCase().replace(/\s+/g, ' ').trim() ===
            control.value.toLowerCase().replace(/\s+/g, ' ').trim();
      }
    });

    return duplicate.length >= 1 ? { duplicateTestName: true } : null;
  }

  validateSubjectDuplicate(control: FormControl) {
    const tests = this.entranceTests.value as Array<any>;

    console.log('validateSubjectDuplicate', control.value, tests);

    if (!control.value) return null;

    const duplicate = tests.filter((el, i) => {
      console.log('validateSubjectDuplicate', this.entranceTests.controls[i] == control);
      return el.id_subject === control.value} );

    return duplicate.length >= 1 ? { duplicateSubject: true } : null;
  }



  addEducationPrograms() {
    this.educationPrograms.push(this.newEducationProgramsControl());

  }

  deleteEducationPrograms(index) {
    this.educationPrograms.removeAt(index);
    this.educationPrograms.updateValueAndValidity();
  }

  addEntranceTests() {
    this.entranceTests.push( this.newEntranceTestsControl( this.entranceTests.length ));
  }

  deleteEntranceTests(index) {
    this.entranceTests.removeAt(index);
    this.validateFormsControls();
  }


  newEducationProgramsControl() {
    return this.fb.group(
        {
          name: [null, Validators.required],
          uid:  [null]
        }
    );

  }

  newEntranceTestsControl(ind) {
    return this.fb.group( {
      id_entrance_test_type:  [null, Validators.required],
      id_subject:             [null, [
                                      Validators.required,
                                      RxwebValidators.unique()
                                      ]],
      priority:               [null, [
                                      Validators.required,
                                      Validators.min(1),
                                      Validators.max(10),
                                      RxwebValidators.unique()
                              ]],
      uid:                    [null],
      test_name:              [null, [  ]],
      min_score:              [null, Validators.required],
      is_ege:                 [true, Validators.required],
    });
  }

  save() {
    const data = {
      competitive: this.competitiveForm.getRawValue(),
      education_programs: this.educationPrograms.value,
      entrance_tests: this.entranceTests.value,
    };
    this.campaignService.addCompetitive(data)
        .subscribe( res => {
            if ( res.done ) {
              this.snackBar.open('Конкурсная группа успешно добавлена');
              this.router.navigate(['../', this.idCampaign, 'competitive'],{relativeTo: this.route.parent} );
            } else {
              this.snackBar.open('При попытке добавить данные произошла ошибка' + ((res.message) ? `: ${res.message}` : '') )
            }
        });
  }


  validForms() {
      return  !this.competitiveForm.pristine && this.competitiveForm.valid &&
          this.educationGroupForm.valid && this.entranceTestsGroupForm.valid;
  }

  changeEge($event, item: AbstractControl) {
    console.log('event', $event);
    if ($event === true) {
      // тип ЕГЭ
      item.get('test_name').setValidators([]);
      item.get('test_name').setValue(null);
      item.get('id_subject').setValidators([ RxwebValidators.unique(), Validators.required ]);

    } else {
      //тип не егэ
      item.get('id_subject').setValidators([]);
      item.get('id_subject').setValue(null);
      item.get('min_score').setValidators([ Validators.min(0), item.get('min_score').validator]);

      item.get('test_name').setValidators([RxwebValidators.unique(), Validators.required ]);
    }
    item.get('test_name').updateValueAndValidity();
    item.get('id_subject').updateValueAndValidity();
    item.get('min_score').updateValueAndValidity();
  }


  changeSubject(id: any, item: AbstractControl) {
    const subject = this.cls.subjects_no_ege.data.find( (element) => element.id === id);
    if ( subject ) {
      if ( subject.min_scope >= item.get('min_score').value) item.get('min_score').setValue(subject.min_scope);
      item.get('min_score').setValidators([
          Validators.min(subject.min_scope),
          item.get('min_score').validator
      ]);
    } else {
      item.get('min_score').setValidators([
        Validators.min(0),
        item.get('min_score').validator
      ]);
    }
    this.entranceTests.updateValueAndValidity();

  }

  ngOnDestroy() {
    console.log('unsubscribe');
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  private validateSubject(ind: number) {
    return (control: AbstractControl) => {
      console.log( ind, control );
      const tests = this.entranceTests.value as Array<any>;

      console.log('validateSubjectDuplicate', control.value, tests);

      if (!control.value) return null;

      const duplicate = tests.filter((el, i) => {
        console.log('validateSubjectDuplicate', i, ind);
        return el.id_subject === control.value && i !== ind; } );

      return duplicate.length >= 1 ? { duplicateSubject: true } : null;

    };
  }

  getForms() {
    console.log( this.entranceTestsGroupForm);
  }

  validateFormsControls(keys: Array<string> = [ 'test_name', 'id_subject', 'priority' ]) {

    for (let i = 0; i < this.entranceTests.length; i++ ) {
      let test = this.entranceTests.controls[i];

      console.log(test);
      keys.forEach( val => {
          test.get(val).updateValueAndValidity({emitEvent: true});
      });
    }
  }
}
