import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {CampaignService} from '@/cabinets/university/campaign-menu/campaign.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {distinctUntilChanged, switchMap, takeUntil} from 'rxjs/operators';
import {ClsService} from '@/common/services/cls.service';
import {OrgsService} from '@/cabinets/university/orgs-menu/orgs.service';
import {Subject} from 'rxjs';
import {ConfirmModalComponent} from '@art-lib/components/confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-competitive-edit',
  templateUrl: './competitive-edit.component.html',
  styleUrls: ['./competitive-edit.component.scss']
})
export class CompetitiveEditComponent implements OnInit, OnDestroy {

  readonly EDUC_SOURC_PAID = 3;

  idCampaign: number;
  idCompetitive: number;

  data: CompetitiveMain;

  competitiveForm: FormGroup;

  isLoad: boolean;

  private ngUnsubscribe: Subject<void> = new Subject<void>();
  educationLevelChange = new Subject<number>();

  cls = {
    education_level: {
      data: [],
      initFunc:  () => this.campaignService.getEducationLevelsByCampaignId( this.idCampaign ),
      isLoad: true
    },
    education_forms: {
      data: [],
      initFunc: () => this.campaignService.getEducationFormsByCampaignId( this.idCampaign ),
      isLoad: true
    },
    education_source: {
      data: [],
      initFunc: () => this.clsService.getClsData('education_sources'),
      isLoad: true
    },
    level_budget: {
      data: [],
      initFunc: () => this.clsService.getClsData('level_budget'),
      isLoad: true
    },
    direction: {
      data: [],
      initFunc: (idEducLevel?) => this.orgsService.getDirectionsSelectList('', {id_education_level: idEducLevel}),
      isLoad: true,
      search: ''
    },
  };

  constructor(private router: Router,
              private route: ActivatedRoute,
              private fb: FormBuilder,
              private campaignService: CampaignService,
              private clsService: ClsService,
              private orgsService: OrgsService,
              private snackBar: MatSnackBar,
              private dialog: MatDialog) { }

  ngOnInit() {
    this.route.params.subscribe(res => {
      this.idCompetitive = +res.id;
      this.loadInfo();
      this.initForm();
    });
  }

  loadInfo() {
    this.isLoad = true;
    this.campaignService.getCompetitiveInfoMain(this.idCompetitive)
        .subscribe(res => {
          if( res.done ) {
            this.data = res.data;
            this.idCampaign = this.data.id_campaign;
            this.fillForm();
            this.initCls();
            this.subscribeValueChanged();
          } else {
            this.snackBar.open('Произошла ошибка' + ( res.message ? ': ' + res.message : '' ) );
          }

          this.isLoad = false;
        });
  }

  initCls() {
    for (let key in this.cls) {
      if ( this.cls[key].isLoad ) {
        // console.log('initFunc', this.cls[key].initFunc);
        this.cls[key].initFunc()
            .pipe(  takeUntil(this.ngUnsubscribe) )
            .subscribe( res => {
              this.cls[key].data = res;
            });
      }
    }

  }

  initForm() {
    this.competitiveForm = this.fb.group( {
      name:                 [null, Validators.required ],
      uid:                  [null, Validators.maxLength(36)],
      id_education_level:   [null, Validators.required],
      id_education_source:  [null, Validators.required],
      id_level_budget:      [null, Validators.required],
      id_education_form:    [null, Validators.required],
      id_direction:         [null],
      number:               [null, [Validators.required,  Validators.pattern("^[0-9]*$")]],
      comment:              [null]
    });
  }

  fillForm() {
    for ( let key in this.competitiveForm.controls) {
      if (this.data.hasOwnProperty(key) ) {
        this.competitiveForm.get(key).setValue( this.data[key] );
      }
    }
  }

  subscribeValueChanged() {

    this.competitiveForm.controls['id_education_source'].valueChanges.subscribe( res => {
      if (res === this.EDUC_SOURC_PAID ) {
        this.competitiveForm.controls['id_level_budget'].setValue(null);
        this.competitiveForm.controls['id_level_budget'].setValidators([]);
      }
    });

    this.educationLevelChange.pipe(
        takeUntil(this.ngUnsubscribe),
        distinctUntilChanged(),
        switchMap((id: any) => {
          //console.log('term', id);
          return this.cls.direction.initFunc(id);
        })
    ).subscribe( res => {
      this.competitiveForm.controls['id_direction'].setValue(null);
      this.cls.direction.data = res;
    } );

  }


  cancel() {
    const dialog = this.dialog.open(ConfirmModalComponent, {
      data: {
        title: 'Сброс значений',
        content: 'Вы уверены, что хотите отменить изменения?'
      },

    });
    dialog.afterClosed().subscribe(res => {
      if (res) {
        this.fillForm();
        this.competitiveForm.reset(this.competitiveForm.value);
      }
    });
  }

  addData() {
    this.campaignService.editCompetitiveInfoMain(this.idCompetitive, this.competitiveForm.value)
        .subscribe( res => {
           if ( res.done ) {
             this.snackBar.open( 'Данные успешно сохранены');
             this.router.navigate( ['../'], { relativeTo: this.route} );
           } else {
             this.snackBar.open( 'Произошла ошибка' + ( res.message ? ': ' + res.message : '' ) );
           }
        });
  }

  ngOnDestroy() {
    console.log('unsubscribe');
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }


}
