import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable, Subject} from 'rxjs';
import {CampaignService} from '@/cabinets/university/campaign-menu/campaign.service';
import * as cloneDeep from 'lodash/cloneDeep';
import {debounceTime, distinctUntilChanged, switchMap, tap} from 'rxjs/operators';
import {untilDestroyed} from 'ngx-take-until-destroy';
import {Title} from '@angular/platform-browser';
import {artAnimations} from '@art-lib/animations';


@Component({
  selector: 'app-campaign-list',
  templateUrl: './campaign-list.component.html',
  styleUrls: ['./campaign-list.component.scss'],
  animations: artAnimations
})
export class CampaignListComponent implements OnInit, OnDestroy {

  isLoad = true;

  campaigns: Array<ICampaignItem>;

  pagination = new Subject<number>();

  defaultParams = {
    page: 1,
    limit: 10
  };

  /*
  * pagination params
  *  */
  params: Params = {
    ...this.defaultParams
  };
  total: number;
  pageToShow = 3;

  /* search */
  searchText = new Subject<string>();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private campaignService: CampaignService,
    private titleService: Title
  ) {

  }

  ngOnInit() {
    this.getQueryParamsFromUrl();
    this.subscribeOnChanges();
    this.getData();
    this.titleService.setTitle('Список приёмных кампаний');
  }

  subscribeOnChanges() {
    this.pagination.pipe(
      distinctUntilChanged(),
      debounceTime(200),
      tap((pageNumber: number) => {
        this.params.page = pageNumber;
      }),
      switchMap(() => this.getDataFromService()),
      untilDestroyed(this)
    ).subscribe(response => this.setData(response));

    this.searchText.pipe(
      distinctUntilChanged(),
      debounceTime(500),
      tap(() => {
        this.params.page = 1;
      }),
      switchMap(() => this.getDataFromService()),
      untilDestroyed(this)
    ).subscribe(response => this.setData(response));
  }

  setData(response) {
    if (response.done) {

      this.campaigns = response.data;
      this.params.page = response.paginator.page;
      this.params.limit = response.paginator.limit;
      this.total = response.paginator.total;

    }

    this.isLoad = false;
  }

  createNewCampaign() {
    this.router.navigate(['./new'], {relativeTo: this.route});
  }

  getData() {
    this.getDataFromService()
      .subscribe(response => this.setData(response));
  }


  getDataFromService(): Observable<any> {
    this.isLoad = true;
    const queryParams = this.getQueryParams(this.params);
    this.passQueryParamsToUrl(queryParams);
    return this.campaignService.getCampaignList(this.params);
  }

  passQueryParamsToUrl(queryParams) {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams
    });
  }

  getQueryParams(params) {
    const queryParams: any = {};
    Object.keys(params).forEach((key) => {
      if (this.defaultParams.hasOwnProperty(key) && (this.defaultParams[key] !== params[key] && params[key] !== '') ||
        (!this.defaultParams.hasOwnProperty(key) && params[key] !== '')) {
        if ((key === 'frDt' || key === 'toDt') && params.dateF !== 'FromTo') {
          return;
        }
        queryParams[key] = params[key];
      }
    });

    //console.log('queryparams', queryParams);
    return queryParams;
  }

  getQueryParamsFromUrl() {
    const queryParams = cloneDeep(this.route.snapshot.queryParams);
    this.setParams(queryParams);
  }

  setParams(params) {
    Object.keys(params).forEach(key => {
      switch (key) {
        case 'page':
          this.params[key] = +params[key];
          break;
        default:
          this.params[key] = params[key];
      }
    });
  }

  ngOnDestroy(): void {
  }

}

interface Params {
  page: number;
  limit: number;
  // dateF: string;
  search_name?: string;
  // frDt?: string;
  // toDt?: string;
  // published?: boolean;
}
