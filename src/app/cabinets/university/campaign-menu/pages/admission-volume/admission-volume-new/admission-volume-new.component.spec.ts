import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmissionVolumeNewComponent } from './admission-volume-new.component';

describe('AdmissionVolumeNewComponent', () => {
  let component: AdmissionVolumeNewComponent;
  let fixture: ComponentFixture<AdmissionVolumeNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdmissionVolumeNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmissionVolumeNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
