import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CampaignService} from '@/cabinets/university/campaign-menu/campaign.service';
import {ClsService} from '@/common/services/cls.service';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {BehaviorSubject, Subject, timer} from 'rxjs';
import {debounce, debounceTime, distinctUntilChanged, switchMap, takeUntil} from 'rxjs/operators';
import {MatSnackBar} from '@angular/material/snack-bar';
import {OrgsService} from '@/cabinets/university/orgs-menu/orgs.service';

@Component({
  selector: 'app-admission-volume-new',
  templateUrl: './admission-volume-new.component.html',
  styleUrls: ['./admission-volume-new.component.scss']
})
export class AdmissionVolumeNewComponent implements OnInit {

  idCampaign: number;

  admissionForm: FormGroup;

  searchOrg = new FormControl('');

  items: any;

  clsSearch = [];

  private ngUnsubscribe: Subject<void> = new Subject<void>();

  search = {
      campaign: '',
      items: []
  };

  cls = {
      campaign : {
          data: []
      },

    education_level: {
      data: []
    },

    items: []
  };



  constructor(private router: Router,
              private route: ActivatedRoute,
              private campaignService: CampaignService,
              private orgsService: OrgsService,
              private clsService: ClsService,
              private fb: FormBuilder,
              private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.route.queryParams.subscribe( res => {
        this.idCampaign = +res.id_campaign;
        this.loadCompanyList();

        if ( this.idCampaign ) {
           this.initEducationLevel(this.idCampaign );
       }

        this.initForm();
    } );

    this.clsSearch.push( this.addBehaviorSubject() );
    this.cls.items.push( this.addCls(0) );
    this.search.items.push( this.addSearch());

    this.searchOrg.valueChanges
          .pipe(
              distinctUntilChanged(),
              debounceTime(600))
          .subscribe((val) => {
              this.loadCompanyList(val);
          });
  }

  initForm() {
    this.admissionForm = this.fb.group( {
          id_campaign: [ this.idCampaign, Validators.required ],
          data: this.fb.array( [this.addGroup()], this.validateSize)
    });

    this.items = this.admissionForm.get('data') as FormArray;

    console.log('items', this.items);


    this.admissionForm.controls['id_campaign'].valueChanges.subscribe( res => {
        this.router.navigate(['.'], {relativeTo: this.route, queryParams: {id_campaign: res}});
        /*this.initEducationLevel(res);*/
    });

  }

  loadCompanyList(search = '') {
      this.clsService.getCampaignList(search)
          .pipe( takeUntil(this.ngUnsubscribe))
          .subscribe( res => {
                this.cls.campaign.data = res;
                console.log(this.cls.campaign);
          } );
  }

  initEducationLevel(idCampaign) {
    this.campaignService.getEducationLevelsByCampaignId( idCampaign )
        .subscribe( res => {
          console.log('res', res);
           this.cls.education_level.data = res;
        });
  }

  validateSize(arr: FormArray) {
    return arr.length <= 0 ? {
      invalidSize: true
    } : null;
  }

  addGroup() {
    return this.fb.group({
      id_education_level: [ null, Validators.required],
      id_group_speciality: [ null, Validators.required ],
      id_speciality: [ null, Validators.required ]
    });
  }

  addCls(i) {
    return {
      group_speciality$: this.clsSearch[i].group_speciality.pipe(
            distinctUntilChanged(),
            switchMap((id: number) => {
                  return this.orgsService.getDirectionsParentSelectList( '' , { id_education_level: id});
                }
            )),
      speciality$: this.clsSearch[i].speciality.pipe(
            distinctUntilChanged(),
            switchMap((id: number) => {
                  return this.orgsService.getDirectionsSelectList('', { id_parent: id});
                }
            )),
    };
  }
  private addSearch() {
     return {
         group_speciality: '',
         speciality: ''
     };
  }

  addBehaviorSubject() {
    return {
      group_speciality: new Subject<number>(),
      speciality: new Subject<number>()
    };
  }

  addContorls() {
    this.items.push(this.addGroup());
    this.clsSearch.push(this.addBehaviorSubject());
    this.cls.items.push(this.addCls(this.clsSearch.length - 1));
    this.search.items.push( this.addSearch() );
  }

  editEducation(id, i) {
    this.items.controls[i].controls['id_group_speciality'].setValue(null);
    this.clsSearch[i].group_speciality.next(id);
  }

  editGroupspeciality(id, i) {
    this.items.controls[i].controls['id_speciality'].setValue(null);
    this.clsSearch[i].speciality.next(id);
  }

  save() {
    const data = this.admissionForm.getRawValue();



    this.campaignService.addAdmissionVolume(data)
        .subscribe( res => {
          if (res.done) {
              this.snackBar.open('Направление подготовки успешно добавлены');
              this.router.navigate([`../${this.idCampaign}/admission/`], {relativeTo: this.route.parent});
          } else {
              this.snackBar.open('При попытке добавить данные произошла ошибка' + ((res.message) ? `: ${res.message}` : '') );
          }
        } );
  }

    del(i: number) {
      this.clsSearch.splice(i, 1);
      this.cls.items.splice(i, 1);
      this.search.items.splice(i, 1);
      this.items.removeAt(i);
      //console.log(this.items, this.admissionForm.controls);
    }

}
