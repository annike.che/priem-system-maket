import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmissionVolumeEditComponent } from './admission-volume-edit.component';

describe('AdmissionVolumeEditComponent', () => {
  let component: AdmissionVolumeEditComponent;
  let fixture: ComponentFixture<AdmissionVolumeEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdmissionVolumeEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmissionVolumeEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
