import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmissionVolumeDetailComponent } from './admission-volume-detail.component';

describe('AdmissionVolumeDetailComponent', () => {
  let component: AdmissionVolumeDetailComponent;
  let fixture: ComponentFixture<AdmissionVolumeDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdmissionVolumeDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmissionVolumeDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
