import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdmissionVolumeDetailComponent } from './admission-volume-detail/admission-volume-detail.component';
import { AdmissionVolumeEditComponent } from './admission-volume-edit/admission-volume-edit.component';
import { AdmissionVolumeNewComponent } from './admission-volume-new/admission-volume-new.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '@/common/modules/shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ArtLibModule} from '@art-lib/art-lib.module';
import {EditUidModule} from '@/cabinets/university/campaign-menu/components/_edit-dialog/edit-uid/edit-uid.module';

const routes: Routes = [
    { path: '', redirectTo: 'new', pathMatch: 'full' },
    {path: 'new', component: AdmissionVolumeNewComponent },
    {path: ':id', component: AdmissionVolumeDetailComponent },
    {path: ':id/edit', component: AdmissionVolumeEditComponent },
];

@NgModule({
  declarations: [
      AdmissionVolumeDetailComponent,
      AdmissionVolumeEditComponent,
      AdmissionVolumeNewComponent
  ],
	imports: [
		CommonModule,
		RouterModule.forChild(routes),
		SharedModule,
		ReactiveFormsModule,
		FormsModule,
		ArtLibModule,
    EditUidModule
	]
})
export class AdmissionVolumeModule { }
