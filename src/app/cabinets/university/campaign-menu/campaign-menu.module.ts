import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import { CampaignListComponent } from './pages/campaign-list/campaign-list.component';
import { CampaignNewComponent } from './pages/campaign/campaign-new/campaign-new.component';
import {CampaignEditComponent} from './pages/campaign/campaign-edit/campaign-edit.component';
import {SharedModule} from '@/common/modules/shared.module';
import {CampaignItemModule} from '@/cabinets/university/campaign-menu/components/campaign-item/campaign-item.module';
import {ArtPaginationModule} from '@art-lib/components/pagination/pagination.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgxMaskModule} from 'ngx-mask';
import { EditBudgetLevelsComponent } from './components/edit-budget-levels/edit-budget-levels.component';
import {ArtLibModule} from '@art-lib/art-lib.module';

const routes: Routes = [
  { path: '', component: CampaignListComponent },
  {
    path: 'new',
    component: CampaignNewComponent
  },
  {
    path: ':id',
    loadChildren: () => import('./pages/campaign/campaign-info/campaign-info.module').then( m => m.CampaignInfoModule)
  },
  {
    path: ':id/edit',
    component: CampaignEditComponent
  },
  {
    path: 'achievement',
    loadChildren: () => import('./pages/achievement/achievement.module').then( m => m.AchievementModule)
  },
  {
    path: 'admission-volume',
    loadChildren: () => import ('./pages/admission-volume/admission-volume.module').then( m => m.AdmissionVolumeModule )
  },
  {
    path: 'competitive',
    loadChildren: () => import('./pages/competitive/competitive.module').then( m => m.CompetitiveModule )
  }


];



@NgModule({
  declarations: [
      CampaignListComponent,
      CampaignEditComponent,
      CampaignNewComponent,
      EditBudgetLevelsComponent,
  ],
    imports: [
        SharedModule,
        RouterModule.forChild(routes),
        CampaignItemModule,
        ArtPaginationModule,
        FormsModule,
        ReactiveFormsModule,
        NgxMaskModule.forRoot(),
        ArtLibModule
    ]
})
export class CampaignMenuModule { }
