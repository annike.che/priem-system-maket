import {Component, OnDestroy, OnInit} from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ActivatedRoute, Router} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {OrdersService} from '@/cabinets/university/orders-menu/orders.service';
import {Observable, Subject} from 'rxjs';
import {PageEvent} from '@angular/material/paginator';
import {debounceTime, distinctUntilChanged, switchMap, tap} from 'rxjs/operators';
import {untilDestroyed} from 'ngx-take-until-destroy';
import {SortDirection} from '@angular/material/sort';
import * as cloneDeep from 'lodash/cloneDeep';
import {artAnimations} from '@art-lib/animations';

@Component({
  selector: 'app-orders-list',
  templateUrl: './orders-list.component.html',
  styleUrls: ['./orders-list.component.scss'],
  animations: artAnimations
})
export class OrdersListComponent implements OnInit, OnDestroy {

    data: Array<OrderItem> = [];
    displayedColumns: Array<string>;
    searchColumnsHeader: Array<string>;
    pageSizeOptions: number[];

    isLoad: boolean;


    defaultParams = {
      page: 1,
      limit: 20
    };

    search = new Subject<Params>();
    sortEvent = new Subject<SortEvent>();
    pagination = new Subject<PageEvent>();

    total: number;

    params: Params = {
      ...this.defaultParams
    };

  constructor(
      private ordersService: OrdersService,
      private snackBar: MatSnackBar,
      private route: ActivatedRoute,
      private router: Router,
      private titleService: Title
  ) {
    this.titleService.setTitle('Приказы');

    this.displayedColumns = ['id', 'campaign', 'orderName', 'orderDate', 'published',
      'educationLevelName', 'educationFormName', 'educationSourceName',
      'foreigners', 'admissionType', 'admissionStatus', 'uid', 'actions'];

    this.searchColumnsHeader = ['idSearch', 'campaignSearch', 'orderNameSearch', 'idSearch', 'idSearch',
      'educationLevelNameSearch', 'educationFormNameSearch', 'educationSourceNameSearch',
      'idSearch', 'idSearch', 'idSearch', 'uidSearch', 'actionsSearch'
    ];

    this.pageSizeOptions = [5, 10, 15, 20, 30, 50, 100];

  }

  ngOnInit() {
    this.getQueryParamsFromUrl();
    this.subscribeOnChanges();
    this.loadData();
  }

  subscribeOnChanges() {

    this.search.pipe(
        // distinctUntilChanged(),
        debounceTime(500),
        tap(() => {
          this.params.page = 1;
        }),
        switchMap((value, index) => {
          // console.log('value', value);
          return this.getDataFromService();
        }),
        untilDestroyed(this)
    ).subscribe(response => this.setData(response));

    this.pagination.pipe(
        debounceTime(200),
        tap((event: PageEvent) => {
          if (this.params.limit !== event.pageSize) {
            this.params.limit = event.pageSize;
            this.params.page = 1;
          }
          if (this.params.page !== event.pageIndex + 1) {
            this.params.page = event.pageIndex + 1;
          }
        }),
        switchMap(() => this.getDataFromService()),
        untilDestroyed(this)
    )
        .subscribe(response => this.setData(response));


    this.sortEvent.pipe(
        debounceTime(200),
        tap((event: SortEvent) => {
          this.params.order = event.direction as SortDirection;
          this.params.sortby = event.active;
        }),
        switchMap(() => this.getDataFromService()),
        untilDestroyed(this)
    )
        .subscribe(response => this.setData(response));

  }

  loadData() {
    this.getDataFromService()
        .subscribe(response => this.setData(response));
  }

  setData(response) {
    if (response.done) {

      this.data = response.data;
      this.params.page = response.paginator.page;
      this.params.limit = response.paginator.limit;
      this.total = response.paginator.total;

    }
    this.isLoad = false;
  }


  getQueryParamsFromUrl() {
    const queryParams = cloneDeep(this.route.snapshot.queryParams);
    this.setParams(queryParams);
  }

  setParams(params) {
    Object.keys(params).forEach(key => {
      switch (key) {
        case 'page':
          this.params[key] = +params[key];
          break;
        default:
          this.params[key] = params[key];
      }
    });
  }

  passQueryParamsToUrl(queryParams) {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams
    });
  }

  getDataFromService(): Observable<any> {
    this.isLoad = true;
    const queryParams = this.getQueryParams(this.params);
    this.passQueryParamsToUrl(queryParams);
    return this.ordersService.getOrdersList(this.params);
  }

  getQueryParams(params) {
    const queryParams: any = {};
    Object.keys(params).forEach((key) => {
      if (this.defaultParams.hasOwnProperty(key) && (this.defaultParams[key] !== params[key] && params[key] !== '') ||
          (!this.defaultParams.hasOwnProperty(key) && params[key] !== '')) {
        queryParams[key] = params[key];
      }
    });

    return queryParams;
  }

  ngOnDestroy(): void {

  }

  clearFilter($event: MouseEvent) {
    delete this.params.search_name_campaign;
    delete this.params.search_order_name;
    delete this.params.search_name_education_level;
    delete this.params.search_name_education_form;
    delete this.params.search_name_education_source;
    delete this.params.search_uid;


    this.search.next(this.params);
  }
}

interface Params {
  page: number;
  limit: number;
  order?: SortDirection; // 'ASC' || 'DESC'
  sortby?: string;

  search_name_campaign?: string;
  search_order_name?: string;
  search_name_education_level?: string;
  search_name_education_form?: string;
  search_name_education_source?: string;
  search_uid?: string;
}
