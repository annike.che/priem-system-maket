import {AfterViewChecked, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatTabNav} from '@angular/material/tabs';

@Component({
  selector: 'app-orders-detail',
  templateUrl: './orders-detail.component.html',
  styleUrls: ['./orders-detail.component.scss']
})
export class OrdersDetailComponent implements OnInit, AfterViewChecked {

  tabs = [
    {
      path: './main',
      label: 'Основные данные'
    },
    {
      path: './applications',
      label: 'Список заявлений'
    },
  ];

  @ViewChild('navs', {static: false}) navs: MatTabNav;

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewChecked(): void {
    // ререндер нижнего подчеркивания в табах, чтобы не съезжал
    setTimeout(() => this.navs._alignInkBarToSelectedTab(), 0);
  }

}
