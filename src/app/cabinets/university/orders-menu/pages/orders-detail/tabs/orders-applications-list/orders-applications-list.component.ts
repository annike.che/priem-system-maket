import {Component, OnDestroy, OnInit} from '@angular/core';
import {artAnimations} from '@art-lib/animations';
import {debounceTime, switchMap, tap} from 'rxjs/operators';
import {Title} from '@angular/platform-browser';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpResponse} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {MatDialog} from '@angular/material/dialog';
import {SortDirection} from '@angular/material/sort';
import {untilDestroyed} from 'ngx-take-until-destroy';
import {PageEvent} from '@angular/material/paginator';
import {ApplicationTableListSettingsComponent} from '@/cabinets/university/application-menu/components/application-table-list-settings/application-table-list-settings.component';
import {MatSnackBar} from '@angular/material/snack-bar';
import * as cloneDeep from 'lodash/cloneDeep';
import {OrdersService} from '@/cabinets/university/orders-menu/orders.service';

@Component({
  selector: 'app-orders-applications-list',
  templateUrl: './orders-applications-list.component.html',
  styleUrls: ['./orders-applications-list.component.scss'],
  animations: artAnimations
})
export class OrdersApplicationsListComponent implements OnInit, OnDestroy {

  applications: Array<IApplication>;

  idOrder: number;

  isLoad = true;

  // отображаемые колонки
  columnsHeader = ['number', 'fullname', 'snils',
    'nameCompetitive', 'stateName', 'registrationDate', 'changedDate',
    'original', 'agreed', 'agreedDate', 'epgu', 'action'];

  searchColumnsHeader = ['numberSearch', 'fullnameSearch', 'snilsSearch',
    'nameCompetitiveSearch', 'stateNameSearch', 'registrationDateSearch', 'changedDateSearch',
    'originalSearch', 'agreedSearch', 'agreedDateSearch', 'epguSearch', 'actionSearch'];
  total: number;

  defaultParams = {
    page: 1,
    limit: 20
  };

  search = new Subject<Params>();

  params: Params = {
    ...this.defaultParams
  };

  pagination = new Subject<PageEvent>();
  sortEvent = new Subject<SortEvent>();
  pageSizeOptions: number[];

  constructor(
      private ordersService: OrdersService,
      private titleService: Title,
      private router: Router,
      private route: ActivatedRoute,
      public dialog: MatDialog,
      private snackBar: MatSnackBar
  ) {
    this.pageSizeOptions = [5, 10, 15, 20, 30, 50, 100];
  }

  ngOnInit() {
    this.route.parent.params.subscribe( res => {
      this.idOrder = +res['id'];
      if( this.idOrder ) {
        this.getQueryParamsFromUrl();
        this.getApplicationList();
        this.subscribeOnChanges();
      }
    } );

  }

  subscribeOnChanges() {
    this.search.pipe(
        // distinctUntilChanged(),
        debounceTime(500),
        tap(() => {
          this.params.page = 1;
        }),
        switchMap((value, index) => {
          // console.log('value', value);
          return this.getDataFromService();
        }),
        untilDestroyed(this)
    ).subscribe(response => this.setData(response));

    this.pagination.pipe(
        debounceTime(200),
        tap((event: PageEvent) => {
          if (this.params.limit !== event.pageSize) {
            this.params.limit = event.pageSize;
            this.params.page = 1;
          }
          if (this.params.page !== event.pageIndex + 1) {
            this.params.page = event.pageIndex + 1;
          }
        }),
        switchMap(() => this.getDataFromService()),
        untilDestroyed(this)
    )
        .subscribe(response => this.setData(response));

    this.sortEvent.pipe(
        debounceTime(200),
        tap((event: SortEvent) => {
          this.params.order = event.direction as SortDirection;
          this.params.sortby = event.active;
        }),
        switchMap(() => this.getDataFromService()),
        untilDestroyed(this)
    )
        .subscribe(response => this.setData(response));
  }

  getDataFromService(): Observable<any> {
    this.isLoad = true;
    const queryParams = this.getQueryParams(this.params);
    this.passQueryParamsToUrl(queryParams);
    return this.ordersService.getApplicationsList(this.idOrder, this.params);
  }

  getQueryParamsFromUrl() {
    const queryParams = cloneDeep(this.route.snapshot.queryParams);
    this.setParams(queryParams);
  }

  setParams(params) {
    Object.keys(params).forEach(key => {
      switch (key) {
        case 'page':
        case 'limit':
          this.params[key] = +params[key];
          break;

        default:
          this.params[key] = params[key];
      }
    });
  }


  getQueryParams(params) {
    const queryParams: any = {};
    Object.keys(params).forEach((key) => {
      if (this.defaultParams.hasOwnProperty(key) && (this.defaultParams[key] !== params[key] && params[key] !== '') ||
          (!this.defaultParams.hasOwnProperty(key) && params[key] !== '')) {
        queryParams[key] = params[key];
      }
    });

    return queryParams;
  }

  passQueryParamsToUrl(queryParams) {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams
    });
  }

  setData(response) {
    if (response.done) {
      this.applications = response.data;
      this.params.page = response.paginator.page;
      this.params.limit = response.paginator.limit;
      this.total = response.paginator.total;

    }
    this.isLoad = false;
  }

  getApplicationList() {
    this.getDataFromService()
        .subscribe(response => this.setData(response));
  }

  ngOnDestroy(): void {
  }

  clearFilter($event) {
    delete this.params.search_number;
    delete this.params.search_entrant_fullname;
    delete this.params.search_snils;
    delete this.params.search_uid_epgu;

    this.search.next(this.params);
  }

}

interface Params {
  page: number;
  limit: number;
  search_number?: string;
  search_entrant_fullname?: string;
  search_snils?: string;
  search_uid_epgu?: string;
  order?: SortDirection; // 'ASC' || 'DESC'
  sortby?: string;
}
