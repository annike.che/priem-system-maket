import { Component, OnInit } from '@angular/core';
import {OrdersService} from '@/cabinets/university/orders-menu/orders.service';
import {ActivatedRoute, Router} from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';
import {artAnimations} from '@art-lib/animations';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-orders-detail-main',
  templateUrl: './orders-detail-main.component.html',
  styleUrls: ['./orders-detail-main.component.scss'],
  animations: artAnimations
})
export class OrdersDetailMainComponent implements OnInit {

  data: OrderItem;

  idOrder: number;

  isLoad: boolean;

  constructor(
      private ordersService: OrdersService,
      private route: ActivatedRoute,
      private router: Router,
      private snackBar: MatSnackBar,
      private titleService: Title
  ) {
    this.isLoad = false;
    this.titleService.setTitle('Детализация приказа о зачислении');

  }

  ngOnInit() {
      this.route.parent.params.subscribe( res => {
        this.idOrder = +res['id'];
        if( this.idOrder ) {
          this.initData();
        }
      } );
  }

  initData() {
    this.isLoad = true;
    this.ordersService.getOrderDetail(this.idOrder)
        .subscribe( res => {
            if( res.done ) {
              this.data = res.data;
            } else {
              this.snackBar.open('Произошла ошибка' + ( res.message ? ': ' + res.message : '' ) );
            }
            this.isLoad = false;
        });
  }

}
