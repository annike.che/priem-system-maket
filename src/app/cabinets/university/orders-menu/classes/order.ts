interface OrderItem {
  id: number;
  id_campaign: number;
  name_campaign: string;

  id_education_form: number;
  name_education_form: string;

  id_education_level: number;
  name_education_level: string;

  id_education_source: number;
  name_education_source: string;

  order_name: string;
  order_date: string;
  published: string;

  foreigners: boolean; //Признак приказа для иностранцев по межпрофильным соглашениям
  preferential_order: boolean;

  id_order_admission_status: number;
  name_order_admission_status: string;

  id_order_admission_type: number;
  name_order_admission_type: string;

  actual: boolean;
  created: string;

  uid: string | null;
}
