import { NgModule } from '@angular/core';
import { OrdersListComponent } from './pages/orders-list/orders-list.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '@/common/modules/shared.module';
import {ArtLibModule} from '@art-lib/art-lib.module';
import {ReactiveFormsModule} from '@angular/forms';
import {OrdersDetailMainComponent} from '@/cabinets/university/orders-menu/pages/orders-detail/tabs/orders-detail-main/orders-detail-main.component';
import {OrdersDetailComponent} from '@/cabinets/university/orders-menu/pages/orders-detail/orders-detail.component';
import {OrdersApplicationsListComponent} from '@/cabinets/university/orders-menu/pages/orders-detail/tabs/orders-applications-list/orders-applications-list.component';
import {ApplicationTableListSettingsComponent} from '@/cabinets/university/application-menu/components/application-table-list-settings/application-table-list-settings.component';

const routes: Routes = [
  {path: '', component: OrdersListComponent},
  {path: ':id', component: OrdersDetailComponent,
    children: [
      { path: '', redirectTo: 'main', pathMatch: 'full' },
      { path: 'main', component: OrdersDetailMainComponent },
      { path: 'applications', component: OrdersApplicationsListComponent },
      {path: '**', redirectTo: ''}
    ]},
  {path: '**', redirectTo: ''}
];

@NgModule({
  declarations: [
    OrdersListComponent,
    OrdersDetailComponent,
    OrdersApplicationsListComponent,
    OrdersDetailMainComponent
  ],
  imports: [
    SharedModule,
    RouterModule.forChild(routes),
    ArtLibModule,
    ReactiveFormsModule,
  ]
})
export class OrdersMenuModule { }
