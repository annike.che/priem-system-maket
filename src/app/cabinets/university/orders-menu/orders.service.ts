import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {
  urlPrefix: string;
  urlGroup = '/orders';

  constructor(private httpClient: HttpClient) {
    this.urlPrefix = environment.apiUrl;
  }

  getOrdersList(params?): Observable<any> {
    const url = `${this.urlPrefix}${this.urlGroup}/list`;
    console.log('paramsData', params);
    return this.httpClient.get(url, { params });
  }

  getOrderDetail(id): Observable<any> {
    const url = `${this.urlPrefix}${this.urlGroup}/${id}/main`;
    return this.httpClient.get(url);
  }

  getApplicationsList(idOrder, params?): Observable<any> {
    const url = `${this.urlPrefix}${this.urlGroup}/${idOrder}/applications`;
    console.log('paramsData', params);
    return this.httpClient.get(url, { params });
  }
}
