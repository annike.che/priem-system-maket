import {Component, OnDestroy, OnInit} from '@angular/core';
import {NewsService} from '@/cabinets/university/news-menu/news.service';
import {artAnimations} from '@art-lib/animations';
import {Observable, Subject} from 'rxjs';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {debounceTime, distinctUntilChanged, switchMap, tap} from 'rxjs/operators';
import {untilDestroyed} from 'ngx-take-until-destroy';
import * as cloneDeep from 'lodash/cloneDeep';
import {DocsService} from '@/common/services/docs.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-news-list',
  templateUrl: './news-list.component.html',
  styleUrls: ['./news-list.component.scss'],
  animations: artAnimations
})
export class NewsListComponent implements OnInit, OnDestroy {
  news: any = [];
  pageSizeOptions: number[];

  isLoad: boolean = true;
  id: any;
  file: any;

  fileData: any;

  defaultParams = {
    page: 1,
    limit: 6
  };
  campaigns: Array<ICampaignItem>;
  pagination = new Subject<number>();

  /*
  * pagination params
  *  */
  params: Params = {
    ...this.defaultParams
  };
  total: number;
  pageToShow = 3;


  constructor(private newsService: NewsService,
              private route: ActivatedRoute,
              private docsService: DocsService,
              private _snackBar: MatSnackBar,
              private router: Router) {
    this.pageSizeOptions = [6, 12, 24];
  }

  ngOnInit() {
    this.getQueryParamsFromUrl();
    this.subscribeOnChanges();
    this.getNews();
  }

  getQueryParamsFromUrl() {
    const queryParams = cloneDeep(this.route.snapshot.queryParams);
    this.setParams(queryParams);
  }

  getDataFromService(): Observable<any> {
    const queryParams = this.getQueryParams(this.params);
    this.passQueryParamsToUrl(queryParams);
    return this.newsService.getNewsList(this.params);
  }

  subscribeOnChanges() {
    this.pagination.pipe(
      distinctUntilChanged(),
      debounceTime(200),
      tap((pageNumber: number) => {
        this.params.page = pageNumber;
      }),
      switchMap(() => this.getDataFromService()),
      untilDestroyed(this)
    ).subscribe(response => this.setData(response));

  }

  passQueryParamsToUrl(queryParams) {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams
    });
  }

  setData(response) {
    if (response.done) {
      this.news = response.data;
      this.params.page = response.paginator.page;
      this.params.limit = response.paginator.limit;
      this.total = response.paginator.total;

      console.log(response.data);

    }
    this.isLoad = false;
  }

  setParams(params) {
    Object.keys(params).forEach(key => {
      switch (key) {
        case 'page':
          this.params[key] = +params[key];
          break;
        default:
          this.params[key] = params[key];
      }
    });
  }

  getQueryParams(params) {
    const queryParams: any = {};
    Object.keys(params).forEach((key) => {
      if (this.defaultParams.hasOwnProperty(key) && (this.defaultParams[key] !== params[key] && params[key] !== '') ||
        (!this.defaultParams.hasOwnProperty(key) && params[key] !== '')) {
        if ((key === 'frDt' || key === 'toDt') && params.dateF !== 'FromTo') {
          return;
        }
        queryParams[key] = params[key];
      }
    });
    return queryParams;
  }

  getNews() {
    this.getDataFromService()
      .subscribe(response => {
        this.setData(response);
      });
  }

  initFileData(id) {
    this.newsService.getDocFile(id)
      .subscribe(res => {
        console.log('res', res);
        if (res.done) {
          this.fileData = res.data;
          console.log('file', this.fileData);
          this.openFile(this.fileData);
        } else {
          this._snackBar.open('Ошибка! Возможно, файл не существует или был перемещен в другую директорию');
        }
      });
  }

  openFile(file) {
    if (file.type === '.pdf') {
      const pdfWindow = window.open('', '_self');
      pdfWindow.document.write('<html><head><title>' + file.title + '</title></head><body style="margin:0px">' +
        '<iframe style="border-width: 0px;margin: 0px;" width="100%" height="100%" src="data:application/pdf;base64,' +
        encodeURI(file.content) + '"></iframe>');
    } else {

      const linkSource = `data:application/${file.type};base64,${file.content}`;
      const downloadLink = document.createElement('a');
      const fileName = file.title;

      downloadLink.href = linkSource;
      downloadLink.download = fileName;
      downloadLink.click();
    }
  }

  ngOnDestroy(): void {
  }
}
