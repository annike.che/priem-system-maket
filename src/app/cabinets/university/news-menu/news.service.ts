import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NewsService {
  urlPrefix: string;
  urlGroup = '/new';

  constructor(private httpClient: HttpClient) {
    this.urlPrefix = environment.apiUrl;
  }

  getNewsList(params?) {
    const url = `${this.urlPrefix}${this.urlGroup}/list`;
    return this.httpClient.get(url, { params });
  }

  getDocFile(id): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/file/${id}`;
    return this.httpClient.get(url);
  }

  // получение детальной информации о новости

  getById(id):Observable<any> {
    const url = `${this.urlPrefix}${this.urlGroup}/${id}/main`;
    return this.httpClient.get(url)
      .pipe(map((item: any) => {
        return item;
      }))
  }

}
