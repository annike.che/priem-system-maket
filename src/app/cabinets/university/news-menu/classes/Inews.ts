interface INews {
  id: number;
  title: string;
  date_news: string;
  content: string;
  published: boolean;
  created: string;
  deleted: boolean;
  files: Array<any> | null;
}
