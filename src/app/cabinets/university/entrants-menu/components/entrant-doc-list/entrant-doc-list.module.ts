import { NgModule } from '@angular/core';
import {EntrantDocListComponent} from '@/cabinets/university/entrants-menu/components/entrant-doc-list/entrant-doc-list.component';
import {SharedModule} from '@/common/modules/shared.module';
import {DynamicFormsModule} from '@/common/modules/dynamic-forms/dynamic-forms.module';
import {DocsInfoModule} from '@/cabinets/university/entrants-menu/components/docs-info/docs-info.module';
import {ArtLibModule} from '@art-lib/art-lib.module';
import {DocsEditModule} from '@/cabinets/university/entrants-menu/components/docs-edit/docs-edit.module';



@NgModule({
  declarations: [ EntrantDocListComponent ],
  imports: [
    SharedModule,
    ArtLibModule,
    DynamicFormsModule,
    DocsInfoModule,
    DocsEditModule
  ],
  exports: [ EntrantDocListComponent ]
})
export class EntrantDocListModule { }
