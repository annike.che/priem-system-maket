import {ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DocsInfoComponent} from '@/cabinets/university/entrants-menu/components/docs-info/docs-info.component';
import {MatDialog} from '@angular/material/dialog';
import {MatCheckboxChange} from '@angular/material/checkbox';
import {artAnimations} from '@art-lib/animations';
import {DocsEditComponent} from '@/cabinets/university/entrants-menu/components/docs-edit/docs-edit.component';


@Component({
  selector: 'app-entrant-doc-list',
  templateUrl: './entrant-doc-list.component.html',
  styleUrls: ['./entrant-doc-list.component.scss'],
  animations: artAnimations
})
export class EntrantDocListComponent implements OnInit {

  _docs: Array<DocsListEntrant> = [];
  get docs(): Array<DocsListEntrant> {
    return this._docs;
  }

  @Input('docs')
  set docs(value: Array<DocsListEntrant>) {
    console.log('value', value);
    this._docs = [...value];
    //this.changeDetectorRefs.detectChanges();
  }
  /*
  * Наличие чекбокса
  * */

  @Input() hasSelected = false;

  @Input() selectedDoc = [];

  @Output() selectedDocChange = new EventEmitter();
  @Output() valueChanges = new EventEmitter();

  /*
  * Максимальное количество выбранных документов
  * */

  @Input() maxCountSelected: number;

  /*
  * Документы, которые должны быть заблокированы (нельзя выделять)
  * */

  @Input() hasDisabled: Array<DocsListEntrant>;

  _disabledItems = {};
  get disabledItems(): any {
    return this._disabledItems;
  }

  @Input('disabledItems')
  set disabledItems(value: any) {
    console.log('_disabledItems', value);
    if (this.hasDisabled) {
      this._disabledItems = {...value};
      this.setDisabledParams();
    }
  }

  /*
  *  Возможность удалять элементы
  * */

  @Input() delete = false;
  @Output() deleteEvent = new EventEmitter<DeleteDocInfo>();

  /*
  *  Возможность Редактировать элементы
  * */
  @Input() canEdit = false;
  @Output() editEvent = new EventEmitter();


  baseHeaderColumns = ['name', 'issueDate', 'checked'];
  defaultHeaderColumns =  ['series', 'number'];
  customHeaderColumns = {
    ege: ['nameSubject', 'mark'],
    disability: ['number'],
    compatriot: [],
    composition: []
  };

  endCountColumns = 1;

  headerColumns = {};

  maxCountCheckDone = false;

  constructor (
      public dialog: MatDialog,
      private changeDetectorRefs: ChangeDetectorRef
  ) {


  }

  ngOnInit() {


    if ( this.hasSelected ) {
      this.baseHeaderColumns.unshift('checkBox');
      this.checkCountSelected();
    }
    if ( this.delete ) {
      this.baseHeaderColumns.push('delete');
      this.endCountColumns += 1;
    }
    if(this.canEdit) {
      this.baseHeaderColumns.push('edit');
      this.endCountColumns += 1;
    }


   // console.log('this.baseHeaderColumns', this.baseHeaderColumns);
  }

  getColumnsHeader(categoryCode): Array<string> {
    let returnColumns = [...this.baseHeaderColumns];

   // console.log('returnColumns', categoryCode, returnColumns);

    let additionalcCol = this.customHeaderColumns.hasOwnProperty(categoryCode) ?
        this.customHeaderColumns[categoryCode] : this.defaultHeaderColumns;

    returnColumns.splice(returnColumns.length - this.endCountColumns, 0, ...additionalcCol);

   // console.log('returnColumns', categoryCode, returnColumns);

    this.headerColumns[categoryCode] = returnColumns;

    return returnColumns;
  }

  getDocInfo(formname: any, id: any) {
    const dialogRef = this.dialog.open(DocsInfoComponent, {
      data: { formName: formname, id}, minWidth: '80%'
    }).afterClosed().subscribe(result => {
    });
  }

  checkDoc($event: MatCheckboxChange, item: { id: any; type: any, name_category: any, name_type: any }) {
    if ( $event.checked ) {

      if( !this.maxCountCheckDone ) {
        this.selectedDoc.push(item);
      }

    } else {
      this.selectedDoc.forEach( (el, ind) => {
        if ( el.id === item.id && el.type === item.type ) {
          this.selectedDoc.splice(ind, 1);
        }
      } );
    }

    this.checkCountSelected();

    this.selectedDocChange.emit(this.selectedDoc);
    this.valueChanges.emit(true);
  }

  checkCountSelected() {
    let result = false;
    if ( Number.isInteger(this.maxCountSelected)) {
      result = this.selectedDoc.length >= this.maxCountSelected;
    }

    return this.maxCountCheckDone = result;
  }

  /*hasInDisabledItems(codeCategory: string, id: any, data: any) {
    let result = false;
    if ( this.disabledItems.hasOwnProperty(codeCategory) ) {
      result = this.disabledItems[codeCategory].indexOf(id) >= 0;
    }
    console.log( this.disabledItems, this.disabledItems[codeCategory], data.disabled );
    return  data.disabled = result;
  }
*/
  setDisabledParams() {
    for ( let i = 0; i < this.docs.length; i++ ) {
      const keyCategory = this.docs[i].code_category;
      if ( this.disabledItems.hasOwnProperty(keyCategory) ) {
          this.docs[i].docs.forEach( el => {
            el.disabled = this.disabledItems[keyCategory].indexOf(el.id) >= 0;
         });
      }
    }

  }

  deleteDoc(id: any, codeCategory: string, index: number, indexCategory: number) {
    this.deleteEvent.emit( {id, codeCategory, index, indexCategory } );
  }

  editDoc(id: any, code_category: string, index: any, indexCategory: number) {
    const dialogRef = this.dialog.open(DocsEditComponent, {
      data: { formName: code_category, id},
      minWidth: '80%',
      disableClose: true
    }).afterClosed().subscribe( res => {
      if(res) {
        if( res.change ) {
          this.editEvent.emit(true);
        }
      }
    });

  }
}

export interface DeleteDocInfo {
  id: number;
  codeCategory: string;
  index: number;
  indexCategory: number;
}
