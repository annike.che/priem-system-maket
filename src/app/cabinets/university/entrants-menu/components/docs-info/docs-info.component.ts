import {Component, Inject, OnInit, Type} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {EntrantsService} from '@/cabinets/university/entrants-menu/entrants.service';
import {IdentificationViewComponent} from '../docs/view/identification-view.component';
import {CompatriotViewComponent} from '../docs/view/compatriot-view.component';
import {DisabilityViewComponent} from '../docs/view/disability-view.component';
import {EducationsViewComponent} from '../docs/view/educations-view.component';
import {EgeViewComponent} from '../docs/view/ege-view.component';
import {OlympicsViewComponent} from '@/cabinets/university/entrants-menu/components/docs/view/olympics-view.component';
import {OrphansViewComponent} from '@/cabinets/university/entrants-menu/components/docs/view/orphans-view.component';
import {ParentsLostViewComponent} from '@/cabinets/university/entrants-menu/components/docs/view/parents-lost-view.component';
import {OtherViewComponent} from '@/cabinets/university/entrants-menu/components/docs/view/other-view.component';
import {CompositionViewComponent} from '@/cabinets/university/entrants-menu/components/docs/view/composition-view.component';
import {MilitariesViewComponent} from '@/cabinets/university/entrants-menu/components/docs/view/militaries-view.component';
import {RadiationWorkViewComponent} from '@/cabinets/university/entrants-menu/components/docs/view/radiation-work-view.component';
import {VeteranViewComponent} from '@/cabinets/university/entrants-menu/components/docs/view/veteran-view.component';
import {DocsService} from '@/common/services/docs.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import * as fileHandling from '@/common/functions/files';


@Component({
  selector: 'app-docs-info',
  templateUrl: './docs-info.component.html',
  styleUrls: ['./docs-info.component.scss']
})
export class DocsInfoComponent implements OnInit {

  component: Type<any>;
  formName: string;
  formData: any;
  id: number;

  //file: any;

  isLoad = true;

  mapComponent = {
      identification: IdentificationViewComponent,
      orphans: OrphansViewComponent,
      veteran: VeteranViewComponent,
      olympics: OlympicsViewComponent,
      educations: EducationsViewComponent,
      militaries: MilitariesViewComponent,
      other: OtherViewComponent,
      disability: DisabilityViewComponent,
      compatriot: CompatriotViewComponent,
      parents_lost: ParentsLostViewComponent,
      radiation_work: RadiationWorkViewComponent,
      ege: EgeViewComponent,
      composition: CompositionViewComponent
  };

  constructor(
      private entrantsService: EntrantsService,
      private docsService: DocsService,
      private snackBar: MatSnackBar,
      public dialogRef: MatDialogRef<DocsInfoComponent>,
      @Inject(MAT_DIALOG_DATA) public data: any ) {

      this.formName = data.formName;
      this.component = this.mapComponent[ this.formName];
      this.formData = data.data;
      this.id = data.id;

  }

  ngOnInit() {
      if (!this.formData) {
          this.entrantsService.getDocInfo(this.id, this.formName)
              .subscribe( res => {
                  if ( res.done ) {
                    this.formData = res.data;
                  }
                  this.isLoad = false;
              });
      } else {
          this.isLoad = false;
      }



  }

  getFile($event, doc) {
    const type = this.formName === 'identification' ? 'identification' : 'general';
    this.docsService.getDocFile(doc.id, type)
        .subscribe( res => {
          fileHandling.handlingResponse(res, doc.title);
        }, err => {
          console.log('err', err);
          this.snackBar.open( fileHandling.handlingError(err));
        });
  }

}
