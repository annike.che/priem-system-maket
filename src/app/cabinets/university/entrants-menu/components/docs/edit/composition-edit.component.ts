import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ClsService} from '@/common/services/cls.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {EntrantsService} from '@/cabinets/university/entrants-menu/entrants.service';

@Component({
  selector: 'app-composition-edit',
  template: `
    <form [formGroup]="formData">
      
      <div fxLayout="row" >

        <mat-form-field appearance="fill" fxFlex="" >
          <mat-label>Тип документа</mat-label>
          <mat-select formControlName="id_document_type" required>
            <mat-option *ngFor="let item of cls.document_type.data" [value]="item.id">
                {{item.name}}
            </mat-option>
          </mat-select>
        </mat-form-field>
        
        
      </div>

      <div fxLayout="row" fxLayout.lt-lg="column" fxLayoutGap.gt-lg='16px' fxLayoutGap.lg="16px">
        <mat-form-field appearance="fill" fxFlex="" fxFlex.lt-md="100" >
          <mat-label>Наименование документа</mat-label>
          <input matInput formControlName="doc_name" required>
        </mat-form-field>

        <div fxFlex="" fxLayout="row" fxLayout.lt-md="column"  fxLayoutGap.gt-md='16px' fxLayoutGap.md="">
            <mat-form-field appearance="fill" fxFlex="" fxFlex.lt-md="100" >
              <mat-label>uid</mat-label>
              <input matInput formControlName="uid" appNullValue>
            </mat-form-field>
                       
            <mat-form-field appearance="fill" fxFlex="" fxFlex.lt-md="100" >
              <mat-label>Статус</mat-label>
	            <mat-select formControlName="checked" required>
		            <mat-option [value]="true"> Подтверждено </mat-option>
		            <mat-option [value]="false"> Не подтверждено</mat-option>
	            </mat-select>
            </mat-form-field>
        </div>
      </div>



	    <div fxLayout="row" fxLayout.lt-lg="column" fxLayoutGap='16px' fxLayoutGap.lt-lg="">
          <div fxFlex="" fxLayout="row" fxLayout.lt-md="column" fxLayoutGap='16px' fxLayoutGap.lt-md="">
	          <mat-form-field appearance="fill" fxFlex="">
		          <mat-label>Год написания сочинения</mat-label>
		          <input matInput appInteger maxLength="4"
			          formControlName="doc_year" required>
	          </mat-form-field>
	          <mat-form-field appearance="fill" fxFlex="">
		          <mat-label>Результат</mat-label>
		          <mat-select formControlName="result" required>
			          <mat-option *ngFor="let item of [true, false]" [value]="item">
				          {{item | yesNo: 'Зачет':'Не зачет'}}
			          </mat-option>
		          </mat-select>
	          </mat-form-field>
          </div>
          <div fxFlex="" fxLayout="row" fxLayout.lt-md="column" fxLayoutGap='16px' fxLayoutGap.lt-md="">
	          <mat-form-field appearance="fill" fxFlex="">
		          <mat-label>Дата выдачи</mat-label>
		          <input matInput [matDatepicker]="pickerIssueDate"
			          formControlName="issue_date" required>
		          <mat-datepicker-toggle matSuffix [for]="pickerIssueDate"></mat-datepicker-toggle>
		          <mat-datepicker #pickerIssueDate></mat-datepicker>
	          </mat-form-field>

	          <mat-form-field appearance="fill" fxFlex="">
		          <mat-label>Аппеляция</mat-label>
		          <mat-select formControlName="has_appealed" required>
			          <mat-option *ngFor="let item of [true, false]" [value]="item">
				          {{item | yesNo}}
			          </mat-option>
		          </mat-select>
	          </mat-form-field>
          </div>
	    </div>
	    <div fxLayout="row" >
		    <mat-form-field appearance="fill" fxFlex="" >
			    <mat-label>Статус аппеляции</mat-label>
			    <mat-select formControlName="id_appeal_status" required>
				    <mat-option *ngFor="let item of cls.appeal_statuses.data" [value]="item.id">
					    {{item.name}}
				    </mat-option>
			    </mat-select>
		    </mat-form-field>
	    </div>
	    <div fxLayout="row" >
		    <mat-form-field appearance="fill" fxFlex="100" >
			    <mat-label>Тема сочинения</mat-label>
			    <mat-select formControlName="id_composition_theme" required>
				    <mat-option>
					    <ngx-mat-select-search
						    [placeholderLabel]="'Поиск'"
						    [noEntriesFoundLabel]="'Данные не найдены'"
						    [(ngModel)]="search.composition_theme" [ngModelOptions]="{standalone: true}">
						    <mat-icon ngxMatSelectSearchClear svgIcon="close"></mat-icon>
					    </ngx-mat-select-search>
				    </mat-option>
				    <mat-option *ngFor="let item of cls.composition_themes.data | filter: search.composition_theme" [value]="item.id">
					    {{item.name}}
				    </mat-option>
			    </mat-select>
		    </mat-form-field>

	    </div>

      <div>
        <mat-form-field appearance="fill" fxFlex="" >
          <mat-label>Место выдачи</mat-label>
          <textarea matInput formControlName="doc_org" appNullValue> </textarea>
        </mat-form-field>
      </div>

      <div fxLayout="row" fxLayout.lt-md="column">
        <mat-form-field appearance="fill" fxFlex="" fxFlex.lt-md="100" >
          <mat-label>Документ, удостоверяющий личность</mat-label>
          <mat-select formControlName="id_ident_document" required>
            <mat-option *ngFor="let item of cls.idents_docs.data" [value]="item.id">
              {{item.name}}
            </mat-option>
          </mat-select>
        </mat-form-field>
      </div>

    </form>
    
    <div>
      <button mat-flat-button
              (click)="addData()"
              [disabled]="formData.invalid || formData.pristine || !formData.dirty"
              class="mr-16"
              color="primary"> Сохранить</button>
        
      <button mat-stroked-button
              (click)="cancel()"
              [disabled] = "formData.pristine || !formData.dirty"
              color="primary"> Отменить изменения </button>
    </div>
  
  `,
  styles: [``]
})
export class CompositionEditComponent implements OnInit, OnDestroy {

  formData: FormGroup;

  data: CompositiionData;


  sysCategory: string;
  idEntrant: number;

  search = {
    composition_theme: ''
  };

  private ngUnsubscribe: Subject<void> = new Subject<void>();

  @Output() saveData = new EventEmitter();

  cls = {
    document_type: {
      data: [],
      isLoad: true,
      initFunc:  () => {
        return this.clsService.getClsData('v_document_types', '', { name_table: this.sysCategory})}
    },
    composition_themes: {
      data: [],
      isLoad: true,
      initFunc:  () => this.clsService.getClsData('composition_themes'),
    },
    appeal_statuses: {
      data: [],
      isLoad: true,
      initFunc:  () => this.clsService.getClsData('appeal_statuses'),
    },
    idents_docs: {
      data: [],
      isLoad: true,
      initFunc:  () => this.entrantsService.getEntrantIdentsList(this.idEntrant),
    },
  };

  constructor(
      private fb: FormBuilder,
      private clsService: ClsService,
      private entrantsService: EntrantsService
  ) {
    //

  }

  ngOnInit() {

    console.log('this.data', this.data);

    this.idEntrant = this.data.id_entrant;
    this.sysCategory = this.data.code_sys_category;

    this.initCls();

    this.initForm();

    this.fillForm();

  }

  initCls() {
    for (let key in this.cls) {
      if ( this.cls[key].isLoad ) {
        this.cls[key].initFunc()
            .pipe(  takeUntil(this.ngUnsubscribe) )
            .subscribe( res => {
              this.cls[key].data = res;
            });
      }
    }
  }

  initForm() {
    this.formData = this.fb.group({
      id_ident_document:          [ null, Validators.required],
      id_document_type:           [ null, Validators.required],
      doc_name:                   [ null, Validators.required ],
      doc_org:                    [ null ],
      uid:                        [ null ],
      id_composition_theme:       [ null, Validators.required],
      issue_date:                 [ null, Validators.required],
      result:                     [ null, Validators.required],
      doc_year:                   [ null, Validators.required],
      has_appealed:               [ false, Validators.required],
      id_appeal_status:           [ null, Validators.required ],
      checked:                    [ null, Validators.required ]
    });
  }

  fillForm() {
    for ( let key in this.formData.controls) {
      if (this.data.hasOwnProperty(key) ) {
        this.formData.get(key).setValue( this.data[key] );
      }
    }

  }



  ngOnDestroy(): void {
    console.log('unsubscribe');
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  addData() {
    this.saveData.emit(this.formData.value);
  }

  cancel() {
    this.fillForm();
    this.formData.reset(this.formData.value);
  }
}

interface CompositiionData {

  id: number;
  doc_name: string;
  doc_org: string;

  doc_year: string;
  issue_date: string;

  name_composition_theme: string;
  id_composition_theme: number;

  result: boolean;
  has_appealed?: boolean;


  id_document_type: number;
  name_document_type: string;

  id_ident_document: number;
  name_ident_document: number;

  uid: string;

  name_sys_category: string;
  code_sys_category: string;

  id_entrant: number;
  created: string;
  checked?: boolean;
  path_files: string;
}
