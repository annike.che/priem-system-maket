import { NgModule } from '@angular/core';
import {SharedModule} from '@/common/modules/shared.module';
import {FormsModule} from '@angular/forms';
import {ArtLibModule} from '@art-lib/art-lib.module';
import {CompatriotEditComponent} from './compatriot-edit.component';
import {CompositionEditComponent} from './composition-edit.component';
import {DisabilityEditComponent} from './disability-edit.component';
import {EducationsEditComponent} from './educations-edit.component';
import {EgeEditComponent} from './ege-edit.component';
import {IdentificationEditComponent} from './identification-edit.component';
import {MilitariesEditComponent} from './militaries-edit.component';
import {OlympicsEditComponent} from './olympics-edit.component';
import {OrphansEditComponent} from './orphans-edit.component';
import {OtherEditComponent} from './other-edit.component';
import {ParentsLostEditComponent} from './parents-lost-edit.component';
import {RadiationWorkEditComponent} from './radiation-work-edit.component';
import {VeteranEditComponent} from './veteran-edit.component';


@NgModule({
  declarations: [
    CompatriotEditComponent,
    CompositionEditComponent,
    DisabilityEditComponent,
    EducationsEditComponent,
    EgeEditComponent,
    IdentificationEditComponent,
    MilitariesEditComponent,
    OlympicsEditComponent,
    OrphansEditComponent,
    OtherEditComponent,
    ParentsLostEditComponent,
    RadiationWorkEditComponent,
    VeteranEditComponent
  ],
  imports: [
    SharedModule,
    FormsModule,
    ArtLibModule
  ],
  exports: [
   CompatriotEditComponent,
   CompositionEditComponent,
   DisabilityEditComponent,
   EducationsEditComponent,
   EgeEditComponent,
   IdentificationEditComponent,
   MilitariesEditComponent,
   OlympicsEditComponent,
   OrphansEditComponent,
   OtherEditComponent,
   ParentsLostEditComponent,
   RadiationWorkEditComponent,
   VeteranEditComponent
  ],
  entryComponents: [
    CompatriotEditComponent,
    CompositionEditComponent,
    DisabilityEditComponent,
    EducationsEditComponent,
    EgeEditComponent,
    IdentificationEditComponent,
    MilitariesEditComponent,
    OlympicsEditComponent,
    OrphansEditComponent,
    OtherEditComponent,
    ParentsLostEditComponent,
    RadiationWorkEditComponent,
    VeteranEditComponent
  ]
})

export class DocsFormsEditModule { }
