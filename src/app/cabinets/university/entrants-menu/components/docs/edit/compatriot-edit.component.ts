import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ClsService} from '@/common/services/cls.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {EntrantsService} from '@/cabinets/university/entrants-menu/entrants.service';

@Component({
  selector: 'app-compatriot-edit',
  template: `
    <form [formGroup]="formData">
      
      <div fxLayout="row" fxLayout.lt-md="column">

        <mat-form-field appearance="fill" fxFlex="" >
          <mat-label>Тип документа</mat-label>
          <mat-select formControlName="id_document_type" required>
            <mat-option *ngFor="let item of cls.document_type.data" [value]="item.id">
                {{item.name}}
            </mat-option>
          </mat-select>
        </mat-form-field>
        
        
      </div>

      <div fxLayout="row" fxLayout.lt-lg="column" fxLayoutGap.gt-lg='16px' fxLayoutGap.lg="16px">
        <mat-form-field appearance="fill" fxFlex="" fxFlex.lt-md="100" >
          <mat-label>Наименование документа</mat-label>
          <input matInput formControlName="doc_name" required>
        </mat-form-field>

        <div fxFlex="" fxLayout="row" fxLayout.lt-md="column"  fxLayoutGap='16px' fxLayoutGap.lt-md="">
            <mat-form-field appearance="fill" fxFlex=""  >
              <mat-label>uid</mat-label>
              <input matInput formControlName="uid" appNullValue>
            </mat-form-field>
                       
            <mat-form-field appearance="fill" fxFlex="" >
              <mat-label>Статус</mat-label>
	            <mat-select formControlName="checked" required>
		            <mat-option [value]="true"> Подтверждено </mat-option>
		            <mat-option [value]="false"> Не подтверждено</mat-option>
	            </mat-select>
            </mat-form-field>
        </div>
        
          
       
      </div>
      
      <div fxLayout="row" >
        <mat-form-field appearance="fill" fxFlex=""  >
          <mat-label>Категория документа соотечественника</mat-label>
          <mat-select formControlName="id_compatriot_category" required>
            <mat-option *ngFor="let item of cls.compatriot_categories.data" [value]="item.id">
              {{item.name}}
            </mat-option>
          </mat-select>
        </mat-form-field>
      </div>

      <div>
        <mat-form-field appearance="fill" fxFlex="" >
          <mat-label>Место выдачи</mat-label>
          <textarea matInput formControlName="doc_org" appNullValue> </textarea>
        </mat-form-field>
      </div>

      <div fxLayout="row" fxLayout.lt-md="column">
        <mat-form-field appearance="fill" fxFlex="" >
          <mat-label>Документ, удостоверяющий личность</mat-label>
          <mat-select formControlName="id_ident_document" required>
            <mat-option *ngFor="let item of cls.idents_docs.data" [value]="item.id">
              {{item.name}}
            </mat-option>
          </mat-select>
        </mat-form-field>
      </div>

    </form>
    
    <div>
      <button mat-flat-button
              (click)="addData()"
              [disabled]="formData.invalid || formData.pristine || !formData.dirty"
              class="mr-16"
              color="primary"> Сохранить</button>
        
      <button mat-stroked-button
              (click)="cancel()"
              [disabled] = "formData.pristine || !formData.dirty"
              color="primary"> Отменить изменения </button>
    </div>
  
  `,
  styles: [``]
})
export class CompatriotEditComponent implements OnInit, OnDestroy {

  formData: FormGroup;

  data: CompatriotData;


  sysCategory: string;
  idEntrant: number;

  private ngUnsubscribe: Subject<void> = new Subject<void>();

  @Output() saveData = new EventEmitter();

  cls = {
    document_type: {
      data: [],
      isLoad: true,
      initFunc:  () => {
        return this.clsService.getClsData('v_document_types', '', { name_table: this.sysCategory})}
    },
    compatriot_categories: {
      data: [],
      isLoad: true,
      initFunc:  () => this.clsService.getClsData('compatriot_categories'),
    },
    idents_docs: {
      data: [],
      isLoad: true,
      initFunc:  () => this.entrantsService.getEntrantIdentsList(this.idEntrant),
    },
  };

  constructor(
      private fb: FormBuilder,
      private clsService: ClsService,
      private entrantsService: EntrantsService
  ) {
    //

  }

  ngOnInit() {

    console.log('this.data', this.data);

    this.idEntrant = this.data.id_entrant;
    this.sysCategory = this.data.code_sys_category;

    this.initCls();

    this.initForm();

    this.fillForm();

  }

  initCls() {
    for (let key in this.cls) {
      if ( this.cls[key].isLoad ) {
        this.cls[key].initFunc()
            .pipe(  takeUntil(this.ngUnsubscribe) )
            .subscribe( res => {
              this.cls[key].data = res;
            });
      }
    }
  }

  initForm() {
    this.formData = this.fb.group({
      doc_name: [ null, Validators.required],
      doc_org: [ null ],
      id_compatriot_category: [ null, Validators.required],
      id_document_type: [ null, Validators.required],
      id_ident_document: [ null, Validators.required],
      uid: [ null ],
      checked: [null, Validators.required]
    });
  }

  fillForm() {
    for ( let key in this.formData.controls) {
      if (this.data.hasOwnProperty(key) ) {
        this.formData.get(key).setValue( this.data[key] );
      }
    }

  }



  ngOnDestroy(): void {
    console.log('unsubscribe');
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  addData() {
    this.saveData.emit(this.formData.value);
  }

  cancel() {
    this.fillForm();
    this.formData.reset(this.formData.value);
  }
}

interface CompatriotData {

  id: number;
  doc_name: string;
  doc_org: string;
  id_compatriot_category: number;
  name_compatriot_category: string;

  id_document_type: number;
  name_document_type: string;

  id_ident_document: number;
  name_ident_document: number;

  uid: string;

  name_sys_category: string;
  code_sys_category: string;

  id_entrant: number;
  created: string;
  checked?: boolean;
  path_files: string;
}
