import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ClsService} from '@/common/services/cls.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {EntrantsService} from '@/cabinets/university/entrants-menu/entrants.service';

@Component({
  selector: 'app-identification-edit',
  template: `
    <form [formGroup]="formData">

	    <div fxLayout="row" fxLayout.lt-lg="column" fxLayoutGap='16px' fxLayoutGap.lt-lg="">

		    <mat-form-field appearance="fill" fxFlex="" >
			    <mat-label>Тип документа</mat-label>
			    <mat-select formControlName="id_document_type" required>
				    <ngx-mat-select-search
					    [placeholderLabel]="'Поиск'"
					    [noEntriesFoundLabel]="'Данные не найдены'"
					    [(ngModel)]="search.doc_type" [ngModelOptions]="{standalone: true}">
					    <mat-icon ngxMatSelectSearchClear svgIcon="close"></mat-icon>
				    </ngx-mat-select-search>
				    <mat-option *ngFor="let item of cls.document_type.data | filter: search.doc_type" [value]="item.id">
					    {{item.name}}
				    </mat-option>
			    </mat-select>
		    </mat-form-field>
        <div fxFlex="" fxLayout="row" fxLayout.lt-md="column" fxLayoutGap='16px' fxLayoutGap.lt-md="">
	        <mat-form-field appearance="fill" fxFlex="">
		        <mat-label>uid</mat-label>
		        <input matInput formControlName="uid" appNullValue>
	        </mat-form-field>
	        <mat-form-field appearance="fill" fxFlex="" >
		        <mat-label>Статус</mat-label>
		        <mat-select formControlName="checked" required>
			        <mat-option [value]="true"> Подтверждено </mat-option>
			        <mat-option [value]="false"> Не подтверждено</mat-option>
		        </mat-select>
	        </mat-form-field>
        </div>
		   
	    </div>
	    <div fxLayout="row" fxLayout.lt-lg="column" fxLayoutGap='16px' fxLayoutGap.lt-lg="">
		    <div fxFlex="" fxLayout="row" fxLayout.lt-md="column" fxLayoutGap='16px' fxLayoutGap.lt-md="0px">
			    <mat-form-field appearance="fill" fxFlex="">
				    <mat-label>Серия</mat-label>
				    <input matInput formControlName="doc_series" appNullValue>
			    </mat-form-field>
			    <mat-form-field appearance="fill" fxFlex="">
				    <mat-label>Номер</mat-label>
				    <input matInput formControlName="doc_number" required>
			    </mat-form-field>
		    </div>
		    <div fxFlex="" fxLayout="row" fxLayout.lt-md="column" fxLayoutGap='16px' fxLayoutGap.lt-md="0px">
			    <mat-form-field appearance="fill" fxFlex="" >
				    <mat-label>Дата выдачи</mat-label>
				    <input matInput [matDatepicker]="pickerIssueDate"
					    formControlName="issue_date" required>
				    <mat-datepicker-toggle matSuffix [for]="pickerIssueDate"></mat-datepicker-toggle>
				    <mat-datepicker #pickerIssueDate></mat-datepicker>
			    </mat-form-field>
			    <mat-form-field appearance="fill" fxFlex="" >
				    <mat-label>Код подразделения</mat-label>
				    <input matInput formControlName="subdivision_code" appNullValue>
			    </mat-form-field>
		    </div>
	    </div>
	    <div>
		    <mat-form-field appearance="fill" fxFlex="" fxFlex.lt-md="100" >
			    <mat-label>Место выдачи</mat-label>
			    <textarea matInput formControlName="doc_organization" required> </textarea>
		    </mat-form-field>
	    </div>
	    <div fxLayout="row" fxLayout.lt-md="column" fxLayoutGap='16px' fxLayoutGap.lt-md="0px">
		    <mat-form-field appearance="fill" fxFlex="" fxFlex.lt-md="100">
			    <mat-label>Фамилия</mat-label>
			    <input matInput formControlName="surname" required>
		    </mat-form-field>
		    <mat-form-field appearance="fill" fxFlex="" fxFlex.lt-md="100">
			    <mat-label>Имя</mat-label>
			    <input matInput formControlName="name" required>
		    </mat-form-field>
		    <mat-form-field appearance="fill" fxFlex="" fxFlex.lt-md="100">
			    <mat-label>Отчество</mat-label>
			    <input matInput formControlName="patronymic" appNullValue>
		    </mat-form-field>
	    </div>
	    <div>
		    <mat-form-field appearance="fill" fxFlex="" fxFlex.lt-md="100" >
			    <mat-label>Страна выдачи документа</mat-label>
			    <mat-select formControlName="id_okcm" required>
				    <ngx-mat-select-search
					    [placeholderLabel]="'Поиск'"
					    [noEntriesFoundLabel]="'Данные не найдены'"
					    [(ngModel)]="search.oksm" [ngModelOptions]="{standalone: true}">
					    <mat-icon ngxMatSelectSearchClear svgIcon="close"></mat-icon>
				    </ngx-mat-select-search>
				    <mat-option *ngFor="let item of cls.oksm.data | filter: search.oksm" [value]="item.id">
					    {{item.name}}
				    </mat-option>
			    </mat-select>
		    </mat-form-field>
	    </div>

    </form>
    
    <div>
      <button mat-flat-button
              (click)="addData()"
              [disabled]="formData.invalid || formData.pristine || !formData.dirty"
              class="mr-16"
              color="primary"> Сохранить</button>
        
      <button mat-stroked-button
              (click)="cancel()"
              [disabled] = "formData.pristine || !formData.dirty"
              color="primary"> Отменить изменения </button>
    </div>
  
  `,
  styles: [``]
})
export class IdentificationEditComponent implements OnInit, OnDestroy {

  formData: FormGroup;

  data: IdentificationData;

  sysCategory: string;
  idEntrant: number;

  search = {
    oksm: '',
    doc_type: ''
  };

  private ngUnsubscribe: Subject<void> = new Subject<void>();

  @Output() saveData = new EventEmitter();

  cls = {
    document_type: {
      data: [],
      isLoad: true,
      initFunc:  () => {
        return this.clsService.getClsData('v_document_types', '', { name_table: this.sysCategory})}
    },
    oksm: {
      data: [],
      isLoad: true,
      initFunc:  () => this.clsService.getClsData('v_okcm'),
    }
  };

  constructor(
      private fb: FormBuilder,
      private clsService: ClsService,
  ) {
    //

  }

  ngOnInit() {

    console.log('this.data', this.data);

    this.idEntrant = this.data.id_entrant;
    this.sysCategory = this.data.code_sys_category;

    this.initCls();

    this.initForm();

    this.fillForm();

  }

  initCls() {
    for (let key in this.cls) {
      if ( this.cls[key].isLoad ) {
        this.cls[key].initFunc()
            .pipe(  takeUntil(this.ngUnsubscribe) )
            .subscribe( res => {
              this.cls[key].data = res;
            });
      }
    }
  }

  initForm() {
    this.formData = this.fb.group({
      id_document_type:           [ null, Validators.required],
      uid:                        [ null ],
      doc_series:                 [ null ],
      doc_number:                 [ null, Validators.required],
      surname:                    [ null, Validators.required],
      name:                       [ null, Validators.required],
      patronymic:                 [ null ],
      doc_organization:           [ null, Validators.required],
      id_okcm:                    [ null, Validators.required],
      issue_date:                 [ null, Validators.required],
      subdivision_code:           [ null, Validators.maxLength(7) ],
      checked:                    [ null, Validators.required ],
    });
  }

  fillForm() {
    for ( let key in this.formData.controls) {
      if (this.data.hasOwnProperty(key) ) {
        this.formData.get(key).setValue( this.data[key] );
      }
    }

    console.log( this.formData);

  }



  ngOnDestroy(): void {
    console.log('unsubscribe');
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  addData() {
    this.saveData.emit(this.formData.value);
  }

  cancel() {
    this.fillForm();
    this.formData.reset(this.formData.value);
  }
}

interface IdentificationData {

  id: number;
  doc_number: string;
  doc_series: string;
  doc_organization: string;
  issue_date: string;
  subdivision_code: string;
  name: string;
  surname: string;
  patronymic: string;
  id_document_type: number;
  name_document_type: string;
  id_okcm: number;
  name_okcm: string;

  uid: string;

  name_sys_category: string;
  code_sys_category: string;

  id_entrant: number;
  created: string;
  checked?: boolean;
}
