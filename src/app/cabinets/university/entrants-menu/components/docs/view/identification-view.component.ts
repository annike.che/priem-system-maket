import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-identification-view',
  template: `
    <!--[ngStyle]="{'pointer-events':true ? 'none' : 'none' }"-->
    <fieldset>
      <div fxLayout="row" fxLayout.lt-md="column">
        <mat-form-field appearance="outline" fxFlex="100" fxFlex.lt-md="100" class="mx-8">
          <mat-label>Категория документа</mat-label>
          <input matInput [ngModel]="data.name_sys_category" [readonly]="true">
        </mat-form-field>
      </div>
      <div fxLayout="row" fxLayout.lt-md="column">

        <mat-form-field appearance="outline" fxFlex="100" fxFlex.lt-md="100" class="mx-8">
          <mat-label>Тип документа</mat-label>
          <input matInput [ngModel]="data.name_document_type" [readonly]="true">
        </mat-form-field>
      </div>

      <div fxLayout="row" fxLayout.lt-md="column">

        <mat-form-field appearance="outline" fxFlex="50" fxFlex.lt-md="100" class="mx-8">
          <mat-label>Наименование документа</mat-label>
          <input matInput [ngModel]="data.name_document_type" [readonly]="true">
        </mat-form-field>

        <mat-form-field appearance="outline" fxFlex="25" fxFlex.lt-md="100" class="mx-8">
          <mat-label>Время и дата создания</mat-label>
          <input matInput [ngModel]="data.created  | date: 'HH:mm    dd.MM.yyyy ' " [readonly]="true">
        </mat-form-field>

        <mat-form-field appearance="outline" fxFlex="25" fxFlex.lt-md="100" class="mx-8">
          <mat-label>Статус документа</mat-label>
          <input matInput [ngModel]="data.checked | stateDocument" [readonly]="true">
        </mat-form-field>


      </div>

      <div fxLayout="row" fxLayout.lt-md="column">

        <mat-form-field appearance="outline" fxFlex="" fxFlex.lt-md="100" class="mx-8">
          <mat-label>Серия</mat-label>
          <input matInput [ngModel]="data.doc_series" [readonly]="true">
        </mat-form-field>

        <mat-form-field appearance="outline" fxFlex="" fxFlex.lt-md="100" class="mx-8">
          <mat-label>Номер</mat-label>
          <input matInput [ngModel]="data.doc_number" [readonly]="true">
        </mat-form-field>

        <mat-form-field appearance="outline" fxFlex="" fxFlex.lt-md="100" class="mx-8">
          <mat-label>Дата выдачи</mat-label>
          <input matInput [ngModel]="data.issue_date | date: 'dd.MM.yyyy' " [readonly]="true">
        </mat-form-field>

        <mat-form-field appearance="outline" fxFlex="" fxFlex.lt-md="100" class="mx-8">
          <mat-label>Код подразделения</mat-label>
          <input matInput [ngModel]="data.subdivision_code" [readonly]="true">
        </mat-form-field>
      </div>

      <div>
        <mat-form-field appearance="outline" fxFlex="" fxFlex.lt-md="100" class="mx-8">
          <mat-label>Место выдачи</mat-label>
          <textarea matInput [ngModel]="data.doc_organization" [readonly]="true"> </textarea>
        </mat-form-field>

      </div>

      <div fxLayout="row" fxLayout.lt-md="column">
        <mat-form-field appearance="outline" fxFlex="" fxFlex.lt-md="100" class="mx-8">
          <mat-label>Фамилия</mat-label>
          <input matInput [ngModel]="data.surname" [readonly]="true">
        </mat-form-field>

        <mat-form-field appearance="outline" fxFlex="" fxFlex.lt-md="100" class="mx-8">
          <mat-label>Имя</mat-label>
          <input matInput [ngModel]="data.name" [readonly]="true">
        </mat-form-field>

        <mat-form-field appearance="outline" fxFlex="" fxFlex.lt-md="100" class="mx-8">
          <mat-label>Отчество</mat-label>
          <input matInput [ngModel]="data.patronymic" [readonly]="true">
        </mat-form-field>

      </div>

      <div fxLayout="row" fxLayout.lt-md="column">
        <mat-form-field appearance="outline" fxFlex="" fxFlex.lt-md="100" class="mx-8">
          <mat-label>Страна выдачи документа</mat-label>
          <input matInput [ngModel]="data.name_okcm" [readonly]="true">
        </mat-form-field>

      </div>

    </fieldset>
  `,
  styles: [``]
})

export class IdentificationViewComponent implements OnInit {

  data: IdentificationDoc;

  constructor() { }

  ngOnInit() {
    console.log('data', this.data);

  }

}

interface IdentificationDoc {
  id: number;
  doc_number: string;
  doc_series: string;
  doc_organization: string;
  issue_date: string;
  subdivision_code: string;
  name: string;
  surname: string;
  patronymic: string;
  id_document_type: number;
  name_document_type: string;
  id_okcm: number;
  name_okcm: string;
  name_sys_category: string;
  checked: boolean;
  created: string;
}


