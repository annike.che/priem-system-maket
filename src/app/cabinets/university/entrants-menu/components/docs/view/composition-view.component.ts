import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-other-view',
    template: `
    <!--[ngStyle]="{'pointer-events':true ? 'none' : 'none' }"-->
    <fieldset>
	    <div fxLayout="row" fxLayout.lt-md="column">

		    <mat-form-field appearance="outline" fxFlex="100" fxFlex.lt-md="100" class="mx-8">
			    <mat-label>Категория документа</mat-label>
			    <input matInput [ngModel]="data.name_sys_category" [readonly]="true">
		    </mat-form-field>
	    </div>
	    <div fxLayout="row" fxLayout.lt-md="column">

		    <mat-form-field appearance="outline" fxFlex="100" fxFlex.lt-md="100" class="mx-8">
			    <mat-label>Тип документа</mat-label>
			    <input matInput [ngModel]="data.name_document_type" [readonly]="true">
		    </mat-form-field>
	    </div>

	    <div fxLayout="row" fxLayout.lt-md="column">

		    <mat-form-field appearance="outline" fxFlex="50" fxFlex.lt-md="100" class="mx-8">
			    <mat-label>Наименование документа</mat-label>
			    <input matInput [ngModel]="data.doc_name" [readonly]="true">
		    </mat-form-field>

		    <mat-form-field appearance="outline" fxFlex="" fxFlex.lt-md="100" class="mx-8">
			    <mat-label>Время и дата создания</mat-label>
			    <input matInput [ngModel]="data.created  | date: 'HH:mm    dd.MM.yyyy ' " [readonly]="true">
		    </mat-form-field>

		    <mat-form-field appearance="outline" fxFlex="" fxFlex.lt-md="100" class="mx-8">
			    <mat-label>Статус документа</mat-label>
			    <input matInput [ngModel]="data.checked | stateDocument" [readonly]="true">
		    </mat-form-field>
	    </div>

	    <div fxLayout="row" fxLayout.lt-md="column">
		    <mat-form-field appearance="outline" fxFlex="25" fxFlex.lt-md="100" class="mx-8">
			    <mat-label>Год написания сочинения</mat-label>
			    <input matInput [ngModel]="data.doc_year" [readonly]="true">
		    </mat-form-field>
		    <mat-form-field appearance="outline" fxFlex="25" fxFlex.lt-md="100" class="mx-8">
			    <mat-label>Результат</mat-label>
			    <input matInput [ngModel]="data.result | yesNo: 'Зачет':'Не зачет' " [readonly]="true">
		    </mat-form-field>
		    <mat-form-field appearance="outline" fxFlex="25" fxFlex.lt-md="100" class="mx-8">
			    <mat-label>Дата выдачи</mat-label>
			    <input matInput [ngModel]="data.issue_date | date: 'dd.MM.yyyy' " [readonly]="true">
		    </mat-form-field>
		    <mat-form-field appearance="outline" fxFlex="25" fxFlex.lt-md="100" class="mx-8">
			    <mat-label>Аппеляция</mat-label>
			    <input matInput [ngModel]="data.has_appealed | yesNo" [readonly]="true">
		    </mat-form-field>
	    </div>
	    <div fxLayout="row" fxLayout.lt-md="column" *ngIf="data.has_appealed">
		    <mat-form-field appearance="outline" fxFlex="100" fxFlex.lt-md="100" class="mx-8">
			    <mat-label>Статус аппеляции</mat-label>
			    <input matInput [ngModel]="data.name_appeal_status" [readonly]="true">
		    </mat-form-field>
	    </div>
	    <div fxLayout="row" fxLayout.lt-md="column">
		    <mat-form-field appearance="outline" fxFlex="100" fxFlex.lt-md="100" class="mx-8">
			    <mat-label>Тема сочинения</mat-label>
			    <textarea matInput [ngModel]="data.name_composition_theme" [readonly]="true"> </textarea>
		    </mat-form-field>

	    </div>
	    <div fxLayout="row" fxLayout.lt-md="column">
		    <mat-form-field appearance="outline" fxFlex="100" fxFlex.lt-md="100" class="mx-8">
			    <mat-label>Место выдачи</mat-label>
			    <textarea matInput [ngModel]="data.doc_org" [readonly]="true"> </textarea>
		    </mat-form-field>

	    </div>

    </fieldset>
  `,
    styles: [``]
})
export class CompositionViewComponent implements OnInit {

    data: CompositionView;

    constructor() { }

    ngOnInit() {
    }

}

interface CompositionView {
    id: number;
    doc_name?: string;
    doc_org: string;
    doc_year: string;
    issue_date: string;
    name_composition_theme: string;
    result: boolean;
    has_appealed?: boolean;
    name_appeal_status?: string;
    name_document_type: string;
    name_sys_category: string;
    created: string;
    checked: boolean;
    path_file?: string;
}
