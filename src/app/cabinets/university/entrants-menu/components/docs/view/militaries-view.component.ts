import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-militaries-view',
    template: `
    <!--[ngStyle]="{'pointer-events':true ? 'none' : 'none' }"-->
    <fieldset>
      <div fxLayout="row" fxLayout.lt-md="column">

        <mat-form-field appearance="outline" fxFlex="100" fxFlex.lt-md="100" class="mx-8">
          <mat-label>Категория документа</mat-label>
          <input matInput [ngModel]="data.name_sys_category" [readonly]="true">
        </mat-form-field>
      </div>
      <div fxLayout="row" fxLayout.lt-md="column">

        <mat-form-field appearance="outline" fxFlex="100" fxFlex.lt-md="100" class="mx-8">
          <mat-label>Тип документа</mat-label>
          <input matInput [ngModel]="data.name_document_type" [readonly]="true">
        </mat-form-field>
      </div>

      <div fxLayout="row" fxLayout.lt-md="column">

        <mat-form-field appearance="outline" fxFlex="50" fxFlex.lt-md="100" class="mx-8">
          <mat-label>Наименование документа</mat-label>
          <input matInput [ngModel]="data.doc_name" [readonly]="true">
        </mat-form-field>

        <mat-form-field appearance="outline" fxFlex="25" fxFlex.lt-md="100" class="mx-8">
          <mat-label>Время и дата создания</mat-label>
          <input matInput [ngModel]="data.created  | date: 'HH:mm    dd.MM.yyyy ' " [readonly]="true">
        </mat-form-field>

        <mat-form-field appearance="outline" fxFlex="25" fxFlex.lt-md="100" class="mx-8">
          <mat-label>Статус документа</mat-label>
          <input matInput [ngModel]="data.checked | stateDocument" [readonly]="true">
        </mat-form-field>
      </div>

      <div fxLayout="row" fxLayout.lt-md="column">
        <mat-form-field appearance="outline" fxFlex="" fxFlex.lt-md="100" class="mx-8">
          <mat-label>Серия</mat-label>
          <input matInput [ngModel]="data.doc_series" [readonly]="true">
        </mat-form-field>
        <mat-form-field appearance="outline" fxFlex="" fxFlex.lt-md="100" class="mx-8">
          <mat-label>Номер</mat-label>
          <input matInput [ngModel]="data.doc_number" [readonly]="true">
        </mat-form-field>
        <mat-form-field appearance="outline" fxFlex="25" fxFlex.lt-md="100" class="mx-8">
          <mat-label>Дата выдачи</mat-label>
          <input matInput [ngModel]="data.issue_date | date: 'dd.MM.yyyy' " [readonly]="true">
        </mat-form-field>
      </div>

      <div fxLayout="row" fxLayout.lt-md="column">
        <mat-form-field appearance="outline" fxFlex="100" fxFlex.lt-md="100" class="mx-8">
          <mat-label>Категория военнослужащего</mat-label>
          <textarea matInput [ngModel]="data.name_category" [readonly]="true"></textarea>
        </mat-form-field>

      </div>
      <div fxLayout="row" fxLayout.lt-md="column">
        <mat-form-field appearance="outline" fxFlex="100" fxFlex.lt-md="100" class="mx-8">
          <mat-label>Место выдачи</mat-label>
          <textarea matInput [ngModel]="data.doc_org" [readonly]="true"> </textarea>
        </mat-form-field>

      </div>

    </fieldset>
  `,
    styles: [``]
})
export class MilitariesViewComponent implements OnInit {

    data: MilitariesView;

    constructor() { }

    ngOnInit() {
    }

}

interface MilitariesView {
    id: number;
    doc_number: string;
    doc_series: string;
    doc_name?: string;
    doc_org: string;
    issue_date: string;
    name_category: string;
    name_document_type: string;
    name_sys_category: string;
    created: string;
    checked: boolean;
    path_file?: string;
}

