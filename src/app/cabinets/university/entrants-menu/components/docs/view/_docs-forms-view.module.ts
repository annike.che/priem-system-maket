import { NgModule } from '@angular/core';
import {IdentificationViewComponent} from './identification-view.component';
import {CompatriotViewComponent} from './compatriot-view.component';
import {CompositionViewComponent} from './composition-view.component';
import {DisabilityViewComponent} from './disability-view.component';
import {EducationsViewComponent} from './educations-view.component';
import {EgeViewComponent} from './ege-view.component';
import {MilitariesViewComponent} from './militaries-view.component';
import {OlympicsViewComponent} from './olympics-view.component';
import {OrphansViewComponent} from './orphans-view.component';
import {OtherViewComponent} from './other-view.component';
import {ParentsLostViewComponent} from './parents-lost-view.component';
import {RadiationWorkViewComponent} from './radiation-work-view.component';
import {VeteranViewComponent} from './veteran-view.component';
import {SharedModule} from '@/common/modules/shared.module';
import {FormsModule} from '@angular/forms';
import {ArtLibModule} from '@art-lib/art-lib.module';



@NgModule({
  declarations: [
    IdentificationViewComponent,
    CompatriotViewComponent,
    CompositionViewComponent,
    DisabilityViewComponent,
    EducationsViewComponent,
    EgeViewComponent,
    MilitariesViewComponent,
    OlympicsViewComponent,
    OrphansViewComponent,
    OtherViewComponent,
    ParentsLostViewComponent,
    RadiationWorkViewComponent,
    VeteranViewComponent,
  ],
  imports: [
    SharedModule,
    FormsModule,
    ArtLibModule
  ],
  exports: [
    IdentificationViewComponent,
    CompatriotViewComponent,
    CompositionViewComponent,
    DisabilityViewComponent,
    EducationsViewComponent,
    EgeViewComponent,
    MilitariesViewComponent,
    OlympicsViewComponent,
    OrphansViewComponent,
    OtherViewComponent,
    ParentsLostViewComponent,
    RadiationWorkViewComponent,
    VeteranViewComponent
  ],
  entryComponents: [
    IdentificationViewComponent,
    CompatriotViewComponent,
    CompositionViewComponent,
    DisabilityViewComponent,
    EducationsViewComponent,
    EgeViewComponent,
    MilitariesViewComponent,
    OlympicsViewComponent,
    OrphansViewComponent,
    OtherViewComponent,
    ParentsLostViewComponent,
    RadiationWorkViewComponent,
    VeteranViewComponent
  ]
})
export class DocsFormsViewModule { }
