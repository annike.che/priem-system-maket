import { NgModule } from '@angular/core';
import {DocsFormsViewModule} from './view/_docs-forms-view.module';
import {DocsFormsAddModule} from './add/_docs-forms-add.module';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {DocsFormsEditModule} from '@/cabinets/university/entrants-menu/components/docs/edit/_docs-forms-edit.module';

const MY_DATE_FORMATS = {
    parse: {
        dateInput: ['DD.MM.YYYY', 'LL', ],
    },
    display: {
        dateInput: 'DD.MM.YYYY',
        monthYearLabel: 'YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY'
    },
};

@NgModule({
  declarations: [],
    imports: [
        DocsFormsViewModule,
        DocsFormsAddModule,
        DocsFormsEditModule
    ],
  exports: [
      DocsFormsViewModule,
      DocsFormsAddModule,
      DocsFormsEditModule
  ],
    providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS }
  ]
})
export class DocsFormsModule { }
