import { NgModule } from '@angular/core';
import {SharedModule} from '@/common/modules/shared.module';
import {FormsModule} from '@angular/forms';
import {ArtLibModule} from '@art-lib/art-lib.module';
import {CompatriotAddComponent} from './compatriot-add.component';
import {CompositionAddComponent} from '@/cabinets/university/entrants-menu/components/docs/add/composition-add.component';
import {DisabilityAddComponent} from '@/cabinets/university/entrants-menu/components/docs/add/disability-add.component';
import {EducationsAddComponent} from '@/cabinets/university/entrants-menu/components/docs/add/educations-add.component';
import {EgeAddComponent} from '@/cabinets/university/entrants-menu/components/docs/add/ege-add.component';
import {IdentificationAddComponent} from '@/cabinets/university/entrants-menu/components/docs/add/identification-add.component';
import {MilitariesAddComponent} from '@/cabinets/university/entrants-menu/components/docs/add/militaries-add.component';
import {OrphansAddComponent} from '@/cabinets/university/entrants-menu/components/docs/add/orphans-add.component';
import {OtherAddComponent} from '@/cabinets/university/entrants-menu/components/docs/add/other-add.component';
import {ParentsLostAddComponent} from '@/cabinets/university/entrants-menu/components/docs/add/parents-lost-add.component';
import {RadiationWorkAddComponent} from '@/cabinets/university/entrants-menu/components/docs/add/radiation-work-add.component';
import {VeteranAddComponent} from '@/cabinets/university/entrants-menu/components/docs/add/veteran-add.component';
import {OlympicsAddComponent} from '@/cabinets/university/entrants-menu/components/docs/add/olympics-add.component';


@NgModule({
  declarations: [
    CompatriotAddComponent,
    CompositionAddComponent,
    DisabilityAddComponent,
    EducationsAddComponent,
    EgeAddComponent,
    IdentificationAddComponent,
    MilitariesAddComponent,
    OrphansAddComponent,
    OlympicsAddComponent,
    OtherAddComponent,
    ParentsLostAddComponent,
    RadiationWorkAddComponent,
    VeteranAddComponent
  ],
  imports: [
    SharedModule,
    FormsModule,
    ArtLibModule
  ],
  exports: [
    CompatriotAddComponent,
    CompositionAddComponent,
    DisabilityAddComponent,
    EducationsAddComponent,
    EgeAddComponent,
    IdentificationAddComponent,
    MilitariesAddComponent,
    OrphansAddComponent,
    OlympicsAddComponent,
    OtherAddComponent,
    ParentsLostAddComponent,
    RadiationWorkAddComponent,
    VeteranAddComponent
  ],
  entryComponents: [
    CompatriotAddComponent,
    CompositionAddComponent,
    DisabilityAddComponent,
    EducationsAddComponent,
    EgeAddComponent,
    IdentificationAddComponent,
    MilitariesAddComponent,
    OrphansAddComponent,
    OlympicsAddComponent,
    OtherAddComponent,
    ParentsLostAddComponent,
    RadiationWorkAddComponent,
    VeteranAddComponent
  ]
})
export class DocsFormsAddModule { }
