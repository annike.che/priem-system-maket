import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ClsService} from '@/common/services/cls.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {EntrantsService} from '@/cabinets/university/entrants-menu/entrants.service';
import * as prepare from '@/common/functions/prepareData';

@Component({
    selector: 'app-other-add',
    template: `
    <form [formGroup]="formData">
        
      <div fxLayout="row" fxLayout.lt-md="column" fxLayoutGap='16px'>

        <mat-form-field appearance="fill" fxFlex="100" fxFlex.lt-md="100" >
          <mat-label>Тип документа</mat-label>
          <mat-select formControlName="id_document_type" required>
            <mat-option *ngFor="let item of cls.document_type.data" [value]="item.id">
                {{item.name}}
            </mat-option>
          </mat-select>
        </mat-form-field>
      </div>

      <div fxLayout="row" fxLayout.lt-md="column" fxLayoutGap='16px'fxLayoutGap.lt-md="">
        <mat-form-field appearance="fill" fxFlex=""  >
          <mat-label>Наименование документа</mat-label>
          <input matInput formControlName="doc_name" required>
        </mat-form-field>

        <mat-form-field appearance="fill" fxFlex="" >
          <mat-label>uid</mat-label>
          <input matInput formControlName="uid" appNullValue>
        </mat-form-field>
      </div>
      <div fxLayout="row" fxLayout.lt-md="column" fxLayoutGap='16px' fxLayoutGap.lt-md="">
          <mat-form-field appearance="fill" fxFlex="" fxFlex.lt-md="100" >
              <mat-label>Серия</mat-label>
              <input matInput formControlName="doc_series" appNullValue>
          </mat-form-field>
          <mat-form-field appearance="fill" fxFlex="" fxFlex.lt-md="100" >
              <mat-label>Номер</mat-label>
              <input matInput formControlName="doc_number" appNullValue>
          </mat-form-field>
	      <mat-form-field appearance="fill" fxFlex="" fxFlex.lt-md="100" >
		      <mat-label>Дата выдачи</mat-label>
		      <input matInput [matDatepicker]="pickerIssueDate"
		             formControlName="issue_date" required>
		      <mat-datepicker-toggle matSuffix [for]="pickerIssueDate"></mat-datepicker-toggle>
		      <mat-datepicker #pickerIssueDate></mat-datepicker>
	      </mat-form-field>
        </div>
      <div>
        <mat-form-field appearance="fill" fxFlex="">
          <mat-label>Место выдачи</mat-label>
          <textarea matInput formControlName="doc_org" appNullValue> </textarea>
        </mat-form-field>
      </div>

      <div fxLayout="row" fxLayout.lt-md="column">
        <mat-form-field appearance="fill" fxFlex="">
          <mat-label>Документ, удостоверяющий личность</mat-label>
          <mat-select formControlName="id_ident_document" required>
            <mat-option *ngFor="let item of cls.idents_docs.data" [value]="item.id">
              {{item.name}}
            </mat-option>
          </mat-select>
        </mat-form-field>
      </div>
    <div fxLayout="column" class="my-8">
        <mat-label class="mb-8">Файл документа</mat-label>
        <app-file-upload  fxFlex="" formControlName="file">
        </app-file-upload>
    </div>

    </form>
    
    <div mat-dialog-actions align="end">
      <button mat-flat-button
              (click)="addData()"
              [disabled]="formData.invalid"
              color="primary"> Сохранить</button>
    </div>
  
  `,
    styles: [``]
})
export class OtherAddComponent implements OnInit, OnDestroy {

    formData: FormGroup;

    data: any;

    sysCategory: string;
    idEntrant: string;

    private ngUnsubscribe: Subject<void> = new Subject<void>();

    @Output() saveData = new EventEmitter();

    cls = {
        document_type: {
            data: [],
            isLoad: true,
            initFunc: () =>
                this.clsService.getClsData('v_document_types', '', {name_table: this.sysCategory})
        },
        idents_docs: {
            data: [],
            isLoad: true,
            initFunc:  () => this.entrantsService.getEntrantIdentsList(this.idEntrant),
        }

    };

    constructor(
        private fb: FormBuilder,
        private clsService: ClsService,
        private entrantsService: EntrantsService
    ) {
        //

    }

    ngOnInit() {
        console.log('data', this.data);

        this.sysCategory = this.data.sysCategory;
        this.idEntrant = this.data.idEntrant;

        this.initCls();

        this.formData = this.fb.group({
            id_ident_document:          [ null, Validators.required],
            id_document_type:           [ null, Validators.required],
            doc_name:                   [ null, Validators.required ],
            doc_org:                    [ null ],
            uid:                        [ null ],
            doc_number:                 [ null ],
            doc_series:                 [ null ],
            issue_date:                 [ null, Validators.required],
            file:                       [ null ]
        });

    }

    initCls() {
        for (let key in this.cls) {
            if ( this.cls[key].isLoad ) {
                // console.log('initFunc', this.cls[key].initFunc);
                this.cls[key].initFunc()
                    .pipe(  takeUntil(this.ngUnsubscribe) )
                    .subscribe( res => {
                        this.cls[key].data = res;
                    });
            }
        }
    }



    ngOnDestroy(): void {
        console.log('unsubscribe');
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }

    addData() {
        this.saveData.emit(prepare.prepareDate(this.formData.value));
    }
}
