import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ClsService} from '@/common/services/cls.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import * as prepare from '@/common/functions/prepareData';

@Component({
    selector: 'app-identification-add',
    template: `
    <form [formGroup]="formData">
        
      <div fxLayout="row" fxLayout.lt-lg="column" fxLayoutGap='16px' fxLayoutGap.lt-lg="0px">

        <mat-form-field appearance="fill" fxFlex="" >
          <mat-label>Тип документа</mat-label>
          <mat-select formControlName="id_document_type" required>
              <ngx-mat-select-search
                  [placeholderLabel]="'Поиск'"
                  [noEntriesFoundLabel]="'Данные не найдены'"
                  [(ngModel)]="search.doc_type" [ngModelOptions]="{standalone: true}">
                  <mat-icon ngxMatSelectSearchClear svgIcon="close"></mat-icon>
              </ngx-mat-select-search>
              <mat-option *ngFor="let item of cls.document_type.data | filter: search.doc_type" [value]="item.id">
                  {{item.name}}
              </mat-option>
          </mat-select>
        </mat-form-field>
        <mat-form-field appearance="fill" fxFlex="" >
           <mat-label>uid</mat-label>
           <input matInput formControlName="uid" appNullValue>
        </mat-form-field>
      </div>
      <div fxLayout="row" fxLayout.lt-lg="column" fxLayoutGap='16px' fxLayoutGap.lt-lg="0px">
          <div fxFlex="" fxLayout="row" fxLayout.lt-md="column" fxLayoutGap='16px' fxLayoutGap.lt-md="0px">
              <mat-form-field appearance="fill" fxFlex="">
                  <mat-label>Серия</mat-label>
                  <input matInput formControlName="doc_series" appNullValue>
              </mat-form-field>
              <mat-form-field appearance="fill" fxFlex="">
                  <mat-label>Номер</mat-label>
                  <input matInput formControlName="doc_number" required>
              </mat-form-field>
          </div>
          <div fxFlex="" fxLayout="row" fxLayout.lt-md="column" fxLayoutGap='16px' fxLayoutGap.lt-md="0px">
              <mat-form-field appearance="fill" fxFlex="" >
                  <mat-label>Дата выдачи</mat-label>
                  <input matInput [matDatepicker]="pickerIssueDate"
                      formControlName="issue_date" required>
                  <mat-datepicker-toggle matSuffix [for]="pickerIssueDate"></mat-datepicker-toggle>
                  <mat-datepicker #pickerIssueDate></mat-datepicker>
              </mat-form-field>
              <mat-form-field appearance="fill" fxFlex="" >
                  <mat-label>Код подразделения</mat-label>
                  <input matInput formControlName="subdivision_code" appNullValue>
              </mat-form-field>
          </div>
        </div>
	    <div>
		    <mat-form-field appearance="fill" fxFlex="" fxFlex.lt-md="100" >
			    <mat-label>Место выдачи</mat-label>
			    <textarea matInput formControlName="doc_organization" required> </textarea>
		    </mat-form-field>
	    </div>
	    <div fxLayout="row" fxLayout.lt-md="column" fxLayoutGap='16px' fxLayoutGap.lt-md="0px">
		    <mat-form-field appearance="fill" fxFlex="" fxFlex.lt-md="100">
			    <mat-label>Фамилия</mat-label>
			    <input matInput formControlName="surname" required>
		    </mat-form-field>
		    <mat-form-field appearance="fill" fxFlex="" fxFlex.lt-md="100">
			    <mat-label>Имя</mat-label>
			    <input matInput formControlName="name" required>
		    </mat-form-field>
		    <mat-form-field appearance="fill" fxFlex="" fxFlex.lt-md="100">
			    <mat-label>Отчество</mat-label>
			    <input matInput formControlName="patronymic" appNullValue>
		    </mat-form-field>
        </div>
	    <div>
		    <mat-form-field appearance="fill" fxFlex="" fxFlex.lt-md="100" >
			    <mat-label>Страна выдачи документа</mat-label>
			    <mat-select formControlName="id_okcm" required>
				    <ngx-mat-select-search
					    [placeholderLabel]="'Поиск'"
					    [noEntriesFoundLabel]="'Данные не найдены'"
					    [(ngModel)]="search.oksm" [ngModelOptions]="{standalone: true}">
					    <mat-icon ngxMatSelectSearchClear svgIcon="close"></mat-icon>
				    </ngx-mat-select-search>
				    <mat-option *ngFor="let item of cls.oksm.data | filter: search.oksm" [value]="item.id">
					    {{item.name}}
				    </mat-option>
			    </mat-select>
		    </mat-form-field>
	    </div>
    <div fxLayout="column" class="my-8">
        <mat-label class="mb-8">Файл документа</mat-label>
        <app-file-upload  fxFlex="" formControlName="file">
        </app-file-upload>
    </div>

    </form>
    
    <div mat-dialog-actions align="end">
      <button mat-flat-button
              (click)="addData()"
              [disabled]="formData.invalid"
              color="primary"> Сохранить</button>
    </div>
  
  `,
    styles: [``]
})
export class IdentificationAddComponent implements OnInit, OnDestroy {

    formData: FormGroup;

    data: any;

    sysCategory: string;
    idEntrant: string;

    search = {
        oksm: '',
        doc_type: ''
    };

    private ngUnsubscribe: Subject<void> = new Subject<void>();

    @Output() saveData = new EventEmitter();

    cls = {
        document_type: {
            data: [],
            isLoad: true,
            initFunc:  () => {
                return this.clsService.getClsData('v_document_types', '', { name_table: this.sysCategory})}
        },
        oksm: {
            data: [],
            isLoad: true,
            initFunc:  () => this.clsService.getClsData('v_okcm'),
        }

    };

    constructor(
        private fb: FormBuilder,
        private clsService: ClsService,
    ) {
        //

    }

    ngOnInit() {

        this.sysCategory = this.data.sysCategory;
        this.idEntrant = this.data.idEntrant;

        this.initCls();

        this.formData = this.fb.group({
            id_document_type:           [ null, Validators.required],
            uid:                        [ null ],
            doc_series:                 [ null ],
            doc_number:                 [ null, Validators.required],
            surname:                    [ null, Validators.required],
            name:                       [ null, Validators.required],
            patronymic:                 [ null ],
            doc_organization:           [ null, Validators.required],
            id_okcm:                    [ null, Validators.required],
            issue_date:                 [ null, Validators.required],
            subdivision_code:           [ null, Validators.maxLength(7) ],
            file:                       [ null ]
        });

        // console.log('form', this.formData);
    }

    initCls() {
        for (let key in this.cls) {
            if ( this.cls[key].isLoad ) {
                // console.log('initFunc', this.cls[key].initFunc);
                this.cls[key].initFunc()
                    .pipe(  takeUntil(this.ngUnsubscribe) )
                    .subscribe( res => {
                        this.cls[key].data = res;
                    });
            }
        }
    }



    ngOnDestroy(): void {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }

    addData() {
        this.saveData.emit(prepare.prepareDate(this.formData.value));
    }
}
