import {Component, Inject, OnInit, Type} from '@angular/core';
import {EntrantsService} from '@/cabinets/university/entrants-menu/entrants.service';
import {DocsService} from '@/common/services/docs.service';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {ConfirmModalComponent} from '@art-lib/components/confirm-modal/confirm-modal.component';
import {MatSnackBar} from '@angular/material/snack-bar';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CompatriotEditComponent} from '@/cabinets/university/entrants-menu/components/docs/edit/compatriot-edit.component';
import * as prepare from '@/common/functions/prepareData';
import {CompositionEditComponent} from '@/cabinets/university/entrants-menu/components/docs/edit/composition-edit.component';
import {DisabilityEditComponent} from '@/cabinets/university/entrants-menu/components/docs/edit/disability-edit.component';
import {EducationsEditComponent} from '@/cabinets/university/entrants-menu/components/docs/edit/educations-edit.component';
import {EgeEditComponent} from '@/cabinets/university/entrants-menu/components/docs/edit/ege-edit.component';
import {IdentificationEditComponent} from '@/cabinets/university/entrants-menu/components/docs/edit/identification-edit.component';
import {MilitariesEditComponent} from '@/cabinets/university/entrants-menu/components/docs/edit/militaries-edit.component';
import {OlympicsEditComponent} from '@/cabinets/university/entrants-menu/components/docs/edit/olympics-edit.component';
import {OrphansEditComponent} from '@/cabinets/university/entrants-menu/components/docs/edit/orphans-edit.component';
import {OtherEditComponent} from '@/cabinets/university/entrants-menu/components/docs/edit/other-edit.component';
import {ParentsLostEditComponent} from '@/cabinets/university/entrants-menu/components/docs/edit/parents-lost-edit.component';
import {RadiationWorkEditComponent} from '@/cabinets/university/entrants-menu/components/docs/edit/radiation-work-edit.component';
import {VeteranEditComponent} from '@/cabinets/university/entrants-menu/components/docs/edit/veteran-edit.component';

@Component({
  selector: 'app-docs-edit',
  templateUrl: './docs-edit.component.html',
  styleUrls: ['./docs-edit.component.scss']
})
export class DocsEditComponent implements OnInit {

  component: Type<any>;
  formName: string;
  formData: any;
  id: number;

  doc: any;

  addFileForm: FormGroup;

  wasChanged: boolean;


  isLoad = true;

  mapComponent = {
    identification: IdentificationEditComponent,
    orphans: OrphansEditComponent,
    veteran: VeteranEditComponent,
    olympics: OlympicsEditComponent,
    educations: EducationsEditComponent,
    militaries: MilitariesEditComponent,
    other: OtherEditComponent,
    disability: DisabilityEditComponent,
    compatriot: CompatriotEditComponent,
    parents_lost: ParentsLostEditComponent,
    radiation_work: RadiationWorkEditComponent,
    ege: EgeEditComponent,
    composition: CompositionEditComponent
  };

  constructor(
      private entrantsService: EntrantsService,
      private docsService: DocsService,
      private dialog: MatDialog,
      private snackBar: MatSnackBar,
      private fb: FormBuilder,
      public dialogRef: MatDialogRef<DocsEditComponent>,
      @Inject(MAT_DIALOG_DATA) public data: any ) {

    this.formName = data.formName;
    this.component = this.mapComponent[ this.formName];
    this.formData = data.data;

    this.id = data.id;

  }

  ngOnInit() {
   this.initData();
  }

  initData() {
    this.isLoad = true;
    this.entrantsService.getDocInfo(this.id, this.formName)
        .subscribe( res => {
          if ( res.done ) {
            this.formData = res.data;
            this.doc = {
              file: res.data.file,
              ...res.data.doc
            };
          }

          this.isLoad = false;
        });
  }

  getFile($event, doc) {
    const type = this.formName === 'identification' ? 'identification' : 'general';
    this.docsService.getDocFile(doc.id, type)
        .subscribe( res => {
          console.log('res', res);
          const blob = new Blob([res], { type: res.type });
          this.downloadFile(blob, doc.title);
        }, err => {
          console.log('err', err);
          var message;
          const reader = new FileReader();
          const blob = new Blob([err.error], { type: err.error.type });

          reader.onload =  (() => {
            const data = JSON.parse(reader.result.toString());
            message = data.message;
            this.snackBar.open('Произошла ошибка' + (message ? ': ' + message : ''));
          });

          reader.readAsText(blob);
          this.snackBar.open('Произошла ошибка' + (message ? ': ' + message : ''));
        });
  }

  downloadFile(file: Blob, fileName: string) {

    const downloadLink = document.createElement("a");
    const objectUrl = URL.createObjectURL(file);


    downloadLink.href = objectUrl;
    downloadLink.download = fileName;
    downloadLink.target = '_self';
    document.body.appendChild(downloadLink);

    downloadLink.click();
    URL.revokeObjectURL(objectUrl);
  }

  deleteFile($event: MouseEvent) {
    this.dialog.open(ConfirmModalComponent, {
      data: {
        title: 'Удаление файла',
        content: 'Вы уверены, что хотите удалить файл из документа?'
      }
    })
    .afterClosed().subscribe(res => {
      if (res) {
        const type = this.formName === 'identification' ? 'identification' : 'general';
        this.docsService.deleteDocFile(this.id, type)
            .subscribe( res => {
                if ( res.done ) {
                  this.doc.file = false;
                  this.snackBar.open('Файл успешно удален из документа');
                } else {
                  this.snackBar.open('При попытке удалить файл из документа произошла ошибка' + ( res.message ? ': ' + res.message : '' ) );
                }
            });
      }
    });
  }

  initForm() {
    this.addFileForm = this.fb.group( {
      file: [ null, Validators.required ]
    });
  }

  addFile() {
    const type = this.formName === 'identification' ? 'identification' : 'general';
    this.docsService.addDocFile(this.id, this.addFileForm.value, type)
        .subscribe( res => {
            if (res.done) {
              this.doc = {
                file: true,
                ...res.data.doc
              };

              this.addFileForm = null;
              this.snackBar.open('Файл успешно загружен');
            } else {
              this.snackBar.open('При попытке загрузить файл произошла ошибка' + ( res.message ? ': ' + res.message : '' ) );
            }
        });
  }

  saveData(data: any) {
    console.log('saveData', data);
    const sendData = prepare.prepareDate(data);
    this.entrantsService.editDoc(this.id, this.formName, sendData)
        .subscribe( res => {
          if ( res.done ) {
            this.initData();
            //this.component = this.mapComponent[ this.formName];
            this.snackBar.open('Данные успешно сохранены');
            this.wasChanged = true;
          } else {
            this.snackBar.open('Произошла ошибка' + ( res.message ? ': ' + res.message : '' ) );
          }
        });

  }

  close() {
    this.dialogRef.close({ change: this.wasChanged });
  }
}
