import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DocsEditComponent} from '@/cabinets/university/entrants-menu/components/docs-edit/docs-edit.component';
import {DocsFormsModule} from '@/cabinets/university/entrants-menu/components/docs/docs-forms.module';
import {SharedModule} from '@/common/modules/shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DynamicFormsModule} from '@/common/modules/dynamic-forms/dynamic-forms.module';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {ArtLibModule} from '@art-lib/art-lib.module';

const MY_DATE_FORMATS = {
  parse: {
    dateInput: ['DD.MM.YYYY', 'LL', 'DD-MM-YYYY' ],
  },
  display: {
    dateInput: 'DD.MM.YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY'
  },
};

@NgModule({
  declarations: [DocsEditComponent],
  imports: [
    DocsFormsModule,
    SharedModule,
    FormsModule,
    DynamicFormsModule,
    ReactiveFormsModule,
    ArtLibModule,
  ],
  exports: [ DocsEditComponent ],
  entryComponents: [ DocsEditComponent ],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS }
  ]
})
export class DocsEditModule { }
