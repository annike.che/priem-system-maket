import {Component, Inject, OnInit, Type} from '@angular/core';
import {EntrantsService, toResponseBody, uploadProgress} from '@/cabinets/university/entrants-menu/entrants.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ClsService} from '@/common/services/cls.service';
import {CompatriotAddComponent} from '../docs/add/compatriot-add.component';
import {MatSnackBar} from '@angular/material/snack-bar';
import {CompositionAddComponent} from '../docs/add/composition-add.component';
import {DisabilityAddComponent} from '../docs/add/disability-add.component';
import {EducationsAddComponent} from '../docs/add/educations-add.component';
import {EgeAddComponent} from '../docs/add/ege-add.component';
import {IdentificationAddComponent} from '../docs/add/identification-add.component';
import {MilitariesAddComponent} from '../docs/add/militaries-add.component';
import {OrphansAddComponent} from '../docs/add/orphans-add.component';
import {OtherAddComponent} from '../docs/add/other-add.component';
import {ParentsLostAddComponent} from '../docs/add/parents-lost-add.component';
import {RadiationWorkAddComponent} from '../docs/add/radiation-work-add.component';
import {OlympicsAddComponent} from '../docs/add/olympics-add.component';
import {VeteranAddComponent} from '@/cabinets/university/entrants-menu/components/docs/add/veteran-add.component';

@Component({
  selector: 'app-docs-add',
  templateUrl: './docs-add.component.html',
  styleUrls: ['./docs-add.component.scss']
})
export class DocsAddComponent implements OnInit {

  component: Type<any>;
  formName: string;

  sysCategories: [];

  sendData: any;

  mapComponent = {
    identification: IdentificationAddComponent,
    orphans: OrphansAddComponent,
    veteran: VeteranAddComponent,
    olympics: OlympicsAddComponent,
    educations: EducationsAddComponent,
    militaries: MilitariesAddComponent,
    other: OtherAddComponent,
    disability: DisabilityAddComponent,
    compatriot: CompatriotAddComponent,
    parents_lost: ParentsLostAddComponent,
    radiation_work: RadiationWorkAddComponent,
    ege: EgeAddComponent,
    composition: CompositionAddComponent
  };

  constructor( private entrantsService: EntrantsService,
               private clsService: ClsService,
               private snackBar: MatSnackBar,
               public dialogRef: MatDialogRef<DocsAddComponent>,
               @Inject(MAT_DIALOG_DATA) public data: any) {

    this.sendData = {
        sysCategory: null,
        idEntrant: data.idEntrant,
    };
   /* console.log('data', this.sendData);*/

  }
  ngOnInit() {

    this.clsService.getSysCategories()
        .subscribe(res => {
          this.sysCategories = res;
        });

  }


  addData(data) {
      console.log('save', data);
      this.entrantsService.addEntrantDocument( this.sendData.idEntrant, this.sendData.sysCategory, data)
          .pipe(
             /* uploadProgress(progress => (this.percentDone = progress)),
              toResponseBody()*/
          )
          .subscribe( res => {
            console.log('res', res);
            if ( res.done ) {
              this.dialogRef.close(true);
              this.snackBar.open('Документ успешно добавлен');
            } else {
              let message = 'При попытке добавить документ произошла ошибка' + ((res.message) ? `: ${res.message}` : '');
              this.snackBar.open(message);
            }
          } );
  }
}
