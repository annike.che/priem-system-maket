import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import { Ng2ImgMaxService } from 'ng2-img-max';
import {MatSnackBar} from '@angular/material/snack-bar';
import {EntrantsService} from '@/cabinets/university/entrants-menu/entrants.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import * as moment from 'moment';


@Component({
  selector: 'app-entrant-upload-photo-dialog',
  templateUrl: './entrant-upload-photo-dialog.component.html',
  styleUrls: ['./entrant-upload-photo-dialog.component.scss']
})

export class EntrantUploadPhotoDialogComponent implements OnInit {

  formData: FormGroup;

  uploadFile: File;

  maxImageSize = 1.5 as const;      // До какого размера сжимаем в МБ

  minWidth = 300;
  minHeight = 300;

  maxWidth = 2000;
  maxHeight = 2000;

  initWidth: number;
  initHeight: number;
  fileOrientation: number;

  fileWasChecked = false;
  fileWasResizes = false;
  fileWasCompressed = false;

  rotate = false;

  localCompressedURl: any;

  isLoad = false;


  constructor(
      private fb: FormBuilder,
      private ng2ImgMaxService: Ng2ImgMaxService,
      private snackBar: MatSnackBar,
      private entrantService: EntrantsService,
      public dialogRef: MatDialogRef<EntrantUploadPhotoDialogComponent>,
      @Inject(MAT_DIALOG_DATA) public idEntrant: number
  ) {

  }

  ngOnInit() {
    this.initForm();
    this.subscribeFileChanges();
  }

  initForm() {
    this.formData = this.fb.group( {
        file: [null, Validators.required]
    });


  }

  subscribeFileChanges() {

    this.formData.get('file').valueChanges.subscribe( res => {

      console.log('file', res, this.isLoad);

      if (!res) {
        this.uploadFile = null;
        this.localCompressedURl = null;
        return;
      }

      this.isLoad = true;

      if( res ) {
        this.uploadFile = res;
        let imageSize = res['size']/(1024*1024);
        console.log('file size:', imageSize);

        let reader = new FileReader();

        reader.onload = (event: any) => {


          var image = new Image();
          image.src = event.target.result;
          //console.log('image', image.width, image.height);

          image.onload = (e) => {

            this.initHeight = image.naturalHeight;
            this.initWidth = image.naturalWidth;

            console.log('init size', this.initHeight, this.initWidth);

            if ( image.height < this.minHeight || image.width < this.minWidth ) {
              this.snackBar.open('Загруженное изображение меньше чем ' + this.minWidth + 'х' + this.minHeight + 'px');
              this.fileWasChecked = false;
              this.isLoad = false;
              return;
            }
            if ( image.height > this.maxHeight || image.width > this.maxWidth || imageSize/(1024*1024) > this.maxImageSize )  {
              // получаем ориентацию иходного изображения
              this.getOrientation(res, (orientation) => { this.fileOrientation = orientation; });

              // сжимаем и уменьшаем изображение
              this.compressImage(res);

            } else {
              this.localCompressedURl = image.src;
              this.fileWasChecked = true;
              this.isLoad = false;
            }

            return true;
          };

        };

        reader.readAsDataURL(res);

      }


    });

  }


  compressImage( file: File ) {
    console.log('file size before', file.size/(1024*1024));


    // сначала уменьшаем изображение до max количества height и width
    this.ng2ImgMaxService.resizeImage(file, this.maxWidth, this.maxHeight).subscribe(resizable => {

      this.fileWasResizes = true;

      console.log('file size after resize', resizable.size / (1024*1024));

      // если фото все еще больше max размера, то сжимаем качество
      if( resizable.size / (1024*1024) > this.maxImageSize ) {

        this.ng2ImgMaxService.compressImage(resizable, this.maxImageSize)
            .subscribe( compressed => {
              this.fileWasCompressed = true;

              if( compressed.size/(1024*1024) > this.maxImageSize ) {
                this.snackBar.open( 'Вы приложили очень тяжелый файл. Не удается сжать его до нужных размеров без серьезной потери качества. ' +
                    'Попробуйте загрузить другой файл' );
                this.isLoad = false;
              } else {
                // устанавливаем результирующий файл в нужную переменную
                this.setCompessedFileInData(compressed);
              }
            },
                error => {
                  this.snackBar.open( 'При попытке обработать файл произошла ошибка' );
                  this.isLoad = false;
                } );
      } else {
        // устанавливаем результирующий файл в нужную переменную
        this.setCompessedFileInData(resizable);
      }



    }, error => {
      this.snackBar.open( 'При попытке обработать файл произошла ошибка' );
      this.isLoad = false;
    });

  }

  setCompessedFileInData(file: File) {

    let reader = new FileReader();



    reader.onload = (event: any) => {
      console.log('setCompessedFileInData');
      let image = new Image();

      let newImage = new Image();

      image.src = event.target.result;

      image.onload = (e) => {

        // устанавливаем ориентацию по исходному изображению

        newImage.src = this.resetOrientation(image);
        newImage.width = image.naturalWidth;
        newImage.height = image.naturalHeight;

      };

      newImage.onload = (e) => {
        const blob = this.dataURItoBlob(newImage.src);

        // формируюем файл для отправки

        this.uploadFile = new File([blob], file.name);

        // отображаем превью

        this.localCompressedURl = newImage.src;
        this.fileWasChecked = true;
        this.isLoad = false;
      };
    };



    reader.readAsDataURL(file);
  }

  loadImage() {
    this.entrantService.addEntrantPhoto(this.idEntrant, {file: this.uploadFile})
        .subscribe((res: any) => {
          if (res.done) {
            this.snackBar.open('Фотография загружена');
            this.dialogRef.close(res.data.doc);
          } else {
            this.snackBar.open('Произошла ошибка' +  (res.message ? ': ' + res.message : ''));
          }
        });

  }

  getOrientation = (file: File, callback: Function) => {
    var reader = new FileReader();

    reader.onload = (event: ProgressEvent) => {

      if (! event.target) {
        return;
      }

      const file = event.target as FileReader;
      const view = new DataView(file.result as ArrayBuffer);

      if (view.getUint16(0, false) != 0xFFD8) {
        return callback(-2);
      }

      const length = view.byteLength;
      let offset = 2;

      while (offset < length)
      {
        if (view.getUint16(offset+2, false) <= 8) return callback(-1);
        let marker = view.getUint16(offset, false);
        offset += 2;

        if (marker == 0xFFE1) {
          if (view.getUint32(offset += 2, false) != 0x45786966) {
            return callback(-1);
          }

          let little = view.getUint16(offset += 6, false) == 0x4949;
          offset += view.getUint32(offset + 4, little);
          let tags = view.getUint16(offset, little);
          offset += 2;
          for (let i = 0; i < tags; i++) {
            if (view.getUint16(offset + (i * 12), little) == 0x0112) {
              return callback(view.getUint16(offset + (i * 12) + 8, little));
            }
          }
        } else if ((marker & 0xFF00) != 0xFF00) {
          break;
        }
        else {
          offset += view.getUint16(offset, false);
        }
      }
      return callback(-1);
    };

    reader.readAsArrayBuffer(file);
  };

  resetOrientation(image) {

    // устанавливаем правильную ориентацию по исходному изображению

    console.log('this.fileOrientation', this.fileOrientation);

    let canvas = document.createElement('canvas');
    let ctx = canvas.getContext("2d");

    if (4 < this.fileOrientation && this.fileOrientation < 9) {
      canvas.width = image.height;
      canvas.height = image.width;
    } else {
      canvas.width = image.width;
      canvas.height = image.height;
    }

    // transform context before drawing image
    // В примере 6 и 8 были поменяны местами, отрабатывает некорректно
    switch (this.fileOrientation) {
      case 2: ctx.transform(-1, 0, 0, 1, image.width, 0); break;
      case 3: ctx.transform(-1, 0, 0, -1, image.width, image.height); break;
      case 4: ctx.transform(1, 0, 0, -1, 0, image.height); break;
      case 5: ctx.transform(0, 1, 1, 0, 0, 0); break;
      case 6: ctx.transform(0, -1, 1, 0, 0, image.width); break;
      case 7: ctx.transform(0, -1, -1, 0, image.height, image.width); break;
      case 8: ctx.transform(0, 1, -1, 0, image.height, 0); break;
      default: break;
    }

    // draw image
    ctx.drawImage(image, 0, 0);

    return canvas.toDataURL();
  }

  dataURItoBlob(dataURI) {
  // convert base64/URLEncoded data component to raw binary data held in a string
    let byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0) {
      byteString = atob(dataURI.split(',')[1]);
    } else {
      byteString = unescape(dataURI.split(',')[1]);
    }
    // separate out the mime component
    const mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
    // write the bytes of the string to a typed array
    const ia = new Uint8Array(byteString.length);
    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ia], {type: mimeString});
  }
}
