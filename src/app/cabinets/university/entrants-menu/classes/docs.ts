interface DocsShortItem {
    doc_number?: string;
    doc_series?: string;
    id: number;
    id_document_type: number;
    name_document_type: string;
    issue_date: string;
    mark?: number;
    name_subject?: string;
    can_edit: boolean;

    disabled: boolean;
}

interface DocsListEntrant {
    name_category: string;
    code_category: string;
    docs: Array<DocsShortItem>;
}
