export interface IEntrants {
    id: number;
    birthday: string;
    name: string;
    surname: string;
    patronymic: string;
    created?: Date;
    gender_name: string;
    id_gender: number;
    id_okcm: number;
    okcm_name: string;
    snils: string;
}
