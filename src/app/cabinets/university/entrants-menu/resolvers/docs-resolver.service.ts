import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import {ActivatedRouteSnapshot, Resolve, Route, Router, RouterStateSnapshot} from '@angular/router';
import {EntrantsService} from '@/cabinets/university/entrants-menu/entrants.service';

@Injectable({
  providedIn: 'root'
})
export class DocsResolverService implements Resolve<any> {

  types = {
    others: { no_categories: 'identification' },
    idents: { categories: 'identification' }
  };

  constructor(
      private entrantsService: EntrantsService,
      private router: Router
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<any> {

    console.log('route', route.parent);

    const type = route.paramMap.get('type');
    const idEntrant = route.parent.parent.params['id'];


    if ( this.types.hasOwnProperty(type) ) {
      return this.entrantsService.getShortDocList(idEntrant, this.types[type] );
    } else {
      console.log(route.parent.parent);
      return of({done: false});
    }



  }



}
