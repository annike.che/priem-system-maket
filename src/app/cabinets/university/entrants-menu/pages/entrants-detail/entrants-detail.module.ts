import { NgModule } from '@angular/core';
import { EntrantsDetailComponent } from './entrants-detail.component';
import { EntrantsMainDetailComponent } from './tabs/entrants-main-detail/entrants-main-detail.component';
import { EntrantsDocumentsDetailComponent } from './tabs/entrants-documents-detail/entrants-documents-detail.component';
import { EntrantsApplicationsDetailComponent } from './tabs/entrants-applications-detail/entrants-applications-detail.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '@/common/modules/shared.module';
import {FormsModule} from '@angular/forms';
import {DynamicFormsModule} from '@/common/modules/dynamic-forms/dynamic-forms.module';
import {DocsAddComponent} from '@/cabinets/university/entrants-menu/components/docs-add/docs-add.component';
import {ArtLibModule} from '@art-lib/art-lib.module';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {EntrantDocListModule} from '@/cabinets/university/entrants-menu/components/entrant-doc-list/entrant-doc-list.module';
import {EntrantUploadPhotoDialogComponent} from '@/cabinets/university/entrants-menu/components/entrant-upload-photo-dialog/entrant-upload-photo-dialog.component';

import {Ng2ImgMaxModule} from 'ng2-img-max';

const MY_DATE_FORMATS = {
    parse: {
        dateInput: ['DD.MM.YYYY', 'LL', 'DD-MM-YYYY' ],
    },
    display: {
        dateInput: 'DD.MM.YYYY',
        monthYearLabel: 'YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY'
    },
};

const routes: Routes = [
  { path: '', component: EntrantsDetailComponent,
    children: [
      { path: '', component: EntrantsMainDetailComponent },
      { path: 'docs', children: [
          { path: ':type', component: EntrantsDocumentsDetailComponent}
      ]},
      { path: 'applications', component: EntrantsApplicationsDetailComponent }
    ]},
];

@NgModule({
  declarations: [
    EntrantsDetailComponent,
    EntrantsMainDetailComponent,
    EntrantsDocumentsDetailComponent,
    EntrantsApplicationsDetailComponent,
    DocsAddComponent,
    EntrantUploadPhotoDialogComponent,
  ],
    imports: [
        SharedModule,
        RouterModule.forChild(routes),
        FormsModule,
        DynamicFormsModule,
        ArtLibModule,
        EntrantDocListModule,
        Ng2ImgMaxModule
    ],
    entryComponents: [
        DocsAddComponent,
        EntrantUploadPhotoDialogComponent
    ],
    providers: [
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
        { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS }
    ]
})
export class EntrantsDetailModule { }
