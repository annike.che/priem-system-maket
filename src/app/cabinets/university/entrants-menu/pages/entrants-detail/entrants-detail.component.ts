import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {EntrantsService} from '@/cabinets/university/entrants-menu/entrants.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import {ConfirmModalComponent} from '@art-lib/components/confirm-modal/confirm-modal.component';
import {artAnimations} from '@art-lib/animations';
import {FullPhotoModalComponent} from '@art-lib/components/full-photo-modal/full-photo-modal.component';
import {EntrantUploadPhotoDialogComponent} from '@/cabinets/university/entrants-menu/components/entrant-upload-photo-dialog/entrant-upload-photo-dialog.component';


@Component({
  selector: 'app-entrants-detail',
  templateUrl: './entrants-detail.component.html',
  styleUrls: ['./entrants-detail.component.scss'],
  animations: artAnimations
})
export class EntrantsDetailComponent implements OnInit {

  menu: Array<any>;

  idEntrant: number;
  entrantInfo: any;

  dataPhoto: any;

  imageToShow: any;

  isLoad: boolean;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private entrantService: EntrantsService,
    private snackBar: MatSnackBar,
    public dialog: MatDialog
  ) {

    this.menu = [
      {
        title: 'Персональные данные',
        router: ''
      },
      {
        title: 'Документы',
        router: 'docs',
        opened: false,
        children: [
          {
            title: 'Удостоверяющие личность',
            router: 'idents'
          },
          {
            title: 'Другие',
            router: 'others'
          },
        ]
      },/*
      {
        title: 'Вступительные испытания',
        router: 'tests'
      },*/
      {
        title: 'Заявления',
        router: 'applications'
      }
    ];

  }

  ngOnInit() {
    this.route.params.subscribe(res => {
      this.idEntrant = +res['id'];
      this.loadEntrantInfo();
    });

  }

  loadEntrantInfo() {
    this.entrantService.getEntrantShortInfo(this.idEntrant)
      .subscribe(res => {
        if (res.done) {
          this.entrantInfo = res.data;
          console.log(this.entrantInfo);
          if (this.entrantInfo.photo) {
            this.dataPhoto = this.entrantInfo.photo;
            this.getImageFromService();
          } else {
            this.isLoad = false;
          }
        } else {
          this.snackBar.open('Произошла ошбка' + ((res.message) ? `: ${res.message}` : ''));
          this.router.navigate(['../'], {relativeTo: this.route.parent});
          this.isLoad = false;
        }

      });
  }

  toggle(item) {
    console.log('toggle');
    item.opened = !item.opened;
  }

  goToEntrantList() {
    this.router.navigate(['../'], {relativeTo: this.route.parent});
  }


  createImageFromBlob(image: Blob) {
    let reader = new FileReader();
    reader.addEventListener('load', () => {
      this.imageToShow = reader.result;

    }, false);
    if (image) {
      reader.readAsDataURL(image);

    }
  }

  getImageFromService() {
    this.isLoad = true;
    this.entrantService.getEntrantPhoto(this.dataPhoto.id)
      .subscribe((data: Blob) => {
        this.createImageFromBlob(data);
        this.isLoad = false;
      });
  }



  openDialogUploadPhoto() {
    this.dialog.open(EntrantUploadPhotoDialogComponent, {
      data: this.idEntrant,
      width: '652px',
      disableClose: true
    })
        .afterClosed().subscribe( res =>  {
          if (res) {
            this.dataPhoto = res;
            this.entrantInfo.photo = res;
            this.imageToShow = null;
            this.getImageFromService();
          }

    });
  }

  openDeleteDialog() {
    this.dialog.open(ConfirmModalComponent, {
      data: {
        title: 'Удаление фото',
        content: 'Вы уверены, что хотите удалить фото?'
      }
    })
      .afterClosed().subscribe(res => {
      if (res) {
        this.entrantService.removeEntrantPhoto(this.dataPhoto.id)
          .subscribe( (resp: any) => {
            if ( resp.done ) {
              this.snackBar.open('Фото удалено');
              this.dataPhoto = undefined;
              this.entrantInfo.photo = null;
              this.imageToShow  = null;
            } else {
              this.snackBar.open('Произошла ошбка' + ((resp.message) ? `: ${resp.message}` : ''));
            }

          })
      }
    });

  }

  fullPhoto() {

    this.dialog.open(FullPhotoModalComponent, {
      data: {
        title: this.entrantInfo.name + ' ' + this.entrantInfo.patronymic + ' ' + this.entrantInfo.surname,
        image: this.imageToShow
      },
      //width: '652px'
    })
  }
}

