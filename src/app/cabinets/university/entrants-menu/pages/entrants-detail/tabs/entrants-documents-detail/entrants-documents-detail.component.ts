import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, ActivatedRouteSnapshot, Router} from '@angular/router';
import {FormGroup} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import {DocsInfoComponent} from '@/cabinets/university/entrants-menu/components/docs-info/docs-info.component';
import {DocsAddComponent} from '@/cabinets/university/entrants-menu/components/docs-add/docs-add.component';
import {MatSnackBar} from '@angular/material/snack-bar';
import {BehaviorSubject} from 'rxjs';
import {EntrantsService} from '@/cabinets/university/entrants-menu/entrants.service';

@Component({
    selector: 'app-entrants-documents-detail',
    templateUrl: './entrants-documents-detail.component.html',
    styleUrls: ['./entrants-documents-detail.component.scss']
})
export class EntrantsDocumentsDetailComponent implements OnInit {

    data: Array<DocsListEntrant> = [];

    idEntrant: number;
    type: string;

    types = {
        others: { no_categories: 'identification' },
        idents: { categories: 'identification' }
    };

    isLoad: boolean;
    doneInit = false;


    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private snackBar: MatSnackBar,
        private entrantsService: EntrantsService,
        private dialog: MatDialog
    ) {


    }

    ngOnInit() {
        /*console.log('this.route', this.route.snapshot.params);*/
        this.idEntrant = this.route.parent.parent.snapshot.params['id'];

        this.route.params.subscribe( res => {
            this.type = res.type;

            if ( this.types.hasOwnProperty(this.type) ) {
                this.initData();
            }
        } );


    }

    initData() {
        this.isLoad = true;
        this.entrantsService.getShortDocList(this.idEntrant, this.types[ this.type ] )
            .subscribe( res => {
                if (res.done) {
                    this.data = res.data;
                   // console.log('data', this.data);
                } else {
                    this.data = [];
                    this.snackBar.open('При попытке загрузить документы произошла ошбика' + ( res.message ? (': ' + res.message) : '' ) );
                }
                this.doneInit = true;
                this.isLoad = false;
            } );
    }

    setFragment(name) {
        console.log('name', name);
    }

    addDocument() {
        const dialogRef = this.dialog.open(DocsAddComponent, {
            data: { idEntrant: this.idEntrant},
            width: `80%`,
            disableClose: true
        }).afterClosed().subscribe(result => {
            if( result ) {
                this.initData();
            }
        });
    }
}
