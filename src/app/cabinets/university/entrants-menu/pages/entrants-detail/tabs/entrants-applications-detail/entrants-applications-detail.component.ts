import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {EntrantsService} from '@/cabinets/university/entrants-menu/entrants.service';
import {artAnimations} from '@art-lib/animations';

@Component({
  selector: 'app-entrants-applications-detail',
  templateUrl: './entrants-applications-detail.component.html',
  styleUrls: ['./entrants-applications-detail.component.scss'],
  animations: artAnimations
})
export class EntrantsApplicationsDetailComponent implements OnInit {

  constructor( private route: ActivatedRoute,
               private router: Router,
               private entrantService: EntrantsService)
  { }

  idEntrant: number;

  applications: Array<IApplication> = [];

  columnsHeader = ['number', 'nameCompetitive', 'stateName', 'registrationDate', 'original', 'agreed', 'rating', 'epgu' ];

  ngOnInit() {

    this.route.parent.parent.params.subscribe( res => {
      this.idEntrant = +res.id;

      this.loadApplications();

    });


  }

  loadApplications() {
    this.entrantService.getApplicationsList(this.idEntrant).subscribe(res => {
      if (res.done ) {
        this.applications = res.data;
      }
    });
  }

  createApp() {
    this.router.navigate(['../../../application/new/entrant', this.idEntrant], {relativeTo: this.route});
  }
}
