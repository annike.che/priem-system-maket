import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {EntrantsService} from '@/cabinets/university/entrants-menu/entrants.service';
import {artAnimations} from '@art-lib/animations';
import {Meta, Title} from '@angular/platform-browser';

@Component({
  selector: 'app-entrants-main-detail',
  templateUrl: './entrants-main-detail.component.html',
  styleUrls: ['./entrants-main-detail.component.scss'],
  animations: artAnimations
})
export class EntrantsMainDetailComponent implements OnInit {

  id: number;

  data: any;

  addresses = [
    {
      id: 'registration_address',
      id_full: 'registration_addr_full',
      title: 'Адрес Регистрации'
    },
    {
      id: 'fact_address',
      id_full: 'fact_addr_full',
      title: 'Адрес фактического места жительства'
    },
  ];

  constructor(
      private route: ActivatedRoute,
      private router: Router,
      private entrantsService: EntrantsService,
      private titleService: Title,
      private meta: Meta
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params['id'];
      this.getData();
    });

  }

  getData() {
    this.entrantsService.getEntrantsMainInfo(this.id)
        .subscribe(res => {
          if (res.done) {
            this.data = res.data;
            this.titleService.setTitle(
              'Информация об абитуриенте | ' + this.data.name + " " + this.data.surname);
            this.meta.addTag({name: 'description', content: 'Полная информация об абитуриенте'});
          }
        });
  }

}
