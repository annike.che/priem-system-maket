import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {EntrantsEditComponent} from './entrants-edit.component';
import { EntrantsMainEditComponent } from './tabs/entrants-main-edit/entrants-main-edit.component';



@NgModule({
  declarations: [EntrantsEditComponent, EntrantsMainEditComponent],
  imports: [
    CommonModule
  ]
})
export class EntrantsEditModule { }
