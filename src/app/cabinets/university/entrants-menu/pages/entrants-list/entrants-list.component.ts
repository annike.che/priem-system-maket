import {Component, OnDestroy, OnInit} from '@angular/core';
import {IEntrants} from '@/cabinets/university/entrants-menu/classes/entrant';
import {EntrantsService} from '@/cabinets/university/entrants-menu/entrants.service';
import {Observable, Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, switchMap, tap} from 'rxjs/operators';
import {untilDestroyed} from 'ngx-take-until-destroy';
import {ActivatedRoute, Router} from '@angular/router';
import * as cloneDeep from 'lodash/cloneDeep';
import {PageEvent} from '@angular/material/paginator';
import {artAnimations} from '@art-lib/animations';
import {Title} from '@angular/platform-browser';
import {SortDirection} from '@angular/material/sort';

@Component({
  selector: 'app-entrants-list',
  templateUrl: './entrants-list.component.html',
  styleUrls: ['./entrants-list.component.scss'],
  animations: artAnimations
})
export class EntrantsListComponent implements OnInit, OnDestroy {

  persons: Array<IEntrants>;

  isLoad: boolean;


  displayedColumns: string[];
  searchColumns: string[];
  pageSizeOptions: number[];

  search = new Subject<string[]>();
  sortEvent = new Subject<SortEvent>();

  pagination = new Subject<PageEvent>();

  defaultParams = {
    page: 1,
    limit: 20
  };
  total: number;

  params: Params = {
    ...this.defaultParams
  };

  constructor(
    private entrantsService: EntrantsService,
    private route: ActivatedRoute,
    private router: Router,
    private titleService: Title
  ) {

    this.isLoad = false;
    this.titleService.setTitle('Список абитуриентов');

    this.persons = [];

    this.displayedColumns = ['surname', 'name', 'patronymic', 'snils', 'birthday'];
    this.searchColumns = ['searchSurname', 'searchName', 'searchPatronymic', 'searchSnils', 'searchBirthday'];
    this.pageSizeOptions = [5, 10, 15, 20, 30, 50, 100];


  }

  ngOnInit() {
    this.getQueryParamsFromUrl();
    this.subscribeOnChanges();
    this.getPersons();
  }

  subscribeOnChanges() {
    this.search.pipe(
      distinctUntilChanged(),
      debounceTime(500),
      tap(() => {
        this.params.page = 1;
      }),
      switchMap((value, index) => {
       // console.log('value', value);
        return this.getDataFromService();
      }),
      untilDestroyed(this)
    ).subscribe(response => this.setData(response));

    this.pagination.pipe(
      debounceTime(200),
      tap((event: PageEvent) => {
        if (this.params.limit !== event.pageSize) {
          this.params.limit = event.pageSize;
          this.params.page = 1;
        }
        if (this.params.page !== event.pageIndex + 1) {
          this.params.page = event.pageIndex + 1;
        }
      }),
      switchMap(() => this.getDataFromService()),
      untilDestroyed(this)
    )
      .subscribe(response => this.setData(response));

    this.sortEvent.pipe(
        debounceTime(200),
        tap((event: SortEvent) => {
          this.params.order = event.direction as SortDirection;
          this.params.sortby = event.active;
        }),
        switchMap(() => this.getDataFromService()),
        untilDestroyed(this)
    )
        .subscribe(response => this.setData(response));


  }

  getDataFromService(): Observable<any> {
    this.isLoad = true;
    const queryParams = this.getQueryParams(this.params);
    this.passQueryParamsToUrl(queryParams);
    return this.entrantsService.getEntrantsList(this.params);
  }

  getQueryParams(params) {
    const queryParams: any = {};
    Object.keys(params).forEach((key) => {
      if (this.defaultParams.hasOwnProperty(key) && (this.defaultParams[key] !== params[key] && params[key] !== '') ||
        (!this.defaultParams.hasOwnProperty(key) && params[key] !== '')) {
        if ((key === 'frDt' || key === 'toDt') && params.dateF !== 'FromTo') {
          return;
        }
        queryParams[key] = params[key];
      }
    });

    return queryParams;
  }

  passQueryParamsToUrl(queryParams) {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams
    });
  }

  setData(response) {
    if (response.done) {

      this.persons = response.data;
      this.params.page = response.paginator.page;
      this.params.limit = response.paginator.limit;
      this.total = response.paginator.total;

    }
    this.isLoad = false;
  }

  getQueryParamsFromUrl() {
    const queryParams = cloneDeep(this.route.snapshot.queryParams);
    this.setParams(queryParams);
  }


  setParams(params) {
    Object.keys(params).forEach(key => {
      switch (key) {
        case 'page':
          this.params[key] = +params[key];
          break;
        default:
          this.params[key] = params[key];
      }
    });
  }


  getPersons() {
    this.getDataFromService()
      .subscribe(response => this.setData(response));
  }

  ngOnDestroy(): void {
  }

  clearFilter($event) {
    this.params.search_name = '';
    this.params.search_surname = '';
    this.params.search_patronymic = '';
    this.params.search_snils = '';

    this.search.next($event);
  }

}

interface SearchData {
  name?: string;
  surname?: string;
  patronymic?: string;
  snils?: string;
}

interface Params {
  page: number;
  limit: number;
  search_name?: string;
  search_surname?: string;
  search_patronymic?: string;
  search_snils?: string;
  order?: SortDirection; // 'ASC' || 'DESC'
  sortby?: string;
}


