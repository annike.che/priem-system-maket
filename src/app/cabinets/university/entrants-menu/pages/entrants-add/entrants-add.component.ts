import {Component, OnDestroy, OnInit} from '@angular/core';
import {BehaviorSubject, Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, switchMap, takeUntil} from 'rxjs/operators';
import {ClsService} from '@/common/services/cls.service';
import {AbstractControl, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import * as moment from 'moment';
import {EntrantsService} from '@/cabinets/university/entrants-menu/entrants.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ActivatedRoute, Router} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {StepperSelectionEvent} from '@angular/cdk/stepper';
import {MatCheckboxChange} from '@angular/material/checkbox';

@Component({
  selector: 'app-entrants-add',
  templateUrl: './entrants-add.component.html',
  styleUrls: ['./entrants-add.component.scss']
})
export class EntrantsAddComponent implements OnInit, OnDestroy {

  entrantForm: FormGroup;
  identificationForm: FormGroup;
  educationForm: FormGroup;

  private ngUnsubscribe: Subject<void> = new Subject<void>();

  search = {
    okcm: '',
    directions: '',
    regions: '',
  };

  cls = {
    gender: {
      data: [],
      initFunc:  () => this.clsService.getClsData('gender'),
      isLoad: true
    },
    okcm: {
      data: [],
      initFunc:  () => this.clsService.getClsData('v_okcm'),
      isLoad: true
    },
    educationDoc: {
      data: [],
      initFunc:  () => this.clsService.getClsData('v_document_types', '', { name_table: 'educations'}),
      isLoad: true
    },
    educationLevel: {
      data: [],
      initFunc:  () => this.clsService.getClsData('document_education_levels'),
      isLoad: true
    },
    directions: {
      data: [],
      initFunc:  () =>  this.clsService.getClsData('v_direction_specialty'),
      isLoad: true,
    },
    regions: {
      data: [],
      initFunc:  () =>  this.clsService.getClsData('regions'),
      isLoad: true,
    }
  };

  addresses = [
    {
      id: 'registration_addr',
      title: 'Адрес Регистрации',
      search: {
        region: ''
      }
    },
    {
      id: 'fact_addr',
      title: 'Адрес фактического места жительства',
      search: {
        region: ''
      }
    },
  ];

  date: any;


  constructor( private clsService: ClsService,
               private entrantService: EntrantsService,
               private fb: FormBuilder,
               private snackBar: MatSnackBar,
               private router: Router,
               private route: ActivatedRoute,
               private titleService: Title) { }

  ngOnInit() {
    this.initCls();
    this.initForms();
    this.titleService.setTitle('Добавление абитуриента');
  }

  initCls() {
    for (let key in this.cls) {
      if ( this.cls[key].isLoad ) {
        this.cls[key].initFunc()
            .pipe(  takeUntil(this.ngUnsubscribe) )
            .subscribe( res => {
              this.cls[key].data = res;
            });
      }
    }
  }

  initForms() {
    this.entrantForm = this.fb.group({
      name:         [null, Validators.required ],
      surname:      [null, Validators.required ],
      patronymic:   [null],
      birthday:     [null, Validators.required],
      id_gender:    [null, Validators.required],
      id_okcm:      [185, Validators.required],
      birthplace:   [null, Validators.required],
      phone:        [null],
      email:        [null],
      snils:        [null, Validators.required],
    });

    this.identificationForm = this.fb.group({
      id_document_type: [1, Validators.required],
      surname:          [null, Validators.required],
      name:             [null, Validators.required],
      patronymic:       [null],
      doc_series:       [null, Validators.required],
      doc_number:       [null, Validators.required],
      doc_organization: [null, Validators.required],
      issue_date:       [null, Validators.required],
      subdivision_code: [null, [Validators.required, Validators.maxLength(7)]],
      id_okcm:          [null, Validators.required],

    });

    this.educationForm = this.fb.group({
      id_document_type:     [null, Validators.required],
      doc_name:             [null, Validators.required],
      doc_org:              [null],
      register_number:      [null],
      doc_number:           [null, Validators.required],
      doc_series:           [null, Validators.required],
      issue_date:           [null, Validators.required],
      id_direction:         [null],
      id_education_level:   [null, Validators.required],
    });

    this.entrantForm.valueChanges.subscribe( res => {

    });
  }

  initAddrControls() {
    return  this.fb.group({
      index_addr:         [null, Validators.required],
      id_region:          [null, Validators.required],
      area:               [null],
      city:               [null, Validators.required],
      city_area:          [null],
      place:              [null],
      street:             [null, Validators.required],
      additional_area:    [null],
      additional_street:  [null],
      house:              [null, Validators.required],
      building1:          [null],
      building2:          [null],
      apartment:          [null],
    });
  }

  getForms() {
    /*console.log(this.entrantForm.value);
    const data = this.entrantForm.value;
    console.log(data, moment(this.entrantForm.controls['birthday'].value).format());
*/
  }

  addAddress(key: string) {
    this.entrantForm.addControl(key, this.initAddrControls());
  }

  delAddress(key: string) {
    this.entrantForm.removeControl(key);
  }

  sendData() {
    const sendData = {
      entrant: this.prepareData(this.entrantForm.value),
      identification: this.prepareData(this.identificationForm.value),
      education: this.prepareData(this.educationForm.value)
    };

    console.log('Send data', sendData);

    this.entrantService.addEntrant(sendData)
        .subscribe( res => {
          console.log('res', res);
          if (res.done) {
            this.snackBar.open('Добавление прошло успешно');
            const id = res.data.id_entrant;
            this.router.navigate(['../', id], {relativeTo: this.route});
          } else {
            this.snackBar.open(res.message || 'Во время добавления произошла ошибка');
          }
        });
  }

  prepareData(data) {
    let returnData = data;
    for (let key in returnData) {
      if ( key === 'issue_date' || key === 'birthday' ) {
       // console.log('before', key, moment(returnData[key]), moment(returnData[key]).format());
        returnData[key] = moment(returnData[key]).format();

      }
    }

    return returnData;
  }

  setData() {
    this.entrantForm.setValue({
      name:         'Иван',
      surname:      'Иванов',
      patronymic:   'Иванович',
      birthday:     moment('2005-09-01'),
      id_gender:    1,
      id_okcm:      185,
      birthplace:   'Москва',
      phone:        null,
      email:        null,
      snils:        '555-555-555 23',
    });

    this.identificationForm.setValue({
      id_document_type: 1,
      surname:          'Иванов',
      name:             'Иван',
      patronymic:       'Иванович',
      doc_series:       '5555',
      doc_number:       '555555',
      doc_organization: 'Москва',
      issue_date:       moment('2020-04-15'),
      subdivision_code: '5003',
      id_okcm:          185,
    });

    this.educationForm.setValue( {
      id_document_type:     7,
      doc_name:             'Аттестат',
      doc_org:              'школа №3',
      register_number:       null,
      doc_number:           '7777777',
      doc_series:           '77 AB',
      issue_date:           moment('2020-04-15'),
      id_direction:         null,
      id_education_level:   1,
    });
  }

  ngOnDestroy(): void {
    //console.log('unsubscribe');
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  changeIndex($event: StepperSelectionEvent) {
    if( $event.selectedIndex === 1 && $event.previouslySelectedIndex === 0 ) {
      let keys = ['surname', 'name', 'patronymic', 'id_okcm' ];
      for ( let k in keys ) {
        let control = this.identificationForm.get(keys[k]) as FormControl;
        //console.log( 'control', control );
        // заполняем контролы в identification только если они пустые
        if (!control.value && this.entrantForm.get(keys[k]).value) {
            control.setValue(this.entrantForm.get(keys[k]).value);
        }
      }
    }

  }


  copyAddress($event: MatCheckboxChange, controlToA: AbstractControl, controlFromA: AbstractControl) {
    const controlTo = controlToA as FormGroup;
    const controlFrom = controlFromA as FormGroup;
    if ($event.checked ) {
      for ( let key in controlFrom.controls ) {
        controlTo.get(key).setValue(controlFrom.get(key).value);
      }
    }
  }
}
