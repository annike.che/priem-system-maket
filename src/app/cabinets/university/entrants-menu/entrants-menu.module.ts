import { NgModule } from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {SharedModule} from '@/common/modules/shared.module';
import { EntrantsListComponent } from './pages/entrants-list/entrants-list.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { EntrantsAddComponent } from './pages/entrants-add/entrants-add.component';
import {NgxMaskModule} from 'ngx-mask';
import {IMaskModule} from 'angular-imask';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {ArtLibModule} from '@art-lib/art-lib.module';



const MY_DATE_FORMATS = {
    parse: {
        dateInput: ['DD.MM.YYYY', 'LL', ],
    },
    display: {
        dateInput: 'DD.MM.YYYY',
        monthYearLabel: 'YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY'
    },
};

const routes: Routes = [
  { path: '', component: EntrantsListComponent  },
  { path: 'add', component: EntrantsAddComponent},
  { path: ':id',
    loadChildren: () => import('./pages/entrants-detail/entrants-detail.module').then( m => m.EntrantsDetailModule ),
    data: {preload: true}
  }
 /* { path: ':id/edit', loadChildren: () => import('./pages/entrants-edit/entrants-edit.module').then( m => m.EntrantsEditModule ) }*/
];

@NgModule({
  declarations: [
      EntrantsListComponent,
      EntrantsAddComponent
      ],
  imports: [
    SharedModule,
    RouterModule.forChild(routes),
    FormsModule,
    NgxMaskModule.forRoot(),
    IMaskModule,
    ReactiveFormsModule,
    ArtLibModule
  ],
    providers: [
      { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
      { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS }
    ]
})
export class EntrantsMenuModule { }
