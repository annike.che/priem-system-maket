import { Injectable } from '@angular/core';
import {HttpClient, HttpEvent, HttpEventType, HttpHeaders, HttpParams, HttpResponse} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {Observable, pipe} from 'rxjs';
import {filter, map, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EntrantsService {

  urlPrefix: string;
  urlGroup = '/entrants';

  constructor(private httpClient: HttpClient) {
    this.urlPrefix = environment.apiUrl;
  }

  getEntrantsList(params?) {
    const url = `${this.urlPrefix}${this.urlGroup}/list`;


    console.log('paramsData', params);


    return this.httpClient.get(url, { params });
  }

  getEntrantsMainInfo(id): Observable<any> {
    const url = `${this.urlPrefix}${this.urlGroup}/${id}/main`;

    return this.httpClient.get(url);
  }

  getDocsList(idEntrant: number, type: string): Observable<any> {
    const url = `${this.urlPrefix}${this.urlGroup}/${idEntrant}/${type}`;

    return this.httpClient.get(url);
  }

  getDocInfo(id: number, docType: string): Observable<any> {
    const url = `${this.urlPrefix}/docs/${docType}/${id}/info`;

    return this.httpClient.get(url);
  }

  editDoc( id: number, docType: string, data: any): Observable<any> {
    const url = `${this.urlPrefix}/docs/${docType}/${id}/edit`;

    return this.httpClient.post(url, data);
  }

  addEntrant(data): Observable<any> {
    const url = `${this.urlPrefix}${this.urlGroup}/add`;

    return this.httpClient.post(url, data);
  }

  addEntrantDocument(idEntrant, tableName, data): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${idEntrant}/docs/${tableName}/add`;

    const headers = new HttpHeaders({
      'key': 'Content-Type',
      'value': 'multipart/form-data'
    });

    return this.httpClient.post(url, toFormData(data), { headers});
  }

  getEntrantIdentsList(id): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${id}/idents/list`;
    return this.httpClient.get(url)
        .pipe(
            map((resp: any) => {
              return (resp.done) ? resp.data : [];
            })
        );

  }

  getEntrantShortInfo(id): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${id}/short`;

    return this.httpClient.get(url);
  }

  getShortDocList(id,  params?): Observable<any> {
    const url =  this.urlPrefix + this.urlGroup + `/${id}/docs/short`;

    return this.httpClient.get(url, {params});
  }

  getApplicationsList( id ): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${id}/applications`;
    return this.httpClient.get(url);
  }

  getEntrantPhoto(id): Observable<Blob> {
    const url = this.urlPrefix + this.urlGroup + `/photo/${id}/file`;
    return this.httpClient.get(url, { responseType: 'blob' });
  }
  removeEntrantPhoto(id): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/photo/${id}/file/remove`;
    return this.httpClient.post(url, id);
  }

  addEntrantPhoto(id, data) {
    const headers = new HttpHeaders({
      'key': 'Content-Type',
      'value': 'multipart/form-data'
    });

    const url = this.urlPrefix + this.urlGroup + `/${id}/photo/file/add`;
    return this.httpClient.post(url, toFormData(data), { headers});
  }

}
export function toFormData<T>( formValue: T ) {
  const formData = new FormData();

  for ( const key of Object.keys(formValue) ) {
    const value = formValue[key];
    if( value ) formData.append(key, value);
  }


  return formData;
}

export function toResponseBody<T>() {
  return pipe(
      filter(( event: HttpEvent<T> ) => event.type === HttpEventType.Response),
      map(( res: HttpResponse<T> ) => res.body)
  );
}
export function uploadProgress<T>( cb: ( progress: number ) => void ) {
  return tap(( event: HttpEvent<T> ) => {
    if ( event.type === HttpEventType.UploadProgress ) {
      cb(Math.round((100 * event.loaded) / event.total));
    }
  });
}

