import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {PageEvent} from '@angular/material/paginator';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ActivatedRoute, Router} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {PackagesRatingService} from '@/cabinets/university/packages-menu/pages/rating/packages-rating.service';
import {debounceTime, distinctUntilChanged, switchMap, tap} from 'rxjs/operators';
import {untilDestroyed} from 'ngx-take-until-destroy';
import {SortDirection} from '@angular/material/sort';
import * as cloneDeep from 'lodash/cloneDeep';
import {artAnimations} from '@art-lib/animations';

@Component({
  selector: 'app-packages-rating-detail',
  templateUrl: './packages-rating-detail.component.html',
  styleUrls: ['./packages-rating-detail.component.scss'],
  animations: artAnimations
})
export class PackagesRatingDetailComponent implements OnInit, OnDestroy {
  idPackage: number;
  data: Array<any> = [];
  packageInfo: any;
  displayedColumns: Array<string>;
  pageSizeOptions: number[];

  isLoad: boolean;


  defaultParams = {
    page: 1,
    limit: 20
  };

  search = new Subject<string>();
  sortEvent = new Subject<SortEvent>();
  pagination = new Subject<PageEvent>();

  total: number;

  params: Params = {
    ...this.defaultParams
  };

  constructor(private packagesRatingService: PackagesRatingService,
              private snackBar: MatSnackBar,
              private route: ActivatedRoute,
              private router: Router,
              private titleService: Title) {

    this.titleService.setTitle('Детализация пакета с данными о конкурных списках');

    this.displayedColumns = ['id', 'fio',
      'entranceTest1', 'result1', 'entranceTest2', 'result2',
      'entranceTest3', 'result3', 'entranceTest4', 'result4',
      'entranceTest5', 'result5',  'sumMark',
      'withoutTests', 'benefit', 'rating', 'agreed', 'original',
      'checked', 'error'];

    this.pageSizeOptions = [5, 10, 15, 20, 30, 50, 100];

  }

  ngOnInit() {

    this.route.params.subscribe( res => {
      this.idPackage = +res.id;

      if( this.idPackage ) {
        this.getQueryParamsFromUrl();
        this.subscribeOnChanges();
        this.loadData();
      }
    });

  }

  subscribeOnChanges() {

    this.search.pipe(
        distinctUntilChanged(),
        debounceTime(700),
        tap(() => {
          this.params.page = 1;
        }),
        switchMap((value, index) => {
          console.log('value', value);
          return this.getDataFromService();
        }),
        untilDestroyed(this)
    ).subscribe(response => this.setData(response));

    this.pagination.pipe(
        debounceTime(200),
        tap((event: PageEvent) => {
          if (this.params.limit !== event.pageSize) {
            this.params.limit = event.pageSize;
            this.params.page = 1;
          }
          if (this.params.page !== event.pageIndex + 1) {
            this.params.page = event.pageIndex + 1;
          }
        }),
        switchMap(() => this.getDataFromService()),
        untilDestroyed(this)
    )
        .subscribe(response => this.setData(response));


    this.sortEvent.pipe(
        debounceTime(200),
        tap((event: SortEvent) => {
          this.params.order = event.direction as SortDirection;
          this.params.sortby = event.active;
        }),
        switchMap(() => this.getDataFromService()),
        untilDestroyed(this)
    )
        .subscribe(response => this.setData(response));

  }

  loadData() {
    this.getDataFromService()
        .subscribe(response => this.setData(response));
  }

  setData(response) {
    if (response.done) {

      this.data = response.data.items || [];
      this.packageInfo = response.data.package_info;
      this.params.page = response.paginator.page;
      this.params.limit = response.paginator.limit;
      this.total = response.paginator.total;

    }
    this.isLoad = false;
  }


  getQueryParamsFromUrl() {
    const queryParams = cloneDeep(this.route.snapshot.queryParams);
    this.setParams(queryParams);
  }

  setParams(params) {
    Object.keys(params).forEach(key => {
      switch (key) {
        case 'page':
          this.params[key] = +params[key];
          break;
        default:
          this.params[key] = params[key];
      }
    });
  }

  passQueryParamsToUrl(queryParams) {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams
    });
  }

  getDataFromService(): Observable<any> {
    this.isLoad = true;
    const queryParams = this.getQueryParams(this.params);
    this.passQueryParamsToUrl(queryParams);
    return this.packagesRatingService.getPackageRatingInfo(this.idPackage, this.params);
  }

  getQueryParams(params) {
    const queryParams: any = {};
    Object.keys(params).forEach((key) => {
      if (this.defaultParams.hasOwnProperty(key) && (this.defaultParams[key] !== params[key] && params[key] !== '') ||
          (!this.defaultParams.hasOwnProperty(key) && params[key] !== '')) {
        queryParams[key] = params[key];
      }
    });

    return queryParams;
  }

  ngOnDestroy(): void {

  }

}

interface Params {
  page: number;
  limit: number;
  order?: SortDirection; // 'ASC' || 'DESC'
  sortby?: string;
}

