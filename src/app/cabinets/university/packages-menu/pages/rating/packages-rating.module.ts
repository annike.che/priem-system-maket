import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '@/common/modules/shared.module';
import {ArtLibModule} from '@art-lib/art-lib.module';
import { PackagesRatingListComponent } from './packages-rating-list/packages-rating-list.component';
import { PackagesRatingDetailComponent } from './packages-rating-detail/packages-rating-detail.component';
import {AddPackageRatingDialogComponent} from '@/cabinets/university/packages-menu/components/add-package-rating-dialog/add-package-rating-dialog.component';

const routes: Routes = [
  { path: '', component: PackagesRatingListComponent },
  { path: ':id', component: PackagesRatingDetailComponent }
];

@NgModule({
  declarations: [
    PackagesRatingListComponent,
    PackagesRatingDetailComponent,
    AddPackageRatingDialogComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    SharedModule,
    ArtLibModule
  ],
  entryComponents: [
    AddPackageRatingDialogComponent
  ]
})
export class PackagesRatingModule { }
