import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {toFormData} from '@/cabinets/university/entrants-menu/entrants.service';

@Injectable({
  providedIn: 'root'
})
export class PackagesOrderService {

  urlPrefix: string;
  urlGroup = '/packages';
  urlSubGroup = '/order';

  constructor(private httpClient: HttpClient) {
    this.urlPrefix = environment.apiUrl;
  }

  getPackagesList(params?): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + this.urlSubGroup + '/list';

    return this.httpClient.get(url, { params });
  }

  getPackagesInfo(id, params?): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + this.urlSubGroup +  `/${id}/list`;

    return this.httpClient.get(url, {params});

  }

  addPackage(data): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + this.urlSubGroup + '/file/add';

    const headers = new HttpHeaders({
      'key': 'Content-Type',
      'value': 'multipart/form-data'
    });

    return this.httpClient.post(url, toFormData(data), { headers});
  }

  getPackage(id): Observable<HttpResponse<Blob>> {
    const url = this.urlPrefix + this.urlGroup + this.urlSubGroup + `/${id}/file/get`;


    return this.httpClient.get<Blob>(url, {responseType: 'blob' as 'json',  observe: 'response' });
  }
}
