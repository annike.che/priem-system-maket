import { NgModule } from '@angular/core';
import { PackagesOrderListComponent } from './packages-order-list/packages-order-list.component';
import { PackagesOrderDetailComponent } from './packages-order-detail/packages-order-detail.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '@/common/modules/shared.module';
import {ArtLibModule} from '@art-lib/art-lib.module';
import {AddPackageOrderDialogComponent} from '@/cabinets/university/packages-menu/components/add-package-order-dialog/add-package-order-dialog.component';

const routes: Routes = [
  { path: '', component: PackagesOrderListComponent },
  { path: ':id', component: PackagesOrderDetailComponent }
];

@NgModule({
  declarations: [
      PackagesOrderListComponent,
      PackagesOrderDetailComponent,
      AddPackageOrderDialogComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    SharedModule,
    ArtLibModule
  ],
  entryComponents: [
    AddPackageOrderDialogComponent
  ]
})
export class PackagesOrderModule { }
