import { NgModule } from '@angular/core';
import { PackagesCompetitiveListComponent } from './packages-competitive-list/packages-competitive-list.component';
import { PackagesCompetitiveDetailComponent } from './packages-competitive-detail/packages-competitive-detail.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '@/common/modules/shared.module';
import {ArtLibModule} from '@art-lib/art-lib.module';
import {AddPackageCompetitiveDialogComponent} from '@/cabinets/university/packages-menu/components/add-package-competitive-dialog/add-package-competitive-dialog.component';
const routes: Routes = [
  { path: '', component: PackagesCompetitiveListComponent },
  { path: ':id', component: PackagesCompetitiveDetailComponent }
];

@NgModule({
  declarations: [
    PackagesCompetitiveListComponent,
    PackagesCompetitiveDetailComponent,
    AddPackageCompetitiveDialogComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    SharedModule,
    ArtLibModule
  ],
  entryComponents: [
    AddPackageCompetitiveDialogComponent
  ]
})
export class PackagesCompetitiveModule { }
