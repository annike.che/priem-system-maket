import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {PageEvent} from '@angular/material/paginator';
import {PackagesRatingService} from '@/cabinets/university/packages-menu/pages/rating/packages-rating.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog';
import {PackagesCompetitiveService} from '@/cabinets/university/packages-menu/pages/competitive/packages-competitive.service';
import {debounceTime, distinctUntilChanged, switchMap, tap} from 'rxjs/operators';
import {untilDestroyed} from 'ngx-take-until-destroy';
import {SortDirection} from '@angular/material/sort';
import * as cloneDeep from 'lodash/cloneDeep';
import {AddPackageCompetitiveDialogComponent} from '@/cabinets/university/packages-menu/components/add-package-competitive-dialog/add-package-competitive-dialog.component';
import {artAnimations} from '@art-lib/animations';
import * as fileHandling from '@/common/functions/files';

@Component({
  selector: 'app-packages-competitive-list',
  templateUrl: './packages-competitive-list.component.html',
  styleUrls: ['./packages-competitive-list.component.scss'],
  animations: artAnimations
})
export class PackagesCompetitiveListComponent implements OnInit, OnDestroy {

  packages: Array<PackageList> = [];

  isLoad: boolean;

  displayedColumns: Array<string>;
  pageSizeOptions: number[];

  defaultParams = {
    page: 1,
    limit: 20
  };

  search = new Subject<string>();
  sortEvent = new Subject<SortEvent>();
  pagination = new Subject<PageEvent>();

  total: number;

  params: Params = {
    ...this.defaultParams
  };

  constructor(
     private packagesCompetitiveService: PackagesCompetitiveService,
     private router: Router,
     private route: ActivatedRoute,
     private titleService: Title,
     private snackBar: MatSnackBar,
     private dialog: MatDialog
  ) {
    this.isLoad = false;
    this.titleService.setTitle('Загруженные пакеты c рейтингами по конкурсу');

    this.displayedColumns = ['id', 'name', 'count_all', 'count_add', 'status', 'created', 'duration', 'error', 'downloadPackage'];
    this.pageSizeOptions = [5, 10, 15, 20, 30, 50, 100];

  }

  ngOnInit() {
    this.getQueryParamsFromUrl();
    this.subscribeOnChanges();
    this.getRatingList();
  }

  subscribeOnChanges() {

    this.search.pipe(
        distinctUntilChanged(),
        debounceTime(700),
        tap(() => {
          this.params.page = 1;
        }),
        switchMap((value, index) => {
          console.log('value', value);
          return this.getDataFromService();
        }),
        untilDestroyed(this)
    ).subscribe(response => this.setData(response));

    this.pagination.pipe(
        debounceTime(200),
        tap((event: PageEvent) => {
          if (this.params.limit !== event.pageSize) {
            this.params.limit = event.pageSize;
            this.params.page = 1;
          }
          if (this.params.page !== event.pageIndex + 1) {
            this.params.page = event.pageIndex + 1;
          }
        }),
        switchMap(() => this.getDataFromService()),
        untilDestroyed(this)
    )
        .subscribe(response => this.setData(response));


    this.sortEvent.pipe(
        debounceTime(200),
        tap((event: SortEvent) => {
          this.params.order = event.direction as SortDirection;
          this.params.sortby = event.active;
        }),
        switchMap(() => this.getDataFromService()),
        untilDestroyed(this)
    )
        .subscribe(response => this.setData(response));

  }

  getRatingList() {
    this.getDataFromService()
        .subscribe(response => this.setData(response));
  }

  setData(response) {
    if (response.done) {

      this.packages = response.data;
      this.params.page = response.paginator.page;
      this.params.limit = response.paginator.limit;
      this.total = response.paginator.total;

    }
    this.isLoad = false;
  }


  getQueryParamsFromUrl() {
    const queryParams = cloneDeep(this.route.snapshot.queryParams);
    this.setParams(queryParams);
  }

  setParams(params) {
    Object.keys(params).forEach(key => {
      switch (key) {
        case 'page':
          this.params[key] = +params[key];
          break;
        default:
          this.params[key] = params[key];
      }
    });
  }

  passQueryParamsToUrl(queryParams) {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams
    });
  }

  getDataFromService(): Observable<any> {
    this.isLoad = true;
    const queryParams = this.getQueryParams(this.params);
    this.passQueryParamsToUrl(queryParams);
    return this.packagesCompetitiveService.getPackagesList(this.params);
  }

  getQueryParams(params) {
    const queryParams: any = {};
    Object.keys(params).forEach((key) => {
      if (this.defaultParams.hasOwnProperty(key) && (this.defaultParams[key] !== params[key] && params[key] !== '') ||
          (!this.defaultParams.hasOwnProperty(key) && params[key] !== '')) {
        queryParams[key] = params[key];
      }
    });

    return queryParams;
  }

  addPackage() {
    this.dialog.open(AddPackageCompetitiveDialogComponent, {
      minWidth: '550px'
    }).afterClosed().subscribe( data => {
      if ( data ) {
        this.getRatingList();
      }
    });
  }

  getPackage(id: number, name: string) {
    this.packagesCompetitiveService.getPackage(id)
        .subscribe( res => {

          const header = res.headers.get('content-disposition');
          if( header  ) {
            var result = header.split(';')[1].trim().split('=')[1];
            name = result.replace(/"/g, '') ;
          }

          fileHandling.handlingResponse(res.body, name);

        }, err => {
          this.snackBar.open( fileHandling.handlingError(err));
        });
  }

  ngOnDestroy(): void {

  }

}

interface Params {
  search_name?: string;
  page: number;
  limit: number;
  order?: SortDirection; // 'ASC' || 'DESC'
  sortby?: string;
}


