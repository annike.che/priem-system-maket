import { NgModule } from '@angular/core';
import { PackagesEgeListComponent } from './packages-ege-list/packages-ege-list.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '@/common/modules/shared.module';
import {ArtLibModule} from '@art-lib/art-lib.module';
import {AddPackageEgeDialogComponent} from '@/cabinets/university/packages-menu/components/add-package-ege-dialog/add-package-ege-dialog.component';
import { PackagesEgeDetailComponent } from './packages-ege-detail/packages-ege-detail.component';

const routes: Routes = [
  { path: '', component: PackagesEgeListComponent},
  { path: ':id', component: PackagesEgeDetailComponent }
];


@NgModule({
  declarations: [
      PackagesEgeListComponent,
      AddPackageEgeDialogComponent,
      PackagesEgeDetailComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    SharedModule,
    ArtLibModule
  ],
  entryComponents: [
    AddPackageEgeDialogComponent
  ]
})
export class PackagesEgeModule { }
