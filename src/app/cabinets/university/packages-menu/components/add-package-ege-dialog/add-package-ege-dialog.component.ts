import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {PackagesEgeService} from '@/cabinets/university/packages-menu/pages/ege/packages-ege.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-add-package-ege-dialog',
  templateUrl: './add-package-ege-dialog.component.html',
  styleUrls: ['./add-package-ege-dialog.component.scss']
})
export class AddPackageEgeDialogComponent implements OnInit {

  form: FormGroup;
  isLoad: boolean;

  constructor(
      private fb: FormBuilder,
      private egeService: PackagesEgeService,
      private snackBar: MatSnackBar,
      public dialogRef: MatDialogRef<AddPackageEgeDialogComponent>
  ) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.form = this.fb.group( {
        file: [null, Validators.required],
        name: [null, Validators.required]
    });

    this.form.controls['file'].valueChanges.subscribe( res => {
      console.log('res', res);
      if ( res && res instanceof File) {
        let nameFiled = this.form.controls['name'] as AbstractControl;
        if( !nameFiled.value || nameFiled.value === '') {
          nameFiled.setValue(res.name);
        }
      }
    });
  }

  save() {
    this.isLoad = true;
    this.egeService.addPackageEge(this.form.value)
        .subscribe( res => {
            if( res.done ) {
              this.snackBar.open('Данные успешно загружены');
              this.dialogRef.close({done: true});
            } else {
              this.snackBar.open('Произошла ошибка' + (res.message ? ': ' + res.message : ''));
            }
            this.isLoad = false;
        });
  }
}
