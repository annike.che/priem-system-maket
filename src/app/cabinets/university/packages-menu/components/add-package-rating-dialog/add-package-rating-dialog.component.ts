import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PackagesEgeService} from '@/cabinets/university/packages-menu/pages/ege/packages-ege.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialogRef} from '@angular/material/dialog';
import {PackagesRatingService} from '@/cabinets/university/packages-menu/pages/rating/packages-rating.service';

@Component({
  selector: 'app-add-package-rating-dialog',
  templateUrl: './add-package-rating-dialog.component.html',
  styleUrls: ['./add-package-rating-dialog.component.scss']
})
export class AddPackageRatingDialogComponent implements OnInit {

  form: FormGroup;
  isLoad: boolean;

  constructor(
      private fb: FormBuilder,
      private ratingService: PackagesRatingService,
      private snackBar: MatSnackBar,
      public dialogRef: MatDialogRef<AddPackageRatingDialogComponent>
  ) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.form = this.fb.group( {
      file: [null, Validators.required],
      name: [null, Validators.required]
    });

    this.form.controls['file'].valueChanges.subscribe( res => {
      console.log('res', res);
      if ( res && res instanceof File) {
        let nameFiled = this.form.controls['name'] as AbstractControl;
        if( !nameFiled.value || nameFiled.value === '') {
          nameFiled.setValue(res.name);
        }
      }
    });
  }

  save() {
    this.isLoad = true;
    this.ratingService.addPackageRating(this.form.value)
        .subscribe( res => {
          if ( res.done ) {
            this.snackBar.open('Данные успешно загружены');
            this.dialogRef.close({done: true});
          } else {
            this.snackBar.open('Произошла ошибка' + (res.message ? ': ' + res.message : ''));
          }
          this.isLoad = false;
        });
  }

}
