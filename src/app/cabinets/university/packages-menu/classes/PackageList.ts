interface PackageList {
  id: number;
  name: string;
  id_status: number;
  name_status: string;
  code_status: string;
  count_add: number;
  count_all: number;
  error: string | null;
  created: string;
}
