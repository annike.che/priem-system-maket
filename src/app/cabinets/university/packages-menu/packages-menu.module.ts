import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'ege', pathMatch: 'full'},
  { path: 'ege', loadChildren: () => import('./pages/ege/packages-ege.module').then( m => m.PackagesEgeModule )  },
  { path: 'rating', loadChildren: () => import('./pages/rating/packages-rating.module').then( m => m.PackagesRatingModule)  },
  { path: 'competitive', loadChildren: () => import('./pages/competitive/packages-competitive.module').then( m => m.PackagesCompetitiveModule)},
  { path: 'order', loadChildren: () => import('./pages/order/packages-order.module').then( m => m.PackagesOrderModule)},

];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes),
  ]
})
export class PackagesMenuModule { }
