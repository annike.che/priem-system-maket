import { Component, OnInit } from '@angular/core';
import {Subject} from 'rxjs';
import {Router} from '@angular/router';

@Component({
  selector: 'app-university',
  templateUrl: './university.component.html',
  styleUrls: ['./university.component.scss']
})
export class UniversityComponent implements OnInit {

  menuItems: Array<IMenuItems>;

  toggleMenu: Subject<void> = new Subject<void>();

  currUrl = '';

  constructor(private router: Router) {
    this.currUrl = router.url;

    this.menuItems  = [
      {
        title: 'Приемная кампания',
        router: 'campaign',
        icon: 'flag-3'
      },
      {
        title: 'Абитуриенты',
        router: 'entrants',
        icon: 'student-hat'
      },
      {
        title: 'Заявления',
        router: 'application',
        icon: 'file'
      },
      {
        title: 'Приказы',
        router: 'orders',
        icon: 'diploma'
      },
      /*{
        title: 'Справочники',
        router: '',
        icon: 'books'
      },
      {
        title: 'Настройки',
        router: '',
        icon: 'gear'
      },*/
      {
        title: 'Организация',
        router: 'orgs',
        icon: 'org-1',
        open: this.currUrl.indexOf('/orgs') >= 0,
        children: [
          {
            title: 'Направления подготовки',
            router: 'directions'
          },
          {
            title: 'Управление',
            router: 'settings'
          }
        ]
      },
      {
        title: 'Пакетная загрузка',
        router: 'packages',
        icon: 'upload-packages-1',
        open: this.currUrl.indexOf('/packages') >= 0,
        children: [
          {
            title: 'Результаты ЕГЭ',
            router: 'ege'
          },
          {
            title: 'Рейтинги по конкурсу',
            router: 'competitive'
          },
          {
            title: 'Конкурсные списки',
            router: 'rating'
          },
          {
            title: 'Приказы о зачислении',
            router: 'order'
          }
        ]
      },
      {
        title: 'Новости',
        router: 'news',
        icon: 'news'
      },
    ];

  }

  ngOnInit() {
  }

  emitEventToggleMenu($event) {
    this.toggleMenu.next($event);
  }

}
