interface DirectionsItem {
    code: string;
    code_parent: string;
    id: number;
    id_education_level: number;
    id_parent: number;
    name: string;
    name_education_level: string;
    name_parent: string;
}
