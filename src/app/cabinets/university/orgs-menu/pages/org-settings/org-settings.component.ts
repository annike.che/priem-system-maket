import {Component, OnInit} from '@angular/core';
import {artAnimations} from '@art-lib/animations';
import {OrgsService} from '@/cabinets/university/orgs-menu/orgs.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import {delay} from 'rxjs/operators';
import {UserProfileService} from '@/common/services/user-profile.service';

@Component({
  selector: 'app-org-settings',
  templateUrl: './org-settings.component.html',
  styleUrls: ['./org-settings.component.scss'],
  animations: artAnimations
})
export class OrgSettingsComponent implements OnInit {

  certificate: any;
  formData: FormGroup;

  isOovo = false;
  isOovo_c = false;

  uinfo: User;

  isLoad: boolean = false;

  constructor(private orgService: OrgsService,
              private fb: FormBuilder,
              private _snackBar: MatSnackBar,
              private uinfoService: UserProfileService) {
  }

  ngOnInit() {
    this.initData();

    this.uinfo = this.uinfoService.getProfile();
    console.log('uinfo', this.uinfo);
    if (this.uinfo.hasOwnProperty('organization')) {
      this.isOovo = this.uinfo.organization.is_oovo;
      this.isOovo_c = this.uinfo.organization.is_oovo;
    }
  }

  initForm() {
    this.formData =  this.fb.group( {
      file : [null, Validators.required]
    });
  }

  initData() {
    this.orgService.getSertificateInfo().subscribe(res => {
          if (res.done) {
            this.certificate = res.data;
          } else {
            this._snackBar.open('При попытке загрузить файл произошла ошибка' + ((res.message) ? ': ' + res.message : '') );
          }
        }
    );
  }

  download() {
    this.isLoad = true;
    this.orgService.changeSetificate(this.formData.value)
        .subscribe(resp => {
          if (resp.done) {
            this.initData();
            this.formData = undefined;
            this._snackBar.open('Файл успешно загружен');
          } else {
            this._snackBar.open( 'При загрузке файла произошла ошибка' + (resp.message ? ': ' + resp.message : ''),  '', {duration: 5000});
          }
          this.isLoad = false;
    });
  }


  saveOovo() {
    this.orgService.saveOovo(this.isOovo)
        .subscribe( res => {
            if( res.done ) {
              this.isOovo_c = this.isOovo;
              this.uinfoService.setIsOovo(this.isOovo);
              this._snackBar.open('Данные успешно обновлены');
            } else {
              this._snackBar.open( 'Произошла ошибка' + (res.message ? ': ' + res.message : ''),  '', {duration: 5000});
            }
        });
  }
}
