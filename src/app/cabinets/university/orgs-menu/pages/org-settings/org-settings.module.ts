import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrgSettingsComponent } from './org-settings.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '@/common/modules/shared.module';
import {ArtLibModule} from '@art-lib/art-lib.module';

const routes: Routes = [
  { path: '', component: OrgSettingsComponent },

];

@NgModule({
  declarations: [OrgSettingsComponent],
  imports: [
    RouterModule.forChild(routes),
    SharedModule,
    ArtLibModule
  ]
})
export class OrgSettingsModule { }
