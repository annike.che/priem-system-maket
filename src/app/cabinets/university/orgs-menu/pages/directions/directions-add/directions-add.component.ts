import {Component, OnDestroy, OnInit} from '@angular/core';
import {ClsService} from '@/common/services/cls.service';
import {OrgsService} from '@/cabinets/university/orgs-menu/orgs.service';
import {Subject} from 'rxjs';
import {distinctUntilChanged, switchMap, takeUntil} from 'rxjs/operators';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-directions-add',
  templateUrl: './directions-add.component.html',
  styleUrls: ['./directions-add.component.scss']
})
export class DirectionsAddComponent implements OnInit, OnDestroy {

  directionsForm: FormGroup;

  items: any;

  search: Array<SearchModel> = [];

  private ngUnsubscribe: Subject<void> = new Subject<void>();

  cls = {
    education_level: {
      data: []
    },
    items: []
  };

  clsSearch = [];

  constructor(
      private clsService: ClsService,
      private orgsService: OrgsService,
      private fb: FormBuilder,
      private snackBar: MatSnackBar,
      private router: Router,
      private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.initForm();

    this.clsSearch.push( this.addBehaviorSubject() );
    this.search.push( this.addSearch() );
    this.cls.items.push( this.addCls(0) );


    this.initEducationLevels();

  }

  initForm() {
    this.directionsForm = this.fb.group( {
      data: this.fb.array( [this.addGroup()], this.validateSize)
    });

    this.items = this.directionsForm.get('data') as FormArray;


  }

  validateSize(arr: FormArray) {
    return arr.length <= 0 ? {
      invalidSize: true
    } : null;
  }

  addGroup() {
    return this.fb.group({
      id_education_level: [ null, Validators.required],
      id_group_speciality: [ null, Validators.required ],
      id_speciality: [ null, Validators.required ]
    });
  }

  initEducationLevels() {
    this.clsService.getClsData( 'education_levels' )
        .subscribe( res => {
          this.cls.education_level.data = res;
        });
  }

  addCls(i) {
    return {
      group_speciality$: this.clsSearch[i].group_speciality.pipe(
          distinctUntilChanged(),
          takeUntil(this.ngUnsubscribe),
          switchMap((id: number) => {
                return this.clsService.getClsData('v_okso_enlarged_group', '' , { id_education_level: id});
              }
          )),
      speciality$: this.clsSearch[i].speciality.pipe(
          distinctUntilChanged(),
          takeUntil(this.ngUnsubscribe),
          switchMap((id: number) => {
                return this.clsService.getClsData('v_okso_specialty',  '', { id_parent: id});
              }
          )),
    };
  }

  private addSearch() {
    return {
      group_speciality: '',
      speciality: ''
    };
  }

  private addBehaviorSubject() {
    return {
      group_speciality: new Subject<number>(),
      speciality: new Subject<number>()
    };
  }


  addDirection() {
    this.items.push(this.addGroup());
    this.clsSearch.push(this.addBehaviorSubject());
    this.cls.items.push(this.addCls(this.clsSearch.length - 1));
    this.search.push( this.addSearch() );
  }

  editEducation(id, i) {
    this.items.controls[i].controls['id_group_speciality'].setValue(null);
    this.clsSearch[i].group_speciality.next(id);
  }

  editGroupspeciality(id, i) {
    this.items.controls[i].controls['id_speciality'].setValue(null);
    this.clsSearch[i].speciality.next(id);
  }

  del(i: number) {
    this.clsSearch.splice(i, 1);
    this.cls.items.splice(i, 1);
    this.search.splice(i, 1);
    this.items.removeAt(i);
    //console.log(this.items, this.admissionForm.controls);
  }

  save() {
    let directions = [];
    const data = this.items.value;
    console.log('data', data);
    for ( let i = 0; i < data.length; i++ ) {
      console.log(directions, data[i].id_speciality);
      directions = Array.from( new Set(directions.concat( data[i].id_speciality )));
    }

    console.log('directions', directions);

    this.orgsService.addDirections( {directions} )
        .subscribe( (res: any ) => {
          if ( res.done ) {
            this.snackBar.open('Направления подготовки успешно добавлены');
            this.router.navigate(['../'], { relativeTo: this.route });
          } else {
            this.snackBar.open('При добавлении направлений подготовки произошла ошибка' + ((res.message) ? ': ' + res.message : '' ));
          }
        });
  }


  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}

interface SearchModel {
  group_speciality: string;
  speciality: string;
}
