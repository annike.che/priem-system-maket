import { NgModule } from '@angular/core';

import { DirectionsListComponent } from './directions-list/directions-list.component';
import { DirectionsAddComponent } from './directions-add/directions-add.component';
import {Router, RouterModule, Routes} from '@angular/router';
import {SharedModule} from '@/common/modules/shared.module';
import {ArtLibModule} from '@art-lib/art-lib.module';

const routes: Routes = [
  { path: '', component: DirectionsListComponent },
  { path: 'add', component: DirectionsAddComponent },

];

@NgModule({
  declarations: [
    DirectionsListComponent,
    DirectionsAddComponent
  ],
  imports: [
    RouterModule.forChild(routes),
      SharedModule,
      ArtLibModule
  ]
})
export class DirectionsModule { }
