import {Component, OnDestroy, OnInit} from '@angular/core';
import {OrgsService} from '@/cabinets/university/orgs-menu/orgs.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable, Subject} from 'rxjs';
import {PageEvent} from '@angular/material/paginator';
import {Title} from '@angular/platform-browser';
import {debounceTime, distinctUntilChanged, switchMap, tap} from 'rxjs/operators';
import {untilDestroyed} from 'ngx-take-until-destroy';
import * as cloneDeep from 'lodash/cloneDeep';
import {artAnimations} from '@art-lib/animations';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ClsService} from '@/common/services/cls.service';

@Component({
  selector: 'app-directions-list',
  templateUrl: './directions-list.component.html',
  styleUrls: ['./directions-list.component.scss'],
  animations: artAnimations
})
export class DirectionsListComponent implements OnInit, OnDestroy {

  directions: Array<DirectionsItem>;

  isLoad: boolean;

  displayedColumns: string[];
  searchColumns: string[];
  pageSizeOptions: number[];

  pagination = new Subject<PageEvent>();

  defaultParams = {
    page: 1,
    limit: 20
  };

  educationsLevel: Array<any> = [];

  search = new Subject<string[]>();

  total: number;

  params: Params = {
    ...this.defaultParams
  };


  constructor(
      private orgService: OrgsService,
      private clsService: ClsService,
      private router: Router,
      private route: ActivatedRoute,
      private titleService: Title,
      private snackBar: MatSnackBar
  ) {
    this.isLoad = false;
    this.titleService.setTitle('Список направлений подготовки');

    this.displayedColumns = [ 'educationLevel', 'parent', 'directions', 'actions' ];
    this.searchColumns = [ 'educationLevelSearch', 'parentSearch', 'directionsSearch', 'actionsSearch'];

    this.pageSizeOptions = [5, 10, 15, 20, 30, 50, 100];
  }

  ngOnInit() {
    this.initEducationsLevel();
    this.getQueryParamsFromUrl();
    this.subscribeOnChanges();
    this.getDirections();

  }

  initEducationsLevel() {
    this.clsService.getClsData('education_levels')
        .subscribe( res => {
             this.educationsLevel = res;
        });
  }

  subscribeOnChanges() {

    this.search.pipe(
        distinctUntilChanged(),
        debounceTime(700),
        tap(() => {
          this.params.page = 1;
        }),
        switchMap((value, index) => {
          console.log('value', value);
          return this.getDataFromService();
        }),
        untilDestroyed(this)
    ).subscribe(response => this.setData(response));

    this.pagination.pipe(
        debounceTime(200),
        tap((event: PageEvent) => {
          if (this.params.limit !== event.pageSize) {
            this.params.limit = event.pageSize;
            this.params.page = 1;
          }
          if (this.params.page !== event.pageIndex + 1) {
            this.params.page = event.pageIndex + 1;
          }
        }),
        switchMap(() => this.getDataFromService()),
        untilDestroyed(this)
    )
        .subscribe(response => this.setData(response));

  }

  getDataFromService(): Observable<any> {
    this.isLoad = true;
    const queryParams = this.getQueryParams(this.params);
    this.passQueryParamsToUrl(queryParams);
    return this.orgService.getDirectionsList(this.params);
  }

  getQueryParams(params) {
    const queryParams: any = {};
    Object.keys(params).forEach((key) => {
      if (this.defaultParams.hasOwnProperty(key) && (this.defaultParams[key] !== params[key] && params[key] !== '') ||
          (!this.defaultParams.hasOwnProperty(key) && params[key] !== '')) {
        queryParams[key] = params[key];
      }
    });

    return queryParams;
  }

  passQueryParamsToUrl(queryParams) {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams
    });
  }

  setData(response) {
    if (response.done) {

      this.directions = response.data;
      this.params.page = response.paginator.page;
      this.params.limit = response.paginator.limit;
      this.total = response.paginator.total;

    }
    this.isLoad = false;
  }

  getQueryParamsFromUrl() {
    const queryParams = cloneDeep(this.route.snapshot.queryParams);
    this.setParams(queryParams);
  }

  setParams(params) {
    Object.keys(params).forEach(key => {
      switch (key) {
        case 'page':
          this.params[key] = +params[key];
          break;
        default:
          this.params[key] = params[key];
      }
    });
  }

  getDirections() {
    this.getDataFromService()
        .subscribe(response => this.setData(response));
  }

  delete(id) {
      this.orgService.deleteDirectios([id])
          .subscribe( res => {
            if (res.done) {
              this.snackBar.open('Направление подготовки успешно удалено');
              this.getDirections();
            } else {
              this.snackBar.open('При попытке удалить направление подготовки произошла ошибка' + ( res.message ? ': ' + res.message : '' ));
            }
          });
  }

  ngOnDestroy(): void {
  }

  clearFilter($event) {
    this.params.search_specialty = '';
    this.params.search_education_level = [];
    this.params.search_parent = '';

    this.search.next($event);
  }
}

interface Params {
  search_specialty?: string;
  search_education_level?: Array<number>;
  search_parent?: string;
  page: number;
  limit: number;
}
