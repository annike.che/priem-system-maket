import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {toFormData} from '@/cabinets/university/entrants-menu/entrants.service';

@Injectable({
  providedIn: 'root'
})
export class OrgsService {
  urlPrefix: string;
  urlGroup = '/organizations';

  constructor(private httpClient: HttpClient) {
    this.urlPrefix = environment.apiUrl;
  }


  // выпадайка
  getDirectionsSelectList(search?, params = {}) {
    const url = this.urlPrefix + this.urlGroup + '/directions/select';

    let sentParams: any = search && search.trim() ? {search} : {};

    sentParams = { ... params};

    return this.httpClient.get(url, {params: sentParams})
        .pipe(
            map((resp: any) => {
              return (resp.done) ? resp.data : [];
            } )
        );
  }

  getDirectionsParentSelectList( search?, params = {} ): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + '/directions/parents/select';

    let sentParams: any = search && search.trim() ? {search} : {};

    sentParams = { ... params};

    return this.httpClient.get(url, {params: sentParams})
        .pipe(
            map((resp: any) => {
              return (resp.done) ? resp.data : [];
            } )
        );
  }

  getDirectionsList(params?): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + '/directions/list';

    return this.httpClient.get(url, { params });
  }

  addDirections(params): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + '/directions/add';

    return this.httpClient.post(url, params );

  }

  deleteDirectios(directions): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + '/directions/remove';
    return this.httpClient.post(url, { directions: [... directions] } );
  }

  changeSetificate(data): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/certificates/change`;

    const headers = new HttpHeaders({
      'key': 'Content-Type',
      'value': 'multipart/form-data'
    });

    return this.httpClient.post(url, toFormData(data), { headers});
  }

  getSertificateInfo(): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/certificates/info`;

    return this.httpClient.get(url);
  }

  saveOovo(is_oovo: boolean): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/isoovo/change`;

    return this.httpClient.post(url, {is_oovo});
  }


}
