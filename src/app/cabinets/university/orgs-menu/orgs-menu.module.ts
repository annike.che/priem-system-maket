import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'directions', pathMatch: 'full'},
  { path: 'directions', loadChildren: () => import('./pages/directions/directions.module').then( m => m.DirectionsModule )  },
  { path: 'settings', loadChildren: () => import('./pages/org-settings/org-settings.module').then( m => m.OrgSettingsModule )  }

];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes),
  ]
})
export class OrgsMenuModule { }
