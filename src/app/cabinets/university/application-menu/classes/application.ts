interface IApplication {
    id: number;
    app_number: string;

    entrant_fullname?: string;
    entrant_snils?: string;

    id_status: number;
    name_status: string;

    name_competitive_group: string;
    id_competitive_group: string;

    agreed: boolean;
    original: boolean;
    rating: number;
    need_hostel: boolean;

    registration_date: string;
    changed: string;
}
