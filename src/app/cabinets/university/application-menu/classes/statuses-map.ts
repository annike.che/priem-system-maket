const mapStatuses = {
  'entrant_agreed_call_off': {       //статус 20 может переходить только в 23
    notDisabled: ['23']
  },
  'app_call_off': {                  //статус 21 может переходить только в 24
    notDisabled: ['24']
  },
  '23': {
    notDisabled: []
  },
  '24': {
    notDisabled: []
  }
};


export function getMapStatuses(): any {
  return mapStatuses;
}
export function getValidStatuses(statusCode): Array<string> | boolean {
  let result = false;
  if ( mapStatuses.hasOwnProperty(statusCode) ) {
    result = mapStatuses[statusCode].notDisabled;
  }

  return result;
}

export function getDisabledStatuses(statusCode, allStatuses: Array<any>): Array<string> {
  let result = [];
  if ( mapStatuses.hasOwnProperty(statusCode) ) {
    allStatuses.forEach( el => {
      if (mapStatuses[statusCode].notDisabled.indexOf(el.code) < 0 ) {
        result.push(el.code);
      }
    });
  }

  return result;
}

export function isDisabledStatus(currStatusCode, newStatusCode): boolean {
  let result = false;
  if ( mapStatuses.hasOwnProperty(currStatusCode) ) {
    result = mapStatuses[currStatusCode].notDisabled.indexOf(newStatusCode) < 0;
  }

  return result;
}


