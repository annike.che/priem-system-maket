import { NgModule } from '@angular/core';
import { ApplicationListComponent } from './pages/application-list/application-list.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '@/common/modules/shared.module';
import {ArtLibModule} from '@art-lib/art-lib.module';
import { SelectStatusDialogComponent } from './components/select-status-dialog/select-status-dialog.component';
import { ApplicationTableListSettingsComponent } from './components/application-table-list-settings/application-table-list-settings.component';
import { ApplicationDeleteDialogComponent } from './components/application-delete-dialog/application-delete-dialog.component';
import {ApplicationFilterModule} from '@/cabinets/university/application-menu/components/application-filter/application-filter.module';
import { PrintDialogComponent } from './components/print-dialog/print-dialog.component';
import {EntrantDocListModule} from '@/cabinets/university/entrants-menu/components/entrant-doc-list/entrant-doc-list.module';


const routes: Routes = [
    {path: '', component: ApplicationListComponent},
    //{path: 'new', loadChildren: () => import('./pages/application-new/application-new.module').then( m => m.ApplicationNewModule )},
    {path: ':id',
      loadChildren: () => import('./pages/application-detail/application-detail.module').then( m => m.ApplicationDetailModule ),
      data: {preload: true}
    }
];

@NgModule({
  declarations: [
      ApplicationListComponent,
      SelectStatusDialogComponent,
      ApplicationTableListSettingsComponent,
      ApplicationDeleteDialogComponent,
      PrintDialogComponent,


  ],
  imports: [
    RouterModule.forChild(routes),
    SharedModule,
    ArtLibModule,
    ApplicationFilterModule,
    EntrantDocListModule
  ],
    entryComponents: [
        SelectStatusDialogComponent,
        ApplicationTableListSettingsComponent,
        ApplicationDeleteDialogComponent,
        PrintDialogComponent
    ]
})
export class ApplicationMenuModule { }
