import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApplicationService {

  urlPrefix: string;
  urlGroup = '/applications';

  constructor(private httpClient: HttpClient) {
    this.urlPrefix = environment.apiUrl;
  }

  getApplicationList(params?): Observable<any> {

    const url = this.urlPrefix + this.urlGroup + `/list`;

    return this.httpClient.get(url, { params });

  }

  addApplication( data ): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + '/add';

    return  this.httpClient.post(url, data);
  }

  getApplicationInfo(id): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${id}/info`;

    return  this.httpClient.get(url);

  }

  deleteApplication(id, comment): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${id}/remove`;

    return this.httpClient.post(url, { status_comment: comment });
  }

  saveApplicationInfo(id, data): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${id}/info/edit`;

    return  this.httpClient.post(url, data);
  }

  getApplicationMainData(id): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${id}/main`;

    return  this.httpClient.get(url);

  }

  getApplicationDocsList(id): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${id}/docs/list`;

    return  this.httpClient.get(url);
  }

  getApplicationDocsShortList(id): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${id}/docs/short`;

    return  this.httpClient.get(url);
  }

  addApplicationDocs(id, data): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${id}/docs/add`;

    return  this.httpClient.post(url, { docs: data });
  }

  deleteApplicationDocs(idApp, idDoc, codeCategory): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${idApp}/docs/${idDoc}/${codeCategory}/remove`;

    return this.httpClient.get(url);
  }

  getApplicationTestsList(id): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${id}/tests/list`;

    return  this.httpClient.get(url);
  }

  addApplicationTest(id, data): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${id}/tests/add`;

    return  this.httpClient.post(url, data);
  }

  deleteApplicationTests(id): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/tests/${id}/remove`;

    return this.httpClient.get(url);
  }

  editApplicationTest(idApplication, id, data): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${idApplication}/test/${id}/edit`;

    return this.httpClient.post(url, data);
  }

  chooseApplicationTestDate(idApp, idTest, idDate): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${idApp}/calendar/choose`;

    return this.httpClient.post( url, { id_entrance_test: idTest, id_entrance_calendar: idDate } );
  }

  deleteApplicationTestDate(idApp, idTestAgreed): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${idApp}/calendar/${idTestAgreed}/remove`;

    return this.httpClient.post( url, {  } );
  }


  getApplicationAchievementList(id): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${id}/achievements/list`;

    return  this.httpClient.get(url);
  }

  addApplicationAchievement(id, data): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${id}/achievements/add`;

    return this.httpClient.post(url, data);
  }

  deleteApplicationAchievement(id): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/achievements/${id}/remove`;

    return this.httpClient.get(url);
  }

  setStatusByCode(id: number, code: string, status_comment: string): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${id}/status/set`;

    return this.httpClient.post(url, {code, status_comment});
  }

  getApplicationAchievementFile( id ): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/achievements/file/${id}/get`;

    return this.httpClient.get<any>(url, {responseType: 'blob' as 'json'});
  }

  getApplicaitionListInExcel(): Observable<HttpResponse<Blob>> {
    const url = this.urlPrefix + `/generate/applications`;
    return this.httpClient.get<Blob>(url, {responseType: 'blob' as 'json',  observe: 'response' });
  }

  printApplication(idApplication, data, type = 'agreed'): Observable<HttpResponse<Blob>> {
    console.log(idApplication, data, type);
    const url = this.urlPrefix + this.urlGroup + `/${idApplication}/generate/${type}/pdf`;

    return this.httpClient.post<Blob>(url, {docs: data}, {responseType: 'blob' as 'json',  observe: 'response' });
  }

  checkAgreed(idApplication): Observable<any> {
    const url = this.urlPrefix + this.urlGroup + `/${idApplication}/info/agreed`;

    return this.httpClient.get(url);
  }

}
