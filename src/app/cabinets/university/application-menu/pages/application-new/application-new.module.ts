import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApplicationNewComponent } from './application-new.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '@/common/modules/shared.module';
import {ArtLibModule} from '@art-lib/art-lib.module';
import {ApplicationAddComponent} from './application-add/application-add.component';
import {AddApplicationModule} from '@/cabinets/university/application-menu/components/add-application/add-application.module';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {EntrantDocListModule} from '@/cabinets/university/entrants-menu/components/entrant-doc-list/entrant-doc-list.module';

const MY_DATE_FORMATS = {
  parse: {
    dateInput: ['DD.MM.YYYY', 'LL', ],
  },
  display: {
    dateInput: 'DD.MM.YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY'
  },
};

const routes: Routes = [
  {path: '', component: ApplicationNewComponent, children: [
      { path: '', redirectTo: 'entrant/:id', pathMatch: 'full' },
      { path: 'entrant/:id', component: ApplicationAddComponent },
      { path: '**', redirectTo: '' },
    ]},

];


@NgModule({
  declarations: [ApplicationNewComponent, ApplicationAddComponent],
  imports: [
    RouterModule.forChild(routes),
    SharedModule,
    ArtLibModule,
    AddApplicationModule,
    EntrantDocListModule
  ],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS }
  ]
})
export class ApplicationNewModule { }
