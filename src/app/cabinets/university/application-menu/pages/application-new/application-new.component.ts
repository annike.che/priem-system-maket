import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {EntrantsService} from '@/cabinets/university/entrants-menu/entrants.service';

@Component({
  selector: 'app-application-new',
  templateUrl: './application-new.component.html',
  styleUrls: ['./application-new.component.scss']
})
export class ApplicationNewComponent implements OnInit {

  entrantId: number;
  entrant: EntrantShort;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private entrantService: EntrantsService) { }

  ngOnInit() {
    if ( this.route.firstChild ) {
      this.route.firstChild.params.subscribe( res => {
        console.log('res', res);
        this.entrantId = res['id'] ? +res['id'] : undefined;
        console.log( 'entrantId', this.entrantId );
        this.getEntrantInfo();
      });
    }
  }

  setEntrant(id: number) {
    console.log('you set entrant #', id);
    this.entrantId = id;
    this.getEntrantInfo();
    this.router.navigate(['./entrant', id ], { relativeTo: this.route} );
  }

  getEntrantInfo() {
    this.entrantService.getEntrantShortInfo(this.entrantId)
        .subscribe( res => {
          if( res.done ) {
            this.entrant = res.data;
          } else {
            this.entrantId = null;
            this.router.navigate(['../']);
          }
        });
  }
}

interface EntrantShort {
  surname: string;
  name: string;
  patronymic: string;
  snils: string;
  birthday: string;

}
