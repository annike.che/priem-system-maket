import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import * as moment from 'moment';

@Component({
  selector: 'app-application-add',
  templateUrl: './application-add.component.html',
  styleUrls: ['./application-add.component.scss']
})
export class ApplicationAddComponent implements OnInit {

  applications: Array<AppListItem> = [];
  idEntrant: number;

  constructor( private route: ActivatedRoute,
               private router: Router) { }

  ngOnInit() {

      this.route.params.subscribe( res => {
          this.idEntrant = +res.id;


      });
  }

  addApplication() {
    this.applications.push( { name: 'Заявление ' + moment().format('HH:mm:ss DD.MMMM') } );
  }

	savedApp($event: any) {
      if( $event ) {
        this.router.navigate(['../../entrants', this.idEntrant, 'applications'], {relativeTo: this.route.parent});
      }
	}
}

interface AppListItem {
  name: string;
  id?: number;
}
