import {Component, OnDestroy, OnInit} from '@angular/core';
import {ApplicationService} from '@/cabinets/university/application-menu/application.service';
import {Title} from '@angular/platform-browser';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable, Subject} from 'rxjs';
import {PageEvent} from '@angular/material/paginator';
import {debounceTime, distinctUntilChanged, distinctUntilKeyChanged, switchMap, tap} from 'rxjs/operators';
import {untilDestroyed} from 'ngx-take-until-destroy';
import {artAnimations} from '@art-lib/animations';
import {MatDialog} from '@angular/material/dialog';
import * as cloneDeep from 'lodash/cloneDeep';
import {ApplicationTableListSettingsComponent} from '@/cabinets/university/application-menu/components/application-table-list-settings/application-table-list-settings.component';
import {ConfirmModalComponent} from '@art-lib/components/confirm-modal/confirm-modal.component';
import {ApplicationDeleteDialogComponent} from '@/cabinets/university/application-menu/components/application-delete-dialog/application-delete-dialog.component';
import {MatSnackBar} from '@angular/material/snack-bar';
import {SortDirection} from '@angular/material/sort';
import * as fileHandling from '@/common/functions/files';
import {HttpResponse} from '@angular/common/http';

@Component({
  selector: 'app-application-list',
  templateUrl: './application-list.component.html',
  styleUrls: ['./application-list.component.scss'],
  animations: artAnimations
})
export class ApplicationListComponent implements OnInit, OnDestroy {

  applications: Array<IApplication>;

  isLoad = true;

  // отображаемые колонки
  columnsHeader = [];

  // колонки по умочанию
  defaultColumnsHeader = ['number', 'fullname', 'snils',
    'nameCompetitive', 'stateName', 'registrationDate',
    'original', 'agreed', 'disagreed', 'needHostel', 'rating', 'epgu', 'action'];

  // все возможные колонки
  allColumnsHeader = ['number', 'fullname', 'snils',
    'nameCompetitive', 'stateName', 'registrationDate', 'changedDate',
    'original', 'agreed', 'agreedDate', 'disagreed', 'disagreedDate',
    'needHostel', 'rating', 'epgu',  'action'];

  searchColumnsHeader = ['numberSearch', 'fullnameSearch', 'snilsSearch', 'nameCompetitiveSearch',
    'stateNameSearch', 'registrationDateSearch', 'changedDateSearch', 'originalSearch',
    'agreedSearch', 'agreedDateSearch', 'disagreedSearch', 'disagreedDateSearch',
    'needHostelSearch', 'ratingSearch', 'epguSearch',  'actionSearch'];

  total: number;

  defaultParams = {
    page: 1,
    limit: 20
  };

  search = new Subject<Params>();

  params: Params = {
    ...this.defaultParams
  };

  openFilter: boolean;

  pagination = new Subject<PageEvent>();
  sortEvent = new Subject<SortEvent>();
  pageSizeOptions: number[];

  constructor(
    private applicationService: ApplicationService,
    private titleService: Title,
    private router: Router,
    private route: ActivatedRoute,
    public dialog: MatDialog,
    private snackBar: MatSnackBar
  ) {
    this.titleService.setTitle('Заявления');
    this.pageSizeOptions = [5, 10, 15, 20, 30, 50, 100];
  }

  ngOnInit() {
    this.initHeaderColumns();
    this.getQueryParamsFromUrl();
    this.getApplicationList();
    this.subscribeOnChanges();

    this.openFilter = localStorage.getItem('app-open-filter') === 'true';

    //console.log('open Filter', this.openFilter, !localStorage.getItem('app-open-filter'));

  }

  subscribeOnChanges() {
    this.search.pipe(
     // distinctUntilChanged(),
      debounceTime(500),
      tap(() => {
        this.params.page = 1;
      }),
      switchMap((value, index) => {
       // console.log('value', value);
        return this.getDataFromService();
      }),
      untilDestroyed(this)
    ).subscribe(response => this.setData(response));

    this.pagination.pipe(
      debounceTime(200),
      tap((event: PageEvent) => {
        if (this.params.limit !== event.pageSize) {
          this.params.limit = event.pageSize;
          this.params.page = 1;
        }
        if (this.params.page !== event.pageIndex + 1) {
          this.params.page = event.pageIndex + 1;
        }
      }),
      switchMap(() => this.getDataFromService()),
      untilDestroyed(this)
    )
      .subscribe(response => this.setData(response));

    this.sortEvent.pipe(
        debounceTime(200),
        tap((event: SortEvent) => {
          this.params.order = event.direction as SortDirection;
          this.params.sortby = event.active;
        }),
        switchMap(() => this.getDataFromService()),
        untilDestroyed(this)
    )
        .subscribe(response => this.setData(response));
  }

  getDataFromService(): Observable<any> {
    this.isLoad = true;
    const queryParams = this.getQueryParams(this.params);
    this.passQueryParamsToUrl(queryParams);
    return this.applicationService.getApplicationList(this.params);
  }

  getQueryParamsFromUrl() {
    const queryParams = cloneDeep(this.route.snapshot.queryParams);
    this.setParams(queryParams);
  }

  setParams(params) {
    Object.keys(params).forEach(key => {
      switch (key) {
        case 'page':
        case 'limit':
          this.params[key] = +params[key];
          break;
        case 'filter_competitive':
        case 'filter_campaign':
        case 'filter_status':
        case 'type_id':
          if ( params[key] ) {
            if (Array.isArray(params[key])) {
              this.params[key] = params[key].map( el => +el);
            } else {
              this.params[key] = [+params[key]];
            }
          }
          break;

        default:
          this.params[key] = params[key];
      }
    });
  }


  getQueryParams(params) {
    const queryParams: any = {};
    Object.keys(params).forEach((key) => {
      if (this.defaultParams.hasOwnProperty(key) && (this.defaultParams[key] !== params[key] && params[key] !== '') ||
          (!this.defaultParams.hasOwnProperty(key) && params[key] !== '')) {
        queryParams[key] = params[key];
      }
    });

    return queryParams;
  }

  passQueryParamsToUrl(queryParams) {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams
    });
  }

  setData(response) {
    if (response.done) {
      this.applications = response.data;
      this.params.page = response.paginator.page;
      this.params.limit = response.paginator.limit;
      this.total = response.paginator.total;

    }
    this.isLoad = false;
  }

  getApplicationList() {
    this.getDataFromService()
      .subscribe(response => this.setData(response));
  }

  ngOnDestroy(): void {
  }

  clearFilter($event) {
    delete this.params.search_number;
    delete this.params.search_fullname;
    delete this.params.search_snils;
    delete this.params.search_uid_epgu;

    this.search.next(this.params);
  }

  initHeaderColumns() {
    const localStorageSettings = JSON.parse(localStorage.getItem('application-list-table-setting'));
    console.log('localStorageSettings', localStorageSettings);
    if (localStorageSettings && Array.isArray(localStorageSettings)) {
      // проверка наличия столбцов из локал сторадж в общем массиве
      const checking = localStorageSettings.filter( val => {
        return  this.allColumnsHeader.indexOf(val) >= 0;
      });

      this.columnsHeader = checking.length ? checking : this.defaultColumnsHeader;

    } else {
      this.columnsHeader = this.defaultColumnsHeader;
    }

    this.initSearchColumns();
  }

  tableSetting() {

    this.dialog.open(ApplicationTableListSettingsComponent, {
      minWidth: '500px',
      data: cloneDeep( this.columnsHeader )
    }).afterClosed().subscribe(data => {
      if (data) {

        // для отображения чекбоксов присваиваем настройкам значения из формы
        this.columnsHeader = data.columns;

        // инициализируем колонки поиска
        this.initSearchColumns();

        // сохраняем в локалсторадж
        localStorage.setItem('application-list-table-setting', JSON.stringify(this.columnsHeader));
      }
    });
  }

  initSearchColumns() {
    this.searchColumnsHeader = this.columnsHeader.map( value => {
         return value + 'Search';
      });
  }

  deleteApp(id: number) {
    this.dialog.open(ConfirmModalComponent, {
      data: {
        title: 'Удалить заявление',
        content: 'Вы уверены, что хотите удалить заявление?'
      }
    })
    .afterClosed().subscribe(res => {
      if (res) {
        this.dialog.open(ApplicationDeleteDialogComponent, {minWidth: '400px'} )
            .afterClosed().subscribe( resp => {
              if (resp && resp.hasOwnProperty('comment')) {
                this.applicationService.deleteApplication(id, res.comment)
                    .subscribe( res => {
                      if ( res.done ) {
                        this.snackBar.open( 'Заявление успешно удалено');
                        this.getApplicationList();
                      } else {
                        this.snackBar.open( 'Произошла ошибка' + ( res.message ? ': ' + res.message : '' ) );
                      }
                    });
              }
        } );
      }
    });
  }

  changeFilter(data: any) {
      this.search.next(this.params);
  }

  toggleFilter() {
    this.openFilter = !this.openFilter;
    localStorage.setItem('app-open-filter', this.openFilter.toString());
  }

  getApplicationToExcel() {
      this.applicationService.getApplicaitionListInExcel()
          .subscribe( (res: HttpResponse<Blob>) => {
            const header = res.headers.get('content-disposition');
            var result = header.split(';')[1].trim().split('=')[1];
            const name = result.replace(/"/g, '') ;
            fileHandling.handlingResponse(res.body, name);
          }, err => {
            this.snackBar.open( fileHandling.handlingError(err));
          });
  }
}

interface Params {
  page: number;
  limit: number;
  search_number?: string;
  search_fullname?: string;
  search_snils?: string;
  search_uid_epgu?: string;
  filter_competitive?: Array<number> | null;
  filter_campaign?: Array<number> | null;
  filter_status?: Array<number> | null;
  order?: SortDirection; // 'ASC' || 'DESC'
  sortby?: string;
}

