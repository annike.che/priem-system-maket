import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ApplicationService} from '@/cabinets/university/application-menu/application.service';
import {IEntrants} from '@/cabinets/university/entrants-menu/classes/entrant';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog, MatDialogActions} from '@angular/material/dialog';
import {DocsAddComponent} from '@/cabinets/university/entrants-menu/components/docs-add/docs-add.component';
import {SelectStatusDialogComponent} from '@/cabinets/university/application-menu/components/select-status-dialog/select-status-dialog.component';
import {ConfirmModalComponent} from '@art-lib/components/confirm-modal/confirm-modal.component';
import {ApplicationDeleteDialogComponent} from '@/cabinets/university/application-menu/components/application-delete-dialog/application-delete-dialog.component';
import {PrintDialogComponent} from '@/cabinets/university/application-menu/components/print-dialog/print-dialog.component';
import {HttpResponse} from '@angular/common/http';
import * as fileHandling from '@/common/functions/files';
import * as statusMap from '../../classes/statuses-map';

@Component({
  selector: 'app-application-detail',
  templateUrl: './application-detail.component.html',
  styleUrls: ['./application-detail.component.scss']
})
export class ApplicationDetailComponent implements OnInit {

  idApplication: number;

  isEditStatus: boolean;

  data: DataInfo;

  validStatuses: Array<string> | boolean;

  agreed: boolean | null = null;

  constructor(
      private route: ActivatedRoute,
      private router: Router,
      private appService: ApplicationService,
      private snackBar: MatSnackBar,
      private dialog: MatDialog
  ) { }

  ngOnInit() {
    this.route.params.subscribe( res => {
       this.idApplication = +res.id || null;

       if ( this.idApplication ) {
         this.loadData();
         this.checkAgreed();
       } else {
         this.snackBar.open('Неверный формат ID заявления');
         this.router.navigate(['../'], { relativeTo: this.route.parent });
       }
    } );

  }

  loadData() {
    this.appService.getApplicationMainData(this.idApplication)
        .subscribe( res => {
            if (res.done) {
              this.data = res.data;
              this.checkEditStatus();
              this.getValidStatuses();
            }
        });
  }

    setEditStatus() {
        this.openDialogStatus('При попытке перейти к редактированию произошла ошибка', 'app_edit', true);
    }

    finishEditStatus() {
        this.openDialogStatus('При попытке завершить редактирование произошла ошибка', 'app_get_oovo', true);
    }

    checkEditStatus() {
      this.isEditStatus = ['app_edit'].indexOf(this.data.application.code_status) >= 0;
    }

    changeStatus( idApplicant, status, comment, errMessage) {
        this.appService.setStatusByCode(idApplicant, status, comment)
            .subscribe( res => {
                if ( res.done ) {
                    this.loadData();
                } else {
                    this.snackBar.open( errMessage + ( res.message ? ': ' + res.message : '' ) );
                }
            } );
    }


    selectStatus($event: MouseEvent) {
        const message = 'При попытке изменить статус произошла ошибка';
        this.openDialogStatus(message);
    }

    openDialogStatus( errMessage, newStatus?, isDisabled? ) {

      const status = newStatus || this.data.application.code_status;

      this.dialog.open(SelectStatusDialogComponent, {
        data: { status, isDisabled},
        minWidth: '650px'
      }).afterClosed().subscribe(result => {
        if ( result ) {
          if ( result.status !== this.data.application.code_status) {
            this.changeStatus(this.idApplication, result.status, result.comment, errMessage);
          }
        }
      });
    }

  deleteApp() {
    if ( !this.data.application.uid_epgu ) {
      this.dialog.open(ConfirmModalComponent, {
        data: {
          title: 'Удалить заявление',
          content: 'Вы уверены, что хотите удалить заявление?'
        }
      })
       .afterClosed().subscribe(res => {
        if (res) {
          this.dialog.open(ApplicationDeleteDialogComponent, {minWidth: '400px'} )
              .afterClosed().subscribe( res => {
                if (res && res.hasOwnProperty('comment')) {
                  this.appService.deleteApplication(this.idApplication, res.comment)
                      .subscribe( res => {
                        if ( res.done ) {
                          this.snackBar.open( 'Заявление успешно удалено');
                          this.router.navigate(['../'], { relativeTo: this.route });
                        } else {
                          this.snackBar.open( 'Произошла ошибка' + ( res.message ? ': ' + res.message : '' ) );
                        }
                      });
                }
          } );
        }
      });
    }

  }

  goToEntrantDoc() {
    this.router.navigate([ '../../entrants', this.data.entrant.id, 'docs', 'others'], {relativeTo: this.route.parent});
  }

  printAgreed() {
    this.dialog.open(PrintDialogComponent, {
      data: {
        title: 'Печать согласия на зачисление',
        description: 'Выберите один документ удостоверяющий личность и один документ об образовании',
        idEntrant: this.data.entrant.id,
        categoriesDoc: ['identification', 'educations']
      },
      width: '80%',
      disableClose: true
    }).afterClosed().subscribe( res => {
        if( res ) {
          this.print(res, 'agreed', 'Согласие_на_зачисление');
        }
    });
  }

  printDisagreed() {
    this.dialog.open(PrintDialogComponent, {
      data: {
        title: 'Печать отзыва согласия на зачисление',
        description: 'Выберите один документ удостоверяющий личность и один документ об образовании',
        idEntrant: this.data.entrant.id,
        categoriesDoc: ['identification', 'educations']
      },
      width: '80%',
      disableClose: true
    }).afterClosed().subscribe( res => {
      if( res ) {
        this.print(res, 'agreed', 'Отзыв_согласия_на_зачисление');
      }
    });
  }

  print(data, type, name) {
    this.appService.printApplication(this.idApplication, data, type)
        .subscribe( (res: HttpResponse<Blob>) => {

          const header = res.headers.get('content-disposition');
          if( header  ) {
            var result = header.split(';')[1].trim().split('=')[1];
            name = result.replace(/"/g, '') ;
          }

          fileHandling.handlingResponse(res.body, name);

        }, err => {
          console.log('err', err);
          var message;
          const reader = new FileReader();
          const blob = new Blob([err.error], { type: err.error.type });

          reader.onload =  (() => {
            const data = JSON.parse(reader.result.toString());
            message = data.message;
            this.snackBar.open('Произошла ошибка' + (message ? ': ' + message : ''));
          });

          reader.readAsText(blob);
          this.snackBar.open('Произошла ошибка' + (message ? ': ' + message : ''));
        });
  }

  checkAgreed() {
    this.appService.checkAgreed(this.idApplication)
        .subscribe( res => {
             if( res.done ) {
               this.agreed = res.data.agreed;
               //console.log(this.agreed);
             }
        });
  }

  getValidStatuses(status = this.data.application.code_status) {
    this.validStatuses = statusMap.getValidStatuses(status);
  }

  checkValidStatuses(newCode, currCode =  this.data.application.code_status): boolean {
    return statusMap.isDisabledStatus(currCode, newCode);
  }
}

interface DataInfo {
  entrant?: IEntrants;
  application?: any;
  competitive: any;
}
