import { NgModule } from '@angular/core';
import { ApplicationDetailComponent } from './application-detail.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '@/common/modules/shared.module';
import {ArtLibModule} from '@art-lib/art-lib.module';
import {EntrantDocListModule} from '@/cabinets/university/entrants-menu/components/entrant-doc-list/entrant-doc-list.module';
import {ApplicationDetailViewModule} from './tabs/application-view/application-detail-view.module';
import {ApplicationDetailEditModule} from './tabs/application-edit/application-detail-edit.module';

const routes: Routes = [
  {path: '', component: ApplicationDetailComponent},

];

@NgModule({
  declarations: [ApplicationDetailComponent],
    imports: [
        RouterModule.forChild(routes),
        SharedModule,
        ArtLibModule,
        EntrantDocListModule,
        ApplicationDetailViewModule,
        ApplicationDetailEditModule
    ]
})
export class ApplicationDetailModule { }
