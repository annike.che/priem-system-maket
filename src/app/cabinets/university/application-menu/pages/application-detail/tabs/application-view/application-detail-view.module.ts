import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApplicationDetailMainViewComponent } from './application-detail-main-view/application-detail-main-view.component';
import { ApplicationDetailDocsViewComponent } from './application-detail-docs-view/application-detail-docs-view.component';
import { ApplicationDetailTestsViewComponent } from './application-detail-tests-view/application-detail-tests-view.component';
import { ApplicationDetailAchievementsViewComponent } from './application-detail-achievements-view/application-detail-achievements-view.component';
import {SharedModule} from '@/common/modules/shared.module';
import {ArtLibModule} from '@art-lib/art-lib.module';
import {EntrantDocListModule} from '@/cabinets/university/entrants-menu/components/entrant-doc-list/entrant-doc-list.module';



@NgModule({
  declarations: [
      ApplicationDetailMainViewComponent,
      ApplicationDetailDocsViewComponent,
      ApplicationDetailTestsViewComponent,
      ApplicationDetailAchievementsViewComponent,
  ],
    imports: [
        CommonModule,
        SharedModule,
        ArtLibModule,
        EntrantDocListModule,
    ],
  exports: [
    ApplicationDetailMainViewComponent,
    ApplicationDetailDocsViewComponent,
    ApplicationDetailTestsViewComponent,
    ApplicationDetailAchievementsViewComponent
  ],
})
export class ApplicationDetailViewModule { }
