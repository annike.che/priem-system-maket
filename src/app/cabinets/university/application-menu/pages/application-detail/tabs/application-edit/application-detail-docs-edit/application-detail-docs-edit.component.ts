import {Component, Input, OnInit} from '@angular/core';
import {ApplicationService} from '@/cabinets/university/application-menu/application.service';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {AddDocumentsDialogComponent} from '@/cabinets/university/application-menu/pages/application-detail/tabs/application-edit/_dialogs/add-documents-dialog/add-documents-dialog.component';
import {ConfirmModalComponent} from '@art-lib/components/confirm-modal/confirm-modal.component';
import {DeleteDocInfo} from '@/cabinets/university/entrants-menu/components/entrant-doc-list/entrant-doc-list.component';

@Component({
  selector: 'app-application-detail-docs-edit',
  templateUrl: './application-detail-docs-edit.component.html',
  styleUrls: ['./application-detail-docs-edit.component.scss']
})
export class ApplicationDetailDocsEditComponent implements OnInit {

  @Input() appId: number;
  @Input() entrantId: number;

  isLoad = true;

  hasError = false;
  error: string;

  docs: Array<DocsListEntrant>;

  constructor(
      private applicationService: ApplicationService,
      private dialog: MatDialog,
      private snackBar: MatSnackBar
  ) { }

  ngOnInit() {

    this.initData();
  }

  initData() {
    this.isLoad = true;
    this.hasError = false;
    this.applicationService.getApplicationDocsList(this.appId)
        .subscribe( res => {
          if( res.done ) {
            this.docs = res.data.docs;
          } else {
            this.hasError = true;
            this.error = res.message || 'При попытке загрузить данные произошла ошибка';
          }
          this.isLoad = false;
        } );
  }

  addDocs() {
    this.dialog.open( AddDocumentsDialogComponent, {
      data: { idApplication: this.appId, idEntrant: this.entrantId },
      width: `80%`,
      disableClose: true
    }).afterClosed().subscribe(result => {
      if( result ) {
          if(result.done) this.initData();
      }
    });
  }

  delete(data: DeleteDocInfo) {
    console.log(data);
    if (data.hasOwnProperty('id')) {
      this.dialog.open(ConfirmModalComponent, {
        data: {
          title: 'Удаление Документа из Заявления',
          content: 'Вы уверены, что хотите удалить Документ из заявления?'
        }
      }).afterClosed().subscribe(res => {
          this.applicationService.deleteApplicationDocs(this.appId, data.id, data.codeCategory)
              .subscribe( res => {
                if (res.done) {
                  this.snackBar.open('Документ успешно удален из заявления');
                  this.docs[data.indexCategory].docs.splice(data.index, 1);
                  this.docs = [... this.docs];

                } else {
                  const message = 'При попытке удалить документ произошла ошибка' +  (res.message ? (': ' + res.message) : '');
                  this.snackBar.open(message);
                }
              });
        });
    }
  }
}
