import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ApplicationService} from '@/cabinets/university/application-menu/application.service';

@Component({
  selector: 'app-select-documents-dialog',
  templateUrl: './select-documents-dialog.component.html',
  styleUrls: ['./select-documents-dialog.component.scss']
})
export class SelectDocumentsDialogComponent implements OnInit {

  appId: number;

  isLoad = true;

  docList: Array<DocsListEntrant>;
  selectedDoc: Array<DocsListEntrant> = [];

  constructor(
      private applicationService: ApplicationService,
      public dialogRef: MatDialogRef<SelectDocumentsDialogComponent>,
      @Inject(MAT_DIALOG_DATA) public data: any) {
    this.appId = data.appId;

  }

  ngOnInit() {

    if( this.appId ) {
      this.initDocList();
    }
  }

  initDocList() {
    this.applicationService.getApplicationDocsList(this.appId)
        .subscribe( res => {
          if ( res.done ) {
            this.docList = res.data.docs;
          }

          this.isLoad = false;
        } );
  }

  save() {
    this.dialogRef.close( this.selectedDoc[0] );
  }

}
