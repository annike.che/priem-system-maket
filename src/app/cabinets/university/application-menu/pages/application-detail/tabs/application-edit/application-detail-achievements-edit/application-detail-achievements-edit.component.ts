import {ChangeDetectorRef, Component, Input, OnInit, ViewChild} from '@angular/core';
import {ApplicationService} from '@/cabinets/university/application-menu/application.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CampaignService} from '@/cabinets/university/campaign-menu/campaign.service';
import {MatDialog} from '@angular/material/dialog';
import {SelectDocumentsDialogComponent} from '@/cabinets/university/application-menu/pages/application-detail/tabs/application-edit/_dialogs/select-documents-dialog/select-documents-dialog.component';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ConfirmModalComponent} from '@art-lib/components/confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-application-detail-achievements-edit',
  templateUrl: './application-detail-achievements-edit.component.html',
  styleUrls: ['./application-detail-achievements-edit.component.scss']
})
export class ApplicationDetailAchievementsEditComponent implements OnInit {

  @Input() appId: number;

  @Input() competitiveId: number;

  isLoad = true;

  hasError = false;
  error: string;

  addingData: AchievementListItem;

  achievements: Array<AchievementListItem>;

  headerColumns = ['name', 'category', 'maxScore', 'mark', 'docInfo', 'uid', 'actions'];

  addingColumns = ['selectId', 'maxScopeSelect', 'setMark', 'selectDoc', 'setUid', 'actionsSave' ];


  cls = {
    achievement: {
      data: [],
      initFunc:  () => this.campaignService.getAchievementsByCompetitiveIdSelectList(this.competitiveId),
      isLoad: true,
      search: ''
    }
  };


  isAddAchievement = (index, item) => { return  item ? !item.id : true; }

  constructor(
      private applicationService: ApplicationService,
      private campaignService: CampaignService,
      private fb: FormBuilder,
      private dialog: MatDialog,
      private snackBar: MatSnackBar
  ) { }

  ngOnInit() {

    this.initData();
    this.initCls();
  }

  initCls() {
    for (let key in this.cls) {
      if ( this.cls[key].isLoad ) {
        this.cls[key].initFunc()
            .subscribe( res => {
              this.cls[key].data = res;
            });
      }
    }
  }

  initData() {
    this.isLoad = true;
    this.hasError = false;
    this.applicationService.getApplicationAchievementList(this.appId)
        .subscribe( res => {
          if( res.done ) {
            this.achievements = res.data;
          } else {
            this.hasError = true;
            this.error = res.message || 'При попытке загрузить данные произошла ошибка';
          }
          this.isLoad = false;
        } );
  }

  deleteForm() {
    this.achievements.splice(0, 1);
    this.achievements = [ ...this.achievements];
    this.addingData = undefined;
  }

  addAchievement() {
    this.addingData = {
      id_achievement: null,
      mark: null,
      uid: null,
      id_document: null
    };

    this.achievements.unshift( this.addingData);
    this.achievements = [ ...this.achievements];
  }

  selectDoc() {
    this.dialog.open(SelectDocumentsDialogComponent, {
      minWidth: '80%', data: { appId: this.appId }
    }).afterClosed().subscribe( data => {
        if ( data && data.hasOwnProperty('id') ) {
          this.addingData.id_document = data.id;
          this.addingData.document = data;
        }
    } );
  }

  changeAchievement($event: any) {
    console.log('event', $event);
    this.addingData.id_achievement = $event.id;
  }

  save() {
    this.isLoad = true;
    this.applicationService.addApplicationAchievement( this.appId, this.addingData)
        .subscribe( res => {
            if (res.done) {
              this.deleteForm();
              this.initData();
              this.snackBar.open('Индивидуальное достижение успешно добавлено');
            } else {
              this.snackBar.open('При попытке добавить индивидуальное достижение произошла ошибка' + ( (res.message) ? ': ' + res.message : '' ));
            }

            this.isLoad = false;
        } );
  }

  delete($event: MouseEvent, id: any, index: number) {
    this.dialog.open(ConfirmModalComponent, {
      data: {
        title: 'Удаление Индивидуального достижения в заявлении',
        content: 'Вы уверены, что хотите удалить Индивидуальное Достижение из заявления?'
      }
    })
        .afterClosed().subscribe(res => {
      if (res) {
        this.applicationService.deleteApplicationAchievement(id)
            .subscribe( (res: any) => {
              if(res.done) {
                this.achievements.splice(index, 1);
                this.achievements = [ ... this.achievements];
                this.snackBar.open('Индивидуальное достижение успешно удалено');
              } else {
                this.snackBar.open('При удалении индивидуального достижения произошла ошибка' + (res.message? ': '+res.message : '' ) );
              }
            });

      }
    });
  }

  openFile($event, data) {
    this.applicationService.getApplicationAchievementFile(data.id)
        .subscribe( res => {
          const blob = new Blob([res], { type: res.type });
          this.downloadFile(blob, data.path_file);
        }, err => {
          console.log('err', err);
          var message;
          const reader = new FileReader();
          const blob = new Blob([err.error], { type: err.error.type });

          reader.onload =  (() => {
            const data = JSON.parse(reader.result.toString());
            message = data.message;
            this.snackBar.open('Произошла ошибка' + (message ? ': ' + message : ''));
          });

          reader.readAsText(blob);
          this.snackBar.open('Произошла ошибка' + (message ? ': ' + message : ''));
        });
  }

  downloadFile(file: Blob, fileName: string) {

    const downloadLink = document.createElement("a");
    const objectUrl = URL.createObjectURL(file);


    downloadLink.href = objectUrl;
    downloadLink.download = fileName;
    downloadLink.target = '_self';
    document.body.appendChild(downloadLink);

    downloadLink.click();
    URL.revokeObjectURL(objectUrl);
  }
}

interface AchievementListItem {
  id?: number;
  mark?: number;
  max_value?: number;

  id_achievement?: number;
  name_achievement?: string;
  achievement?: any;

  id_category?: number;
  name_category?: string;
  uid?: string;
  document?: DocInfo;
  id_document?: number;
}

interface DocInfo {
  code_category: string;
  id: number;
  name_type: string;
}

