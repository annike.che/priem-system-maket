import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationDetailMainEditComponent } from './application-detail-main-edit.component';

describe('ApplicationDetailMainEditComponent', () => {
  let component: ApplicationDetailMainEditComponent;
  let fixture: ComponentFixture<ApplicationDetailMainEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationDetailMainEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationDetailMainEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
