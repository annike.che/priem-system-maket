import {Component, Input, OnInit} from '@angular/core';
import {ApplicationService} from '@/cabinets/university/application-menu/application.service';

@Component({
  selector: 'app-application-detail-main-view',
  templateUrl: './application-detail-main-view.component.html',
  styleUrls: ['./application-detail-main-view.component.scss']
})
export class ApplicationDetailMainViewComponent implements OnInit {

  @Input() appId: number;

  application: any;

  error: string;
  hasEror = false;

  isLoad = true;

  constructor(
      private applicationService: ApplicationService
  ) { }

  ngOnInit() {
    this.initData();
  }

  initData() {
    this.hasEror = false;
    this.isLoad = true;

    this.applicationService.getApplicationInfo(this.appId)
        .subscribe( res => {
          if(res.done) {
            this.application = res.data;
          } else {
            this.hasEror = true;
            this.error = res.message || 'При попытке загрузить данные произошла ошибка';
          }
          this.isLoad = false;
        });
  }

}
