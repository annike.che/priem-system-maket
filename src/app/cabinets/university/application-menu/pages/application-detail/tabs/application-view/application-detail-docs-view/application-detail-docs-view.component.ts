import {Component, Input, OnInit} from '@angular/core';
import {ApplicationService} from '@/cabinets/university/application-menu/application.service';

@Component({
  selector: 'app-application-detail-docs-view',
  templateUrl: './application-detail-docs-view.component.html',
  styleUrls: ['./application-detail-docs-view.component.scss']
})
export class ApplicationDetailDocsViewComponent implements OnInit {

  @Input() appId: number;

  isLoad = true;

  hasError = false;
  error: string;

  docs: Array<DocsListEntrant>;

  constructor(
      private applicationService: ApplicationService
  ) { }

  ngOnInit() {

    this.initData();
  }

  initData() {
    this.isLoad = true;
    this.hasError = false;
      this.applicationService.getApplicationDocsList(this.appId)
          .subscribe( res => {
            if( res.done ) {
              this.docs = res.data.docs;
            } else {
              this.hasError = true;
              this.error = res.message || 'При попытке загрузить данные произошла ошибка';
            }
            this.isLoad = false;
          } );
  }

}
