import {Component, Input, OnInit} from '@angular/core';
import {ApplicationService} from '@/cabinets/university/application-menu/application.service';
import {ConfirmModalComponent} from '@art-lib/components/confirm-modal/confirm-modal.component';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {SelectDocumentsDialogComponent} from '@/cabinets/university/application-menu/pages/application-detail/tabs/application-edit/_dialogs/select-documents-dialog/select-documents-dialog.component';
import {CampaignService} from '@/cabinets/university/campaign-menu/campaign.service';
import * as cloneDeep from 'lodash/cloneDeep';
import {ChooseEntrantTestDateDialogComponent} from '@/cabinets/university/application-menu/pages/application-detail/tabs/application-edit/_dialogs/choose-entrant-test-date-dialog/choose-entrant-test-date-dialog.component';

@Component({
  selector: 'app-application-detail-tests-edit',
  templateUrl: './application-detail-tests-edit.component.html',
  styleUrls: ['./application-detail-tests-edit.component.scss']
})
export class ApplicationDetailTestsEditComponent implements OnInit {

  @Input() appId: number;
  @Input() competitiveId: number;

  isLoad = true;

  hasError = false;
  error: string;

  addingData: TestResult;
  indexAddingData: number;

  tests: Array<TestDetail>;

  testsColumns = ['testType', 'subject', 'ege', 'minScore', 'priority', 'dates', 'chooseDate', 'mark', 'docInfo', 'uid', 'actions'];

  addingColumns = ['testType', 'subject', 'ege', 'minScore', 'priority', 'dates', 'chooseDate', 'setMark', 'selectDoc', 'setUid', 'actionsSave' ];

  isAddTest = (index, item) =>  index === this.indexAddingData;

  cls = {
    tests: {
      data: [],
      initFunc:  () => this.campaignService.getTestListByCompetitiveIdSelectList(this.competitiveId),
      isLoad: true,
      search: ''
    }
  };

  constructor(
      private applicationService: ApplicationService,
      private campaignService: CampaignService,
      private dialog: MatDialog,
      private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.initData();

    this.initCls();
  }

  initData() {
    this.isLoad = true;
    this.hasError = false;
    this.applicationService.getApplicationTestsList(this.appId)
        .subscribe( res => {
          if ( res.done ) {
            this.tests = res.data;
          } else {
            this.hasError = true;
            this.error = res.message || 'При попытке загрузить данные произошла ошибка';
          }
          this.isLoad = false;
        } );
  }

  initCls() {
    for (let key in this.cls) {
      if ( this.cls[key].isLoad ) {
        this.cls[key].initFunc()
            .subscribe( res => {
              this.cls[key].data = res;
            });
      }
    }
  }

  delete($event: MouseEvent, id: number, index: number) {
    this.dialog.open(ConfirmModalComponent, {
      data: {
        title: 'Удаление Вступительного испытания',
        content: 'Вы уверены, что хотите удалить Вступительное испытание из заявления?'
      }
    })
        .afterClosed().subscribe(res => {
      if (res) {
        this.applicationService.deleteApplicationTests(id)
            .subscribe( (res: any) => {
              if (res.done) {
                this.tests[index].app_entrance_test = null;
                this.tests = [ ... this.tests];
                this.snackBar.open('Вступительное испытание успешно удалено');
              } else {
                this.snackBar.open('При удалении вступительного испытания произошла ошибка' + (res.message ? ': ' + res.message : '' ) );
              }
            });

      }
    });
  }
  cancel() {

    this.addingData = undefined;
    this.indexAddingData = undefined;

    this.tests = [ ...this.tests];

  }

  addTest(row: TestDetail, index) {
    this.addingData = {
      id_entrance_test: row.id,
      result_value: null,
      uid: null,
      id_document: null
    };
    this.indexAddingData = index;

    this.tests = [ ...this.tests ];
  }

  selectDoc() {
    this.dialog.open(SelectDocumentsDialogComponent, {
      minWidth: '80%', data: { appId: this.appId }
    }).afterClosed().subscribe( data => {
      if ( data && data.hasOwnProperty('id') ) {
        this.addingData.id_document = data.id;
        this.addingData.document_name_category = data.name_category;
        this.addingData.document_code_category = data.code_category;
      }
    } );
  }

  changeSelectTest($event: any) {
    this.addingData.id_entrance_test = $event.id;
    this.addingData.is_ege = $event.is_ege;
    this.addingData.min_score = $event.min_score;
  }

  add() {
    this.isLoad = true;
    this.applicationService.addApplicationTest( this.appId, this.addingData)
        .subscribe( res => {
          if (res.done) {
            this.cancel();
            this.initData();
            this.snackBar.open('Вступительное испытание успешно добавлено');
          } else {
            this.snackBar.open('Произошла ошибка' + ( (res.message) ? ': ' + res.message : '' ));
          }

          this.isLoad = false;
        } );
  }

  edit(row: TestResult, index: number) {
      this.addingData = {...row};
      this.indexAddingData = index;

      this.tests = [ ...this.tests ];
  }

  save(index) {
    this.isLoad = true;

    const editData = {
      uid: this.addingData.uid,
      result_value: this.addingData.result_value,
      id_document: this.addingData.id_document
    };

    this.applicationService.editApplicationTest( this.appId, this.addingData.id, editData )
        .subscribe( res => {
            if( res.done ) {
              this.tests[index].app_entrance_test = { ...this.tests[index].app_entrance_test, ...editData };
              this.addingData = undefined;
              this.indexAddingData = undefined;
              this.tests = [ ...this.tests];

              this.snackBar.open( 'Данные успешно обновлены' );

            } else {
              this.snackBar.open('Произошла ошибка' + ( (res.message) ? ': ' + res.message : '' ));
            }

            this.isLoad = false;
        });
  }

  chooseTestDate(row: TestDetail, index) {
    this.campaignService.loadEntranceTestCalendar(row.id)
        .subscribe( res => {
          if( res.done ) {
              this.dialog.open(ChooseEntrantTestDateDialogComponent,
                  { data: { row, dates: res.data, idApplication: this.appId } }
                  ).afterClosed().subscribe( data => {
                    if (data) {
                      this.tests[index].choose_entrance_test_date = { ...data};
                      console.log(this.tests[index].choose_entrance_test_date, this.tests);
                      this.tests = [ ... this.tests ];
                    }

              });
          } else {
            this.snackBar.open('Произошла ошибка' + ( (res.message) ? ': ' + res.message : '' ));
          }
        });

  }

  deleteTestDate(date: ChooseDate, index: number) {
    this.dialog.open(ConfirmModalComponent, {
      data: {
        title: 'Удаление Выбранной даты проведения Ви',
        content: 'Вы уверены, что хотите удалить выбранную дату проведения ВИ?'
      }
    })
        .afterClosed().subscribe(res => {
      if (res) {
        this.applicationService.deleteApplicationTestDate(this.appId, date.id_entrance_test_agreed)
            .subscribe( (res: any) => {
              if (res.done) {
                date = undefined;
                this.tests[index].choose_entrance_test_date = undefined;
                this.tests = [ ... this.tests];
                this.snackBar.open('Выбранная дата успешно удалена');
              } else {
                this.snackBar.open('Произошла ошибка' + (res.message ? ': ' + res.message : '' ) );
              }
            });

      }
    });
  }
}
export interface TestDetail {

  app_entrance_test?: TestResult;
  entrance_test_calendar_dates: Array<string>;
  choose_entrance_test_date: ChooseDate;
  id: number;

  is_ege?: true;
  min_score?: number;
  name_subject?: string;
  priority?: number;
  test_name?: string;
  uid?: string;
}

export interface TestResult {
  document_code_category?: string;
  document_name_category?: string;
  id_document: number;
  id?: number;

  result_value: number;

  entrance_test?: any;

  id_entrance_test: number;
  id_entrance_test_type?: number;
  name_entrance_test_type?: string;
  is_ege?: true;
  min_score?: number;
  name_subject?: string;
  priority?: number;
  test_name?: string;

  uid?: string;
}

export interface ChooseDate {
  id_entrance_test_agreed: number;
  id_entrance_test_calendar: number;
  date: string;
  exam_location: string;
  uid: string;
}
