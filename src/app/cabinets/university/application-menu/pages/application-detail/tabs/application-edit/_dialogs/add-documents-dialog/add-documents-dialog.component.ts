import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {EntrantsService} from '@/cabinets/university/entrants-menu/entrants.service';
import {ApplicationService} from '@/cabinets/university/application-menu/application.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-add-documents-dialog',
  templateUrl: './add-documents-dialog.component.html',
  styleUrls: ['./add-documents-dialog.component.scss']
})
export class AddDocumentsDialogComponent implements OnInit {

  isLoad = true;

  idEntrant: number;

  idApplication: number;

  disabledDocs = {};

  allDocs: Array<DocsListEntrant>;

  selectedDoc: Array<DocsListEntrant> = [];

  constructor(
      private entrantService: EntrantsService,
      private appService: ApplicationService,
      private snackBar: MatSnackBar,
      public dialogRef: MatDialogRef<AddDocumentsDialogComponent>,
      @Inject(MAT_DIALOG_DATA) public data: any) {

    this.idEntrant = data.idEntrant;
    this.idApplication = data.idApplication;


  }

  ngOnInit() {
    this.initAllDocs();
    this.initDisabledDocs();
  }

  initAllDocs() {
    this.entrantService.getShortDocList(this.idEntrant)
        .subscribe( res => {
            if (res.done) {
              this.allDocs = res.data;
            }
        } );
  }

  initDisabledDocs() {
    this.appService.getApplicationDocsShortList(this.idApplication)
        .subscribe( res => {
          if ( res.done ) {
            this.disabledDocs = res.data;
          }
        });

  }

  sendData() {
    console.log('selected doc', this.selectedDoc);
    this.appService.addApplicationDocs(this.idApplication, this.selectedDoc)
        .subscribe( res => {
          if (res.done) {
            this.snackBar.open('Документы успешно добавлены');
            this.dialogRef.close({done: true});
          } else {
            this.snackBar.open('При прпытке добавить документы произошла ошибка' + (res.message ? ': ' + res.message : ''));
          }
        });
  }
}
