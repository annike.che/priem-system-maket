import {Component, Inject, OnInit} from '@angular/core';
import {MatCheckboxChange} from '@angular/material/checkbox';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {
  TestDetail
} from '@/cabinets/university/application-menu/pages/application-detail/tabs/application-edit/application-detail-tests-edit/application-detail-tests-edit.component';
import {ApplicationService} from '@/cabinets/university/application-menu/application.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-choose-entrant-test-date-dialog',
  templateUrl: './choose-entrant-test-date-dialog.component.html',
  styleUrls: ['./choose-entrant-test-date-dialog.component.scss']
})
export class ChooseEntrantTestDateDialogComponent implements OnInit {

  dates: Array<EntrantsTestCalendar>;

  row: TestDetail;

  idApplication: number;

  checkedDone = false;
  checkedData: EntrantsTestCalendar;



  columnsHeader = ['checkBox', 'id', 'date', 'examLocation', 'count', 'uid'];

  constructor(
      private applicationService: ApplicationService,
      private snackBar: MatSnackBar,
      public dialogRef: MatDialogRef<ChooseEntrantTestDateDialogComponent>,
      @Inject(MAT_DIALOG_DATA) public data: any) {
    this.dates = data.dates;
    this.row = data.row;
    this.idApplication = data.idApplication;

  }

  ngOnInit() {
  }

  saveData() {
    this.applicationService.chooseApplicationTestDate(this.idApplication, this.row.id, this.checkedData.id)
        .subscribe( res => {
            if ( res.done ) {
              this.snackBar.open('Дата успешно выбрана');
              const sendData = {
                id_entrance_test_agreed: res.data.id_entrance_test_agreed,
                id_entrance_test_calendar: res.data.id_entrance_test_calendar,
                date: this.checkedData.entrance_test_date
              };

              this.dialogRef.close( sendData );
            } else {
              this.snackBar.open('Произошла ошибка' + ( (res.message) ? ': ' + res.message : '' ));
            }
        });
  }

  checkDate($event: MatCheckboxChange, row: EntrantsTestCalendar) {
    if ( $event.checked ) {
      this.checkedData = row;
      this.checkedDone = true;
    } else {
      this.checkedDone = false;
      this.checkedData = undefined;
    }
  }
}
