import {Component, Input, OnInit} from '@angular/core';
import {ApplicationService} from '@/cabinets/university/application-menu/application.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-application-detail-achievements-view',
  templateUrl: './application-detail-achievements-view.component.html',
  styleUrls: ['./application-detail-achievements-view.component.scss']
})
export class ApplicationDetailAchievementsViewComponent implements OnInit {

  @Input() appId: number;

  isLoad = true;

  hasError = false;
  error: string;

  achievements: Array<any>;

  headerColumns = ['name', 'category', 'maxScore', 'mark', 'docInfo'];

  constructor(
      private applicationService: ApplicationService,
      private snackBar: MatSnackBar,
      private sanitizer: DomSanitizer
  ) { }

  ngOnInit() {

    this.initData();
  }

  initData() {
    this.isLoad = true;
    this.hasError = false;
    this.applicationService.getApplicationAchievementList(this.appId)
        .subscribe( res => {
          if( res.done ) {
            this.achievements = res.data;
          } else {
            this.hasError = true;
            this.error = res.message || 'При попытке загрузить данные произошла ошибка';
          }
          this.isLoad = false;
        } );
  }

  openFile($event, data) {
    this.applicationService.getApplicationAchievementFile(data.id)
        .subscribe( res => {
            const blob = new Blob([res], { type: res.type });
            this.downloadFile(blob, data.path_file);
        }, err => {
            console.log('err', err);
            var message;
            const reader = new FileReader();
            const blob = new Blob([err.error], { type: err.error.type });

            reader.onload =  (() => {
              const data = JSON.parse(reader.result.toString());
              message = data.message;
              this.snackBar.open('Произошла ошибка' + (message ? ': ' + message : ''));
            });

            reader.readAsText(blob);
            this.snackBar.open('Произошла ошибка' + (message ? ': ' + message : ''));
        });
  }

  downloadFile(file: Blob, fileName: string) {

    const downloadLink = document.createElement("a");
    const objectUrl = URL.createObjectURL(file);


    downloadLink.href = objectUrl;
    downloadLink.download = fileName;
    downloadLink.target = '_self';
    document.body.appendChild(downloadLink);

    downloadLink.click();
    URL.revokeObjectURL(objectUrl);
  }
}
