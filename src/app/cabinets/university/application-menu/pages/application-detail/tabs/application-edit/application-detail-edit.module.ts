import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApplicationDetailDocsEditComponent } from './application-detail-docs-edit/application-detail-docs-edit.component';
import { ApplicationDetailMainEditComponent } from './application-detail-main-edit/application-detail-main-edit.component';
import { ApplicationDetailTestsEditComponent } from './application-detail-tests-edit/application-detail-tests-edit.component';
import { ApplicationDetailAchievementsEditComponent } from './application-detail-achievements-edit/application-detail-achievements-edit.component';
import {ArtLibModule} from '@art-lib/art-lib.module';
import {SharedModule} from '@/common/modules/shared.module';
import {SelectDocumentsDialogComponent} from './_dialogs/select-documents-dialog/select-documents-dialog.component';
import {EntrantDocListModule} from '@/cabinets/university/entrants-menu/components/entrant-doc-list/entrant-doc-list.module';
import {MaxDirective} from '@art-lib/directives/max.directive';
import { AddDocumentsDialogComponent } from './_dialogs/add-documents-dialog/add-documents-dialog.component';
import { ChooseEntrantTestDateDialogComponent } from './_dialogs/choose-entrant-test-date-dialog/choose-entrant-test-date-dialog.component';


@NgModule({
  declarations: [
    ApplicationDetailDocsEditComponent,
    ApplicationDetailMainEditComponent,
    ApplicationDetailTestsEditComponent,
    ApplicationDetailAchievementsEditComponent,
    SelectDocumentsDialogComponent,
    AddDocumentsDialogComponent,
    ChooseEntrantTestDateDialogComponent
  ],
  imports: [
    SharedModule,
    ArtLibModule,
    EntrantDocListModule
  ],
  exports: [
    ApplicationDetailDocsEditComponent,
    ApplicationDetailMainEditComponent,
    ApplicationDetailTestsEditComponent,
    ApplicationDetailAchievementsEditComponent
  ],
  entryComponents: [
    SelectDocumentsDialogComponent,
    AddDocumentsDialogComponent,
    ChooseEntrantTestDateDialogComponent
  ]
})
export class ApplicationDetailEditModule { }
