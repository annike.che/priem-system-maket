import {Component, Input, OnInit} from '@angular/core';
import {ApplicationService} from '@/cabinets/university/application-menu/application.service';
import {ClsService} from '@/common/services/cls.service';
import {takeUntil} from 'rxjs/operators';
import * as cloneDeep from 'lodash/cloneDeep';
import {MatDialog} from '@angular/material/dialog';
import {ConfirmModalComponent} from '@art-lib/components/confirm-modal/confirm-modal.component';
import * as prepare from '@/common/functions/prepareData';
import {MatSnackBar} from '@angular/material/snack-bar';
import {NgForm} from '@angular/forms';


@Component({
  selector: 'app-application-detail-main-edit',
  templateUrl: './application-detail-main-edit.component.html',
  styleUrls: ['./application-detail-main-edit.component.scss']
})
export class ApplicationDetailMainEditComponent implements OnInit {

  @Input() appId: number;

  application: AppDetailMainInfo;
  application_c: AppDetailMainInfo;

  error: string;
  hasError = false;

  isLoad = true;


  cls = {
    return_types: {
      data: [],
      isLoad: true,
      initFunc:  () => this.clsService.getClsData( 'return_types')
    }
  };

  constructor(
      private applicationService: ApplicationService,
      private clsService: ClsService,
      private dialog: MatDialog,
      private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.initData();
    this.initCls();
  }

  initCls() {
    for (let key in this.cls) {
      if ( this.cls[key].isLoad ) {
        console.log('initFunc', this.cls[key].initFunc);
        this.cls[key].initFunc()
            .subscribe( res => {
              this.cls[key].data = res;
            });
      }
    }
  }

  initData() {
    this.hasError = false;
    this.isLoad = true;

    this.applicationService.getApplicationInfo(this.appId)
        .subscribe( res => {
          if(res.done) {
            this.application = res.data;
            this.application_c =  cloneDeep(res.data);
          } else {
            this.hasError = true;
            this.error = res.message || 'При попытке загрузить данные произошла ошибка';
          }
          this.isLoad = false;
        });
  }

  save(f: NgForm) {
    this.isLoad = true;
    this.application = prepare.prepareDate(this.application, ['agreed_date', 'disagreed_date', 'return_date', 'original_doc']);

    console.log('application', this.application);

    this.applicationService.saveApplicationInfo(this.appId, this.application)
        .subscribe( res => {
            if (res.done) {
              this.snackBar.open('Сохранение успешно выполнено');
              f.onReset();
              //this.appForm.dirty = false;
              this.initData();
            } else {
              this.snackBar.open('При попытке сохранить данные произошла ошибка' + (res.message ? ': ' + res.message : ''));
            }

            this.isLoad = false;
        });

  }

  cancel() {
    this.dialog.open(ConfirmModalComponent, {data: {
        title: 'Отменить изменения',
        content: 'Вы уверены, что хотите отменить изменения?'
      }})
        .afterClosed().subscribe( res => {
          if(res) {
            this.application = cloneDeep(this.application_c);
          }
    });
  }
}

export interface AppDetailMainInfo {
  id: number;
  uid?: string;
  distance_place?: string;
  distance_test: boolean;
  first_higher_education: boolean;
  id_status: number;
  need_hostel: boolean;
  rating: number;
  priority: number;
  registration_date: string;
  special_conditions: boolean;

  agreed?: boolean;
  disagreed?: boolean;
  agreed_date?: string;
  disagreed_date?: string;

  original: boolean;
  original_doc?: string;
  return_date?: string;
  id_return_type?: number;
}
