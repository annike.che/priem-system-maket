import {Component, Input, OnInit} from '@angular/core';
import {ApplicationService} from '@/cabinets/university/application-menu/application.service';
import {TestDetail} from '@/cabinets/university/application-menu/pages/application-detail/tabs/application-edit/application-detail-tests-edit/application-detail-tests-edit.component';

@Component({
  selector: 'app-application-detail-tests-view',
  templateUrl: './application-detail-tests-view.component.html',
  styleUrls: ['./application-detail-tests-view.component.scss']
})
export class ApplicationDetailTestsViewComponent implements OnInit {

  @Input() appId: number;

  isLoad = true;

  hasError = false;
  error: string;

  tests: Array<TestDetail>;

  testsColumns = ['testType', 'subject', 'ege', 'minScore', 'priority', 'dates', 'chooseDate', 'mark', 'docInfo', 'uid'];

  constructor(
      private applicationService: ApplicationService
  ) { }

  ngOnInit() {

    this.initData();
  }

  initData() {
    this.isLoad = true;
    this.hasError = false;
    this.applicationService.getApplicationTestsList(this.appId)
        .subscribe( res => {
          if( res.done ) {
            this.tests = res.data;
          } else {
            this.hasError = true;
            this.error = res.message || 'При попытке загрузить данные произошла ошибка';
          }
          this.isLoad = false;
        } );
  }


}
