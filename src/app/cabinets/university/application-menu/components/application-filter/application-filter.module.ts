import { NgModule } from '@angular/core';
import { ApplicationFilterComponent } from './application-filter.component';
import {ArtLibModule} from '@art-lib/art-lib.module';
import {SharedModule} from '@/common/modules/shared.module';



@NgModule({
  declarations: [ApplicationFilterComponent],
  imports: [
    ArtLibModule,
    SharedModule
  ],
  exports: [
    ApplicationFilterComponent
  ]
})
export class ApplicationFilterModule { }
