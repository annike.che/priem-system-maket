import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {ClsService} from '@/common/services/cls.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {FormBuilder,FormGroup} from '@angular/forms';
import {MatSelect} from '@angular/material/select';
import {MatOption} from '@angular/material/core';

@Component({
  selector: 'app-application-filter',
  templateUrl: './application-filter.component.html',
  styleUrls: ['./application-filter.component.scss']
})
export class ApplicationFilterComponent implements OnInit, OnDestroy {

  @Input() open: boolean;

  _params: number;
  get params(): any {
    return this._params;
  }

  @Input('params')
  set params(value: any) {
   // console.log('params', value);
    this._params = {...value};
   // console.log('change params', value);
   // this.changeDetectorRefs.detectChanges();
  }

  @Output() paramsChange = new EventEmitter<any>();

  @Output() filterChanges = new EventEmitter();

  filterForm: FormGroup;

  formWasInit = false;

  cls = {
    competitive: {
      data: [],
      initFunc: () =>  this.clsService.getCompetitiveSelectList(),
      isLoad: true,
      search: '',
      viewSelected: []
    },

    campaign: {
      data: [],
      initFunc: () =>  this.clsService.getCampaignList(),
      isLoad: true,
      search: '',
      viewSelected: []
    },

    status: {
      data: [],
      initFunc: () =>  this.clsService.getAppStatusList(),
      isLoad: true,
      search: '',
      viewSelected: []
    },
  };

  private ngUnsubscribe: Subject<void> = new Subject<void>();

  @ViewChild(MatSelect, {static: false}) select: MatSelect;


  constructor(
      private clsService: ClsService,
      private fb: FormBuilder,
      private changeDetectorRefs: ChangeDetectorRef
  ) {

  }

  ngOnInit() {
    this.initCls();
    console.log('select', this.select);
  }

  initCls() {
    for (let key in this.cls) {
      if ( this.cls[key].isLoad ) {
        // console.log('initFunc', this.cls[key].initFunc);
        this.cls[key].initFunc()
            .pipe(  takeUntil(this.ngUnsubscribe) )
            .subscribe( res => {
              this.cls[key].data = res;
            });
      }
    }

  }

  initSelectTrigger() {
    for (let key in this.params) {

    }
  }


  getSelectData(select: MatSelect, maxLength = 2): Array<string> {

    console.log('getSelectData', select);

    if (select.empty) return [''];

    let selectedOptions = (select.selected as MatOption[]).map(option => option.viewValue);

    if ( maxLength && maxLength < selectedOptions.length) {
      selectedOptions = selectedOptions.splice(0, maxLength);
    }

    if (select._isRtl()) {
      selectedOptions.reverse();
    }

    console.log('select', maxLength, selectedOptions);

    return selectedOptions;
  }

  changeSelectValue($event: any, select: MatSelect, key: string, maxLength?) {
    this.paramsChange.emit( this.params );
    this.filterChanges.emit();
  }

  deleteSelect(paramKey: string, selectCampaign: MatSelect, key: string) {
    this.params[paramKey] = undefined;
    this.paramsChange.emit( this.params );
    this.filterChanges.emit();
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
