import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-application-table-list-settings',
  templateUrl: './application-table-list-settings.component.html',
  styleUrls: ['./application-table-list-settings.component.scss']
})
export class ApplicationTableListSettingsComponent implements OnInit {

  settings = {
    number: false,
    fullname: false,
    snils: false,
    nameCompetitive: false,
    stateName: false,
    registrationDate: false,
    changedDate: false,
    original: false,
    agreed: false,
    agreedDate: false,
    disagreed: false,
    disagreedDate: false,
    needHostel: false,
    rating: false,
    epgu: false,
    action: true,
  };

  constructor(public dialogRef: MatDialogRef<ApplicationTableListSettingsComponent>,
              @Inject(MAT_DIALOG_DATA) public data,
  ) {
    if (data.length) {
      this.initSettings(data);
    }
  }

  ngOnInit() {
  }

  initSettings(data: Array<string>) {

    data.forEach( val => {
      if (this.settings.hasOwnProperty(val)) {
        this.settings[val] = true;
      }
    });

  }

  initArray( ) {
    let keys = Object.keys( this.settings);
    const columns = keys.filter(val => {
      return this.settings[val] === true;
    });

    return columns;
  }

  save() {
    const columns = this.initArray();

    this.dialogRef.close({columns});
  }
}
