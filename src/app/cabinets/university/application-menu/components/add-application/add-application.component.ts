import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ClsService} from '@/common/services/cls.service';
import {CampaignService} from '@/cabinets/university/campaign-menu/campaign.service';
import {EntrantsService} from '@/cabinets/university/entrants-menu/entrants.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ApplicationService} from '@/cabinets/university/application-menu/application.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Router} from '@angular/router';
import {OrgsService} from '@/cabinets/university/orgs-menu/orgs.service';
import * as prepare from '@/common/functions/prepareData';

@Component({
  selector: 'app-add-application',
  templateUrl: './add-application.component.html',
  styleUrls: ['./add-application.component.scss']
})
export class AddApplicationComponent implements OnInit {


    _idEntrant: number;
    get idEntrant(): number {
        return this._idEntrant;
    }

    @Input('idEntrant')
    set idEntrant(value: number) {
        this._idEntrant = value;
    }

    @Output() saved = new EventEmitter();

    competitiveFilter: boolean = false;
    competitiveFilterData: CompetitiveFilter;

    cls = {
        campaign: {
          data: [],
          initFunc:  () => this.clsService.getCampaignList(),
          isLoad: true
        },
        education_level: {
          data: [],
          initFunc:  () => this.clsService.getClsData('education_levels'),
          isLoad: true
        },
        education_forms: {
          data: [],
          initFunc: () => this.clsService.getClsData('education_forms'),
          isLoad: true
        },
        education_source: {
          data: [],
          initFunc: () => this.clsService.getClsData('education_sources'),
          isLoad: true
        },
        direction: {
            data: [],
            initFunc: () => this.clsService.getDirectionByIdEntrant(this.idEntrant),
            isLoad: true
        },
        competitive: {
          data: [],
          isLoad: false
        }
    };

    docs: Array<DocsListEntrant> = [];

    search = {
        campaign: '',
        direction: '',
        competitive: ''
    };
    competitiveId: number;
    competitiveInfo: Competitive;

    otherDataForm: FormGroup;

    selectedDoc = {
        valid: false,
        items: [],
    };

  constructor(
      private clsService: ClsService,
      private orgService: OrgsService,
      private campaignService: CampaignService,
      private entrantsService: EntrantsService,
      private fb: FormBuilder,
      private appService: ApplicationService,
      private snackBar: MatSnackBar,
      private router: Router
  ) {

  }

  ngOnInit() {
    this.competitiveFilterData = {
      id_entrant: this.idEntrant
    };

    this.initCls();
    this.loadCompetitive();
    this.loadDocList();
    this.initOtherForm();


  }

  initCls() {
    for (let key in this.cls) {
      if ( this.cls[key].isLoad ) {
        // console.log('initFunc', this.cls[key].initFunc);
        this.cls[key].initFunc()
            .pipe(   )
            .subscribe( res => {
              this.cls[key].data = res;
            });
      }
    }
  }

  loadCompetitive() {
      console.log('id Edntrant', this.idEntrant, this.competitiveFilterData);
      this.clsService.getCompetitiveSelectListByEntrantId(this.competitiveFilterData)
          .subscribe( res => {
              this.cls.competitive.data = res;
              this.competitiveId = null;
              this.competitiveInfo = null;
          } );

  }

  loadDocList() {
      this.entrantsService.getShortDocList(this.idEntrant)
          .subscribe( res => {
              if (res.done) {
                  this.docs = res.data;
                  console.log('docList', this.docs);
              }
          } );
  }

  initOtherForm() {
      this.otherDataForm = this.fb.group( {
          app_number: [ null, Validators.required ],
          registration_date: [ null, Validators.required ],
          uid: [ null ],
          rating: [ null ],
          priority: [ null ],
          first_higher_education: [ true ],
          original: [ false ],
          original_doc: [null],
          agreed: [null],
          agreed_date: [null],
          need_hostel: [ false ],
          special_conditions: [ false ],
          distance_test: [ false ],
          distance_place: [ null ],
      } );

      this.otherDataForm.controls['original'].valueChanges.subscribe( res => {
        if (res === false ) {
          this.otherDataForm.controls['original_doc'].setValidators([]);
          this.otherDataForm.controls['original_doc'].setValue(null);
        } else {
          this.otherDataForm.controls['original_doc'].setValue(null);
          this.otherDataForm.controls['original_doc'].setValidators([Validators.required]);
        }
        this.otherDataForm.controls['original_doc'].updateValueAndValidity();
      });

      this.otherDataForm.controls['agreed'].valueChanges.subscribe( res => {
        if (res === false ) {
          this.otherDataForm.controls['agreed_date'].setValidators([]);
          this.otherDataForm.controls['agreed_date'].setValue(null);
        } else {
          this.otherDataForm.controls['agreed_date'].setValue(null);
          this.otherDataForm.controls['agreed_date'].setValidators([Validators.required]);
        }
        this.otherDataForm.controls['agreed_date'].updateValueAndValidity();
      });
  }


  deleteCompetitiveFilter() {
    this.competitiveFilterData = {
        id_entrant: this.idEntrant
    };
  }

  deleteCompetitive() {
      this.competitiveInfo = undefined;
      this.competitiveId = undefined;
  }


    canSaveApp(): boolean {

      return !(this.otherDataForm.valid && this.idEntrant && this.competitiveId && this.selectedDoc.valid);
    }

    validDocForms(): boolean {
      let hasEducation = false;
      let hasInd = false;

      this.selectedDoc.items.forEach( (el, ind) => {
            hasEducation =  (el.type === 'educations') ?         true : hasEducation;
            hasInd       =  (el.type === 'identification') ?    true : hasInd;
        } );

      return  hasEducation && hasInd;
    }

    changeSelect($event) {
      if ( $event) {
          this.selectedDoc.valid = this.validDocForms();
      }
    }

    saveApp() {
      let saveData = {
          id_competitive_group: this.competitiveId,
          id_entrant: this.idEntrant,
          docs: this.selectedDoc.items,
          ... prepare.prepareDate(this.otherDataForm.value, ['agreed_date', 'original_doc', 'registration_date'])
        };



      this.appService.addApplication(saveData)
          .subscribe( res => {
             if ( res.done ) {
                 this.snackBar.open( 'Заяление успешно создано');
                 this.saved.emit(true);

             } else {
                 this.snackBar.open( 'При создании заявления произошла ошибка' + ((res.message) ? `: ${res.message}` : '') );
             }
          });



    }

}

interface CompetitiveFilter {
    id_campaign?: number;
    id_education_level?: number;
    id_education_form?: number;
    id_education_source?: number;
    id_direction?: number;
    id_entrant: number;
}
