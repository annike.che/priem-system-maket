import { NgModule } from '@angular/core';
import { AddApplicationComponent } from './add-application.component';
import {SharedModule} from '@/common/modules/shared.module';
import {ArtLibModule} from '@art-lib/art-lib.module';
import {EntrantDocListModule} from '@/cabinets/university/entrants-menu/components/entrant-doc-list/entrant-doc-list.module';

/*const MY_DATE_FORMATS = {
    parse: {
        dateInput: ['DD.MM.YYYY', 'LL', 'DD-MM-YYYY' ],
    },
    display: {
        dateInput: 'DD.MM.YYYY',
        monthYearLabel: 'YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY'
    },
};*/


@NgModule({
  declarations: [AddApplicationComponent],
  imports: [
    ArtLibModule,
    SharedModule,
    EntrantDocListModule
  ],
  exports: [
    AddApplicationComponent,
  ],
  entryComponents: [  ]
})
export class AddApplicationModule { }
