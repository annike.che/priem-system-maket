import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {EntrantsService} from '@/cabinets/university/entrants-menu/entrants.service';
import {ApplicationService} from '@/cabinets/university/application-menu/application.service';

@Component({
  selector: 'app-print-dialog',
  templateUrl: './print-dialog.component.html',
  styleUrls: ['./print-dialog.component.scss']
})
export class PrintDialogComponent implements OnInit {

  title = 'Печать';
  description: string;
  idEntrant: number;
  docsAll: Array<DocsListEntrant> = [];

  categoriesDoc: Array<any>;

  isLoad: boolean;

  selectedDoc = {
    valid: false,
    items: [],
  };

  constructor(
      private entrantService: EntrantsService,
      public dialogRef: MatDialogRef<PrintDialogComponent>,
      @Inject(MAT_DIALOG_DATA) public data: any) {
    this.title = data.title || this.title;
    this.description = data.description;
    this.idEntrant = +data.idEntrant;
    this.categoriesDoc = data.categoriesDoc;

  }

  ngOnInit() {
    if (this.idEntrant) {
      this.getEntrantDoc();
    }
  }

  getEntrantDoc() {
    this.isLoad = true;
    this.entrantService.getShortDocList(this.idEntrant, {categories: this.categoriesDoc})
        .subscribe( res => {
          if( res.done ) {
            this.docsAll = res.data;
          }
          this.isLoad = false;
        });
  }

  changeSelect($event) {
    if ( $event) {
      this.selectedDoc.valid = this.validDocForms();
    }
  }

  validDocForms(): boolean {
    let hasEducation = false;
    let hasInd = false;

    this.selectedDoc.items.forEach( (el, ind) => {
      hasEducation =  (el.type === 'educations') ?         true : hasEducation;
      hasInd       =  (el.type === 'identification') ?    true : hasInd;
    } );

    return  hasEducation && hasInd;
  }

  print() {
    this.dialogRef.close(this.selectedDoc.items);
  }
}
