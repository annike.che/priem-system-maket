import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ClsService} from '@/common/services/cls.service';
import * as statusMap from '../../classes/statuses-map';

@Component({
  selector: 'app-select-status-dialog',
  templateUrl: './select-status-dialog.component.html',
  styleUrls: ['./select-status-dialog.component.scss']
})
export class SelectStatusDialogComponent implements OnInit {

  statuses = [];
  currStatus: string;
  currStatus_c: string;
  comment: string;
  isDisabled: boolean;

  disabledStatuses = [];

  constructor(
      private clsService: ClsService,
      public dialogRef: MatDialogRef<SelectStatusDialogComponent>,
      @Inject(MAT_DIALOG_DATA) public data: any) {

    this.currStatus = data.status;
    this.currStatus_c = data.status;

    this.isDisabled = data.isDisabled;
  }

  ngOnInit() {

    this.clsService.getAppStatusList({set_vuz: true})
        .subscribe( res => {
            this.statuses = res;
            this.initDisabledStatuses();
        } );


  }

  save() {
    if (this.currStatus !== this.currStatus_c || this.isDisabled) {
      this.dialogRef.close({status: this.currStatus, comment: this.comment});
    }
  }

  initDisabledStatuses() {
   // console.log(this.currStatus_c);
    this.disabledStatuses = statusMap.getDisabledStatuses(this.currStatus_c, this.statuses);
  }



}
