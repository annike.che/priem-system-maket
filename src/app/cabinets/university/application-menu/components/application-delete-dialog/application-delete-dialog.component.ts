import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-application-delete-dialog',
  templateUrl: './application-delete-dialog.component.html',
  styleUrls: ['./application-delete-dialog.component.scss']
})
export class ApplicationDeleteDialogComponent implements OnInit {

  comment: string;

  constructor(public dialogRef: MatDialogRef<ApplicationDeleteDialogComponent>) { }

  ngOnInit() {
  }

  save() {
      this.dialogRef.close({comment: this.comment});
  }
}
