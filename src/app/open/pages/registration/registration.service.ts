import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {UserProfileService} from '@/common/services/user-profile.service';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs';
import {toFormData} from '@/cabinets/university/entrants-menu/entrants.service';

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  urlPrefix: string;

  constructor(
      private httpClient: HttpClient,
  ) {
    this.urlPrefix = environment.apiUrl;
  }

  registration(data): Observable<any> {
    const url = `${this.urlPrefix}/registration`;

    return this.httpClient.post(url,  toFormData(data));
  }
}
