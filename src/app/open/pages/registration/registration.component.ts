import { Component, OnInit } from '@angular/core';
import {artAnimations} from '@art-lib/animations';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {OrgsAdminService} from '@/cabinets/admin/orgs-menu/orgs-admin.service';
import {OrgsCommonService} from '@/common/services/orgs-common.service';
import * as generator from '@/common/functions/password';
import {RegistrationService} from '@/open/pages/registration/registration.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
  animations: artAnimations
})
export class RegistrationComponent implements OnInit {

  registerForm: FormGroup;

  organizationList: Array<any> = [];
  searchSelect = '';

  typePass = 'password';
  showPass = false;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private fb: FormBuilder,
              private orgsSevice: OrgsCommonService,
              private regService: RegistrationService,
              private snackBar: MatSnackBar) {

  }

  ngOnInit() {
    this.createForm();
    this.initOrgList();
  }

  createForm() {
    this.registerForm = this.fb.group({
      name:               [ null, Validators.required ],
      surname:            [ null, Validators.required ],
      patronymic:         [ null ],
      login:              [ null, [ Validators.required,
        Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')
      ]],
      password:           [ null, Validators.required ],
      phone:              [ null ],
      email:              [ null, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')],
      id_organization:    [ null, Validators.required ],
      file:               [ null, Validators.required ]
    });
  }

  initOrgList() {
    this.orgsSevice.getOrgsSelectList()
        .subscribe( res => {
          this.organizationList = res;
        } );
  }

  registrate() {
    console.log( this.registerForm.value );

    let data = {
      ...this.registerForm.value,
      password: generator.hashPass(this.registerForm.get('login').value, this.registerForm.get('password').value)
    };

    data.id_organization = data.id_organization.id;

    this.regService.registration( data)
        .subscribe( res => {
          if( res.done ) {
            this.snackBar.open('Вы успешно отправили заявку на регистрацию');
          }
        });


  }

  changeShowPass() {
    this.showPass = !this.showPass;
    this.typePass = this.showPass ? 'text' : 'password';
  }
}
