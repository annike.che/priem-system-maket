import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import { sha256 } from 'js-sha256';
import {environment} from '../../../../environments/environment';
import {map} from 'rxjs/operators';
import {UserProfileService} from '@/common/services/user-profile.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  urlPrefix: string;

  constructor(
      private httpClient: HttpClient,
      private userProfile: UserProfileService
  ) {
    this.urlPrefix = environment.apiUrl;
  }

  login(login: string, pass: string): Observable<any> {
    const url = `${this.urlPrefix}/login`;
    const hash = sha256(`${login.toUpperCase()}${pass}`);
    return this.httpClient.post(url, { login, password: hash})
        .pipe(
            map( (res: any) => {
              if ( res.done ) {
                this.userProfile.setProfile(res.user_info);
                this.userProfile.setOrganizationsList(res.organizations_list);
              }
              return res;
            } )
        );

  }

  logout(): Observable<any> {
    const url = `${this.urlPrefix}/logout`;
    return this.httpClient.get(url);
  }

  isAuth(): Observable<any> {
    const url = `${this.urlPrefix}/is-auth`;
    return this.httpClient.get(url);
  }


}
