import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '@/open/pages/auth/auth.service';
import {UserProfileService} from '@/common/services/user-profile.service';
import * as roles from '@/common/functions/roles-strategy';

import { artAnimations } from '@art-lib/animations';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
  animations: artAnimations
})
export class AuthComponent implements OnInit {

  authForm: FormGroup;
  isLoad: boolean;
  isError: boolean;
  message = 'При попытке авторизации произошла ошибка';


  typePass = 'password';
  showPass = false;

  user: User;
  organizations: Array<any>;

  cabinets: Array<string> | null;

  roles = roles;


  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private authService: AuthService,
    private userProfile: UserProfileService,
    private snackBar: MatSnackBar
  ) {
  }

  ngOnInit() {
    this.user = this.userProfile.getProfile();
    this.organizations = this.userProfile.getOrganizationsList();
    console.log('call user info', this.user);
    this.cabinets = ( this.user.role ) ? roles.getCabinetsByRole(this.user.role.code) : [];
    this.createForm();
  }

  createForm() {

    this.authForm = this.fb.group({
        login: [null, [
          Validators.required,
          Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]
        ],
        pass: [null,
          Validators.required
        ]
      }
    );
  }

  goToCabinet(cabinetName: string) {
    this.router.navigate(['/cabinets', cabinetName]);
  }


  login() {
    this.isLoad = true;
    this.isError = false;

    this.authService.login(this.authForm.value.login, this.authForm.value.pass)
      .subscribe(res => {
          if (res.done) {
            this.user = res.user_info;
            this.organizations = res.organizations_list;
            this.cabinets = ( this.user.role ) ? roles.getCabinetsByRole(this.user.role.code) : [];
          } else {
            this.isError = true;
            this.message = res.message || this.message;
          }
          this.isLoad = false;

        },
        err => {
          this.isError = true;
          this.message = err || this.message;
          this.isLoad = false;
        });


  }

  logout() {
    this.isLoad = true;
    this.authService.logout()
      .subscribe(res => {
        if (res.done) {
        this.user = this.userProfile.setProfile({});
        this.organizations = this.userProfile.setOrganizationsList([]);
        this.cabinets = null;
        } else {
          this.isError = true;
          this.message = 'Не удалось выполнить выход';
        }
        this.isLoad = false;
      },
        err => {
          this.isError = true;
          this.message = err || this.message;
          this.isLoad = false;
        });
  }

  changeShowPass() {
    this.showPass = !this.showPass;
    this.typePass = this.showPass ? 'text' : 'password';
  }

  saveOrganization(organization: any) {
    console.log('organization', organization);
    if (organization) {
      this.userProfile.setNewOrganization(organization)
        .subscribe(res => {
          if(!res.done) {
            this.snackBar.open('Не удалось выбрать организацию' + (res.message ? ': ' + res.message : ''));
          }
        });
    } else {
     this.userProfile.removeOrganization()
         .subscribe( res => {
           if(!res.done) {
             this.snackBar.open('Произошла ошибка' + (res.message ? ': ' + res.message : ''));
           }
         } );
    }
    this.user.organization = organization;
    console.log(this.user.organization);
  }

  goToRegistration() {
      this.router.navigate(['/registration'], {relativeTo: this.route});
  }
}
