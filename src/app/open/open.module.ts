import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {MaterialModule} from '@/common/modules/material/material.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AuthComponent} from '@/open/pages/auth/auth.component';
import { SelectOrganizationComponent } from './components/select-organization/select-organization.component';
import {SharedModule} from "@/common/modules/shared.module";
import {ArtLibModule} from '@art-lib/art-lib.module';
import {CommonPipesModule} from '@/common/pipes/_common-pipes.module';
import { RegistrationComponent } from './pages/registration/registration.component';
import {IMaskModule} from 'angular-imask';

const routes: Routes = [
  {
    path: 'auth',
    component: AuthComponent
  },
  {
    path: 'registration',
    component: RegistrationComponent
  }
];

@NgModule({
  declarations: [
      AuthComponent,
      SelectOrganizationComponent,
      RegistrationComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    SharedModule,
    ArtLibModule,
    CommonPipesModule,
    IMaskModule
  ]
})
export class OpenModule { }
