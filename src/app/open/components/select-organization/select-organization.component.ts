import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {ReplaySubject, Subject} from 'rxjs';
import {MatSelect} from '@angular/material/select';
import {ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR} from '@angular/forms';

import {takeUntil} from 'rxjs/operators';
import {MatFormFieldAppearance} from '@angular/material/form-field';

export interface Org {
  id: string;
  kpp: string;
  ogrn: string;
  short_title: string;
}

@Component({
  selector: 'app-select-organization',
  templateUrl: './select-organization.component.html',
  styleUrls: ['./select-organization.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: SelectOrganizationComponent,
      multi: true
    }]
})
export class SelectOrganizationComponent implements ControlValueAccessor, OnInit, AfterViewInit, OnDestroy {

  @Input() organization: Org;

  @Input() organizations: Org[];

  @Input() appearance: MatFormFieldAppearance = 'outline';

  @Input() label: string = 'Организация';

  @Input() selectedOrgLabel: string = 'Выбранная организация';

  @Input() required;


  private onChange: (value: Org) => void = () => {};
  private onTouched = () => {};


  /** control for the selected org */
  public orgCtrl: FormControl = new FormControl();

  searchOrg = '';

  @ViewChild('singleSelect', { static: true }) singleSelect: MatSelect;

  /** Subject that emits when the component has been destroyed. */
  protected _onDestroy = new Subject<void>();


  constructor() { }

  ngOnInit() {

  }

  registerOnChange(onChange: (value: any) => void): void{
    this.onChange = onChange;
  }

  writeValue( value: Org): void {
    this.organization = value;
    //this.onChange(this.data);
  }

  registerOnTouched( fn: () => {} ): void {
    this.onTouched = fn;
  }

  public onInputBlurred(): void {
    this.onTouched();
  }

  delOrganization() {
    this.orgCtrl.setValue(null);
    this.organization = null;
    this.onChange(this.organization);
    //this.newIdOrganization.emit(this.organization);
  }

  orgChange($event) {
    this.organization = $event;
    this.onChange($event);
    //this.newIdOrganization.emit($event);
  }

  ngAfterViewInit() {

  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }
}
