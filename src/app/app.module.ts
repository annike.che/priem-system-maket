import {BrowserModule, DomSanitizer} from '@angular/platform-browser';
import {APP_INITIALIZER, LOCALE_ID, NgModule} from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatIconRegistry} from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import {registerLocaleData} from '@angular/common';
import localeRu from '@angular/common/locales/ru';
import {SharedModule} from '@/common/modules/shared.module';
import {UserProfileService} from '@/common/services/user-profile.service';
import {MAT_SNACK_BAR_DEFAULT_OPTIONS} from '@angular/material/snack-bar';
import {MatPaginatorIntlRu} from '@/common/classes/mat-paginator-intl-ru';
import {MatPaginatorIntl} from '@angular/material/paginator';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MomentDateAdapter} from '@angular/material-moment-adapter';


export function get_user(userProfileService: UserProfileService) {
  console.log(' get user  APP_INITIALIZER');
  return () => userProfileService.loadUserData();
}
const MY_DATE_FORMATS = {
  parse: {
    dateInput: ['DD.MM.YYYY', 'LL', 'DD-MM-YYYY' ],
  },
  display: {
    dateInput: 'DD.MM.YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY'
  },
};



@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    SharedModule
  ],
  providers: [
    { provide: APP_INITIALIZER, useFactory: get_user, deps: [UserProfileService], multi: true},
    { provide: LOCALE_ID, useValue: 'ru' },
    {provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: {duration: 3000}},
    { provide: MatPaginatorIntl, useClass: MatPaginatorIntlRu },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS }
  ],
  bootstrap: [AppComponent],
  exports: [

  ]
})
export class AppModule {
  constructor(
      private matIconRegistry: MatIconRegistry,
      private domSanitizer: DomSanitizer
  ) {
    this.matIconRegistry.addSvgIconSet(
        this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/mdi.svg')
    );
    this.matIconRegistry.addSvgIcon(
        'student-hat', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/student.svg'));
    this.matIconRegistry.addSvgIcon(
        'file', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/file.svg'));
    this.matIconRegistry.addSvgIcon(
        'diploma', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/diploma.svg'));
    this.matIconRegistry.addSvgIcon(
        'diploma2', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/diploma (1).svg'));
    this.matIconRegistry.addSvgIcon(
        'certificate', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/certificate.svg'));
    this.matIconRegistry.addSvgIcon(
        'file2', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/file(2).svg'));
    this.matIconRegistry.addSvgIcon(
        'medal', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/medal.svg'));
    this.matIconRegistry.addSvgIcon(
        'stamp', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/stamp.svg'));
    this.matIconRegistry.addSvgIcon(
        'winner', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/winner.svg'));
    this.matIconRegistry.addSvgIcon(
        'books', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/books.svg'));
    this.matIconRegistry.addSvgIcon(
        'gear', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/gear.svg'));
    this.matIconRegistry.addSvgIcon(
        'flag', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/flag.svg'));
    this.matIconRegistry.addSvgIcon(
        'flag-1', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/flag (1).svg'));
    this.matIconRegistry.addSvgIcon(
        'flag-3', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/flag (3).svg'));
    this.matIconRegistry.addSvgIcon(
        'shapes', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/shapes.svg'));
    this.matIconRegistry.addSvgIcon(
        'networks', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/networks.svg'));
    this.matIconRegistry.addSvgIcon(
      'org', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/org.svg'));
    this.matIconRegistry.addSvgIcon(
      'news', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/news.svg'));
    this.matIconRegistry.addSvgIcon(
      'upload-packages', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/upload.svg'));
    this.matIconRegistry.addSvgIcon(
      'upload-packages-1', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/upload (2).svg'));
    this.matIconRegistry.addSvgIcon(
      'users', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/friends.svg'));
    this.matIconRegistry.addSvgIcon(
      'org-1', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/connection.svg'));
    this.matIconRegistry.addSvgIcon(
      'org-2', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/move.svg'));

    registerLocaleData(localeRu, 'ru');
  }
}
