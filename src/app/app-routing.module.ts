import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RedirectGuard} from '@/common/guards/redirect.guard';
import {CustomPreloadingStrategyService} from '@/custom-preloading-strategy.service';


const routes: Routes = [
   {
    path: '',
    children: [],
    canActivate: [RedirectGuard]
  },
  {
    path: '',
    loadChildren: () => import('./open/open.module').then( m => m.OpenModule ),
   // canActivate: [RedirectGuard]
  },
  {
    path: 'cabinets',
    loadChildren: () => import('./cabinets/cabinets.module').then( m => m.CabinetsModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,  {
    preloadingStrategy: CustomPreloadingStrategyService
     /* anchorScrolling: 'enabled',
      enableTracing: true,
      scrollPositionRestoration: 'enabled',*/
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
