import { Injectable } from '@angular/core';
import {ActivatedRoute, NavigationEnd, PreloadingStrategy, Route, Router} from '@angular/router';
import {Observable, of} from 'rxjs';
import {filter, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CustomPreloadingStrategyService implements PreloadingStrategy {

  checkpoints: Set<PreloadCheckpoints> = new Set<PreloadCheckpoints>();

  constructor(router: Router, route: ActivatedRoute) {
    router.events.pipe(
        filter(event => event instanceof NavigationEnd),
        map(() => route),
        map(route => {
          while (route.firstChild) route = route.firstChild;  //gets the deepest child
          return route;
        }),
        filter(route => route.outlet === 'primary'))
        .subscribe((route) => {
          //console.log('route', route, route.snapshot.data);
          if (route.snapshot.data['preloadCheckpoint'] !== undefined) {
            this.checkpoints.add(route.snapshot.data['preloadCheckpoint']);
          }
        });

  }

  preload(route: Route, fn: () => Observable<any>): Observable<any> {

    if (route.data && ( route.data['preload'] || this.checkpoints.has(route.data['preloadAfterCheckpoint']) )) {
      return fn(); //preload this route
    } else {
      return of(null);
    }
  }

}

export enum PreloadCheckpoints {
  FRUIT
}

