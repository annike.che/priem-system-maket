import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'replaceArrayToString'
})
export class ReplaceArrayToStringPipe implements PipeTransform {

  transform(value: Array<string> = [''], symbol = ', '): any {
    if (!value) {
      value = [''];
    }
    return value.join(symbol);
  }

}
