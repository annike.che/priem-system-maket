import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'yesNo'
})
export class YesNoPipe implements PipeTransform {

  transform(value: boolean, yesTitle?: string, noTitle?: string): any {
    const yes = yesTitle || 'Да';
    const no = noTitle || 'Нет';
    return (value === true) ? yes : no;
  }

}
