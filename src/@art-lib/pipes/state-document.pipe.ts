import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'stateDocument'
})
export class StateDocumentPipe implements PipeTransform {

  transform(value: boolean, ...args: any[]): any {
    return (value === true) ? 'Подтвержден' : 'Не подтвержден';
  }

}
