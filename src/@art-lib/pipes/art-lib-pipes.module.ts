import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ReplaceArrayToStringPipe} from '@art-lib/pipes/replace-array-to-string.pipe';
import {StateDocumentPipe} from '@art-lib/pipes/state-document.pipe';
import {YesNoPipe} from '@art-lib/pipes/yes-no.pipe';
import {FilterPipe} from '@art-lib/pipes/filter.pipe';
import {SumByKeysPipe} from '@art-lib/pipes/sum-by-keys.pipe';
import {CustomSelectedValuePipe} from '@art-lib/pipes/custom-selected-value.pipe';
import { SafePipe } from './safe.pipe';
import { ReplacePipe } from './replace.pipe';



@NgModule({
  declarations: [
    ReplaceArrayToStringPipe,
    StateDocumentPipe,
    YesNoPipe,
    FilterPipe,
    SumByKeysPipe,
    CustomSelectedValuePipe,
    SafePipe,
    ReplacePipe,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ReplaceArrayToStringPipe,
    StateDocumentPipe,
    YesNoPipe,
    FilterPipe,
    SumByKeysPipe,
    CustomSelectedValuePipe,
    SafePipe,
    ReplacePipe
  ]
})
export class ArtLibPipesModule { }
