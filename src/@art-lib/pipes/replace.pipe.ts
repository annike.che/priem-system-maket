import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'replace'
})
export class ReplacePipe implements PipeTransform {

  transform(value: number | string, strToReplace: string, replacementStr: string = ''): string {

    value = `${value}`;

    if ( !value || ! strToReplace )
    {
      return value;
    }

    return value.replace(new RegExp(strToReplace, 'g'), replacementStr);
  }

}
