import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sumByKeys'
})
export class SumByKeysPipe implements PipeTransform {

  transform(value: any, keys: Array<string> = []): any {
    //console.log(value, keys);

    let sum = 0;
    keys.forEach( el => {
      if (value.hasOwnProperty(el) && Number(value[el])) {
        sum += value[el];
      }
    });
    return sum;
  }

}
