import { Pipe, PipeTransform } from '@angular/core';
import {MatSelect} from '@angular/material/select';
import {MatOption} from '@angular/material/core';

@Pipe({
  name: 'customSelectedValue',
  pure: false
})
export class CustomSelectedValuePipe implements PipeTransform {

  transform( value: any, select: MatSelect, maxLength: number = 2): string {

   // console.log('CustomSelectedValuePipe', value, select.selected);

    if (select.empty) return '';

    const selectedOptions = (select.selected as MatOption[]).map(option => option.viewValue);

   // console.log('selectedOptions', selectedOptions);

    const selectedLength = selectedOptions.length;

    if( selectedLength === 0 ) {
      return '';
    }

    let resultValue = selectedOptions;
    let additional = '';

    if ( maxLength && maxLength < selectedLength) {
      resultValue = selectedOptions.splice(0, maxLength);
      additional = ` ( + еще ${selectedLength - maxLength})`;
    }

    if (select._isRtl()) {
      selectedOptions.reverse();
    }

    let resultString = resultValue.join(', ') + additional;

   // console.log('select end', maxLength, resultString);

    return resultString;
  }

}
