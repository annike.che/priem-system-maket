import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {animate, animateChild, group, query, state, style, transition, trigger} from '@angular/animations';
import {filter, isEmpty} from 'rxjs/operators';
import {PerfectScrollbarConfigInterface} from 'ngx-perfect-scrollbar';
import {Observable} from 'rxjs';
import {ActivatedRoute, NavigationEnd, Router, RouterEvent} from '@angular/router';

@Component({
  selector: 'app-left-sidenav',
  templateUrl: './left-sidenav.component.html',
  styleUrls: ['./left-sidenav.component.scss'],
  animations: [

    trigger('openedMenu', [
      state('true', style({ width: '270px' })),
      state('false', style({ width: '70px' })),
      transition('true => false', [
          group([
            query('@hiddenTitle', [
              animateChild()
            ]),
              animate('200ms 0.05s ease')
            ])
      ]),
      transition('false => true', [
          group([
            query('@hiddenTitle', [
              animateChild()
            ]),
              animate('200ms ease-out')
            ])
      ])
    ]),
    trigger('openedMenuContent', [
      state('true', style({ 'margin-left': '270px' })),
      state('false', style({ 'margin-left': '70px' })),
      transition('true => false', animate('200ms 0.05s ease')),
      transition('false => true', animate('200ms ease-out'))
    ]),
    trigger('hiddenTitle', [
      state('true', style({ visibility: 'hidden', width: '0px', height: '0px', opacity: 0 })),
      state('false', style({ visibility: '*', width: '*', height: '*', opacity: 1  })),
      transition('false => true', animate(0)),
      transition('true => false', animate('0.1s 0.1s'))
    ]),
    trigger('openItem', [
      state('false', style({ transform: 'rotate(0)' })),
      state('true', style({ transform: 'rotate(-270deg)' })),
      transition('false => true', animate('200ms ease-in')),
      transition('true => false', animate('200ms ease-out'))
    ]),

  ]
})
export class LeftSidenavComponent implements OnInit {

  @Input() menuItems: Array<IMenuItems>;

  @Input('toggleMenu') toggleMenuEvent: Observable<void>;

  private keyName = 'open-main-menu';
  openSidebar = localStorage.getItem(this.keyName) !== 'false';

  mouseInSidebar = false;

  currUrl = '';


  constructor(
      private router: Router,
      private route: ActivatedRoute
  ) {
      this.currUrl = router.url;

  }

  ngOnInit() {
    this.toggleMenuEvent.subscribe( ($event) => {
      this.toggleMenu($event);
    });

    this.router.events.pipe(
        filter(event => event instanceof NavigationEnd)
        ).subscribe( (event: NavigationEnd) => {
          this.currUrl = event.urlAfterRedirects;
         // console.log('currUrl', this.currUrl);
        });
  }

  openMenuHover(event) {
    //console.log('event', event);
    if ( !this.openSidebar ) {
      if (event.type === 'mouseenter' && event.target) {
       // console.log('mouseenter', this.openSidebar, event.relatedTarget.prototype );
        if( event.relatedTarget !== 'svg') {
          this.mouseInSidebar = true;
        }



      }
      if (event.type === 'mouseleave') {
        this.mouseInSidebar = false;
      }
    }


  }

  toggleMenu($event) {
    $event.stopPropagation();
    //console.log('event toggleMenu', $event);
    this.openSidebar = !this.openSidebar;

    //если сайдбар закрывается, но мыш на нем, то он должен закрыться
    //this.mouseInSidebar = false;

    localStorage.setItem(this.keyName, `${this.openSidebar}`);


  }

}
