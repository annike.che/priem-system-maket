interface IMenuItems {
    title: string;
    router: string;
    icon?: string;
    children?: Array<IMenuItems>;
    open?: boolean;
}
