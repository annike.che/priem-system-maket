import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmModalComponent } from './confirm-modal.component';
import {MaterialModule} from '@/common/modules/material/material.module';

@NgModule({
  declarations: [ConfirmModalComponent],
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports: [ ConfirmModalComponent ],
  entryComponents: [ConfirmModalComponent],
})
export class ConfirmModalModule { }
