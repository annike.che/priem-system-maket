import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Component, OnInit, Inject } from '@angular/core';


@Component({
  selector: 'app-confirm-modal',
  templateUrl: './confirm-modal.component.html',
  styleUrls: ['./confirm-modal.component.scss']
})
export class ConfirmModalComponent implements OnInit {

  constructor(
    private matDialogRef: MatDialogRef<ConfirmModalComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { }

  title: string;
  content: string;
  btnOk: string;
  btnNo: string;


  ngOnInit() {
    this.title = this.data.title || '';
    this.content = this.data.content || 'Вы уверены, что хотите совершить данное действие?';
    this.btnOk = this.data.btnOk || 'Да';
    this.btnNo = this.data.btnNo || 'Нет';
  }
  public close() {
    this.matDialogRef.close();
  }

  yes(): void {
    this.matDialogRef.close(true);
  }
}
