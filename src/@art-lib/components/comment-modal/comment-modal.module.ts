import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommentModalComponent } from './comment-modal.component';
import {MaterialModule} from '@/common/modules/material/material.module';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [CommentModalComponent],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule
  ],
  exports: [ CommentModalComponent ],
  entryComponents: [ CommentModalComponent ]
})
export class CommentModalModule { }
