import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-comment-modal',
  templateUrl: './comment-modal.component.html',
  styleUrls: ['./comment-modal.component.scss']
})
export class CommentModalComponent implements OnInit {

  comment: string;

  title = 'Комментарий';
  label = 'Введите комментарий';

  constructor(public dialogRef: MatDialogRef<CommentModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.title = data.title || this.title;
    this.label = data.label || this.label;

  }

  ngOnInit() {
  }

  save() {
    this.dialogRef.close({comment: this.comment});
  }

}
