import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';


@Component({
  selector: 'app-full-photo-modal',
  templateUrl: './full-photo-modal.component.html',
  styleUrls: ['./full-photo-modal.component.scss']
})
export class FullPhotoModalComponent implements OnInit {

  title: string;
  imageToShow: string;
  btnClose: string;

  constructor(
    private matDialogRef: MatDialogRef<FullPhotoModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {

    this.title = this.data.title || 'Фото';
    this.btnClose = this.data.btnClose || 'Закрыть';
    this.imageToShow = this.data.image;
  }


  ngOnInit() {
  }
}
