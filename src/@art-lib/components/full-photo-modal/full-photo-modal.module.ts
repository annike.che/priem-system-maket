import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FullPhotoModalComponent} from '@art-lib/components/full-photo-modal/full-photo-modal.component';
import {MaterialModule} from '@/common/modules/material/material.module';

@NgModule({
  declarations: [
    FullPhotoModalComponent
  ],
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports: [ FullPhotoModalComponent ],
  entryComponents: [FullPhotoModalComponent],
})
export class FullPhotoModalModule { }
