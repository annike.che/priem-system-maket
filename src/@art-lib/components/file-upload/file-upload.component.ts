import {Component, ElementRef, HostListener, Input, OnInit, ViewChild} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: FileUploadComponent,
      multi: true
  }]
})
export class FileUploadComponent implements ControlValueAccessor {

  @Input() progress;

  @Input() multiple = false;

  @Input() fileAccept = '*';

  @Input() disabled = false;

  onChange: any;

  file: File | null = null;

  @ViewChild('inputFile', {static: false}) inputFile: ElementRef;

  @HostListener('change', ['$event.target.files']) emitFiles( event: FileList ) {
    //console.log(event);
    const file = event && event.item(0) || null;
    this.onChange(file);
    this.file = file;
    console.log(file);
  }


  constructor(private host: ElementRef<HTMLInputElement>) { }


  writeValue( value: null ) {
    // clear file input
    this.host.nativeElement.value = '';
    this.file = null;
  }

  registerOnChange( fn: void ) {
    this.onChange = fn;
  }

  registerOnTouched( fn: void ) {
  }



  uploadFile(files) {
    console.log(files);
    if ( files ) {
      this.emitFiles(files);
    }
  }

  delete($event) {

    $event.preventDefault();

    this.inputFile.nativeElement.value = '';
    //console.log('this.myInputVariable', this.inputFile );
    this.writeValue(null);
    this.emitFiles(null);
  }

}
