import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FileUploadComponent } from '../file-upload/file-upload.component';
import {MaterialModule} from '@/common/modules/material/material.module';
import {CommonDirectivesModule} from '@art-lib/directives/common-directives.module';



@NgModule({
  declarations: [FileUploadComponent],
    imports: [
        CommonModule,
        MaterialModule,
        CommonDirectivesModule
    ],
  exports: [ FileUploadComponent ]
})
export class FileUploadModule { }
