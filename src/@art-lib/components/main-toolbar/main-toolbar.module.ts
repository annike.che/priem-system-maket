import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainToolbarComponent } from './main-toolbar.component';
import {MaterialModule} from '@/common/modules/material/material.module';
import {SharedModule} from '@/common/modules/shared.module';



@NgModule({
  declarations: [MainToolbarComponent],
    imports: [
        CommonModule,
        MaterialModule,
        SharedModule
    ],
  exports: [MainToolbarComponent]
})
export class MainToolbarModule { }
