import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {UserProfileService} from '@/common/services/user-profile.service';
import {AuthService} from '@/open/pages/auth/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-main-toolbar',
  templateUrl: './main-toolbar.component.html',
  styleUrls: ['./main-toolbar.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MainToolbarComponent implements OnInit {
  organizations: Array<any>;
  user: User;

  @Output() toggleMenu = new EventEmitter();

    private keyName = 'open-main-menu';
    openSidebar = localStorage.getItem(this.keyName) !== 'false';

  constructor(private userProfile: UserProfileService,
              private authService: AuthService,
              private router: Router) {


  }

  ngOnInit() {

    this.user = this.userProfile.getProfile();
  }

  logout() {
    this.authService.logout()
        .subscribe(res => {
              if (res.done) {
                this.router.navigate(['/auth']);
                this.user = this.userProfile.setProfile({});
              }
            },
            err => {
              /*this.isError = true;
              this.message = err || this.message;
              this.isLoad = false;*/
            });
  }

  changeOrg() {
    this.router.navigate(['/auth']);
  }

}
