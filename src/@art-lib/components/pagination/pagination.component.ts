import { Component, Input, EventEmitter, Output, ViewEncapsulation, OnInit } from '@angular/core';

@Component({
  selector: 'app-art-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ArtPaginationComponent implements OnInit {

  // номер текущей страницы
  @Input() page: number;

  // общее количество элементов
  count: number;

  @Input()
  set total(count: number) {
    this.count = count;
    this.lastPage = this.totalPages();
    console.log('this._count', this.count);
  }

  // количество элементов на странице
  @Input() perPage: number;

  // индикатор загрузки
  @Input() loading: boolean;

  // количество отображаемых страниц
  @Input() pagesToShow: number;

  @Output() goPrev = new EventEmitter<boolean>();
  @Output() goNext = new EventEmitter<boolean>();
  @Output() goPage = new EventEmitter<number>();

  firstPage: number;
  lastPage: number;


  constructor() {


  }

  ngOnInit() {
    this.firstPage = 1;

    /*console.log('this.count', this.count);
    console.log('this.perPage', this.perPage);
    this.lastPage = this.totalPages();
    console.log('this.lastPage', this.lastPage);*/

  }

  getMin(): number {
    return ((this.perPage * this.page) - this.perPage) + 1;
  }

  getMax(): number {
    let max = this.perPage * this.page;
    if (max > this.count) {
      max = this.count;
    }
    return max;
  }

  onPage(n: number): void {
    if ( n !== this.page ) {
      this.goPage.emit(n);
    }
  }

  onPrev(): void {
    this.goPrev.emit(true);
  }

  onNext(next: boolean): void {
    this.goNext.emit(next);
  }

  totalPages(): number {
    //console.log('this.count', this.count, this.perPage, this.pagesToShow);
    return Math.ceil(this.count / this.perPage) || 0;
  }

  isLastPage(): boolean {
    // return this.perPage * this.page > this.count;
    /*console.log('isLastPage',  this.count);*/
    //console.log('lastPage', this.lastPage);
    return this.page >= this.lastPage;
  }

  getPages(from?: number): number[] {
    const c = Math.ceil(this.count / this.perPage);
    const p = this.page || 1;
    const pagesToShow = this.pagesToShow || 3;
    const pages: number[] = [];
    pages.push(p);
    const times = pagesToShow - 1;
    for (let i = 0; i < times; i++) {
      if (pages.length < pagesToShow) {
        if (Math.min.apply(null, pages) > 1) {
          pages.push(Math.min.apply(null, pages) - 1);
        }
      }
      if (pages.length < pagesToShow) {
        if (Math.max.apply(null, pages) < c) {
          pages.push(Math.max.apply(null, pages) + 1);
        }
      }
    }
    pages.sort((a, b) => a - b);
    return pages;
  }



}
