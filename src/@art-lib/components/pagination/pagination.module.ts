import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from '@angular/router';
import { MatButtonModule, MatFormFieldModule, MatIconModule, MatInputModule, MatTableModule, MatTabsModule } from '@angular/material';

import { ArtPaginationComponent } from './pagination.component';



@NgModule({
  declarations: [ ArtPaginationComponent ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatTableModule,
    MatTabsModule,
    RouterModule
  ],
  exports: [
      ArtPaginationComponent
  ]
})
export class ArtPaginationModule { }
