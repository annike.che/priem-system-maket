import {Directive, ElementRef, Input, TemplateRef, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[appStatusCode]'
})
export class StatusCodeDirective {

  @Input('appStatusCode') set status(code: string) {
    const className = `background-color-${this.statusColor[code] || 'gray'}`;

    let classList = this.el.nativeElement.classList as DOMTokenList;

    if ( classList.value.includes('background-color-') ) {
      classList.forEach( (el, ind) => {
          if (el.includes('background-color-')) {
            classList.remove(el);
          }
      });
    }


    classList.add(className);
  }


  private statusColor = {
    new                     : 'blue',
    new_cheking             : 'blue',
    send_oovo               : 'blue',
    get_oovo                : 'success',
    app_get_oovo            : 'purple',
    app_data_pre_check_fail : 'warn',
    app_data_check_fail     : 'warn',
    docs_check_fail         : 'warn',
    app_check_fail          : 'warn',
    app_reject              : 'gray-dark',
    additional_info_wait    : 'gray',
    app_data_sucsfl         : 'success',
    entrant_edit            : 'gray',
    app_edit                : 'gray',
    in_competition          : 'yellow',
    16                      : 'gray',
    service_denied          : 'gray-dark',
    entrant_agreed          : 'success',
    entrant_agreed_verify   : 'success',
    entrant_agreed_call_off : 'gray-dark',
    app_call_off            : 'gray-dark',
    22                      : 'gray-dark',
    23                      : 'gray-dark',
    24                      : 'gray-dark',
    25                      : 'gray-dark',
    26                      : 'success',


    not_started             : 'warn',
    continue                : 'cyan',
    finish                  : 'gray'

  };

  constructor(private el: ElementRef) {}

}
