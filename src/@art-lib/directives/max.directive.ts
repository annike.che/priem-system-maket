import {Directive, Input} from '@angular/core';
import {AbstractControl, NG_VALIDATORS, Validator, Validators} from '@angular/forms';

@Directive({
  selector: '[appMax]',
  providers: [{ provide: NG_VALIDATORS, useExisting: MaxDirective, multi: true }]
})
export class MaxDirective implements Validator {

  @Input() appMax: number;

  validate(control: AbstractControl): { [key: string]: any } {
    return Validators.max(this.appMax)(control);
  }

}
