import {Directive, ElementRef, EventEmitter, HostListener, Output, Self} from '@angular/core';
import {NgControl} from '@angular/forms';

@Directive({
  selector: '[appNullValue]'
})
export class NullValueDirective {


  @Output() ngModelChange: EventEmitter<any> = new EventEmitter();

  constructor(private el: ElementRef,
              @Self() private ngControl: NgControl)
  { }

  @HostListener('input', ['$event.target'])
  onEvent(target: HTMLInputElement) {
    //console.log(target.value);
    if(target.value === '') {
      this.ngControl.viewToModelUpdate( null);
      this.ngControl.control.setValue(null);
    }

    //console.log(this.ngControl);
  }

}
