import {Directive, Input} from '@angular/core';
import {AbstractControl, NG_VALIDATORS, Validator, Validators} from '@angular/forms';

@Directive({
  selector: '[appMin]',
  providers: [{ provide: NG_VALIDATORS, useExisting: MinDirective, multi: true }]
})
export class MinDirective implements Validator {

  @Input() appMin: number;

  validate(control: AbstractControl): { [key: string]: any } {
    return Validators.min(this.appMin)(control);
  }

}
