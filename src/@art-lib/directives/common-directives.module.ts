import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DisableControlDirective } from './disable-control.directive';
import {IntegerDirective} from './input-int.directive';
import { DragDropDirective } from './drag-drop.directive';
import { MaxDirective } from './max.directive';
import { MinDirective } from './min.directive';
import { NullValueDirective } from './null-value.directive';
import { StatusCodeDirective } from './status-code.directive';
import { StickyHeaderTableDirective } from './sticky-header-table.directive';
import { NestableFormDirective } from './nestable-form.directive';



@NgModule({
  declarations: [
    IntegerDirective,
    DisableControlDirective,
    DragDropDirective,
    MaxDirective,
    MinDirective,
    NullValueDirective,
    StatusCodeDirective,
    StickyHeaderTableDirective,
    NestableFormDirective,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    IntegerDirective,
    DisableControlDirective,
    DragDropDirective,
    MaxDirective,
    MinDirective,
    NullValueDirective,
    StatusCodeDirective,
    StickyHeaderTableDirective,
    NestableFormDirective
  ]
})
export class CommonDirectivesModule { }
