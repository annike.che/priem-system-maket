import {
  Directive,
  ElementRef, EventEmitter,
  HostListener, Input, OnInit,
  Output,
  Renderer2, Self,
} from '@angular/core';
import {ControlValueAccessor, NgControl} from '@angular/forms';

@Directive({
  selector: '[appInteger]'
})
export class IntegerDirective {

  @Input() negative: boolean = false;

  @Input() positive: boolean = false;


  @Output() ngModelChange: EventEmitter<any> = new EventEmitter();

  expression = /\D/g;

  constructor( private  elementRef: ElementRef,
               private renderer: Renderer2,
               @Self() private ngControl: NgControl
  ) {
  }

  @HostListener('input', ['$event']) onInputChange(event) {
    this.onChangeFunction(event);
  }

  @HostListener('change', ['$event']) onChange(event) {

    const initalValue = this.replacement();

    console.log('initalValue change', initalValue);

    if (initalValue === '' || initalValue === '+' || initalValue === '-') {
      this.ngModelChange.emit(0);
      //this.elementRef.nativeElement.value = 0;
      this.ngControl.control.setValue(0);
    }
  }

  onChangeFunction( event) {

    const initalValue = this.replacement();

    console.log('initalValue onChangeFunction', initalValue);

    const replace = ( initalValue === '' ||  initalValue === '+' || initalValue === '-' ) ? initalValue : +initalValue;
    console.log('replace', replace);

    console.log(this.ngControl);
    this.ngControl.control.setValue(replace);
    this.ngModelChange.emit(replace);
    //this.elementRef.nativeElement.value = +replace;
    return +replace;
  }

  replacement() {
    let value = this.elementRef.nativeElement.value;

    if(!value) return '';

    let sign = '';

    if ( this.positive && this.negative ) {
      sign = (value[0] === '+' || value[0] === '-' ) ? value[0] : '';
    }

    if ( this.positive && !this.negative ) {
      sign = (value[0] === '+') ? value[0] : '';
    }

    if ( !this.positive && this.negative ) {
      sign = (value[0] === '-') ? value[0] : '';
    }
    console.log('sign', sign);


    const initalValue = sign + value.replace(this.expression, '') || '';

    console.log('replacement initalValue', initalValue);

    return initalValue;


  }






}
