import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ConfirmModalModule} from '@art-lib/components/confirm-modal/confirm-modal.module';
import {CommonDirectivesModule} from '@art-lib/directives/common-directives.module';
import {NgVerticalTabsModule} from '@art-lib/components/vertical-tabs/ng-vertical-tabs.module';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {FileUploadModule} from '@art-lib/components/file-upload/file-upload.module';
import {FullPhotoModalModule} from '@art-lib/components/full-photo-modal/full-photo-modal.module';
import {ArtLibPipesModule} from '@art-lib/pipes/art-lib-pipes.module';
import {LoaderComponent} from '@art-lib/components/loader/loader.component';
import {CommentModalModule} from '@art-lib/components/comment-modal/comment-modal.module';



@NgModule({
  declarations: [
    LoaderComponent
  ],
  imports: [
    CommonModule,
    ConfirmModalModule,
    CommentModalModule,
    FullPhotoModalModule,
    CommonDirectivesModule,
    NgVerticalTabsModule.forRoot(),
    MatProgressSpinnerModule,
  ],
  exports: [
    ConfirmModalModule,
    CommentModalModule,
    FullPhotoModalModule,
    CommonDirectivesModule,
    ArtLibPipesModule,
    NgVerticalTabsModule,
    FileUploadModule,
    LoaderComponent
  ],
})
export class ArtLibModule {
}
